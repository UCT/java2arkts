export interface JavaToArkTS {
  id: number

  timestamp: string

  java_code: string

  ast: string

  arkts_code: string

  status: number

  options: string

}
