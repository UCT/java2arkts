import { defineStore } from 'pinia'
import http from '@/support/http'
import { rememberAuthToken, removeAuthToken } from '@/support/helper'
import Message from '@/support/message'
import router from '@/router'
import { JavaToArkTS } from '@/types/JavaToArkTS'

export const useJavaToArkTSStore = defineStore('JavaToArkTSStore', {
  state: (): JavaToArkTS => {
    return {
      id: 0,

      timestamp: '',

      java_code: '',

      ast: '',

      arkts_code: '',

      status: 0,

      options:  '',
    }
  },

  getters: {
    getId(): number {
      return this.id
    },
    getTimestamp(): string {
      return this.timestamp
    },

    getJavaCode(): string {
      return this.java_code
    },

    getAst(): string {
      return this.ast
    },

    getArktsCode(): string {
      return this.arkts_code
    },

    getStatus(): number {
      return this.status
    },

    getOptions(): string {
      return this.options
    },
  },

  actions: {
    setId(id: number) {
      this.id = id
    },
    setTimestamp(timestamp: string) {
      this.timestamp = timestamp
    },

    setJavaCode(java_code: string) {
      this.java_code = java_code
    },

    setAst(ast: string) {
      this.ast = ast
    },

    setArktsCode(arkts_code: string) {
      this.arkts_code = arkts_code
    },

    setStatus(status: number) {
      this.status = status
    },

    setOptions(options: number) {
      this.options = options
    },

    /**
     * java2ts
     *
     * @param params
     * @returns
     */
    java2ts(params: Object) {
      return new Promise<void>((resolve, reject) => {
        http
          .post('/java2ts', params)
          .then(response => {
            console.log(response.data)
            const { arkts_code } = response.data
            this.setArktsCode(arkts_code)
            resolve()
          })
          .catch(e => {
            reject(e)
          })
      })
    },

  },
})
