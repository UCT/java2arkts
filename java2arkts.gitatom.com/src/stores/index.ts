import { createPinia } from 'pinia'
import type { App } from 'vue'
import { createORM } from 'pinia-orm'

const store = createPinia().use(createORM())

export function bootstrapStore(app: App) : void {
  app.use(store)
}

export default store
