import { Model, Repository } from 'pinia-orm'
import { java2ts } from '@/orm/request/api'

export default class JavaToArkts extends Model {
  static entity = 'java_to_arkts'

  static fields() {
    return {
      id: this.attr(null),
      timestamp: this.string(''),
      java_code: this.string(''),
      ast: this.string(''),
      arkts_code: this.string(''),
      status: this.number(0),
      options: this.string('')
    }
  }
}

export class JavaToArktsRepository extends Repository {
  use = JavaToArkts

  /** java 转换arkts api */
  async java2tsApi(data: any) {
    const result = await java2ts(data)
    console.log(result)
    return result
  }
}
