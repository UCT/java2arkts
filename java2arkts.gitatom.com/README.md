# gitatom Web
java2arkts.gitatom.com 是采用[UCToo](https://gitee.com/uctoo/uctoo) V3 Web端技术选型实现的一个代码转换器用户端界面。 

### 版本 v0.0.1 Beta

## 概述
Web端技术选型 [VUE3](https://vuejs.org/) + [element-plus](https://element-plus.org/) + [pinia-orm](https://github.com/CodeDredd/pinia-orm) + vite  
接收用户输入的Java代码，提交至后台接口，接收后台处理的结果反馈给用户。  

## 预览

![java2arkts.png](./src/assets/java2arkts.png)

## 体验地址
[java2arkts.uctoo.com](http://java2arkts.uctoo.com)  暂时没有配置ssl证书，请通过http访问。可能会随时配置上ssl证书，如http无法访问就通过https访问。  
或  
[java2arkts.gitatom.com](http://java2arkts.gitatom.com) 

## 安装  
nodejs版本建议18.x，其他版本未作兼容性测试。  
1. .env文件中配置后端API地址。  
2. npm install   安装依赖  
3. npm run dev   运行开发环境版本  
4. npm run build:release    打包输出部署版本  

## 二次开发  
1.  /src/views/develop/codelabs/index.vue 文件对应用户界面  
2.  /src/orm/models/JavaToArkts.ts  文件示例了符合UMI-ORM编码规范的程序示例，即优先使用前端缓存或前端数据库的数据作为响应式用户界面的数据源。在ORM中集成接口通信能力，简化代码。在分布式系统中一致性同步规范化的ORM数据结构。  

## 条件限制    
目前仅适配chrome内核浏览器  