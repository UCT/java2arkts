## 项目名称： UCToo gitatom 云开发工具
> 请介绍一下你的项目吧  

gitatom是一款鸿蒙应用云开发工具，致力于帮助开发者快速实现鸿蒙原生应用的敏捷开发和快速迭代。gitatom集成了主流公有云的云原生技术体系,支持完备的云开发流水线，提供AI驱动的鸿蒙原生应用全栈敏捷开发能力。  

## 对赛题的理解和技术选型考虑  

对赛题和评审规则有以下考虑：  
1.  如果需要实现“动态特性”，即自动化的识别出Java程序运行阶段的特性自动化转换为ArkTS适合的并发实现方式，则仅通过语法分析词法分析是不够的。超出本团队目前资源能力。  
2.  如果通过AI驱动的方案，则会引入概率。我们认为AI目前仅适合作为编程助手，辅助开发者编程，还达不到100%确定性要求的代码转换场景。也超出本团队目前资源能力。  
3.  通过查阅目前公开的ArkCompiler、ArkTS技术资料，感觉第三方开发者目前可获取的资料可能不足够支持做出较完备的代码转换方案。缺少类似语言白皮书之类的资料，严格定义ArkTS语法词法等内容。      
4.  通过查阅第三方开源项目，发现[java2typescript](https://github.com/mike-lischke/java2typescript) 第三方开源项目与赛题要求较为接近。此java2typescript开源项目使用了[Antlr](https://github.com/antlr)，Antlr是很多编程语言类工具常用的技术选型，选用此技术选型，可能对ArkTS在开源领域的传播推广有一定好处。因此本项目通过集成java2typescript进行实现。    
5.  java2typescript第三方开源项目不支持Java并发代码转换为typescript，但赛题要求所转换的Java并发代码仅需考虑三个关键字的简单并发代码，因此本项目仅对synchronized关键字转为ArkTS对应等价语义的taskpool并发代码进行了特殊实现。    

## 说明文档  
> 列出运行该项目所必须的条件和相关依赖  

api.gitatom.com 是后端服务项目，具体文档请参考目录中[readme](https://atomgit.com/openharmony_jsweet/09-yuzhouzhixin/blob/master/api.gitatom.com/README.md)  
java2arkts.gitatom.com 是前端界面项目，具体文档请参考目录中[readme](https://atomgit.com/openharmony_jsweet/09-yuzhouzhixin/blob/master/java2arkts.gitatom.com/README.md)  

## 演示项目  
> 说明如何运行和使用你的项目，建议给出具体的步骤说明

[java2arkts.uctoo.com](http://java2arkts.uctoo.com)  暂时没有配置ssl证书，请通过http访问。可能会随时配置上ssl证书，如http无法访问就通过https访问。  
或  
[java2arkts.gitatom.com](http://java2arkts.gitatom.com)   域名备案中  

使用流程：  
1.  在左侧文本框中输入Java代码，点击提交，右侧文本框中即可输出转换后的ArkTS代码。  
2.  点击左侧示例代码按钮，可在左侧文本框中填写一段仅包含synchronized关键字的Java并发示例代码。  

根目录中demo.mp4是一段演示视频。  

## 版权信息  
本项目采用MIT开源协议发布。
本项目包含的第三方源码和二进制文件之版权信息另行标注。
版权所有Copyright © 2014-2024 UCToo (https://www.uctoo.com)
All rights reserved