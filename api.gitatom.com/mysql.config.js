import knex from 'knex';

const MYSQL_ENV = {
    MYSQL_HOST: '127.0.0.1', //本地开发环境
    MYSQL_USER: 'uctoo', //本地开发环境
    MYSQL_PASSWORD: 'password', //本地开发环境
    MYSQL_DBNAME: 'todo4', //本地开发环境
};
/*

const database = knex({
    client: 'mysql2',
    connection: {
        host: MYSQL_ENV.MYSQL_HOST,
        port: 3306,
        user: MYSQL_ENV.MYSQL_USER,
        password: MYSQL_ENV.MYSQL_PASSWORD,
        database: MYSQL_ENV.MYSQL_DBNAME
    },
});
*/

const database = knex({
    client: 'mysql2',
    connection: {
        host: process.env.MYSQL_HOST,
        port: 3306,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DBNAME
    },
});

export { database as db, MYSQL_ENV };