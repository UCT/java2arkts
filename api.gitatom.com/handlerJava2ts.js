import { db } from './mysql.config.js';
import { BadRequestResponse, CreatedResponse, NotFoundResponse, OkResponse } from './respons.js';
import fs from "fs";
import {
     JavaToTypescriptConverter,
} from "./java2typescript/output/src/conversion/JavaToTypeScript.js";

const java2ts = async (request, response) => {
    const { java_code = '', ast = '' } = await request.json();
    if (!java_code) {
        return BadRequestResponse(response, 'java_code cannot be null');
    }
    let timestamp = Date.now();
    const [id] = await db('javatoarkts').insert({ timestamp, java_code, ast });  //输入的参数写入数据库
    let savePath = "./sourcecode/" + timestamp.toString();
    let saveFileName = savePath + "/" + timestamp.toString() + ".java"; //输入的代码写入文件
    fs.mkdirSync(savePath, { recursive: true });
    fs.writeFileSync(saveFileName , java_code);
    const antlrToolOptions = {
        packageRoot: savePath,
        outputPath: savePath + "/output",
        debug: {
            pathForPosition: {
                filePattern: timestamp.toString() +".java",
                position: {
                    row: 8,
                    column: 11
                }
            }
        }
    };

    const converter = new JavaToTypescriptConverter(antlrToolOptions);
    await converter.startConversion();

    let readFileName = savePath + "/output/" + timestamp.toString() + ".ts";
    let readFileContent = fs.readFileSync(readFileName,"utf-8");

    const result = {
        id,
        java_code,
        ast,
        arkts_code: readFileContent,
        status: 1,
        options: ''
    };

    await db('javatoarkts').where({ id }).update({ ...result });   //结果更新数据库

    return CreatedResponse(response, result);
};

export { java2ts };