import { CharStream, CommonTokenStream, Interval, ParseTree } from "antlr4ng";
import { JavaLexer } from "../parser/generated/JavaLexer.js";
import { JavaParser, CompilationUnitContext } from "../parser/generated/JavaParser.js";
import { PackageSource } from "./PackageSource.js";
import { ISymbolInfo } from "./conversion/types.js";
/**
 * This interface keeps all concerned parsing parts together, to ensure they stay alive during the entire
 * processing time. Symbol tables and parse trees depend on that.
 */
interface IFileParseInfo {
    content: string;
    inputStream: CharStream;
    lexer: JavaLexer;
    tokenStream: CommonTokenStream;
    parser: JavaParser;
    tree: CompilationUnitContext;
}
/** A source class for a single Java source file. */
export declare class JavaFileSource extends PackageSource {
    private packageRoot;
    private replacements?;
    /** Only set if a file was parsed. */
    fileParseInfo?: IFileParseInfo;
    constructor(packageId: string, sourceFile: string, targetFile: string, packageRoot: string, replacements?: Map<RegExp, string> | undefined);
    get parseTree(): CompilationUnitContext | undefined;
    getQualifiedSymbol: (context: ParseTree, name: string) => ISymbolInfo | undefined;
    printParseTreeForPosition: (position: {
        column: number;
        row: number;
    }) => void;
    protected textFromInterval: (interval: Interval) => string;
    private parse;
}
export {};
