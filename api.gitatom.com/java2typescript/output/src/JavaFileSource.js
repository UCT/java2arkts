/*
 * Copyright (c) Mike Lischke. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import fs from "fs";
import { CharStreams, CommonTokenStream } from "antlr4ng";
import { JavaLexer } from "../parser/generated/JavaLexer.js";
import { JavaParser } from "../parser/generated/JavaParser.js";
import { PackageSource } from "./PackageSource.js";
import { JavaFileSymbolTable } from "./JavaFileSymbolTable.js";
import { JavaErrorListener } from "./parsing/JavaErrorListener.js";
import { printParseTreeStack } from "./utilities.js";
/** A source class for a single Java source file. */
export class JavaFileSource extends PackageSource {
    packageRoot;
    replacements;
    /** Only set if a file was parsed. */
    fileParseInfo;
    constructor(packageId, sourceFile, targetFile, packageRoot, replacements) {
        super(packageId, sourceFile, targetFile);
        this.packageRoot = packageRoot;
        this.replacements = replacements;
    }
    get parseTree() {
        if (!this.fileParseInfo) {
            this.parse();
        }
        return this.fileParseInfo?.tree;
    }
    getQualifiedSymbol = (context, name) => {
        return this.symbolTable.getQualifiedSymbol(context, name);
    };
    printParseTreeForPosition = (position) => {
        if (this.fileParseInfo) {
            printParseTreeStack(this.sourceFile, this.fileParseInfo.tree, this.fileParseInfo.parser.ruleNames, position);
        }
    };
    textFromInterval = (interval) => {
        return this.fileParseInfo?.inputStream.getText(interval) ?? "";
    };
    parse = () => {
        if (!fs.existsSync(this.sourceFile)) {
            console.warn(`\nFile ${this.sourceFile} does not exist.`);
            return;
        }
        let content = fs.readFileSync(this.sourceFile, "utf-8");
        this.replacements?.forEach((value, pattern) => {
            content = content.replace(pattern, value);
        });
        const inputStream = CharStreams.fromString(content);
        const lexer = new JavaLexer(inputStream);
        const tokenStream = new CommonTokenStream(lexer);
        const parser = new JavaParser(tokenStream);
        const listener = new JavaErrorListener();
        parser.addErrorListener(listener);
        const tree = parser.compilationUnit();
        if (listener.errors.length === 0) {
            this.fileParseInfo = {
                content,
                inputStream,
                lexer,
                tokenStream,
                parser,
                tree,
            };
            this.symbolTable = new JavaFileSymbolTable(this, this.packageRoot, this.importList);
            this.importList.delete(this);
        }
        else {
            throw new Error("Parsing failed for " + this.sourceFile);
        }
    };
}
//# sourceMappingURL=JavaFileSource.js.map