import { SymbolTable } from "antlr4-c3";
import { PackageSource } from "./PackageSource.js";
/** A package source specifically for Java imports. It handles symbol resolution for known Java SDK packages. */
export declare class JavaPackageSource extends PackageSource {
    /**
     * Creates the complete symbol the java.base module, which includes all java.* packages and some javax.* packages.
     *
     * @returns The created symbol table.
     */
    protected createSymbolTable(): SymbolTable;
}
