/*
 * Copyright (c) Mike Lischke. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/* eslint-disable max-classes-per-file */
import { BaseSymbol, BlockSymbol, FieldSymbol, MethodSymbol, ParameterSymbol, ScopedSymbol, InterfaceSymbol, Modifier, TypeKind, ReferenceKind, VariableSymbol, } from "antlr4-c3";
import { java } from "jree";
import { JavaParser, } from "../../parser/generated/JavaParser.js";
import { JavaParserListener } from "../../parser/generated/JavaParserListener.js";
import { PackageSourceManager } from "../PackageSourceManager.js";
import { JavaClassSymbol } from "./JavaClassSymbol.js";
export class FileSymbol extends ScopedSymbol {
}
export class AnnotationSymbol extends ScopedSymbol {
}
export class EnumSymbol extends JavaClassSymbol {
}
export class EnumConstantSymbol extends BaseSymbol {
}
export class SwitchBlockGroup extends ScopedSymbol {
}
export class ConstructorSymbol extends MethodSymbol {
}
export class ClassBodySymbol extends ScopedSymbol {
}
/** A symbol for class creators in initialisers. */
export class ClassCreatorSymbol extends ScopedSymbol {
}
export class JavaInterfaceSymbol extends InterfaceSymbol {
    isTypescriptCompatible = false;
    typeParameters;
}
export class InterfaceBodySymbol extends ScopedSymbol {
}
export class InitializerBlockSymbol extends ScopedSymbol {
    isStatic = false;
}
export class TypeSymbol extends BaseSymbol {
}
export class PackageSymbol extends BaseSymbol {
}
export class ImportSymbol extends BaseSymbol {
}
export class JavaParseTreeWalker extends JavaParserListener {
    symbolTable;
    packageRoot;
    importList;
    static typeKindMap = new Map([
        ["Array", TypeKind.Array],
        ["int", TypeKind.Integer],
        ["Integer", TypeKind.Integer],
        ["float", TypeKind.Float],
        ["String", TypeKind.String],
        ["boolean", TypeKind.Boolean],
        ["Boolean", TypeKind.Boolean],
        ["Map", TypeKind.Map],
    ]);
    symbolStack = new java.util.Stack();
    enumSymbol;
    constructor(symbolTable, packageRoot, importList) {
        super();
        this.symbolTable = symbolTable;
        this.packageRoot = packageRoot;
        this.importList = importList;
        this.symbolStack.push(symbolTable);
        // Get the Enum class symbol from the import list, in case we need it to add it to enum symbols.
        // Java implicitly derives enums from the `Enum` class, which we have to emulate here.
        for (const entry of importList) {
            const symbol = entry.symbolTable.symbolFromPath("java.lang.Enum");
            if (symbol) {
                this.enumSymbol = symbol;
                break;
            }
        }
    }
    exitPackageDeclaration = (ctx) => {
        const packageId = ctx.qualifiedName().getText();
        this.symbolTable.addNewSymbolOfType(PackageSymbol, this.symbolStack.peek(), ctx.qualifiedName().getText());
        const sources = PackageSourceManager.fromPackageIdWildcard(packageId);
        sources.forEach((source) => {
            this.importList.add(source);
        });
    };
    exitImportDeclaration = (ctx) => {
        const packageId = ctx.qualifiedName().getText();
        this.symbolTable.addNewSymbolOfType(ImportSymbol, this.symbolStack.peek(), packageId);
        if (ctx.MUL()) {
            const sources = PackageSourceManager.fromPackageIdWildcard(packageId);
            sources.forEach((source) => {
                this.importList.add(source);
            });
        }
        else {
            const source = PackageSourceManager.fromPackageId(packageId);
            this.importList.add(source);
        }
    };
    enterBlock = (ctx) => {
        this.pushNewScope(BlockSymbol, "#block#", ctx);
    };
    exitBlock = () => {
        this.symbolStack.pop();
    };
    enterClassDeclaration = (ctx) => {
        const symbol = this.pushNewScope(JavaClassSymbol, ctx.identifier().getText(), ctx);
        if (ctx.typeParameters()) {
            symbol.typeParameters = ctx.typeParameters().getText();
        }
        this.checkStatic(symbol);
    };
    exitClassDeclaration = () => {
        this.symbolStack.pop();
    };
    enterClassBodyDeclaration = (ctx) => {
        if (ctx.block()) {
            const symbol = this.pushNewScope(InitializerBlockSymbol, "#initializer#", ctx);
            symbol.isStatic = ctx.STATIC() !== undefined;
        }
    };
    exitClassBodyDeclaration = () => {
        if (this.symbolStack.peek().name === "#initializer#") {
            this.symbolStack.pop();
        }
    };
    enterClassCreatorRest = (ctx) => {
        if (ctx.classBody()) { // Anonymous class.
            this.pushNewScope(ClassCreatorSymbol, "#anonymous-class#", ctx);
        }
    };
    exitClassCreatorRest = () => {
        if (this.symbolStack.peek().name === "#anonymous-class#") {
            this.symbolStack.pop();
        }
    };
    enterAnnotationTypeBody = (ctx) => {
        this.pushNewScope(AnnotationSymbol, "#annotationTypeBody#", ctx);
    };
    exitAnnotationTypeBody = () => {
        this.symbolStack.pop();
    };
    enterMethodDeclaration = (ctx) => {
        const symbol = this.pushNewScope(MethodSymbol, ctx.identifier().getText(), ctx);
        this.checkStatic(symbol);
    };
    exitMethodDeclaration = () => {
        this.symbolStack.pop();
    };
    enterInterfaceMethodDeclaration = (ctx) => {
        const symbol = this.pushNewScope(MethodSymbol, ctx.interfaceCommonBodyDeclaration().identifier().getText(), ctx);
        symbol.modifiers.add(Modifier.Static);
    };
    exitInterfaceMethodDeclaration = () => {
        this.symbolStack.pop();
    };
    enterConstructorDeclaration = (ctx) => {
        const symbol = this.pushNewScope(ConstructorSymbol, ctx.identifier().getText(), ctx);
        this.checkStatic(symbol);
    };
    exitConstructorDeclaration = () => {
        this.symbolStack.pop();
    };
    enterInterfaceDeclaration = (ctx) => {
        const symbol = this.pushNewScope(JavaInterfaceSymbol, ctx.identifier().getText(), ctx);
        if (ctx.typeParameters()) {
            symbol.typeParameters = ctx.typeParameters().getText();
        }
        // Check the interface if it is compatible with Typescript.
        symbol.isTypescriptCompatible = true;
        ctx.interfaceBody().interfaceBodyDeclaration().forEach((context) => {
            // Entries without any content can be ignored.
            if (!context.SEMI() && symbol.isTypescriptCompatible) {
                const memberContext = context.interfaceMemberDeclaration();
                // The Java interface is compatible with TS if there are only:
                // - const declarations (will be moved to a side namespace)
                // - static (generic) method declarations (will be moved to a side namespace)
                // - non-static (generic) method declarations without a body.
                if (!memberContext?.constDeclaration()) {
                    let commonBodyDeclaration;
                    if (memberContext?.interfaceMethodDeclaration()) {
                        commonBodyDeclaration = memberContext.interfaceMethodDeclaration()
                            .interfaceCommonBodyDeclaration();
                    }
                    else if (memberContext?.genericInterfaceMethodDeclaration()) {
                        commonBodyDeclaration = memberContext.genericInterfaceMethodDeclaration()
                            .interfaceCommonBodyDeclaration();
                    }
                    if (!commonBodyDeclaration || commonBodyDeclaration.methodBody().block() != null) {
                        symbol.isTypescriptCompatible = false;
                    }
                }
            }
        });
    };
    exitInterfaceDeclaration = () => {
        this.symbolStack.pop();
    };
    enterAnnotationTypeDeclaration = (ctx) => {
        this.pushNewScope(AnnotationSymbol, ctx.identifier().getText(), ctx);
    };
    exitAnnotationTypeDeclaration = () => {
        this.symbolStack.pop();
    };
    enterEnumDeclaration = (ctx) => {
        const symbol = this.pushNewScope(EnumSymbol, ctx.identifier().getText(), ctx);
        if (this.enumSymbol) {
            symbol.extends.push(this.enumSymbol);
            symbol.modifiers.add(Modifier.Static);
        }
    };
    exitEnumConstant = (ctx) => {
        const block = this.symbolStack.peek();
        this.symbolTable.addNewSymbolOfType(EnumConstantSymbol, block, ctx.identifier().getText());
    };
    exitEnumDeclaration = () => {
        this.symbolStack.pop();
    };
    enterExpression = (ctx) => {
        // Pushing an own symbol for an expression gives us an anchor point for symbol search also with
        // references to higher scopes.
        this.pushNewScope(BlockSymbol, "#expression#", ctx);
    };
    exitExpression = () => {
        this.symbolStack.pop();
    };
    enterEnhancedForControl = (ctx) => {
        const block = this.symbolStack.peek();
        if (ctx.typeType()) {
            // Explicit type.
            const type = this.generateTypeRecord(ctx.typeType().getText());
            const id = ctx.variableDeclaratorId().identifier().getText();
            const symbol = this.symbolTable.addNewSymbolOfType(VariableSymbol, block, id, undefined, type);
            symbol.context = ctx;
        }
        else {
            // Auto type.
            const id = ctx.variableDeclaratorId().identifier().getText();
            const symbol = this.symbolTable.addNewSymbolOfType(VariableSymbol, block, id, undefined);
            symbol.context = ctx;
        }
    };
    enterLocalVariableDeclaration = (ctx) => {
        const block = this.symbolStack.peek();
        const type = this.generateTypeRecord(ctx.typeType().getText());
        ctx.variableDeclarators().variableDeclarator().forEach((declarator) => {
            const id = declarator.variableDeclaratorId().identifier().getText();
            const symbol = this.symbolTable.addNewSymbolOfType(VariableSymbol, block, id, undefined, type);
            symbol.context = declarator;
            this.checkStatic(symbol);
        });
    };
    enterFieldDeclaration = (ctx) => {
        const block = this.symbolStack.peek();
        const type = this.generateTypeRecord(ctx.typeType().getText());
        ctx.variableDeclarators().variableDeclarator().forEach((declarator) => {
            const id = declarator.variableDeclaratorId().identifier().getText();
            const symbol = this.symbolTable.addNewSymbolOfType(FieldSymbol, block, id, undefined, type);
            symbol.context = declarator;
            this.checkStatic(symbol);
        });
    };
    enterConstantDeclarator = (ctx) => {
        const block = this.symbolStack.peek();
        const symbol = this.symbolTable.addNewSymbolOfType(FieldSymbol, block, ctx.identifier().getText(), undefined);
        symbol.context = ctx;
    };
    enterFormalParameter = (ctx) => {
        const block = this.symbolStack.peek();
        const type = this.generateTypeRecord(ctx.typeType().getText());
        const id = ctx.variableDeclaratorId().identifier().getText();
        const symbol = this.symbolTable.addNewSymbolOfType(ParameterSymbol, block, id, undefined, type);
        symbol.context = ctx;
        this.checkStatic(symbol);
    };
    enterCatchClause = (ctx) => {
        const block = this.pushNewScope(BlockSymbol, "#catch#", ctx);
        // Can be a union type, but we ignore that here.
        const type = this.generateTypeRecord(ctx.catchType().getText());
        const id = ctx.identifier().getText();
        const symbol = this.symbolTable.addNewSymbolOfType(ParameterSymbol, block, id, undefined, type);
        symbol.context = ctx;
    };
    exitCatchClause = () => {
        this.symbolStack.pop();
    };
    enterSwitchBlockStatementGroup = (ctx) => {
        this.pushNewScope(SwitchBlockGroup, "#switchBlockGroup", ctx);
    };
    exitSwitchBlockStatementGroup = () => {
        this.symbolStack.pop();
    };
    visitTerminal = (_node) => { };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    pushNewScope = (t, name, ctx) => {
        const parent = this.symbolStack.size() === 0 ? undefined : this.symbolStack.peek();
        const symbol = this.symbolTable.addNewSymbolOfType(t, parent, name, [], []);
        symbol.context = ctx;
        this.symbolStack.push(symbol);
        return symbol;
    };
    /**
     * Checks if the symbol is static and marks it as such, if that's the case.
     *
     * @param symbol The symbol to check.
     */
    checkStatic = (symbol) => {
        let found = false;
        let run = symbol.context;
        while (run && !found) {
            switch (run.ruleIndex) {
                case JavaParser.RULE_typeDeclaration: {
                    run.classOrInterfaceModifier().forEach((modifier) => {
                        if (modifier.STATIC()) {
                            symbol.modifiers.add(Modifier.Static);
                        }
                    });
                    found = true;
                    break;
                }
                case JavaParser.RULE_classBodyDeclaration: {
                    run.modifier().forEach((modifier) => {
                        if (modifier.classOrInterfaceModifier() && modifier.classOrInterfaceModifier().STATIC()) {
                            symbol.modifiers.add(Modifier.Static);
                        }
                    });
                    found = true;
                    break;
                }
                case JavaParser.RULE_interfaceBodyDeclaration: {
                    run.modifier().forEach((modifier) => {
                        if (modifier.classOrInterfaceModifier() && modifier.classOrInterfaceModifier().STATIC()) {
                            symbol.modifiers.add(Modifier.Static);
                        }
                    });
                    found = true;
                    break;
                }
                case JavaParser.RULE_interfaceMethodDeclaration: {
                    run.interfaceMethodModifier().forEach((modifier) => {
                        if (modifier.STATIC()) {
                            symbol.modifiers.add(Modifier.Static);
                        }
                    });
                    found = true;
                    break;
                }
                default: {
                    run = run.parent;
                    break;
                }
            }
        }
    };
    generateTypeRecord = (typeText) => {
        const typeParamsCheck = typeText.match(/^[A-Z_.]+[ \t]*(<.+>)/i);
        let baseText = typeText;
        if (typeParamsCheck) {
            baseText = typeText.substring(0, typeText.length - typeParamsCheck[1].length);
        }
        const kind = JavaParseTreeWalker.typeKindMap.get(baseText) ?? TypeKind.Unknown;
        return {
            name: typeText,
            baseTypes: [],
            kind,
            reference: ReferenceKind.Irrelevant,
        };
    };
}
//# sourceMappingURL=JavaParseTreeWalker.js.map