import { BaseSymbol, MethodSymbol, ScopedSymbol, SymbolTable, InterfaceSymbol } from "antlr4-c3";
import { TerminalNode } from "antlr4ng";
import { AnnotationTypeBodyContext, MethodDeclarationContext, BlockContext, EnumDeclarationContext, InterfaceDeclarationContext, AnnotationTypeDeclarationContext, ClassDeclarationContext, ExpressionContext, FormalParameterContext, ConstantDeclaratorContext, PackageDeclarationContext, ImportDeclarationContext, ConstructorDeclarationContext, ClassBodyDeclarationContext, InterfaceMethodDeclarationContext, LocalVariableDeclarationContext, FieldDeclarationContext, EnhancedForControlContext, CatchClauseContext, EnumConstantContext, ClassCreatorRestContext, SwitchBlockStatementGroupContext } from "../../parser/generated/JavaParser.js";
import { JavaParserListener } from "../../parser/generated/JavaParserListener.js";
import { PackageSource } from "../PackageSource.js";
import { JavaClassSymbol } from "./JavaClassSymbol.js";
export declare class FileSymbol extends ScopedSymbol {
}
export declare class AnnotationSymbol extends ScopedSymbol {
}
export declare class EnumSymbol extends JavaClassSymbol {
}
export declare class EnumConstantSymbol extends BaseSymbol {
}
export declare class SwitchBlockGroup extends ScopedSymbol {
}
export declare class ConstructorSymbol extends MethodSymbol {
}
export declare class ClassBodySymbol extends ScopedSymbol {
}
/** A symbol for class creators in initialisers. */
export declare class ClassCreatorSymbol extends ScopedSymbol {
}
export declare class JavaInterfaceSymbol extends InterfaceSymbol {
    isTypescriptCompatible: boolean;
    typeParameters?: string;
}
export declare class InterfaceBodySymbol extends ScopedSymbol {
}
export declare class InitializerBlockSymbol extends ScopedSymbol {
    isStatic: boolean;
}
export declare class TypeSymbol extends BaseSymbol {
}
export declare class PackageSymbol extends BaseSymbol {
}
export declare class ImportSymbol extends BaseSymbol {
}
export declare class JavaParseTreeWalker extends JavaParserListener {
    private symbolTable;
    private packageRoot;
    private importList;
    private static typeKindMap;
    private symbolStack;
    private enumSymbol?;
    constructor(symbolTable: SymbolTable, packageRoot: string, importList: Set<PackageSource>);
    exitPackageDeclaration: (ctx: PackageDeclarationContext) => void;
    exitImportDeclaration: (ctx: ImportDeclarationContext) => void;
    enterBlock: (ctx: BlockContext) => void;
    exitBlock: () => void;
    enterClassDeclaration: (ctx: ClassDeclarationContext) => void;
    exitClassDeclaration: () => void;
    enterClassBodyDeclaration: (ctx: ClassBodyDeclarationContext) => void;
    exitClassBodyDeclaration: () => void;
    enterClassCreatorRest: (ctx: ClassCreatorRestContext) => void;
    exitClassCreatorRest: () => void;
    enterAnnotationTypeBody: (ctx: AnnotationTypeBodyContext) => void;
    exitAnnotationTypeBody: () => void;
    enterMethodDeclaration: (ctx: MethodDeclarationContext) => void;
    exitMethodDeclaration: () => void;
    enterInterfaceMethodDeclaration: (ctx: InterfaceMethodDeclarationContext) => void;
    exitInterfaceMethodDeclaration: () => void;
    enterConstructorDeclaration: (ctx: ConstructorDeclarationContext) => void;
    exitConstructorDeclaration: () => void;
    enterInterfaceDeclaration: (ctx: InterfaceDeclarationContext) => void;
    exitInterfaceDeclaration: () => void;
    enterAnnotationTypeDeclaration: (ctx: AnnotationTypeDeclarationContext) => void;
    exitAnnotationTypeDeclaration: () => void;
    enterEnumDeclaration: (ctx: EnumDeclarationContext) => void;
    exitEnumConstant: (ctx: EnumConstantContext) => void;
    exitEnumDeclaration: () => void;
    enterExpression: (ctx: ExpressionContext) => void;
    exitExpression: () => void;
    enterEnhancedForControl: (ctx: EnhancedForControlContext) => void;
    enterLocalVariableDeclaration: (ctx: LocalVariableDeclarationContext) => void;
    enterFieldDeclaration: (ctx: FieldDeclarationContext) => void;
    enterConstantDeclarator: (ctx: ConstantDeclaratorContext) => void;
    enterFormalParameter: (ctx: FormalParameterContext) => void;
    enterCatchClause: (ctx: CatchClauseContext) => void;
    exitCatchClause: () => void;
    enterSwitchBlockStatementGroup: (ctx: SwitchBlockStatementGroupContext) => void;
    exitSwitchBlockStatementGroup: () => void;
    visitTerminal: (_node: TerminalNode) => void;
    private pushNewScope;
    /**
     * Checks if the symbol is static and marks it as such, if that's the case.
     *
     * @param symbol The symbol to check.
     */
    private checkStatic;
    private generateTypeRecord;
}
