import { ClassSymbol, BaseSymbol } from "antlr4-c3";
/**
 * Extends the standard class symbol type by handling implemented types and adds symbol resolving for inherited members.
 */
export declare class JavaClassSymbol extends ClassSymbol {
    typeParameters?: string;
    resolveSync(name: string, localOnly?: boolean): BaseSymbol | undefined;
}
