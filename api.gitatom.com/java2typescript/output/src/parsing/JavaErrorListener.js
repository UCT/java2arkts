/*
 * Copyright (c) Mike Lischke. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BaseErrorListener } from "antlr4ng";
export class JavaErrorListener extends BaseErrorListener {
    errors = [];
    syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, _e) {
        this.errors.push(msg);
    }
}
//# sourceMappingURL=JavaErrorListener.js.map