import { Recognizer, RecognitionException, Token, ATNSimulator, BaseErrorListener } from "antlr4ng";
export declare class JavaErrorListener extends BaseErrorListener {
    errors: string[];
    syntaxError<S extends Token, T extends ATNSimulator>(recognizer: Recognizer<T>, offendingSymbol: S | null, line: number, charPositionInLine: number, msg: string, _e: RecognitionException | null): void;
}
