import { ParseTree } from "antlr4ng";
import { SymbolTable } from "antlr4-c3";
import { ISymbolInfo } from "./conversion/types.js";
import { PackageSource } from "./PackageSource.js";
export declare class JavaFileSymbolTable extends SymbolTable {
    private source;
    private importList;
    private referencesResolved;
    private objectSymbol;
    constructor(source: PackageSource, packageRoot: string, importList: Set<PackageSource>);
    /**
     * Converts a given symbol name to its fully qualified form, if it can be found.
     *
     * @param context The parse context containing the reference to the symbol to look up.
     * @param name The name of the symbol to find.
     *
     * @returns A record with details about the found symbol. If no symbol could be found then undefined is returned.
     */
    getQualifiedSymbol: (context: ParseTree, name: string) => ISymbolInfo | undefined;
    /**
     * Called when inheritance information might be necessary.
     * Here we search inherited and implemented classes/interfaces and store references to their symbols.
     */
    resolveReferences: () => void;
    private resolveClassSymbols;
    private resolveInterfaceSymbols;
    private resolveFromImports;
    /**
     * Tries to resolve a type which is used as base type for a class or interface. This requires to check an eventual
     * outer class first (iteratively), and then to check the imports.
     *
     * @param contextOrTypeName The context of the type to resolve.
     *
     * @returns The resolved symbol, or `undefined` if no symbol could be resolved.
     */
    private resolveInheritedType;
    private getWrappingClass;
    /**
     * Determines if the given scope needs to be resolved in the outer scope.
     *
     * @param scope The scope to walk up from until a class, interface or enum is found.
     * @param owningClass The class that owns a specific symbol that is accessed from the given scope.
     *
     * @returns Returns true if we can reach the owning class from the given scope, otherwise false.
     */
    private needOuterScope;
    /**
     * Determines if the given scope needs to be resolved in the outer scope.
     *
     * @param scope The scope to walk up from until a class, interface or enum is found.
     * @param owningClass The class that owns a specific symbol that is accessed from the given scope.
     *
     * @returns Returns true if we can reach the owning class from the given scope, otherwise false.
     */
    private getClassParentName;
    /**
     * Tries to resolve a switch case label. For this we have to walk up to check the switch expression to see
     * if that is a symbol we can resolve. If that's possible, take the symbol's type as reference to resolve
     * the case label.
     *
     * @param context The context of the switch label.
     *
     * @returns The symbol info for the label or undefined if it cannot be resolved here.
     */
    private resolveSwitchLabel;
}
