#!/usr/bin/env node
/*
 * Copyright (c) Mike Lischke. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/** This is the main entry point for the java2typescript conversion, when using NPM. */
import fs from "fs/promises";
import path from "path";
import { JavaToTypescriptConverter, } from "./conversion/JavaToTypeScript.js";
const args = process.argv.slice(2);
if (args.length < 1) {
    console.log("Usage: java2ts <config-file.json>");
    process.exit(1);
}
console.log("\nConverting Java to TypeScript...\n");
// Load the given configuration file and create a converter configuration from it.
const configFile = args[0];
const content = await fs.readFile(configFile, { encoding: "utf-8" });
const json = JSON.parse(content);
let options;
if ("options" in json) {
    // The class resolver entries are given as objects, but we need a map.
    const rawResolver = json.options.importResolver;
    let classResolver;
    if (rawResolver) {
        classResolver = new Map([
            ...Object.entries(rawResolver),
        ]);
    }
    options = {
        prefix: json.options.prefix,
        convertAnnotations: json.options.convertAnnotations,
        lib: json.options.lib,
        preferArrowFunctions: json.options.preferArrowFunctions,
        autoAddBraces: json.options.autoAddBraces,
        addNullUnionType: json.options.addNullUnionType,
        suppressTypeWithInitializer: json.options.suppressTypeWithInitializer,
        wrapStringLiterals: json.options.wrapStringLiterals,
        memberOrderOptions: json.options.memberOrderOptions,
        addIndexFiles: json.options.addIndexFiles,
        sourceMappings: json.options.sourceMappings,
        // importResolver?: CustomImportResolver;
        classResolver,
    };
}
let rawReplace = json.sourceReplace;
let sourceReplace;
if (rawReplace) {
    const list = Object.entries(rawReplace).map(([key, value]) => {
        return [new RegExp(key), value];
    });
    sourceReplace = new Map(list);
}
rawReplace = json.targetReplace;
let targetReplace;
if (rawReplace) {
    const list = Object.entries(rawReplace).map(([key, value]) => {
        return [new RegExp(key), value];
    });
    targetReplace = new Map(list);
}
const config = {
    packageRoot: json.packageRoot,
    outputPath: json.outputPath,
    javaLib: json.javaLib,
    include: json.include,
    exclude: json.exclude,
    sourceReplace,
    targetReplace,
    options,
};
if (!config.packageRoot) {
    console.error("ERROR: No package root given in configuration file.");
    process.exit(1);
}
config.packageRoot = path.resolve(process.cwd(), config.packageRoot);
if (!config.outputPath) {
    console.error("ERROR: No output path given in configuration file.");
    process.exit(1);
}
config.outputPath = path.resolve(process.cwd(), config.outputPath);
const converter = new JavaToTypescriptConverter(config);
await converter.startConversion();
//# sourceMappingURL=convert.js.map