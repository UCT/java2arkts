/*
 * Copyright (c) Mike Lischke. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { glob } from "glob";
import path from "path";
import fs from "fs";
import { FileProcessor } from "./FileProcessor.js";
import { PackageSourceManager } from "../PackageSourceManager.js";
export class JavaToTypescriptConverter {
    configuration;
    constructor(configuration) {
        this.configuration = configuration;
        let javaLib;
        if (!configuration.javaLib) {
            javaLib = "jree"; // Use the jree node module as default.
        }
        else if (configuration.javaLib.indexOf("/") < 0) {
            javaLib = configuration.javaLib; // Assume this is another node module.
        }
        else {
            // It's a path, so resolve it relative to the output path.
            javaLib = path.resolve(configuration.outputPath, configuration.javaLib);
        }
        PackageSourceManager.configure(javaLib, configuration.options?.importResolver);
        configuration.packageRoot = path.resolve(configuration.packageRoot);
        // Convert all prefix field variants to a function, so we don't have to test this again later.
        if (configuration.options?.prefix) {
            const prefix = configuration.options.prefix;
            configuration.options.prefix = typeof prefix === "function" ? prefix : () => { return prefix ?? ""; };
        }
    }
    async startConversion() {
        const currentDir = process.cwd();
        // Only the files in this list are converted.
        const toConvert = [];
        // Start off by creating java file source instances for each file in the package root.
        const fileList = glob.sync(this.configuration.packageRoot + "/**/*.java");
        if (fileList.length === 0) {
            console.error("The specified pattern/path did not return any file");
            return;
        }
        fs.mkdirSync(this.configuration.outputPath, { recursive: true });
        console.log(`\nFound ${fileList.length} java files in ${this.configuration.packageRoot}`);
        const root = this.configuration.packageRoot;
        fileList.forEach((entry) => {
            const relativeSource = path.relative(this.configuration.packageRoot, entry);
            const tsName = relativeSource.substring(0, relativeSource.length - 4) + "ts";
            const target = this.configuration.outputPath + "/" + tsName;
            const source = PackageSourceManager.fromFile(entry, path.resolve(currentDir, target), root, this.configuration.sourceReplace);
            if (this.filterFile(entry, this.configuration.include, this.configuration.exclude)) {
                toConvert.push(new FileProcessor(source, this.configuration));
            }
        });
        // Load also all files given by a source mapping. These are never converted, however.
        for (const { sourcePath, importPath } of this.configuration.options?.sourceMappings ?? []) {
            const list = glob.sync(sourcePath + "/**/*.java");
            console.log(`\nFound ${list.length} java files in ${sourcePath}`);
            list.forEach((entry) => {
                PackageSourceManager.fromFile(entry, importPath, sourcePath, this.configuration.sourceReplace);
            });
        }
        console.log(`\nConverting ${toConvert.length} files...`);
        for await (const processor of toConvert) {
            await processor.convertFile();
        }
        if (this.configuration.options?.addIndexFiles) {
            console.log("\nAdding index files...");
            this.addIndexFile(path.resolve(currentDir, this.configuration.outputPath));
        }
        console.log("\nConversion finished");
    }
    /**
     * Check if the given file name matches any entry in the include and exclude filters.
     * If it matches one of the exclusion rules (if given) then the file is filtered out.
     * If the name matches any of the inclusion rules or no inclusion rules are given then the file is accepted.
     * Otherwise the file is filtered out.
     *
     * @param fileName The full path name.
     * @param include The inclusion rules.
     * @param exclude The exclusion rules.
     *
     * @returns True if the file is to be taken in, otherwise false.
     */
    filterFile = (fileName, include, exclude) => {
        if (exclude) {
            for (const filter of exclude) {
                if (fileName.match(filter)) {
                    return false;
                }
            }
        }
        if (include && include.length > 0) {
            for (const filter of include) {
                if (fileName.match(filter)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    };
    addIndexFile = (dir) => {
        const dirList = [];
        const fileList = [];
        const entries = fs.readdirSync(dir, { encoding: "utf-8", withFileTypes: true });
        entries.forEach((entry) => {
            if (!entry.name.startsWith(".") && entry.name !== "index.ts") {
                if (entry.isDirectory()) {
                    dirList.push(`export * from "./${entry.name}";`);
                    this.addIndexFile(dir + "/" + entry.name);
                }
                else if (entry.isFile() && entry.name.endsWith(".ts")) {
                    fileList.push(`export * from "./${path.basename(entry.name, ".ts")}";`);
                }
            }
        });
        fs.writeFileSync(dir + "/index.ts", `// java2typescript: auto generated index file. Disable generation by setting the "addIndexFiles" ` +
            `option to false.\n${dirList.join("\n")}\n${dirList.length > 0 ? "\n" : ""}` +
            `${fileList.join("\n")}\n`);
    };
}
//# sourceMappingURL=JavaToTypeScript.js.map