import { BaseSymbol } from "antlr4-c3";
import { PackageSource } from "../PackageSource.js";
export interface ISymbolInfo {
    /** The resolved symbol or undefined for anonymous class expressions.  */
    symbol?: BaseSymbol;
    qualifiedName: string;
    source?: PackageSource;
}
/** Determines the type of context for which certain processing is going to happen. */
export declare enum ContextType {
    File = 0,
    Class = 1,
    Interface = 2,
    Enum = 3,
    ClassExpression = 4
}
export declare enum MemberType {
    Initializer = 0,
    Field = 1,
    Constructor = 2,
    Method = 3,
    Lambda = 4,
    Static = 5,
    Abstract = 6,
    Annotation = 7,
    Class = 8,
    Interface = 9,
    Enum = 10,
    Empty = 11
}
