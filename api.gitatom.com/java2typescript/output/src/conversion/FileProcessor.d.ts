import { PackageSource } from "../PackageSource.js";
import { IConverterConfiguration } from "./JavaToTypeScript.js";
/** Converts the given Java file to Typescript. */
export declare class FileProcessor {
    private source;
    private configuration;
    private static arrayMethodMap;
    private whiteSpaceAnchor;
    private libraryImports;
    private moduleImports;
    private packageImports;
    private availablePackageImports;
    private typeStack;
    private classResolver;
    private localSymbols;
    private resolvedClasses;
    private memberOrdering;
    /**
     * Constructs a new file process and parses the given file. No conversion is done yet.
     *
     * @param source The source class holding the parser + symbol information.
     * @param configuration The values to configure the conversion process.
     */
    constructor(source: PackageSource, configuration: IConverterConfiguration);
    /**
     * Converts the Java file whose path is given by `source` to Typescript and writes the generated content
     * to the file given by `target`;
     */
    convertFile: () => Promise<void>;
    private processCompilationUnit;
    private processTypeDeclarations;
    private processClassOrInterfaceModifier;
    private processAnnotation;
    private processElementValue;
    private processElementValueArrayInitializer;
    private processClassDeclaration;
    /**
     * Processes the body of a class declaration.
     *
     * @param builder The builder to append the processed content to.
     * @param context The parse context of the class body.
     * @param extraCtorParams Optional extra parameters to add to the constructor.
     *
     * @returns True if the class contains abstract members.
     */
    private processClassBody;
    private processClassBodyDeclarations;
    private processModifier;
    private processMemberDeclaration;
    private processMethodDeclaration;
    private processFormalParameters;
    private processFormalParameterList;
    /**
     * Processes a single formal parameter.
     *
     * @param builder The target buffer to write the converted code to.
     * @param context The parse context to examine.
     *
     * @returns Returns name and type of the parameter.
     */
    private processFormalParameter;
    private processMethodBody;
    private processGenericMethodDeclaration;
    private processFieldDeclaration;
    private processConstructorDeclaration;
    private processGenericConstructorDeclaration;
    private processTypeParameters;
    private processTypeParameter;
    private processTypeBound;
    private processInterfaceDeclaration;
    private processInterfaceBody;
    private processInterfaceBodyDeclarations;
    private processInterfaceMemberDeclaration;
    private processConstDeclaration;
    private processConstantDeclarator;
    private processInterfaceMethodDeclaration;
    private processGenericInterfaceMethodDeclaration;
    private processInterfaceCommonBodyDeclaration;
    private processAnnotationTypeDeclaration;
    private processEnumDeclaration;
    private processEnumConstants;
    private processEnumConstant;
    private processBlock;
    private processBlockStatement;
    private processLocalVariableDeclaration;
    private processVariableModifier;
    private processVariableDeclarators;
    private processVariableDeclarator;
    private processVariableInitializer;
    private processArrayInitializer;
    private processParExpression;
    /**
     * Processes a single expression.
     *
     * @param builder The target buffer to write the result to.
     * @param context The expression context for processing.
     *
     * @returns If the expression results in an identifiable member (e.g. a field) then the symbol for it is returned.
     */
    private processExpression;
    private processExplicitGenericInvocation;
    private processSuperSuffix;
    private processNonWildcardTypeArguments;
    private processInnerCreator;
    private processClassCreatorRest;
    private processArguments;
    private processNonWildcardTypeArgumentsOrDiamond;
    private processLambdaExpression;
    private processLambdaParameters;
    /**
     * Process a method call with either the current class as instance or for the given instance.
     *
     * @param builder The target buffer to write to.
     * @param context The context to be processed.
     * @param instance Optional symbol information, if the method call belongs to this instance. This allows us to
     *                 transform certain method calls to their TS equivalent.
     */
    private processMethodCall;
    private processMethodCallExpression;
    private processExpressionList;
    /**
     * Processes a creator rule for `new` expressions.
     *
     * @param builder The build for the output.
     * @param context The creator context to process.
     *
     * @returns true if the caller should use the `new` operator, otherwise false (e.g. for array initializers).
     */
    private processCreator;
    private processCreatedName;
    private processPrimary;
    private processLiteral;
    private processExplicitGenericInvocationSuffix;
    private processStatement;
    private processResourceSpecification;
    private processResources;
    private processSwitchBlockStatementGroup;
    private processSwitchLabel;
    private processFinallyBlock;
    private processCatchClauses;
    private processForControl;
    private processForInit;
    private processEnhancedForControl;
    private processVariableDeclaratorId;
    private processLocalTypeDeclaration;
    private processTypeList;
    private processTypeTypeOrVoid;
    private processTypeType;
    private processClassType;
    private processClassOrInterfaceType;
    private processTypeArguments;
    private processTypeArgument;
    private processPrimitiveType;
    /**
     * Processing of the individual body members. They may require reordering (static parts) or need a rewrite
     * (overloaded methods).
     *
     * This is also the place where we add generated members.
     *
     * @param builder The target builder to write the final output to.
     * @param members A list of process member entries.
     * @param needOverloadHandling Set for interfaces to indicate that no overload handling is required.
     */
    private processBodyMembers;
    private processConstructorAndMethodMembers;
    /**
     * Depending on the configuration settings this method adds braces around a statement if there aren't any yet.
     *
     * @param builder The target buffer to add content to.
     * @param statement The statement to process.
     */
    private expressionWithBraces;
    /**
     * Returns all white spaces (including comments) between the current white space anchor and the first character
     * covered by the target.
     * The white space anchor is then set to the position directly following the target.
     *
     * @param target A parse tree for which to return the leading white spaces.
     *
     * @returns The original white space text between tokens.
     */
    private getLeadingWhiteSpaces;
    /**
     * Used for constructs that cannot be (fully) represented in Typescript or can simply be taken over as they are.
     *
     * @param builder The target buffer to add content to.
     * @param target A parse tree for which to get the content.
     * @param commented If true the target content is placed in a multi line comment.
     */
    private getContent;
    /**
     * Ignore the content for the given target. It's usually replaced by something else or not handled at all.
     *
     * @param target A parse tree for which to get the content.
     */
    private ignoreContent;
    /**
     * Returns a range of text always commented.
     *
     * @param builder The target buffer to add content to.
     * @param start The context whose start index begins the range.
     * @param stop  The context whose stop index ends the range.
     */
    private getRangeCommented;
    /**
     * Called when a class, interface or enum body construction was finished. It takes all nested declarations for the
     * current type on the type stack and constructs a namespace declaration of it.
     * Also removes the TOS from the stack.
     *
     * @param doExport True if the new namespace must be exported.
     *
     * @returns The new namespace declaration.
     */
    private processNestedContent;
    /**
     * This is the central method to resolve all types that can occur in the source code.
     * Resolving a symbol involves a number of steps:
     *
     * 1. Find a configured replacement via the class resolver, or
     * 2. Find the type in the current file and add certain prefixes, if necessary (e.g. `this.`), or
     * 3. Find the type in the exported type list of any of the imported packages.
     *
     * @param context A parse tree to start searching from for local symbols.
     * @param name The name of the type to check.
     *
     * @returns Either a replacement for the given name or the name itself.
     */
    private resolveType;
    /**
     * Loops over all imports and tries to find the symbol with the given name.
     *
     * @param name The name of the symbol to find.
     *
     * @returns Either the symbol info or the name itself.
     */
    private resolveFromImports;
    /**
     * Iterates over all symbols in the java.lang package (ignoring sub namespaces like java.lang.annotation) and
     * adds them to the default package imports.
     */
    private registerDefaultImports;
    /**
     * Used to add a symbol for import via the java main import (e.g. helper code).
     *
     * @param name The name to add.
     */
    private registerJavaImport;
    /**
     * Takes a set of modifiers and creates a modifier string with the correct order of the individual modifiers.
     *
     * @param modifiers The set of modifiers or undefined.
     *
     * @returns The constructed string (in the case of undefined modifiers the string is empty).
     */
    private createModifierString;
    /**
     * Checks if the given method overrides a method from a base class.
     *
     * @param context The parse context of the method.
     * @param details The details of the method to check.
     *
     * @returns True if the method overrides a method from a base class, otherwise false.
     */
    private overridesMethod;
    /**
     * Called when a primitive type is encountered (maybe in conjunction with an array).
     *
     * @param builder The builder to append the type to.
     * @param type The token type of the primitive type.
     * @param isArray True if the type is part of an array expression, otherwise false.
     */
    private convertPrimitiveType;
}
