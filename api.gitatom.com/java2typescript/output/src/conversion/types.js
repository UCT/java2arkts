/*
 * Copyright (c) Mike Lischke. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/** Determines the type of context for which certain processing is going to happen. */
export var ContextType;
(function (ContextType) {
    ContextType[ContextType["File"] = 0] = "File";
    ContextType[ContextType["Class"] = 1] = "Class";
    ContextType[ContextType["Interface"] = 2] = "Interface";
    ContextType[ContextType["Enum"] = 3] = "Enum";
    ContextType[ContextType["ClassExpression"] = 4] = "ClassExpression";
})(ContextType || (ContextType = {}));
export var MemberType;
(function (MemberType) {
    MemberType[MemberType["Initializer"] = 0] = "Initializer";
    MemberType[MemberType["Field"] = 1] = "Field";
    MemberType[MemberType["Constructor"] = 2] = "Constructor";
    MemberType[MemberType["Method"] = 3] = "Method";
    MemberType[MemberType["Lambda"] = 4] = "Lambda";
    MemberType[MemberType["Static"] = 5] = "Static";
    MemberType[MemberType["Abstract"] = 6] = "Abstract";
    MemberType[MemberType["Annotation"] = 7] = "Annotation";
    MemberType[MemberType["Class"] = 8] = "Class";
    MemberType[MemberType["Interface"] = 9] = "Interface";
    MemberType[MemberType["Enum"] = 10] = "Enum";
    MemberType[MemberType["Empty"] = 11] = "Empty";
})(MemberType || (MemberType = {}));
//# sourceMappingURL=types.js.map