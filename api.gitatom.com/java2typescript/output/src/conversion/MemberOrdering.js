/*
 * Copyright (c) Mike Lischke. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import console from "console";
import { ContextType, MemberType as ContextMemberType } from "./types.js";
export class MemberOrdering {
    #defaultList;
    #classList;
    #interfaceList;
    #classExpressionList;
    constructor(options) {
        this.#defaultList = this.convertOrderConfig(options.default);
        this.#classList = this.convertOrderConfig(options.classes);
        this.#interfaceList = this.convertOrderConfig(options.interfaces);
        this.#classExpressionList = this.convertOrderConfig(options.classExpressions);
    }
    /**
     * Applies the configured member ordering to the given list of members.
     *
     * @param members The members to sort.
     * @param context The context in which the members are defined.
     *
     * @returns The sorted list of members.
     */
    apply = (members, context) => {
        let orderList = this.#defaultList;
        if (context === ContextType.Class && this.#classList) {
            orderList = this.#classList;
        }
        else if (context === ContextType.Interface && this.#interfaceList) {
            orderList = this.#interfaceList;
        }
        else if (context === ContextType.ClassExpression && this.#classExpressionList) {
            orderList = this.#classExpressionList;
        }
        if (!orderList) {
            return members;
        }
        const result = [];
        orderList.forEach((orderGroup) => {
            const group = [];
            orderGroup.forEach((orderItem) => {
                if (orderItem.kind === "call-signature" || orderItem.kind === "get" || orderItem.kind === "set"
                    || orderItem.kind === "signature" || orderItem.kind === "readonly-signature") {
                    // These kinds cannot appear in code translated from Java.
                    return;
                }
                // Loop over the members list and find all entries that match the current order item.
                // Each match is moved to the result list and removed from the members list.
                while (true) {
                    // Find the next entry in the members list that matches the current order item.
                    const index = members.findIndex((member) => {
                        switch (orderItem.kind) {
                            case "constructor": {
                                if (member.type !== ContextMemberType.Constructor) {
                                    return false;
                                }
                                break;
                            }
                            case "field": {
                                // Nested classes are implemented as instance fields and a class expression or
                                // factory function. Hence they count as fields too.
                                if (member.type !== ContextMemberType.Field
                                    && member.type !== ContextMemberType.Class) {
                                    return false;
                                }
                                break;
                            }
                            case "readonly-field": {
                                if (member.type !== ContextMemberType.Field) {
                                    return false;
                                }
                                if (!member.modifiers?.has("readonly")) {
                                    return false;
                                }
                                break;
                            }
                            case "method": {
                                if (member.type !== ContextMemberType.Method) {
                                    return false;
                                }
                                break;
                            }
                            case "initialization": {
                                if (member.type !== ContextMemberType.Initializer) {
                                    return false;
                                }
                                break;
                            }
                            default:
                        }
                        if (orderItem.accessibility && !member.modifiers?.has(orderItem.accessibility)) {
                            return false;
                        }
                        switch (orderItem.scope) {
                            case "static": {
                                if (!member.modifiers?.has("static")) {
                                    return false;
                                }
                                break;
                            }
                            case "instance": {
                                if (member.modifiers?.has("static")) {
                                    return false;
                                }
                                break;
                            }
                            case "decorated": {
                                if (!member.modifiers?.has("decorated")) {
                                    return false;
                                }
                                break;
                            }
                            case "abstract": {
                                if (!member.modifiers?.has("abstract")) {
                                    return false;
                                }
                                break;
                            }
                            default:
                        }
                        return true;
                    });
                    if (index >= 0) {
                        group.push(members[index]);
                        members.splice(index, 1);
                    }
                    else {
                        break;
                    }
                }
            });
            if (group.length > 0) {
                result.push(...group);
            }
        });
        // Some members may not have matched any order item, so add them to the end.
        return result.concat(members);
    };
    convertOrderConfig(config) {
        if (!config || config === "never") {
            return undefined;
        }
        // TODO: handle additional SortedOrderConfig options (alphabetic order etc.).
        if (this.isSortedOrderConfig(config)) {
            console.warn("Sorted order configuration (in memberOrderOptions) not yet supported.");
            return undefined;
        }
        const expandedOrderList = config.map((orderItem) => {
            const list = Array.isArray(orderItem) ? orderItem : [orderItem];
            return list.map((item) => {
                const parts = item.split("-", 3);
                switch (parts.length) {
                    case 1: {
                        return {
                            kind: parts[0],
                        };
                    }
                    case 2: {
                        return {
                            scope: parts[0],
                            kind: parts[1],
                        };
                    }
                    case 3: {
                        return {
                            accessibility: parts[0],
                            scope: parts[1],
                            kind: parts[2],
                        };
                    }
                    default: {
                        throw new Error("Invalid member ordering configuration.");
                    }
                }
            });
        });
        return expandedOrderList;
    }
    isSortedOrderConfig(config) {
        return config.order !== undefined;
    }
}
//# sourceMappingURL=MemberOrdering.js.map