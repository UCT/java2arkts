import { ContextType, MemberType as ContextMemberType } from "./types.js";
/**
 * A class and its associated data structures, for converting the `@typescript-eslint/member-ordering`
 * linter rule setting and applying it to a list of given type members.
 */
/** The structure of the `@typescript-eslint/member-ordering` linter rule setting. */
export interface IMemberOrderOptions {
    default?: OrderConfig;
    classes?: OrderConfig;
    classExpressions?: OrderConfig;
    interfaces?: OrderConfig;
    typeLiterals?: OrderConfig;
}
type OrderConfig = MemberType[] | ISortedOrderConfig | "never";
interface ISortedOrderConfig {
    memberTypes?: MemberType[] | "never";
    optionalityOrder?: "optional-first" | "required-first";
    order: "alphabetically" | "alphabetically-case-insensitive" | "as-written" | "natural" | "natural-case-insensitive";
}
type MemberType = string | string[];
interface IMember {
    type: ContextMemberType;
    modifiers?: Set<string>;
}
export declare class MemberOrdering {
    #private;
    constructor(options: IMemberOrderOptions);
    /**
     * Applies the configured member ordering to the given list of members.
     *
     * @param members The members to sort.
     * @param context The context in which the members are defined.
     *
     * @returns The sorted list of members.
     */
    apply: (members: IMember[], context: ContextType) => IMember[];
    private convertOrderConfig;
    private isSortedOrderConfig;
}
export {};
