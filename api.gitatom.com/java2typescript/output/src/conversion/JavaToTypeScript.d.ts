import { CustomImportResolver } from "../PackageSourceManager.js";
import { IMemberOrderOptions } from "./MemberOrdering.js";
/** A record for the class resolver map. */
export interface IClassResolver {
    alias?: string;
    importPath: string;
}
/**
 * Maps a root package ID to a source path to get the Java source code from, as well as a path that is used
 * for the imports. Source mappings are to provide symbol information for a package, which is however not converted.
 */
export interface ISourceMapping {
    /** The full path to the file to parse. */
    sourcePath: string;
    /**
     * A path specifying the import. It's assumed to be a node module, if it doesn't start with
     * a slash or dot. Otherwise the import will be computed relative to the target file.
     */
    importPath: string;
}
export type ConverterOptionsPrefixFunc = (sourcePath: string, targetPath?: string) => string;
export interface IConverterOptions {
    /** Anything to go before the first code line (e.g. linter settings). */
    prefix?: ConverterOptionsPrefixFunc | string;
    /** If true then Java annotations are converted to Typescript decorators. */
    convertAnnotations?: boolean;
    /**
     * A folder path for additional TS source files required to polyfill Java classes or which implement support code.
     */
    lib?: string;
    /** If true, functions/methods use the arrow syntax. */
    preferArrowFunctions?: boolean;
    /** If true the processor will automatically add braces in IF/ELSE/SWITCH statements, if they are missing. */
    autoAddBraces?: boolean;
    /**
     * If true, the processor will automatically add a `null` alternative to all non-primitive types. Default is: true.
     */
    addNullUnionType?: boolean;
    /**
     * If true, the processor will not the explicit type to a variable/member if it has an initializer.
     * Default is: false.
     */
    suppressTypeWithInitializer?: boolean;
    /**
     * If true, the process will convert string literals (`"..."`) to a template string (``` S`...` ```) which
     * automatically creates a java.lang.String object. Default is: false.
     */
    wrapStringLiterals?: boolean;
    /**
     * Takes a set of options as used for the
     * [`@typescript-eslint/member-ordering`](https://typescript-eslint.io/rules/member-ordering/#options) linter rule.
     * These options are applied on the transformed members, right before overload handling is done.
     */
    memberOrderOptions?: IMemberOrderOptions;
    /**
     * For simpler imports index files can be added to each generated output folder, which export all files in that
     * folder plus the index files of all subfolders.
     */
    addIndexFiles?: boolean;
    /**
     * Normally the processor resolves types to their fully qualified name (e.g. `java.util.List`), regardless of which
     * package import appears in the Java source file. If this option is true then the processor converts Java imports
     * to const statements that map the imported type to its simple name (e.g. `const List = java.util.List`). That
     * way the simple name can be used in the code. Default is: false.
     */
    useUnqualifiedTypes?: boolean;
    /**
     * A mapping of a 3rd party package which is available in source form. Maps root package IDs (without any type
     * name) to a source path, which is then used as package root for that package.
     */
    sourceMappings?: ISourceMapping[];
    /**
     * A function that takes a package ID and returns a package source for it. Used usually to provide hard coded
     * symbol information for packages/modules for which no Java source code is available.
     */
    importResolver?: CustomImportResolver;
    /**
     * A map that provides an import string for a given class name. Names given here do not use qualifiers, but
     * are imported directly from the node module or file/folder given as resolution.
     * No file is parsed and no symbol table is created for the symbols listed here.
     */
    classResolver?: Map<string, IClassResolver>;
}
/** Options used for debugging the transformation process. */
export interface IDebugOptions {
    /**
     *  Specifies a position in a file whose name matches filePattern. The parse tree located covering this position
     * is searched after parsing and the entire parse tree path from the root up to this tree is printed to the console.
     */
    pathForPosition?: {
        filePattern?: string | RegExp;
        position: {
            row: number;
            column: number;
        };
    };
}
export interface IConverterConfiguration {
    /**
     * The root folder of the package to convert. Only files in the file tree are automatically resolved
     * when importing symbols.
     * Package imports from outside (inclusive standard Java packages) need an explicit source resolver
     * (@link options.importResolver).
     *
     * Note: Only Java files are actually transformed.
     */
    packageRoot: string;
    /**
     * The path to the Java runtime. This is what's used for runtime imports.
     * If not given, the `jree` node package is used. A value without at least one slash is assumed to be a node module.
     * If the given path is relative, it is assumed to be relative to the output path.
     */
    javaLib?: string;
    /**
     * An optional inclusion filter for files found in the package. Only files matching this pattern are actually
     * processed and output generated. However, non-Java files are ignored, even if they are included here.
     *
     * Note: All Java files in the package have to be loaded once to get their symbols.
     *       However, they are only parsed if something is actually imported from them, saving so some parsing time.
     */
    include?: Array<string | RegExp>;
    /**
     * Files to exclude from the conversion process.
     */
    exclude?: Array<string | RegExp>;
    /**
     * The root folder for generated files. Relative paths are kept (like in the source tree).
     */
    outputPath: string;
    /** Specifies patterns for string replacements to be done in a Java file before it is parsed. */
    sourceReplace?: Map<RegExp, string>;
    /** Specifies patterns for string replacements to be done in the generated TS file. */
    targetReplace?: Map<RegExp, string>;
    /**
     * Options for the conversion process.
     */
    options?: IConverterOptions;
    /**
     * Additional options for debugging the conversion process.
     */
    debug?: IDebugOptions;
}
export declare class JavaToTypescriptConverter {
    private configuration;
    constructor(configuration: IConverterConfiguration);
    startConversion(): Promise<void>;
    /**
     * Check if the given file name matches any entry in the include and exclude filters.
     * If it matches one of the exclusion rules (if given) then the file is filtered out.
     * If the name matches any of the inclusion rules or no inclusion rules are given then the file is accepted.
     * Otherwise the file is filtered out.
     *
     * @param fileName The full path name.
     * @param include The inclusion rules.
     * @param exclude The exclusion rules.
     *
     * @returns True if the file is to be taken in, otherwise false.
     */
    private filterFile;
    private addIndexFile;
}
