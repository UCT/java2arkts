import { ParseTree, Interval } from "antlr4ng";
import { BaseSymbol, SymbolTable } from "antlr4-c3";
import { CompilationUnitContext } from "../parser/generated/JavaParser.js";
import { ISymbolInfo } from "./conversion/types.js";
/**
 * A class to provide symbol information for a single package or source file. It allows to convert
 * Java imports to JS/TS imports and to expand partial type specifiers to their fully qualified name.
 */
export declare class PackageSource {
    packageId: string;
    sourceFile: string;
    targetFile: string;
    /** Available symbols from the associated file or package. */
    symbolTable: SymbolTable;
    /** The set of sources imported by this source. Might contain unused imports. */
    importList: Set<PackageSource>;
    /**
     * A list of symbol names, which have been resolved at least once, which means they are imported into the
     * file being converted. Hence those names comprise the TS import list.
     */
    protected importedSymbols: Set<string>;
    constructor(packageId: string, sourceFile: string, targetFile: string);
    get parseTree(): CompilationUnitContext | undefined;
    /**
     * A debug helper to print the parse tree for a given position. Only has an effect for Java file sources.
     *
     * @param position The character position in the file.
     * @param position.column The character column.
     * @param position.row The character row (one-based).
     */
    printParseTreeForPosition: (position: {
        column: number;
        row: number;
    }) => void;
    getQualifiedSymbol: (_context: ParseTree, _name: string) => ISymbolInfo | undefined;
    /**
     * Returns original text from the file input, if this source is a Java file source.
     *
     * @param interval The start and stop character indices.
     *
     * @returns The original text for file sources or an empty string for pure package sources.
     */
    getText: (interval: Interval) => string;
    /**
     * Used to reset the recorded list of imported symbols. This has to be done for each processor run.
     */
    clearImportedSymbols: () => void;
    /**
     * Adds the given symbol name to the internal import list, to have it included in the final import info,
     * without going through the entire resolution process. This is useful for symbols that are not part of Java,
     * but still imported with a specific package (helpers for example).
     *
     * @param name The name to add.
     */
    addImportedSymbol: (name: string) => void;
    /**
     * Collects all imported names. Recording is done during symbol resolution.
     *
     * @param importingFile The absolute path to the file, for which to generate the import statements.
     *
     * @returns A tuple containing a list of imported names and the relative import path.
     */
    getImportInfo: (importingFile: string) => [string[], string];
    /**
     * Checks if the given name refers to a type that is known. For nested types (e.g. Class1.Class2.Interface1) it
     * is enough to only specify a part of the entire type chain and the method will return the full chain.
     * A partial specification allows to omit one or more leading type specifiers (e.g. using only Class2.Interface1 or
     * even just Interface1).
     *
     * If a type could be resolved then it's root type (e.g. Class1) is recorded for later import statement creation.
     *
     * @param name The (partial) type name to resolve.
     *
     * @returns The symbol for the given name, if found. Otherwise nothing is returned.
     */
    resolveType: (name: string) => ISymbolInfo | undefined;
    /**
     * Finds an exported type given by name. If found the name is added to the import list of this source.
     *
     * @param name The name to resolve.
     *
     * @returns The symbol for the given name, if found.
     */
    resolveAndImport: (name: string) => BaseSymbol | undefined;
    protected textFromInterval: (_interval: Interval) => string;
    protected createSymbolTable(): SymbolTable;
    /**
     * Allows derived package sources to override the import statement generation.
     *
     * @returns The value to use for all symbols used from this source. This is usually the top level namespace,
     *          if given, and should be importable from the package's target path.
     */
    protected get importOverride(): string | undefined;
}
