import { PackageSource } from "./PackageSource.js";
/** This is the hook for the application to provide custom package sources for symbol resolution. */
export type CustomImportResolver = (packageId: string) => PackageSource | undefined;
/** A class to hold package sources for use by the conversion process. */
export declare class PackageSourceManager {
    private static customImportResolver?;
    private static javaTargetRoot;
    private static sources;
    static configure: (javaTargetRoot: string, customImportResolver?: CustomImportResolver) => void;
    /**
     * Looks up a package source in the internal registry, loaded from the given absolute path.
     * If it doesn't exist yet, it's created from the specified file.
     *
     * @param source The absolute path to a Java file, which can be loaded and parse. This file must exist or an error
     *               is raised.
     * @param target The absolute path to the TS file, which will be generated. Can be empty if no target file is
     *               generated or may contain a reference to some other source like a node module.
     * @param packageRoot The path to the root of the package where the given file is in. Used to resolve
     *                    relative file paths for imports.
     * @param replacements Patterns for string replacements in the source file, before it is parsed.
     *
     * @returns A package source instance. An error is thrown if the source could not be read.
     */
    static fromFile: (source: string, target: string, packageRoot: string, replacements?: Map<RegExp, string>) => PackageSource;
    /**
     * Uses a package ID for look up of a package source. This must be a complete package ID as used by imports.
     * Wildcard imports are handled in `fromPackageIdWildcard`.
     *
     * All packages should be created upfront before invoking any of the package lookup methods. Exceptions are
     * sources created from the custom import resolver.
     *
     * As a last resort an empty package source is created for IDs that are not know to the manager.
     *
     * @param packageId The ID of a package to load.
     *
     * @returns A package source instance which matches the given ID.
     */
    static fromPackageId: (packageId: string) => PackageSource;
    /**
     * Returns a list of package sources for all files in the given package.
     *
     * @param packageId The ID of a package to get all sources for.
     *
     * @returns A list of package source instances which match the given parameters.
     */
    static fromPackageIdWildcard: (packageId: string) => PackageSource[];
    /**
     * Creates and registers an empty package source for the given ID. Such a source has no symbol table
     * and hence cannot contribute to symbol resolutions.
     * If there's already a package source (empty or not) for the given ID, that will be returned instead of
     * creating a new package source.
     *
     * @param packageId The package ID to use.
     *
     * @returns A package source for that ID.
     */
    static emptySource: (packageId: string) => PackageSource;
    /**
     * Removes all imported symbols from all known package sources.
     * Used at each process run to remove imported symbols from a previous run.
     */
    static clearImportedSymbols: () => void;
    /**
     * Takes a full package ID and returns a package source for it, if one exists.
     *
     * @param packageId The package ID as used in a Java import statement (no wildcard).
     *
     * @returns The found package source or undefined, if none could be found.
     */
    private static findSource;
}
