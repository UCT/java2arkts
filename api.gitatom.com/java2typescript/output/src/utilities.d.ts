import { ParseTree } from "antlr4ng";
/**
 * Get the lowest level parse tree, which covers the given position.
 *
 * @param root The start point to search from.
 * @param column The position in the given row.
 * @param row The row position to search for.
 *
 * @returns The parse tree which covers the given position or undefined if none could be found.
 */
export declare const parseTreeFromPosition: (root: ParseTree, column: number, row: number) => ParseTree | undefined;
/**
 * Converts a parse tree invocation stack to a string and prints it to the console.
 *
 * @param fileName The name of the file for which the output is produced.
 * @param root The parse tree to use.
 * @param ruleNames The list of all rule names for pretty printing.
 * @param position The position in the source text where to start parse tree traversal from.
 * @param position.column The column value.
 * @param position.row The row value.
 */
export declare const printParseTreeStack: (fileName: string, root: ParseTree, ruleNames: string[], position: {
    column: number;
    row: number;
}) => void;
