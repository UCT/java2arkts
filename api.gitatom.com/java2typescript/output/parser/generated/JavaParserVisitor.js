// Generated from ./parser/JavaParser.g4 by ANTLR 4.13.1
import { AbstractParseTreeVisitor } from "antlr4ng";
/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by `JavaParser`.
 *
 * @param <Result> The return type of the visit operation. Use `void` for
 * operations with no return type.
 */
export class JavaParserVisitor extends AbstractParseTreeVisitor {
    /**
     * Visit a parse tree produced by `JavaParser.compilationUnit`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitCompilationUnit;
    /**
     * Visit a parse tree produced by `JavaParser.packageDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitPackageDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.importDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitImportDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.typeDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.modifier`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitModifier;
    /**
     * Visit a parse tree produced by `JavaParser.classOrInterfaceModifier`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitClassOrInterfaceModifier;
    /**
     * Visit a parse tree produced by `JavaParser.variableModifier`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitVariableModifier;
    /**
     * Visit a parse tree produced by `JavaParser.classDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitClassDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.typeParameters`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeParameters;
    /**
     * Visit a parse tree produced by `JavaParser.typeParameter`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeParameter;
    /**
     * Visit a parse tree produced by `JavaParser.typeBound`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeBound;
    /**
     * Visit a parse tree produced by `JavaParser.enumDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitEnumDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.enumConstants`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitEnumConstants;
    /**
     * Visit a parse tree produced by `JavaParser.enumConstant`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitEnumConstant;
    /**
     * Visit a parse tree produced by `JavaParser.enumBodyDeclarations`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitEnumBodyDeclarations;
    /**
     * Visit a parse tree produced by `JavaParser.interfaceDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitInterfaceDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.classBody`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitClassBody;
    /**
     * Visit a parse tree produced by `JavaParser.interfaceBody`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitInterfaceBody;
    /**
     * Visit a parse tree produced by `JavaParser.classBodyDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitClassBodyDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.memberDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitMemberDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.methodDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitMethodDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.methodBody`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitMethodBody;
    /**
     * Visit a parse tree produced by `JavaParser.typeTypeOrVoid`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeTypeOrVoid;
    /**
     * Visit a parse tree produced by `JavaParser.genericMethodDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitGenericMethodDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.genericConstructorDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitGenericConstructorDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.constructorDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitConstructorDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.fieldDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitFieldDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.interfaceBodyDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitInterfaceBodyDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.interfaceMemberDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitInterfaceMemberDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.constDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitConstDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.constantDeclarator`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitConstantDeclarator;
    /**
     * Visit a parse tree produced by `JavaParser.interfaceMethodDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitInterfaceMethodDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.interfaceMethodModifier`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitInterfaceMethodModifier;
    /**
     * Visit a parse tree produced by `JavaParser.genericInterfaceMethodDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitGenericInterfaceMethodDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.interfaceCommonBodyDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitInterfaceCommonBodyDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.variableDeclarators`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitVariableDeclarators;
    /**
     * Visit a parse tree produced by `JavaParser.variableDeclarator`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitVariableDeclarator;
    /**
     * Visit a parse tree produced by `JavaParser.variableDeclaratorId`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitVariableDeclaratorId;
    /**
     * Visit a parse tree produced by `JavaParser.variableInitializer`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitVariableInitializer;
    /**
     * Visit a parse tree produced by `JavaParser.arrayInitializer`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitArrayInitializer;
    /**
     * Visit a parse tree produced by `JavaParser.classOrInterfaceType`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitClassOrInterfaceType;
    /**
     * Visit a parse tree produced by `JavaParser.typeArgument`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeArgument;
    /**
     * Visit a parse tree produced by `JavaParser.qualifiedNameList`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitQualifiedNameList;
    /**
     * Visit a parse tree produced by `JavaParser.formalParameters`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitFormalParameters;
    /**
     * Visit a parse tree produced by `JavaParser.receiverParameter`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitReceiverParameter;
    /**
     * Visit a parse tree produced by `JavaParser.formalParameterList`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitFormalParameterList;
    /**
     * Visit a parse tree produced by `JavaParser.formalParameter`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitFormalParameter;
    /**
     * Visit a parse tree produced by `JavaParser.lastFormalParameter`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitLastFormalParameter;
    /**
     * Visit a parse tree produced by `JavaParser.lambdaLVTIList`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitLambdaLVTIList;
    /**
     * Visit a parse tree produced by `JavaParser.lambdaLVTIParameter`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitLambdaLVTIParameter;
    /**
     * Visit a parse tree produced by `JavaParser.qualifiedName`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitQualifiedName;
    /**
     * Visit a parse tree produced by `JavaParser.literal`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitLiteral;
    /**
     * Visit a parse tree produced by `JavaParser.integerLiteral`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitIntegerLiteral;
    /**
     * Visit a parse tree produced by `JavaParser.floatLiteral`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitFloatLiteral;
    /**
     * Visit a parse tree produced by `JavaParser.altAnnotationQualifiedName`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAltAnnotationQualifiedName;
    /**
     * Visit a parse tree produced by `JavaParser.annotation`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAnnotation;
    /**
     * Visit a parse tree produced by `JavaParser.elementValuePairs`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitElementValuePairs;
    /**
     * Visit a parse tree produced by `JavaParser.elementValuePair`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitElementValuePair;
    /**
     * Visit a parse tree produced by `JavaParser.elementValue`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitElementValue;
    /**
     * Visit a parse tree produced by `JavaParser.elementValueArrayInitializer`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitElementValueArrayInitializer;
    /**
     * Visit a parse tree produced by `JavaParser.annotationTypeDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAnnotationTypeDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.annotationTypeBody`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAnnotationTypeBody;
    /**
     * Visit a parse tree produced by `JavaParser.annotationTypeElementDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAnnotationTypeElementDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.annotationTypeElementRest`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAnnotationTypeElementRest;
    /**
     * Visit a parse tree produced by `JavaParser.annotationMethodOrConstantRest`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAnnotationMethodOrConstantRest;
    /**
     * Visit a parse tree produced by `JavaParser.annotationMethodRest`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAnnotationMethodRest;
    /**
     * Visit a parse tree produced by `JavaParser.annotationConstantRest`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAnnotationConstantRest;
    /**
     * Visit a parse tree produced by `JavaParser.defaultValue`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitDefaultValue;
    /**
     * Visit a parse tree produced by `JavaParser.moduleDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitModuleDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.moduleBody`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitModuleBody;
    /**
     * Visit a parse tree produced by `JavaParser.moduleDirective`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitModuleDirective;
    /**
     * Visit a parse tree produced by `JavaParser.requiresModifier`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitRequiresModifier;
    /**
     * Visit a parse tree produced by `JavaParser.recordDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitRecordDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.recordHeader`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitRecordHeader;
    /**
     * Visit a parse tree produced by `JavaParser.recordComponentList`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitRecordComponentList;
    /**
     * Visit a parse tree produced by `JavaParser.recordComponent`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitRecordComponent;
    /**
     * Visit a parse tree produced by `JavaParser.recordBody`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitRecordBody;
    /**
     * Visit a parse tree produced by `JavaParser.block`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitBlock;
    /**
     * Visit a parse tree produced by `JavaParser.blockStatement`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitBlockStatement;
    /**
     * Visit a parse tree produced by `JavaParser.localVariableDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitLocalVariableDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.identifier`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitIdentifier;
    /**
     * Visit a parse tree produced by `JavaParser.localTypeDeclaration`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitLocalTypeDeclaration;
    /**
     * Visit a parse tree produced by `JavaParser.statement`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitStatement;
    /**
     * Visit a parse tree produced by `JavaParser.catchClause`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitCatchClause;
    /**
     * Visit a parse tree produced by `JavaParser.catchType`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitCatchType;
    /**
     * Visit a parse tree produced by `JavaParser.finallyBlock`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitFinallyBlock;
    /**
     * Visit a parse tree produced by `JavaParser.resourceSpecification`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitResourceSpecification;
    /**
     * Visit a parse tree produced by `JavaParser.resources`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitResources;
    /**
     * Visit a parse tree produced by `JavaParser.resource`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitResource;
    /**
     * Visit a parse tree produced by `JavaParser.switchBlockStatementGroup`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitSwitchBlockStatementGroup;
    /**
     * Visit a parse tree produced by `JavaParser.switchLabel`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitSwitchLabel;
    /**
     * Visit a parse tree produced by `JavaParser.forControl`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitForControl;
    /**
     * Visit a parse tree produced by `JavaParser.forInit`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitForInit;
    /**
     * Visit a parse tree produced by `JavaParser.enhancedForControl`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitEnhancedForControl;
    /**
     * Visit a parse tree produced by `JavaParser.parExpression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitParExpression;
    /**
     * Visit a parse tree produced by `JavaParser.expressionList`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitExpressionList;
    /**
     * Visit a parse tree produced by `JavaParser.methodCall`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitMethodCall;
    /**
     * Visit a parse tree produced by `JavaParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitExpression;
    /**
     * Visit a parse tree produced by `JavaParser.pattern`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitPattern;
    /**
     * Visit a parse tree produced by `JavaParser.lambdaExpression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitLambdaExpression;
    /**
     * Visit a parse tree produced by `JavaParser.lambdaParameters`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitLambdaParameters;
    /**
     * Visit a parse tree produced by `JavaParser.lambdaBody`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitLambdaBody;
    /**
     * Visit a parse tree produced by `JavaParser.primary`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitPrimary;
    /**
     * Visit a parse tree produced by `JavaParser.switchExpression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitSwitchExpression;
    /**
     * Visit a parse tree produced by `JavaParser.switchLabeledRule`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitSwitchLabeledRule;
    /**
     * Visit a parse tree produced by `JavaParser.guardedPattern`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitGuardedPattern;
    /**
     * Visit a parse tree produced by `JavaParser.switchRuleOutcome`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitSwitchRuleOutcome;
    /**
     * Visit a parse tree produced by `JavaParser.classType`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitClassType;
    /**
     * Visit a parse tree produced by `JavaParser.creator`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitCreator;
    /**
     * Visit a parse tree produced by `JavaParser.createdName`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitCreatedName;
    /**
     * Visit a parse tree produced by `JavaParser.innerCreator`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitInnerCreator;
    /**
     * Visit a parse tree produced by `JavaParser.arrayCreatorRest`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitArrayCreatorRest;
    /**
     * Visit a parse tree produced by `JavaParser.classCreatorRest`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitClassCreatorRest;
    /**
     * Visit a parse tree produced by `JavaParser.explicitGenericInvocation`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitExplicitGenericInvocation;
    /**
     * Visit a parse tree produced by `JavaParser.typeArgumentsOrDiamond`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeArgumentsOrDiamond;
    /**
     * Visit a parse tree produced by `JavaParser.nonWildcardTypeArgumentsOrDiamond`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitNonWildcardTypeArgumentsOrDiamond;
    /**
     * Visit a parse tree produced by `JavaParser.nonWildcardTypeArguments`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitNonWildcardTypeArguments;
    /**
     * Visit a parse tree produced by `JavaParser.typeList`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeList;
    /**
     * Visit a parse tree produced by `JavaParser.typeType`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeType;
    /**
     * Visit a parse tree produced by `JavaParser.primitiveType`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitPrimitiveType;
    /**
     * Visit a parse tree produced by `JavaParser.typeArguments`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitTypeArguments;
    /**
     * Visit a parse tree produced by `JavaParser.superSuffix`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitSuperSuffix;
    /**
     * Visit a parse tree produced by `JavaParser.explicitGenericInvocationSuffix`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitExplicitGenericInvocationSuffix;
    /**
     * Visit a parse tree produced by `JavaParser.arguments`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitArguments;
}
//# sourceMappingURL=JavaParserVisitor.js.map