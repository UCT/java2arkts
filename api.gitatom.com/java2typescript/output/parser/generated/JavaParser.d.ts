import * as antlr from "antlr4ng";
import { Token } from "antlr4ng";
import { JavaParserListener } from "./JavaParserListener.js";
import { JavaParserVisitor } from "./JavaParserVisitor.js";
export declare class JavaParser extends antlr.Parser {
    static readonly ABSTRACT = 1;
    static readonly ASSERT = 2;
    static readonly BOOLEAN = 3;
    static readonly BREAK = 4;
    static readonly BYTE = 5;
    static readonly CASE = 6;
    static readonly CATCH = 7;
    static readonly CHAR = 8;
    static readonly CLASS = 9;
    static readonly CONST = 10;
    static readonly CONTINUE = 11;
    static readonly DEFAULT = 12;
    static readonly DO = 13;
    static readonly DOUBLE = 14;
    static readonly ELSE = 15;
    static readonly ENUM = 16;
    static readonly EXTENDS = 17;
    static readonly FINAL = 18;
    static readonly FINALLY = 19;
    static readonly FLOAT = 20;
    static readonly FOR = 21;
    static readonly IF = 22;
    static readonly GOTO = 23;
    static readonly IMPLEMENTS = 24;
    static readonly IMPORT = 25;
    static readonly INSTANCEOF = 26;
    static readonly INT = 27;
    static readonly INTERFACE = 28;
    static readonly LONG = 29;
    static readonly NATIVE = 30;
    static readonly NEW = 31;
    static readonly PACKAGE = 32;
    static readonly PRIVATE = 33;
    static readonly PROTECTED = 34;
    static readonly PUBLIC = 35;
    static readonly RETURN = 36;
    static readonly SHORT = 37;
    static readonly STATIC = 38;
    static readonly STRICTFP = 39;
    static readonly SUPER = 40;
    static readonly SWITCH = 41;
    static readonly SYNCHRONIZED = 42;
    static readonly THIS = 43;
    static readonly THROW = 44;
    static readonly THROWS = 45;
    static readonly TRANSIENT = 46;
    static readonly TRY = 47;
    static readonly VOID = 48;
    static readonly VOLATILE = 49;
    static readonly WHILE = 50;
    static readonly MODULE = 51;
    static readonly OPEN = 52;
    static readonly REQUIRES = 53;
    static readonly EXPORTS = 54;
    static readonly OPENS = 55;
    static readonly TO = 56;
    static readonly USES = 57;
    static readonly PROVIDES = 58;
    static readonly WITH = 59;
    static readonly TRANSITIVE = 60;
    static readonly VAR = 61;
    static readonly YIELD = 62;
    static readonly RECORD = 63;
    static readonly SEALED = 64;
    static readonly PERMITS = 65;
    static readonly NON_SEALED = 66;
    static readonly DECIMAL_LITERAL = 67;
    static readonly HEX_LITERAL = 68;
    static readonly OCT_LITERAL = 69;
    static readonly BINARY_LITERAL = 70;
    static readonly FLOAT_LITERAL = 71;
    static readonly HEX_FLOAT_LITERAL = 72;
    static readonly BOOL_LITERAL = 73;
    static readonly CHAR_LITERAL = 74;
    static readonly STRING_LITERAL = 75;
    static readonly TEXT_BLOCK = 76;
    static readonly NULL_LITERAL = 77;
    static readonly LPAREN = 78;
    static readonly RPAREN = 79;
    static readonly LBRACE = 80;
    static readonly RBRACE = 81;
    static readonly LBRACK = 82;
    static readonly RBRACK = 83;
    static readonly SEMI = 84;
    static readonly COMMA = 85;
    static readonly DOT = 86;
    static readonly ASSIGN = 87;
    static readonly GT = 88;
    static readonly LT = 89;
    static readonly BANG = 90;
    static readonly TILDE = 91;
    static readonly QUESTION = 92;
    static readonly COLON = 93;
    static readonly EQUAL = 94;
    static readonly LE = 95;
    static readonly GE = 96;
    static readonly NOTEQUAL = 97;
    static readonly AND = 98;
    static readonly OR = 99;
    static readonly INC = 100;
    static readonly DEC = 101;
    static readonly ADD = 102;
    static readonly SUB = 103;
    static readonly MUL = 104;
    static readonly DIV = 105;
    static readonly BITAND = 106;
    static readonly BITOR = 107;
    static readonly CARET = 108;
    static readonly MOD = 109;
    static readonly ADD_ASSIGN = 110;
    static readonly SUB_ASSIGN = 111;
    static readonly MUL_ASSIGN = 112;
    static readonly DIV_ASSIGN = 113;
    static readonly AND_ASSIGN = 114;
    static readonly OR_ASSIGN = 115;
    static readonly XOR_ASSIGN = 116;
    static readonly MOD_ASSIGN = 117;
    static readonly LSHIFT_ASSIGN = 118;
    static readonly RSHIFT_ASSIGN = 119;
    static readonly URSHIFT_ASSIGN = 120;
    static readonly ARROW = 121;
    static readonly COLONCOLON = 122;
    static readonly AT = 123;
    static readonly ELLIPSIS = 124;
    static readonly WS = 125;
    static readonly COMMENT = 126;
    static readonly LINE_COMMENT = 127;
    static readonly IDENTIFIER = 128;
    static readonly RULE_compilationUnit = 0;
    static readonly RULE_packageDeclaration = 1;
    static readonly RULE_importDeclaration = 2;
    static readonly RULE_typeDeclaration = 3;
    static readonly RULE_modifier = 4;
    static readonly RULE_classOrInterfaceModifier = 5;
    static readonly RULE_variableModifier = 6;
    static readonly RULE_classDeclaration = 7;
    static readonly RULE_typeParameters = 8;
    static readonly RULE_typeParameter = 9;
    static readonly RULE_typeBound = 10;
    static readonly RULE_enumDeclaration = 11;
    static readonly RULE_enumConstants = 12;
    static readonly RULE_enumConstant = 13;
    static readonly RULE_enumBodyDeclarations = 14;
    static readonly RULE_interfaceDeclaration = 15;
    static readonly RULE_classBody = 16;
    static readonly RULE_interfaceBody = 17;
    static readonly RULE_classBodyDeclaration = 18;
    static readonly RULE_memberDeclaration = 19;
    static readonly RULE_methodDeclaration = 20;
    static readonly RULE_methodBody = 21;
    static readonly RULE_typeTypeOrVoid = 22;
    static readonly RULE_genericMethodDeclaration = 23;
    static readonly RULE_genericConstructorDeclaration = 24;
    static readonly RULE_constructorDeclaration = 25;
    static readonly RULE_fieldDeclaration = 26;
    static readonly RULE_interfaceBodyDeclaration = 27;
    static readonly RULE_interfaceMemberDeclaration = 28;
    static readonly RULE_constDeclaration = 29;
    static readonly RULE_constantDeclarator = 30;
    static readonly RULE_interfaceMethodDeclaration = 31;
    static readonly RULE_interfaceMethodModifier = 32;
    static readonly RULE_genericInterfaceMethodDeclaration = 33;
    static readonly RULE_interfaceCommonBodyDeclaration = 34;
    static readonly RULE_variableDeclarators = 35;
    static readonly RULE_variableDeclarator = 36;
    static readonly RULE_variableDeclaratorId = 37;
    static readonly RULE_variableInitializer = 38;
    static readonly RULE_arrayInitializer = 39;
    static readonly RULE_classOrInterfaceType = 40;
    static readonly RULE_typeArgument = 41;
    static readonly RULE_qualifiedNameList = 42;
    static readonly RULE_formalParameters = 43;
    static readonly RULE_receiverParameter = 44;
    static readonly RULE_formalParameterList = 45;
    static readonly RULE_formalParameter = 46;
    static readonly RULE_lastFormalParameter = 47;
    static readonly RULE_lambdaLVTIList = 48;
    static readonly RULE_lambdaLVTIParameter = 49;
    static readonly RULE_qualifiedName = 50;
    static readonly RULE_literal = 51;
    static readonly RULE_integerLiteral = 52;
    static readonly RULE_floatLiteral = 53;
    static readonly RULE_altAnnotationQualifiedName = 54;
    static readonly RULE_annotation = 55;
    static readonly RULE_elementValuePairs = 56;
    static readonly RULE_elementValuePair = 57;
    static readonly RULE_elementValue = 58;
    static readonly RULE_elementValueArrayInitializer = 59;
    static readonly RULE_annotationTypeDeclaration = 60;
    static readonly RULE_annotationTypeBody = 61;
    static readonly RULE_annotationTypeElementDeclaration = 62;
    static readonly RULE_annotationTypeElementRest = 63;
    static readonly RULE_annotationMethodOrConstantRest = 64;
    static readonly RULE_annotationMethodRest = 65;
    static readonly RULE_annotationConstantRest = 66;
    static readonly RULE_defaultValue = 67;
    static readonly RULE_moduleDeclaration = 68;
    static readonly RULE_moduleBody = 69;
    static readonly RULE_moduleDirective = 70;
    static readonly RULE_requiresModifier = 71;
    static readonly RULE_recordDeclaration = 72;
    static readonly RULE_recordHeader = 73;
    static readonly RULE_recordComponentList = 74;
    static readonly RULE_recordComponent = 75;
    static readonly RULE_recordBody = 76;
    static readonly RULE_block = 77;
    static readonly RULE_blockStatement = 78;
    static readonly RULE_localVariableDeclaration = 79;
    static readonly RULE_identifier = 80;
    static readonly RULE_localTypeDeclaration = 81;
    static readonly RULE_statement = 82;
    static readonly RULE_catchClause = 83;
    static readonly RULE_catchType = 84;
    static readonly RULE_finallyBlock = 85;
    static readonly RULE_resourceSpecification = 86;
    static readonly RULE_resources = 87;
    static readonly RULE_resource = 88;
    static readonly RULE_switchBlockStatementGroup = 89;
    static readonly RULE_switchLabel = 90;
    static readonly RULE_forControl = 91;
    static readonly RULE_forInit = 92;
    static readonly RULE_enhancedForControl = 93;
    static readonly RULE_parExpression = 94;
    static readonly RULE_expressionList = 95;
    static readonly RULE_methodCall = 96;
    static readonly RULE_expression = 97;
    static readonly RULE_pattern = 98;
    static readonly RULE_lambdaExpression = 99;
    static readonly RULE_lambdaParameters = 100;
    static readonly RULE_lambdaBody = 101;
    static readonly RULE_primary = 102;
    static readonly RULE_switchExpression = 103;
    static readonly RULE_switchLabeledRule = 104;
    static readonly RULE_guardedPattern = 105;
    static readonly RULE_switchRuleOutcome = 106;
    static readonly RULE_classType = 107;
    static readonly RULE_creator = 108;
    static readonly RULE_createdName = 109;
    static readonly RULE_innerCreator = 110;
    static readonly RULE_arrayCreatorRest = 111;
    static readonly RULE_classCreatorRest = 112;
    static readonly RULE_explicitGenericInvocation = 113;
    static readonly RULE_typeArgumentsOrDiamond = 114;
    static readonly RULE_nonWildcardTypeArgumentsOrDiamond = 115;
    static readonly RULE_nonWildcardTypeArguments = 116;
    static readonly RULE_typeList = 117;
    static readonly RULE_typeType = 118;
    static readonly RULE_primitiveType = 119;
    static readonly RULE_typeArguments = 120;
    static readonly RULE_superSuffix = 121;
    static readonly RULE_explicitGenericInvocationSuffix = 122;
    static readonly RULE_arguments = 123;
    static readonly literalNames: (string | null)[];
    static readonly symbolicNames: (string | null)[];
    static readonly ruleNames: string[];
    get grammarFileName(): string;
    get literalNames(): (string | null)[];
    get symbolicNames(): (string | null)[];
    get ruleNames(): string[];
    get serializedATN(): number[];
    protected createFailedPredicateException(predicate?: string, message?: string): antlr.FailedPredicateException;
    constructor(input: antlr.TokenStream);
    compilationUnit(): CompilationUnitContext;
    packageDeclaration(): PackageDeclarationContext;
    importDeclaration(): ImportDeclarationContext;
    typeDeclaration(): TypeDeclarationContext;
    modifier(): ModifierContext;
    classOrInterfaceModifier(): ClassOrInterfaceModifierContext;
    variableModifier(): VariableModifierContext;
    classDeclaration(): ClassDeclarationContext;
    typeParameters(): TypeParametersContext;
    typeParameter(): TypeParameterContext;
    typeBound(): TypeBoundContext;
    enumDeclaration(): EnumDeclarationContext;
    enumConstants(): EnumConstantsContext;
    enumConstant(): EnumConstantContext;
    enumBodyDeclarations(): EnumBodyDeclarationsContext;
    interfaceDeclaration(): InterfaceDeclarationContext;
    classBody(): ClassBodyContext;
    interfaceBody(): InterfaceBodyContext;
    classBodyDeclaration(): ClassBodyDeclarationContext;
    memberDeclaration(): MemberDeclarationContext;
    methodDeclaration(): MethodDeclarationContext;
    methodBody(): MethodBodyContext;
    typeTypeOrVoid(): TypeTypeOrVoidContext;
    genericMethodDeclaration(): GenericMethodDeclarationContext;
    genericConstructorDeclaration(): GenericConstructorDeclarationContext;
    constructorDeclaration(): ConstructorDeclarationContext;
    fieldDeclaration(): FieldDeclarationContext;
    interfaceBodyDeclaration(): InterfaceBodyDeclarationContext;
    interfaceMemberDeclaration(): InterfaceMemberDeclarationContext;
    constDeclaration(): ConstDeclarationContext;
    constantDeclarator(): ConstantDeclaratorContext;
    interfaceMethodDeclaration(): InterfaceMethodDeclarationContext;
    interfaceMethodModifier(): InterfaceMethodModifierContext;
    genericInterfaceMethodDeclaration(): GenericInterfaceMethodDeclarationContext;
    interfaceCommonBodyDeclaration(): InterfaceCommonBodyDeclarationContext;
    variableDeclarators(): VariableDeclaratorsContext;
    variableDeclarator(): VariableDeclaratorContext;
    variableDeclaratorId(): VariableDeclaratorIdContext;
    variableInitializer(): VariableInitializerContext;
    arrayInitializer(): ArrayInitializerContext;
    classOrInterfaceType(): ClassOrInterfaceTypeContext;
    typeArgument(): TypeArgumentContext;
    qualifiedNameList(): QualifiedNameListContext;
    formalParameters(): FormalParametersContext;
    receiverParameter(): ReceiverParameterContext;
    formalParameterList(): FormalParameterListContext;
    formalParameter(): FormalParameterContext;
    lastFormalParameter(): LastFormalParameterContext;
    lambdaLVTIList(): LambdaLVTIListContext;
    lambdaLVTIParameter(): LambdaLVTIParameterContext;
    qualifiedName(): QualifiedNameContext;
    literal(): LiteralContext;
    integerLiteral(): IntegerLiteralContext;
    floatLiteral(): FloatLiteralContext;
    altAnnotationQualifiedName(): AltAnnotationQualifiedNameContext;
    annotation(): AnnotationContext;
    elementValuePairs(): ElementValuePairsContext;
    elementValuePair(): ElementValuePairContext;
    elementValue(): ElementValueContext;
    elementValueArrayInitializer(): ElementValueArrayInitializerContext;
    annotationTypeDeclaration(): AnnotationTypeDeclarationContext;
    annotationTypeBody(): AnnotationTypeBodyContext;
    annotationTypeElementDeclaration(): AnnotationTypeElementDeclarationContext;
    annotationTypeElementRest(): AnnotationTypeElementRestContext;
    annotationMethodOrConstantRest(): AnnotationMethodOrConstantRestContext;
    annotationMethodRest(): AnnotationMethodRestContext;
    annotationConstantRest(): AnnotationConstantRestContext;
    defaultValue(): DefaultValueContext;
    moduleDeclaration(): ModuleDeclarationContext;
    moduleBody(): ModuleBodyContext;
    moduleDirective(): ModuleDirectiveContext;
    requiresModifier(): RequiresModifierContext;
    recordDeclaration(): RecordDeclarationContext;
    recordHeader(): RecordHeaderContext;
    recordComponentList(): RecordComponentListContext;
    recordComponent(): RecordComponentContext;
    recordBody(): RecordBodyContext;
    block(): BlockContext;
    blockStatement(): BlockStatementContext;
    localVariableDeclaration(): LocalVariableDeclarationContext;
    identifier(): IdentifierContext;
    localTypeDeclaration(): LocalTypeDeclarationContext;
    statement(): StatementContext;
    catchClause(): CatchClauseContext;
    catchType(): CatchTypeContext;
    finallyBlock(): FinallyBlockContext;
    resourceSpecification(): ResourceSpecificationContext;
    resources(): ResourcesContext;
    resource(): ResourceContext;
    switchBlockStatementGroup(): SwitchBlockStatementGroupContext;
    switchLabel(): SwitchLabelContext;
    forControl(): ForControlContext;
    forInit(): ForInitContext;
    enhancedForControl(): EnhancedForControlContext;
    parExpression(): ParExpressionContext;
    expressionList(): ExpressionListContext;
    methodCall(): MethodCallContext;
    expression(): ExpressionContext;
    expression(_p: number): ExpressionContext;
    pattern(): PatternContext;
    lambdaExpression(): LambdaExpressionContext;
    lambdaParameters(): LambdaParametersContext;
    lambdaBody(): LambdaBodyContext;
    primary(): PrimaryContext;
    switchExpression(): SwitchExpressionContext;
    switchLabeledRule(): SwitchLabeledRuleContext;
    guardedPattern(): GuardedPatternContext;
    guardedPattern(_p: number): GuardedPatternContext;
    switchRuleOutcome(): SwitchRuleOutcomeContext;
    classType(): ClassTypeContext;
    creator(): CreatorContext;
    createdName(): CreatedNameContext;
    innerCreator(): InnerCreatorContext;
    arrayCreatorRest(): ArrayCreatorRestContext;
    classCreatorRest(): ClassCreatorRestContext;
    explicitGenericInvocation(): ExplicitGenericInvocationContext;
    typeArgumentsOrDiamond(): TypeArgumentsOrDiamondContext;
    nonWildcardTypeArgumentsOrDiamond(): NonWildcardTypeArgumentsOrDiamondContext;
    nonWildcardTypeArguments(): NonWildcardTypeArgumentsContext;
    typeList(): TypeListContext;
    typeType(): TypeTypeContext;
    primitiveType(): PrimitiveTypeContext;
    typeArguments(): TypeArgumentsContext;
    superSuffix(): SuperSuffixContext;
    explicitGenericInvocationSuffix(): ExplicitGenericInvocationSuffixContext;
    arguments(): ArgumentsContext;
    sempred(localContext: antlr.RuleContext, ruleIndex: number, predIndex: number): boolean;
    private expression_sempred;
    private guardedPattern_sempred;
    static readonly _serializedATN: number[];
    private static __ATN;
    static get _ATN(): antlr.ATN;
    private static readonly vocabulary;
    get vocabulary(): antlr.Vocabulary;
    private static readonly decisionsToDFA;
}
export declare class CompilationUnitContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    EOF(): antlr.TerminalNode;
    packageDeclaration(): PackageDeclarationContext | null;
    importDeclaration(): ImportDeclarationContext[];
    importDeclaration(i: number): ImportDeclarationContext | null;
    typeDeclaration(): TypeDeclarationContext[];
    typeDeclaration(i: number): TypeDeclarationContext | null;
    moduleDeclaration(): ModuleDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class PackageDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    PACKAGE(): antlr.TerminalNode;
    qualifiedName(): QualifiedNameContext;
    SEMI(): antlr.TerminalNode;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ImportDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    IMPORT(): antlr.TerminalNode;
    qualifiedName(): QualifiedNameContext;
    SEMI(): antlr.TerminalNode;
    STATIC(): antlr.TerminalNode | null;
    DOT(): antlr.TerminalNode | null;
    MUL(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    classDeclaration(): ClassDeclarationContext | null;
    enumDeclaration(): EnumDeclarationContext | null;
    interfaceDeclaration(): InterfaceDeclarationContext | null;
    annotationTypeDeclaration(): AnnotationTypeDeclarationContext | null;
    recordDeclaration(): RecordDeclarationContext | null;
    classOrInterfaceModifier(): ClassOrInterfaceModifierContext[];
    classOrInterfaceModifier(i: number): ClassOrInterfaceModifierContext | null;
    SEMI(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ModifierContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    classOrInterfaceModifier(): ClassOrInterfaceModifierContext | null;
    NATIVE(): antlr.TerminalNode | null;
    SYNCHRONIZED(): antlr.TerminalNode | null;
    TRANSIENT(): antlr.TerminalNode | null;
    VOLATILE(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ClassOrInterfaceModifierContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    annotation(): AnnotationContext | null;
    PUBLIC(): antlr.TerminalNode | null;
    PROTECTED(): antlr.TerminalNode | null;
    PRIVATE(): antlr.TerminalNode | null;
    STATIC(): antlr.TerminalNode | null;
    ABSTRACT(): antlr.TerminalNode | null;
    FINAL(): antlr.TerminalNode | null;
    STRICTFP(): antlr.TerminalNode | null;
    SEALED(): antlr.TerminalNode | null;
    NON_SEALED(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class VariableModifierContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    FINAL(): antlr.TerminalNode | null;
    annotation(): AnnotationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ClassDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    CLASS(): antlr.TerminalNode;
    identifier(): IdentifierContext;
    classBody(): ClassBodyContext;
    typeParameters(): TypeParametersContext | null;
    EXTENDS(): antlr.TerminalNode | null;
    typeType(): TypeTypeContext | null;
    IMPLEMENTS(): antlr.TerminalNode | null;
    typeList(): TypeListContext[];
    typeList(i: number): TypeListContext | null;
    PERMITS(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeParametersContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LT(): antlr.TerminalNode;
    typeParameter(): TypeParameterContext[];
    typeParameter(i: number): TypeParameterContext | null;
    GT(): antlr.TerminalNode;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeParameterContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    EXTENDS(): antlr.TerminalNode | null;
    typeBound(): TypeBoundContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeBoundContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext[];
    typeType(i: number): TypeTypeContext | null;
    BITAND(): antlr.TerminalNode[];
    BITAND(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class EnumDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    ENUM(): antlr.TerminalNode;
    identifier(): IdentifierContext;
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    IMPLEMENTS(): antlr.TerminalNode | null;
    typeList(): TypeListContext | null;
    enumConstants(): EnumConstantsContext | null;
    COMMA(): antlr.TerminalNode | null;
    enumBodyDeclarations(): EnumBodyDeclarationsContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class EnumConstantsContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    enumConstant(): EnumConstantContext[];
    enumConstant(i: number): EnumConstantContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class EnumConstantContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    arguments(): ArgumentsContext | null;
    classBody(): ClassBodyContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class EnumBodyDeclarationsContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    SEMI(): antlr.TerminalNode;
    classBodyDeclaration(): ClassBodyDeclarationContext[];
    classBodyDeclaration(i: number): ClassBodyDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class InterfaceDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    INTERFACE(): antlr.TerminalNode;
    identifier(): IdentifierContext;
    interfaceBody(): InterfaceBodyContext;
    typeParameters(): TypeParametersContext | null;
    EXTENDS(): antlr.TerminalNode | null;
    typeList(): TypeListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ClassBodyContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    classBodyDeclaration(): ClassBodyDeclarationContext[];
    classBodyDeclaration(i: number): ClassBodyDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class InterfaceBodyContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    interfaceBodyDeclaration(): InterfaceBodyDeclarationContext[];
    interfaceBodyDeclaration(i: number): InterfaceBodyDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ClassBodyDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    SEMI(): antlr.TerminalNode | null;
    block(): BlockContext | null;
    STATIC(): antlr.TerminalNode | null;
    memberDeclaration(): MemberDeclarationContext | null;
    modifier(): ModifierContext[];
    modifier(i: number): ModifierContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class MemberDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    methodDeclaration(): MethodDeclarationContext | null;
    genericMethodDeclaration(): GenericMethodDeclarationContext | null;
    fieldDeclaration(): FieldDeclarationContext | null;
    constructorDeclaration(): ConstructorDeclarationContext | null;
    genericConstructorDeclaration(): GenericConstructorDeclarationContext | null;
    interfaceDeclaration(): InterfaceDeclarationContext | null;
    annotationTypeDeclaration(): AnnotationTypeDeclarationContext | null;
    classDeclaration(): ClassDeclarationContext | null;
    enumDeclaration(): EnumDeclarationContext | null;
    recordDeclaration(): RecordDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class MethodDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeTypeOrVoid(): TypeTypeOrVoidContext;
    identifier(): IdentifierContext;
    formalParameters(): FormalParametersContext;
    methodBody(): MethodBodyContext;
    LBRACK(): antlr.TerminalNode[];
    LBRACK(i: number): antlr.TerminalNode | null;
    RBRACK(): antlr.TerminalNode[];
    RBRACK(i: number): antlr.TerminalNode | null;
    THROWS(): antlr.TerminalNode | null;
    qualifiedNameList(): QualifiedNameListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class MethodBodyContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    block(): BlockContext | null;
    SEMI(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeTypeOrVoidContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext | null;
    VOID(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class GenericMethodDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeParameters(): TypeParametersContext;
    methodDeclaration(): MethodDeclarationContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class GenericConstructorDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeParameters(): TypeParametersContext;
    constructorDeclaration(): ConstructorDeclarationContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ConstructorDeclarationContext extends antlr.ParserRuleContext {
    _constructorBody?: BlockContext;
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext;
    formalParameters(): FormalParametersContext;
    block(): BlockContext;
    THROWS(): antlr.TerminalNode | null;
    qualifiedNameList(): QualifiedNameListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class FieldDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext;
    variableDeclarators(): VariableDeclaratorsContext;
    SEMI(): antlr.TerminalNode;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class InterfaceBodyDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    interfaceMemberDeclaration(): InterfaceMemberDeclarationContext | null;
    modifier(): ModifierContext[];
    modifier(i: number): ModifierContext | null;
    SEMI(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class InterfaceMemberDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    constDeclaration(): ConstDeclarationContext | null;
    interfaceMethodDeclaration(): InterfaceMethodDeclarationContext | null;
    genericInterfaceMethodDeclaration(): GenericInterfaceMethodDeclarationContext | null;
    interfaceDeclaration(): InterfaceDeclarationContext | null;
    annotationTypeDeclaration(): AnnotationTypeDeclarationContext | null;
    classDeclaration(): ClassDeclarationContext | null;
    enumDeclaration(): EnumDeclarationContext | null;
    recordDeclaration(): RecordDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ConstDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext;
    constantDeclarator(): ConstantDeclaratorContext[];
    constantDeclarator(i: number): ConstantDeclaratorContext | null;
    SEMI(): antlr.TerminalNode;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ConstantDeclaratorContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext;
    ASSIGN(): antlr.TerminalNode;
    variableInitializer(): VariableInitializerContext;
    LBRACK(): antlr.TerminalNode[];
    LBRACK(i: number): antlr.TerminalNode | null;
    RBRACK(): antlr.TerminalNode[];
    RBRACK(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class InterfaceMethodDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    interfaceCommonBodyDeclaration(): InterfaceCommonBodyDeclarationContext;
    interfaceMethodModifier(): InterfaceMethodModifierContext[];
    interfaceMethodModifier(i: number): InterfaceMethodModifierContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class InterfaceMethodModifierContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    annotation(): AnnotationContext | null;
    PUBLIC(): antlr.TerminalNode | null;
    ABSTRACT(): antlr.TerminalNode | null;
    DEFAULT(): antlr.TerminalNode | null;
    STATIC(): antlr.TerminalNode | null;
    STRICTFP(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class GenericInterfaceMethodDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeParameters(): TypeParametersContext;
    interfaceCommonBodyDeclaration(): InterfaceCommonBodyDeclarationContext;
    interfaceMethodModifier(): InterfaceMethodModifierContext[];
    interfaceMethodModifier(i: number): InterfaceMethodModifierContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class InterfaceCommonBodyDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeTypeOrVoid(): TypeTypeOrVoidContext;
    identifier(): IdentifierContext;
    formalParameters(): FormalParametersContext;
    methodBody(): MethodBodyContext;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    LBRACK(): antlr.TerminalNode[];
    LBRACK(i: number): antlr.TerminalNode | null;
    RBRACK(): antlr.TerminalNode[];
    RBRACK(i: number): antlr.TerminalNode | null;
    THROWS(): antlr.TerminalNode | null;
    qualifiedNameList(): QualifiedNameListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class VariableDeclaratorsContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    variableDeclarator(): VariableDeclaratorContext[];
    variableDeclarator(i: number): VariableDeclaratorContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class VariableDeclaratorContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    variableDeclaratorId(): VariableDeclaratorIdContext;
    ASSIGN(): antlr.TerminalNode | null;
    variableInitializer(): VariableInitializerContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class VariableDeclaratorIdContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext;
    LBRACK(): antlr.TerminalNode[];
    LBRACK(i: number): antlr.TerminalNode | null;
    RBRACK(): antlr.TerminalNode[];
    RBRACK(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class VariableInitializerContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    arrayInitializer(): ArrayInitializerContext | null;
    expression(): ExpressionContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ArrayInitializerContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    variableInitializer(): VariableInitializerContext[];
    variableInitializer(i: number): VariableInitializerContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ClassOrInterfaceTypeContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext[];
    identifier(i: number): IdentifierContext | null;
    typeArguments(): TypeArgumentsContext[];
    typeArguments(i: number): TypeArgumentsContext | null;
    DOT(): antlr.TerminalNode[];
    DOT(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeArgumentContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext | null;
    QUESTION(): antlr.TerminalNode | null;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    EXTENDS(): antlr.TerminalNode | null;
    SUPER(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class QualifiedNameListContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    qualifiedName(): QualifiedNameContext[];
    qualifiedName(i: number): QualifiedNameContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class FormalParametersContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LPAREN(): antlr.TerminalNode;
    RPAREN(): antlr.TerminalNode;
    receiverParameter(): ReceiverParameterContext | null;
    COMMA(): antlr.TerminalNode | null;
    formalParameterList(): FormalParameterListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ReceiverParameterContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext;
    THIS(): antlr.TerminalNode;
    identifier(): IdentifierContext[];
    identifier(i: number): IdentifierContext | null;
    DOT(): antlr.TerminalNode[];
    DOT(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class FormalParameterListContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    formalParameter(): FormalParameterContext[];
    formalParameter(i: number): FormalParameterContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    lastFormalParameter(): LastFormalParameterContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class FormalParameterContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext;
    variableDeclaratorId(): VariableDeclaratorIdContext;
    variableModifier(): VariableModifierContext[];
    variableModifier(i: number): VariableModifierContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class LastFormalParameterContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext;
    ELLIPSIS(): antlr.TerminalNode;
    variableDeclaratorId(): VariableDeclaratorIdContext;
    variableModifier(): VariableModifierContext[];
    variableModifier(i: number): VariableModifierContext | null;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class LambdaLVTIListContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    lambdaLVTIParameter(): LambdaLVTIParameterContext[];
    lambdaLVTIParameter(i: number): LambdaLVTIParameterContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class LambdaLVTIParameterContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    VAR(): antlr.TerminalNode;
    identifier(): IdentifierContext;
    variableModifier(): VariableModifierContext[];
    variableModifier(i: number): VariableModifierContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class QualifiedNameContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext[];
    identifier(i: number): IdentifierContext | null;
    DOT(): antlr.TerminalNode[];
    DOT(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class LiteralContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    integerLiteral(): IntegerLiteralContext | null;
    floatLiteral(): FloatLiteralContext | null;
    CHAR_LITERAL(): antlr.TerminalNode | null;
    STRING_LITERAL(): antlr.TerminalNode | null;
    BOOL_LITERAL(): antlr.TerminalNode | null;
    NULL_LITERAL(): antlr.TerminalNode | null;
    TEXT_BLOCK(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class IntegerLiteralContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    DECIMAL_LITERAL(): antlr.TerminalNode | null;
    HEX_LITERAL(): antlr.TerminalNode | null;
    OCT_LITERAL(): antlr.TerminalNode | null;
    BINARY_LITERAL(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class FloatLiteralContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    FLOAT_LITERAL(): antlr.TerminalNode | null;
    HEX_FLOAT_LITERAL(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class AltAnnotationQualifiedNameContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    AT(): antlr.TerminalNode;
    identifier(): IdentifierContext[];
    identifier(i: number): IdentifierContext | null;
    DOT(): antlr.TerminalNode[];
    DOT(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class AnnotationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    AT(): antlr.TerminalNode | null;
    qualifiedName(): QualifiedNameContext | null;
    altAnnotationQualifiedName(): AltAnnotationQualifiedNameContext | null;
    LPAREN(): antlr.TerminalNode | null;
    RPAREN(): antlr.TerminalNode | null;
    elementValuePairs(): ElementValuePairsContext | null;
    elementValue(): ElementValueContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ElementValuePairsContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    elementValuePair(): ElementValuePairContext[];
    elementValuePair(i: number): ElementValuePairContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ElementValuePairContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext;
    ASSIGN(): antlr.TerminalNode;
    elementValue(): ElementValueContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ElementValueContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    expression(): ExpressionContext | null;
    annotation(): AnnotationContext | null;
    elementValueArrayInitializer(): ElementValueArrayInitializerContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ElementValueArrayInitializerContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    elementValue(): ElementValueContext[];
    elementValue(i: number): ElementValueContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class AnnotationTypeDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    AT(): antlr.TerminalNode;
    INTERFACE(): antlr.TerminalNode;
    identifier(): IdentifierContext;
    annotationTypeBody(): AnnotationTypeBodyContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class AnnotationTypeBodyContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    annotationTypeElementDeclaration(): AnnotationTypeElementDeclarationContext[];
    annotationTypeElementDeclaration(i: number): AnnotationTypeElementDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class AnnotationTypeElementDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    annotationTypeElementRest(): AnnotationTypeElementRestContext | null;
    modifier(): ModifierContext[];
    modifier(i: number): ModifierContext | null;
    SEMI(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class AnnotationTypeElementRestContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext | null;
    annotationMethodOrConstantRest(): AnnotationMethodOrConstantRestContext | null;
    SEMI(): antlr.TerminalNode | null;
    classDeclaration(): ClassDeclarationContext | null;
    interfaceDeclaration(): InterfaceDeclarationContext | null;
    enumDeclaration(): EnumDeclarationContext | null;
    annotationTypeDeclaration(): AnnotationTypeDeclarationContext | null;
    recordDeclaration(): RecordDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class AnnotationMethodOrConstantRestContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    annotationMethodRest(): AnnotationMethodRestContext | null;
    annotationConstantRest(): AnnotationConstantRestContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class AnnotationMethodRestContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext;
    LPAREN(): antlr.TerminalNode;
    RPAREN(): antlr.TerminalNode;
    defaultValue(): DefaultValueContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class AnnotationConstantRestContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    variableDeclarators(): VariableDeclaratorsContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class DefaultValueContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    DEFAULT(): antlr.TerminalNode;
    elementValue(): ElementValueContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ModuleDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    MODULE(): antlr.TerminalNode;
    qualifiedName(): QualifiedNameContext;
    moduleBody(): ModuleBodyContext;
    OPEN(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ModuleBodyContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    moduleDirective(): ModuleDirectiveContext[];
    moduleDirective(i: number): ModuleDirectiveContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ModuleDirectiveContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    REQUIRES(): antlr.TerminalNode | null;
    qualifiedName(): QualifiedNameContext[];
    qualifiedName(i: number): QualifiedNameContext | null;
    SEMI(): antlr.TerminalNode;
    requiresModifier(): RequiresModifierContext[];
    requiresModifier(i: number): RequiresModifierContext | null;
    EXPORTS(): antlr.TerminalNode | null;
    TO(): antlr.TerminalNode | null;
    OPENS(): antlr.TerminalNode | null;
    USES(): antlr.TerminalNode | null;
    PROVIDES(): antlr.TerminalNode | null;
    WITH(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class RequiresModifierContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    TRANSITIVE(): antlr.TerminalNode | null;
    STATIC(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class RecordDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    RECORD(): antlr.TerminalNode;
    identifier(): IdentifierContext;
    recordHeader(): RecordHeaderContext;
    recordBody(): RecordBodyContext;
    typeParameters(): TypeParametersContext | null;
    IMPLEMENTS(): antlr.TerminalNode | null;
    typeList(): TypeListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class RecordHeaderContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LPAREN(): antlr.TerminalNode;
    RPAREN(): antlr.TerminalNode;
    recordComponentList(): RecordComponentListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class RecordComponentListContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    recordComponent(): RecordComponentContext[];
    recordComponent(i: number): RecordComponentContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class RecordComponentContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext;
    identifier(): IdentifierContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class RecordBodyContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    classBodyDeclaration(): ClassBodyDeclarationContext[];
    classBodyDeclaration(i: number): ClassBodyDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class BlockContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    blockStatement(): BlockStatementContext[];
    blockStatement(i: number): BlockStatementContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class BlockStatementContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    localVariableDeclaration(): LocalVariableDeclarationContext | null;
    SEMI(): antlr.TerminalNode | null;
    statement(): StatementContext | null;
    localTypeDeclaration(): LocalTypeDeclarationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class LocalVariableDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext;
    variableDeclarators(): VariableDeclaratorsContext;
    variableModifier(): VariableModifierContext[];
    variableModifier(i: number): VariableModifierContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class IdentifierContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    IDENTIFIER(): antlr.TerminalNode | null;
    MODULE(): antlr.TerminalNode | null;
    OPEN(): antlr.TerminalNode | null;
    REQUIRES(): antlr.TerminalNode | null;
    EXPORTS(): antlr.TerminalNode | null;
    OPENS(): antlr.TerminalNode | null;
    TO(): antlr.TerminalNode | null;
    USES(): antlr.TerminalNode | null;
    PROVIDES(): antlr.TerminalNode | null;
    WITH(): antlr.TerminalNode | null;
    TRANSITIVE(): antlr.TerminalNode | null;
    YIELD(): antlr.TerminalNode | null;
    SEALED(): antlr.TerminalNode | null;
    PERMITS(): antlr.TerminalNode | null;
    RECORD(): antlr.TerminalNode | null;
    VAR(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class LocalTypeDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    classDeclaration(): ClassDeclarationContext | null;
    interfaceDeclaration(): InterfaceDeclarationContext | null;
    recordDeclaration(): RecordDeclarationContext | null;
    classOrInterfaceModifier(): ClassOrInterfaceModifierContext[];
    classOrInterfaceModifier(i: number): ClassOrInterfaceModifierContext | null;
    SEMI(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class StatementContext extends antlr.ParserRuleContext {
    _blockLabel?: BlockContext;
    _statementExpression?: ExpressionContext;
    _identifierLabel?: IdentifierContext;
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    block(): BlockContext | null;
    ASSERT(): antlr.TerminalNode | null;
    expression(): ExpressionContext[];
    expression(i: number): ExpressionContext | null;
    SEMI(): antlr.TerminalNode | null;
    COLON(): antlr.TerminalNode | null;
    IF(): antlr.TerminalNode | null;
    parExpression(): ParExpressionContext | null;
    statement(): StatementContext[];
    statement(i: number): StatementContext | null;
    ELSE(): antlr.TerminalNode | null;
    FOR(): antlr.TerminalNode | null;
    LPAREN(): antlr.TerminalNode | null;
    forControl(): ForControlContext | null;
    RPAREN(): antlr.TerminalNode | null;
    WHILE(): antlr.TerminalNode | null;
    DO(): antlr.TerminalNode | null;
    TRY(): antlr.TerminalNode | null;
    finallyBlock(): FinallyBlockContext | null;
    catchClause(): CatchClauseContext[];
    catchClause(i: number): CatchClauseContext | null;
    resourceSpecification(): ResourceSpecificationContext | null;
    SWITCH(): antlr.TerminalNode | null;
    LBRACE(): antlr.TerminalNode | null;
    RBRACE(): antlr.TerminalNode | null;
    switchBlockStatementGroup(): SwitchBlockStatementGroupContext[];
    switchBlockStatementGroup(i: number): SwitchBlockStatementGroupContext | null;
    switchLabel(): SwitchLabelContext[];
    switchLabel(i: number): SwitchLabelContext | null;
    SYNCHRONIZED(): antlr.TerminalNode | null;
    RETURN(): antlr.TerminalNode | null;
    THROW(): antlr.TerminalNode | null;
    BREAK(): antlr.TerminalNode | null;
    identifier(): IdentifierContext | null;
    CONTINUE(): antlr.TerminalNode | null;
    YIELD(): antlr.TerminalNode | null;
    switchExpression(): SwitchExpressionContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class CatchClauseContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    CATCH(): antlr.TerminalNode;
    LPAREN(): antlr.TerminalNode;
    catchType(): CatchTypeContext;
    identifier(): IdentifierContext;
    RPAREN(): antlr.TerminalNode;
    block(): BlockContext;
    variableModifier(): VariableModifierContext[];
    variableModifier(i: number): VariableModifierContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class CatchTypeContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    qualifiedName(): QualifiedNameContext[];
    qualifiedName(i: number): QualifiedNameContext | null;
    BITOR(): antlr.TerminalNode[];
    BITOR(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class FinallyBlockContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    FINALLY(): antlr.TerminalNode;
    block(): BlockContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ResourceSpecificationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LPAREN(): antlr.TerminalNode;
    resources(): ResourcesContext;
    RPAREN(): antlr.TerminalNode;
    SEMI(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ResourcesContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    resource(): ResourceContext[];
    resource(i: number): ResourceContext | null;
    SEMI(): antlr.TerminalNode[];
    SEMI(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ResourceContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    ASSIGN(): antlr.TerminalNode | null;
    expression(): ExpressionContext | null;
    classOrInterfaceType(): ClassOrInterfaceTypeContext | null;
    variableDeclaratorId(): VariableDeclaratorIdContext | null;
    VAR(): antlr.TerminalNode | null;
    identifier(): IdentifierContext | null;
    variableModifier(): VariableModifierContext[];
    variableModifier(i: number): VariableModifierContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class SwitchBlockStatementGroupContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    switchLabel(): SwitchLabelContext[];
    switchLabel(i: number): SwitchLabelContext | null;
    blockStatement(): BlockStatementContext[];
    blockStatement(i: number): BlockStatementContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class SwitchLabelContext extends antlr.ParserRuleContext {
    _constantExpression?: ExpressionContext;
    _enumConstantName?: Token | null;
    _varName?: IdentifierContext;
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    CASE(): antlr.TerminalNode | null;
    COLON(): antlr.TerminalNode;
    typeType(): TypeTypeContext | null;
    expression(): ExpressionContext | null;
    IDENTIFIER(): antlr.TerminalNode | null;
    identifier(): IdentifierContext | null;
    DEFAULT(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ForControlContext extends antlr.ParserRuleContext {
    _forUpdate?: ExpressionListContext;
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    enhancedForControl(): EnhancedForControlContext | null;
    SEMI(): antlr.TerminalNode[];
    SEMI(i: number): antlr.TerminalNode | null;
    forInit(): ForInitContext | null;
    expression(): ExpressionContext | null;
    expressionList(): ExpressionListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ForInitContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    localVariableDeclaration(): LocalVariableDeclarationContext | null;
    expressionList(): ExpressionListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class EnhancedForControlContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    variableDeclaratorId(): VariableDeclaratorIdContext;
    COLON(): antlr.TerminalNode;
    expression(): ExpressionContext;
    typeType(): TypeTypeContext | null;
    VAR(): antlr.TerminalNode | null;
    variableModifier(): VariableModifierContext[];
    variableModifier(i: number): VariableModifierContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ParExpressionContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LPAREN(): antlr.TerminalNode;
    expression(): ExpressionContext;
    RPAREN(): antlr.TerminalNode;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ExpressionListContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    expression(): ExpressionContext[];
    expression(i: number): ExpressionContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class MethodCallContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext | null;
    LPAREN(): antlr.TerminalNode;
    RPAREN(): antlr.TerminalNode;
    expressionList(): ExpressionListContext | null;
    THIS(): antlr.TerminalNode | null;
    SUPER(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ExpressionContext extends antlr.ParserRuleContext {
    _prefix: Token | null;
    _bop: Token | null;
    _postfix: Token | null;
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    primary(): PrimaryContext | null;
    methodCall(): MethodCallContext | null;
    NEW(): antlr.TerminalNode | null;
    creator(): CreatorContext | null;
    LPAREN(): antlr.TerminalNode | null;
    typeType(): TypeTypeContext[];
    typeType(i: number): TypeTypeContext | null;
    RPAREN(): antlr.TerminalNode | null;
    expression(): ExpressionContext[];
    expression(i: number): ExpressionContext | null;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    BITAND(): antlr.TerminalNode[];
    BITAND(i: number): antlr.TerminalNode | null;
    ADD(): antlr.TerminalNode | null;
    SUB(): antlr.TerminalNode | null;
    INC(): antlr.TerminalNode | null;
    DEC(): antlr.TerminalNode | null;
    TILDE(): antlr.TerminalNode | null;
    BANG(): antlr.TerminalNode | null;
    lambdaExpression(): LambdaExpressionContext | null;
    switchExpression(): SwitchExpressionContext | null;
    COLONCOLON(): antlr.TerminalNode | null;
    identifier(): IdentifierContext | null;
    typeArguments(): TypeArgumentsContext | null;
    classType(): ClassTypeContext | null;
    MUL(): antlr.TerminalNode | null;
    DIV(): antlr.TerminalNode | null;
    MOD(): antlr.TerminalNode | null;
    LT(): antlr.TerminalNode[];
    LT(i: number): antlr.TerminalNode | null;
    GT(): antlr.TerminalNode[];
    GT(i: number): antlr.TerminalNode | null;
    LE(): antlr.TerminalNode | null;
    GE(): antlr.TerminalNode | null;
    EQUAL(): antlr.TerminalNode | null;
    NOTEQUAL(): antlr.TerminalNode | null;
    CARET(): antlr.TerminalNode | null;
    BITOR(): antlr.TerminalNode | null;
    AND(): antlr.TerminalNode | null;
    OR(): antlr.TerminalNode | null;
    COLON(): antlr.TerminalNode | null;
    QUESTION(): antlr.TerminalNode | null;
    ASSIGN(): antlr.TerminalNode | null;
    ADD_ASSIGN(): antlr.TerminalNode | null;
    SUB_ASSIGN(): antlr.TerminalNode | null;
    MUL_ASSIGN(): antlr.TerminalNode | null;
    DIV_ASSIGN(): antlr.TerminalNode | null;
    AND_ASSIGN(): antlr.TerminalNode | null;
    OR_ASSIGN(): antlr.TerminalNode | null;
    XOR_ASSIGN(): antlr.TerminalNode | null;
    RSHIFT_ASSIGN(): antlr.TerminalNode | null;
    URSHIFT_ASSIGN(): antlr.TerminalNode | null;
    LSHIFT_ASSIGN(): antlr.TerminalNode | null;
    MOD_ASSIGN(): antlr.TerminalNode | null;
    DOT(): antlr.TerminalNode | null;
    THIS(): antlr.TerminalNode | null;
    innerCreator(): InnerCreatorContext | null;
    SUPER(): antlr.TerminalNode | null;
    superSuffix(): SuperSuffixContext | null;
    explicitGenericInvocation(): ExplicitGenericInvocationContext | null;
    nonWildcardTypeArguments(): NonWildcardTypeArgumentsContext | null;
    LBRACK(): antlr.TerminalNode | null;
    RBRACK(): antlr.TerminalNode | null;
    INSTANCEOF(): antlr.TerminalNode | null;
    pattern(): PatternContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class PatternContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext;
    identifier(): IdentifierContext;
    variableModifier(): VariableModifierContext[];
    variableModifier(i: number): VariableModifierContext | null;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class LambdaExpressionContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    lambdaParameters(): LambdaParametersContext;
    ARROW(): antlr.TerminalNode;
    lambdaBody(): LambdaBodyContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class LambdaParametersContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext[];
    identifier(i: number): IdentifierContext | null;
    LPAREN(): antlr.TerminalNode | null;
    RPAREN(): antlr.TerminalNode | null;
    formalParameterList(): FormalParameterListContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    lambdaLVTIList(): LambdaLVTIListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class LambdaBodyContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    expression(): ExpressionContext | null;
    block(): BlockContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class PrimaryContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LPAREN(): antlr.TerminalNode | null;
    expression(): ExpressionContext | null;
    RPAREN(): antlr.TerminalNode | null;
    THIS(): antlr.TerminalNode | null;
    SUPER(): antlr.TerminalNode | null;
    literal(): LiteralContext | null;
    identifier(): IdentifierContext | null;
    typeTypeOrVoid(): TypeTypeOrVoidContext | null;
    DOT(): antlr.TerminalNode | null;
    CLASS(): antlr.TerminalNode | null;
    nonWildcardTypeArguments(): NonWildcardTypeArgumentsContext | null;
    explicitGenericInvocationSuffix(): ExplicitGenericInvocationSuffixContext | null;
    arguments(): ArgumentsContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class SwitchExpressionContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    SWITCH(): antlr.TerminalNode;
    parExpression(): ParExpressionContext;
    LBRACE(): antlr.TerminalNode;
    RBRACE(): antlr.TerminalNode;
    switchLabeledRule(): SwitchLabeledRuleContext[];
    switchLabeledRule(i: number): SwitchLabeledRuleContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class SwitchLabeledRuleContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    CASE(): antlr.TerminalNode | null;
    switchRuleOutcome(): SwitchRuleOutcomeContext;
    ARROW(): antlr.TerminalNode | null;
    COLON(): antlr.TerminalNode | null;
    expressionList(): ExpressionListContext | null;
    NULL_LITERAL(): antlr.TerminalNode | null;
    guardedPattern(): GuardedPatternContext | null;
    DEFAULT(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class GuardedPatternContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LPAREN(): antlr.TerminalNode | null;
    guardedPattern(): GuardedPatternContext | null;
    RPAREN(): antlr.TerminalNode | null;
    typeType(): TypeTypeContext | null;
    identifier(): IdentifierContext | null;
    variableModifier(): VariableModifierContext[];
    variableModifier(i: number): VariableModifierContext | null;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    AND(): antlr.TerminalNode[];
    AND(i: number): antlr.TerminalNode | null;
    expression(): ExpressionContext[];
    expression(i: number): ExpressionContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class SwitchRuleOutcomeContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    block(): BlockContext | null;
    blockStatement(): BlockStatementContext[];
    blockStatement(i: number): BlockStatementContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ClassTypeContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext;
    classOrInterfaceType(): ClassOrInterfaceTypeContext | null;
    DOT(): antlr.TerminalNode | null;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    typeArguments(): TypeArgumentsContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class CreatorContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    nonWildcardTypeArguments(): NonWildcardTypeArgumentsContext | null;
    createdName(): CreatedNameContext;
    classCreatorRest(): ClassCreatorRestContext | null;
    arrayCreatorRest(): ArrayCreatorRestContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class CreatedNameContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext[];
    identifier(i: number): IdentifierContext | null;
    typeArgumentsOrDiamond(): TypeArgumentsOrDiamondContext[];
    typeArgumentsOrDiamond(i: number): TypeArgumentsOrDiamondContext | null;
    DOT(): antlr.TerminalNode[];
    DOT(i: number): antlr.TerminalNode | null;
    primitiveType(): PrimitiveTypeContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class InnerCreatorContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    identifier(): IdentifierContext;
    classCreatorRest(): ClassCreatorRestContext;
    nonWildcardTypeArgumentsOrDiamond(): NonWildcardTypeArgumentsOrDiamondContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ArrayCreatorRestContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LBRACK(): antlr.TerminalNode[];
    LBRACK(i: number): antlr.TerminalNode | null;
    RBRACK(): antlr.TerminalNode[];
    RBRACK(i: number): antlr.TerminalNode | null;
    arrayInitializer(): ArrayInitializerContext | null;
    expression(): ExpressionContext[];
    expression(i: number): ExpressionContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ClassCreatorRestContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    arguments(): ArgumentsContext;
    classBody(): ClassBodyContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ExplicitGenericInvocationContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    nonWildcardTypeArguments(): NonWildcardTypeArgumentsContext;
    explicitGenericInvocationSuffix(): ExplicitGenericInvocationSuffixContext;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeArgumentsOrDiamondContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LT(): antlr.TerminalNode | null;
    GT(): antlr.TerminalNode | null;
    typeArguments(): TypeArgumentsContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class NonWildcardTypeArgumentsOrDiamondContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LT(): antlr.TerminalNode | null;
    GT(): antlr.TerminalNode | null;
    nonWildcardTypeArguments(): NonWildcardTypeArgumentsContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class NonWildcardTypeArgumentsContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LT(): antlr.TerminalNode;
    typeList(): TypeListContext;
    GT(): antlr.TerminalNode;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeListContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    typeType(): TypeTypeContext[];
    typeType(i: number): TypeTypeContext | null;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeTypeContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    classOrInterfaceType(): ClassOrInterfaceTypeContext | null;
    primitiveType(): PrimitiveTypeContext | null;
    annotation(): AnnotationContext[];
    annotation(i: number): AnnotationContext | null;
    LBRACK(): antlr.TerminalNode[];
    LBRACK(i: number): antlr.TerminalNode | null;
    RBRACK(): antlr.TerminalNode[];
    RBRACK(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class PrimitiveTypeContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    BOOLEAN(): antlr.TerminalNode | null;
    CHAR(): antlr.TerminalNode | null;
    BYTE(): antlr.TerminalNode | null;
    SHORT(): antlr.TerminalNode | null;
    INT(): antlr.TerminalNode | null;
    LONG(): antlr.TerminalNode | null;
    FLOAT(): antlr.TerminalNode | null;
    DOUBLE(): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class TypeArgumentsContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LT(): antlr.TerminalNode;
    typeArgument(): TypeArgumentContext[];
    typeArgument(i: number): TypeArgumentContext | null;
    GT(): antlr.TerminalNode;
    COMMA(): antlr.TerminalNode[];
    COMMA(i: number): antlr.TerminalNode | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class SuperSuffixContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    arguments(): ArgumentsContext | null;
    DOT(): antlr.TerminalNode | null;
    identifier(): IdentifierContext | null;
    typeArguments(): TypeArgumentsContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ExplicitGenericInvocationSuffixContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    SUPER(): antlr.TerminalNode | null;
    superSuffix(): SuperSuffixContext | null;
    identifier(): IdentifierContext | null;
    arguments(): ArgumentsContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
export declare class ArgumentsContext extends antlr.ParserRuleContext {
    constructor(parent: antlr.ParserRuleContext | null, invokingState: number);
    LPAREN(): antlr.TerminalNode;
    RPAREN(): antlr.TerminalNode;
    expressionList(): ExpressionListContext | null;
    get ruleIndex(): number;
    enterRule(listener: JavaParserListener): void;
    exitRule(listener: JavaParserListener): void;
    accept<Result>(visitor: JavaParserVisitor<Result>): Result | null;
}
