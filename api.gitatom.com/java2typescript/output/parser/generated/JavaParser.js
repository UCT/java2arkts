// Generated from ./parser/JavaParser.g4 by ANTLR 4.13.1
import * as antlr from "antlr4ng";
export class JavaParser extends antlr.Parser {
    static ABSTRACT = 1;
    static ASSERT = 2;
    static BOOLEAN = 3;
    static BREAK = 4;
    static BYTE = 5;
    static CASE = 6;
    static CATCH = 7;
    static CHAR = 8;
    static CLASS = 9;
    static CONST = 10;
    static CONTINUE = 11;
    static DEFAULT = 12;
    static DO = 13;
    static DOUBLE = 14;
    static ELSE = 15;
    static ENUM = 16;
    static EXTENDS = 17;
    static FINAL = 18;
    static FINALLY = 19;
    static FLOAT = 20;
    static FOR = 21;
    static IF = 22;
    static GOTO = 23;
    static IMPLEMENTS = 24;
    static IMPORT = 25;
    static INSTANCEOF = 26;
    static INT = 27;
    static INTERFACE = 28;
    static LONG = 29;
    static NATIVE = 30;
    static NEW = 31;
    static PACKAGE = 32;
    static PRIVATE = 33;
    static PROTECTED = 34;
    static PUBLIC = 35;
    static RETURN = 36;
    static SHORT = 37;
    static STATIC = 38;
    static STRICTFP = 39;
    static SUPER = 40;
    static SWITCH = 41;
    static SYNCHRONIZED = 42;
    static THIS = 43;
    static THROW = 44;
    static THROWS = 45;
    static TRANSIENT = 46;
    static TRY = 47;
    static VOID = 48;
    static VOLATILE = 49;
    static WHILE = 50;
    static MODULE = 51;
    static OPEN = 52;
    static REQUIRES = 53;
    static EXPORTS = 54;
    static OPENS = 55;
    static TO = 56;
    static USES = 57;
    static PROVIDES = 58;
    static WITH = 59;
    static TRANSITIVE = 60;
    static VAR = 61;
    static YIELD = 62;
    static RECORD = 63;
    static SEALED = 64;
    static PERMITS = 65;
    static NON_SEALED = 66;
    static DECIMAL_LITERAL = 67;
    static HEX_LITERAL = 68;
    static OCT_LITERAL = 69;
    static BINARY_LITERAL = 70;
    static FLOAT_LITERAL = 71;
    static HEX_FLOAT_LITERAL = 72;
    static BOOL_LITERAL = 73;
    static CHAR_LITERAL = 74;
    static STRING_LITERAL = 75;
    static TEXT_BLOCK = 76;
    static NULL_LITERAL = 77;
    static LPAREN = 78;
    static RPAREN = 79;
    static LBRACE = 80;
    static RBRACE = 81;
    static LBRACK = 82;
    static RBRACK = 83;
    static SEMI = 84;
    static COMMA = 85;
    static DOT = 86;
    static ASSIGN = 87;
    static GT = 88;
    static LT = 89;
    static BANG = 90;
    static TILDE = 91;
    static QUESTION = 92;
    static COLON = 93;
    static EQUAL = 94;
    static LE = 95;
    static GE = 96;
    static NOTEQUAL = 97;
    static AND = 98;
    static OR = 99;
    static INC = 100;
    static DEC = 101;
    static ADD = 102;
    static SUB = 103;
    static MUL = 104;
    static DIV = 105;
    static BITAND = 106;
    static BITOR = 107;
    static CARET = 108;
    static MOD = 109;
    static ADD_ASSIGN = 110;
    static SUB_ASSIGN = 111;
    static MUL_ASSIGN = 112;
    static DIV_ASSIGN = 113;
    static AND_ASSIGN = 114;
    static OR_ASSIGN = 115;
    static XOR_ASSIGN = 116;
    static MOD_ASSIGN = 117;
    static LSHIFT_ASSIGN = 118;
    static RSHIFT_ASSIGN = 119;
    static URSHIFT_ASSIGN = 120;
    static ARROW = 121;
    static COLONCOLON = 122;
    static AT = 123;
    static ELLIPSIS = 124;
    static WS = 125;
    static COMMENT = 126;
    static LINE_COMMENT = 127;
    static IDENTIFIER = 128;
    static RULE_compilationUnit = 0;
    static RULE_packageDeclaration = 1;
    static RULE_importDeclaration = 2;
    static RULE_typeDeclaration = 3;
    static RULE_modifier = 4;
    static RULE_classOrInterfaceModifier = 5;
    static RULE_variableModifier = 6;
    static RULE_classDeclaration = 7;
    static RULE_typeParameters = 8;
    static RULE_typeParameter = 9;
    static RULE_typeBound = 10;
    static RULE_enumDeclaration = 11;
    static RULE_enumConstants = 12;
    static RULE_enumConstant = 13;
    static RULE_enumBodyDeclarations = 14;
    static RULE_interfaceDeclaration = 15;
    static RULE_classBody = 16;
    static RULE_interfaceBody = 17;
    static RULE_classBodyDeclaration = 18;
    static RULE_memberDeclaration = 19;
    static RULE_methodDeclaration = 20;
    static RULE_methodBody = 21;
    static RULE_typeTypeOrVoid = 22;
    static RULE_genericMethodDeclaration = 23;
    static RULE_genericConstructorDeclaration = 24;
    static RULE_constructorDeclaration = 25;
    static RULE_fieldDeclaration = 26;
    static RULE_interfaceBodyDeclaration = 27;
    static RULE_interfaceMemberDeclaration = 28;
    static RULE_constDeclaration = 29;
    static RULE_constantDeclarator = 30;
    static RULE_interfaceMethodDeclaration = 31;
    static RULE_interfaceMethodModifier = 32;
    static RULE_genericInterfaceMethodDeclaration = 33;
    static RULE_interfaceCommonBodyDeclaration = 34;
    static RULE_variableDeclarators = 35;
    static RULE_variableDeclarator = 36;
    static RULE_variableDeclaratorId = 37;
    static RULE_variableInitializer = 38;
    static RULE_arrayInitializer = 39;
    static RULE_classOrInterfaceType = 40;
    static RULE_typeArgument = 41;
    static RULE_qualifiedNameList = 42;
    static RULE_formalParameters = 43;
    static RULE_receiverParameter = 44;
    static RULE_formalParameterList = 45;
    static RULE_formalParameter = 46;
    static RULE_lastFormalParameter = 47;
    static RULE_lambdaLVTIList = 48;
    static RULE_lambdaLVTIParameter = 49;
    static RULE_qualifiedName = 50;
    static RULE_literal = 51;
    static RULE_integerLiteral = 52;
    static RULE_floatLiteral = 53;
    static RULE_altAnnotationQualifiedName = 54;
    static RULE_annotation = 55;
    static RULE_elementValuePairs = 56;
    static RULE_elementValuePair = 57;
    static RULE_elementValue = 58;
    static RULE_elementValueArrayInitializer = 59;
    static RULE_annotationTypeDeclaration = 60;
    static RULE_annotationTypeBody = 61;
    static RULE_annotationTypeElementDeclaration = 62;
    static RULE_annotationTypeElementRest = 63;
    static RULE_annotationMethodOrConstantRest = 64;
    static RULE_annotationMethodRest = 65;
    static RULE_annotationConstantRest = 66;
    static RULE_defaultValue = 67;
    static RULE_moduleDeclaration = 68;
    static RULE_moduleBody = 69;
    static RULE_moduleDirective = 70;
    static RULE_requiresModifier = 71;
    static RULE_recordDeclaration = 72;
    static RULE_recordHeader = 73;
    static RULE_recordComponentList = 74;
    static RULE_recordComponent = 75;
    static RULE_recordBody = 76;
    static RULE_block = 77;
    static RULE_blockStatement = 78;
    static RULE_localVariableDeclaration = 79;
    static RULE_identifier = 80;
    static RULE_localTypeDeclaration = 81;
    static RULE_statement = 82;
    static RULE_catchClause = 83;
    static RULE_catchType = 84;
    static RULE_finallyBlock = 85;
    static RULE_resourceSpecification = 86;
    static RULE_resources = 87;
    static RULE_resource = 88;
    static RULE_switchBlockStatementGroup = 89;
    static RULE_switchLabel = 90;
    static RULE_forControl = 91;
    static RULE_forInit = 92;
    static RULE_enhancedForControl = 93;
    static RULE_parExpression = 94;
    static RULE_expressionList = 95;
    static RULE_methodCall = 96;
    static RULE_expression = 97;
    static RULE_pattern = 98;
    static RULE_lambdaExpression = 99;
    static RULE_lambdaParameters = 100;
    static RULE_lambdaBody = 101;
    static RULE_primary = 102;
    static RULE_switchExpression = 103;
    static RULE_switchLabeledRule = 104;
    static RULE_guardedPattern = 105;
    static RULE_switchRuleOutcome = 106;
    static RULE_classType = 107;
    static RULE_creator = 108;
    static RULE_createdName = 109;
    static RULE_innerCreator = 110;
    static RULE_arrayCreatorRest = 111;
    static RULE_classCreatorRest = 112;
    static RULE_explicitGenericInvocation = 113;
    static RULE_typeArgumentsOrDiamond = 114;
    static RULE_nonWildcardTypeArgumentsOrDiamond = 115;
    static RULE_nonWildcardTypeArguments = 116;
    static RULE_typeList = 117;
    static RULE_typeType = 118;
    static RULE_primitiveType = 119;
    static RULE_typeArguments = 120;
    static RULE_superSuffix = 121;
    static RULE_explicitGenericInvocationSuffix = 122;
    static RULE_arguments = 123;
    static literalNames = [
        null, "'abstract'", "'assert'", "'boolean'", "'break'", "'byte'",
        "'case'", "'catch'", "'char'", "'class'", "'const'", "'continue'",
        "'default'", "'do'", "'double'", "'else'", "'enum'", "'extends'",
        "'final'", "'finally'", "'float'", "'for'", "'if'", "'goto'", "'implements'",
        "'import'", "'instanceof'", "'int'", "'interface'", "'long'", "'native'",
        "'new'", "'package'", "'private'", "'protected'", "'public'", "'return'",
        "'short'", "'static'", "'strictfp'", "'super'", "'switch'", "'synchronized'",
        "'this'", "'throw'", "'throws'", "'transient'", "'try'", "'void'",
        "'volatile'", "'while'", "'module'", "'open'", "'requires'", "'exports'",
        "'opens'", "'to'", "'uses'", "'provides'", "'with'", "'transitive'",
        "'var'", "'yield'", "'record'", "'sealed'", "'permits'", "'non-sealed'",
        null, null, null, null, null, null, null, null, null, null, "'null'",
        "'('", "')'", "'{'", "'}'", "'['", "']'", "';'", "','", "'.'", "'='",
        "'>'", "'<'", "'!'", "'~'", "'?'", "':'", "'=='", "'<='", "'>='",
        "'!='", "'&&'", "'||'", "'++'", "'--'", "'+'", "'-'", "'*'", "'/'",
        "'&'", "'|'", "'^'", "'%'", "'+='", "'-='", "'*='", "'/='", "'&='",
        "'|='", "'^='", "'%='", "'<<='", "'>>='", "'>>>='", "'->'", "'::'",
        "'@'", "'...'"
    ];
    static symbolicNames = [
        null, "ABSTRACT", "ASSERT", "BOOLEAN", "BREAK", "BYTE", "CASE",
        "CATCH", "CHAR", "CLASS", "CONST", "CONTINUE", "DEFAULT", "DO",
        "DOUBLE", "ELSE", "ENUM", "EXTENDS", "FINAL", "FINALLY", "FLOAT",
        "FOR", "IF", "GOTO", "IMPLEMENTS", "IMPORT", "INSTANCEOF", "INT",
        "INTERFACE", "LONG", "NATIVE", "NEW", "PACKAGE", "PRIVATE", "PROTECTED",
        "PUBLIC", "RETURN", "SHORT", "STATIC", "STRICTFP", "SUPER", "SWITCH",
        "SYNCHRONIZED", "THIS", "THROW", "THROWS", "TRANSIENT", "TRY", "VOID",
        "VOLATILE", "WHILE", "MODULE", "OPEN", "REQUIRES", "EXPORTS", "OPENS",
        "TO", "USES", "PROVIDES", "WITH", "TRANSITIVE", "VAR", "YIELD",
        "RECORD", "SEALED", "PERMITS", "NON_SEALED", "DECIMAL_LITERAL",
        "HEX_LITERAL", "OCT_LITERAL", "BINARY_LITERAL", "FLOAT_LITERAL",
        "HEX_FLOAT_LITERAL", "BOOL_LITERAL", "CHAR_LITERAL", "STRING_LITERAL",
        "TEXT_BLOCK", "NULL_LITERAL", "LPAREN", "RPAREN", "LBRACE", "RBRACE",
        "LBRACK", "RBRACK", "SEMI", "COMMA", "DOT", "ASSIGN", "GT", "LT",
        "BANG", "TILDE", "QUESTION", "COLON", "EQUAL", "LE", "GE", "NOTEQUAL",
        "AND", "OR", "INC", "DEC", "ADD", "SUB", "MUL", "DIV", "BITAND",
        "BITOR", "CARET", "MOD", "ADD_ASSIGN", "SUB_ASSIGN", "MUL_ASSIGN",
        "DIV_ASSIGN", "AND_ASSIGN", "OR_ASSIGN", "XOR_ASSIGN", "MOD_ASSIGN",
        "LSHIFT_ASSIGN", "RSHIFT_ASSIGN", "URSHIFT_ASSIGN", "ARROW", "COLONCOLON",
        "AT", "ELLIPSIS", "WS", "COMMENT", "LINE_COMMENT", "IDENTIFIER"
    ];
    static ruleNames = [
        "compilationUnit", "packageDeclaration", "importDeclaration", "typeDeclaration",
        "modifier", "classOrInterfaceModifier", "variableModifier", "classDeclaration",
        "typeParameters", "typeParameter", "typeBound", "enumDeclaration",
        "enumConstants", "enumConstant", "enumBodyDeclarations", "interfaceDeclaration",
        "classBody", "interfaceBody", "classBodyDeclaration", "memberDeclaration",
        "methodDeclaration", "methodBody", "typeTypeOrVoid", "genericMethodDeclaration",
        "genericConstructorDeclaration", "constructorDeclaration", "fieldDeclaration",
        "interfaceBodyDeclaration", "interfaceMemberDeclaration", "constDeclaration",
        "constantDeclarator", "interfaceMethodDeclaration", "interfaceMethodModifier",
        "genericInterfaceMethodDeclaration", "interfaceCommonBodyDeclaration",
        "variableDeclarators", "variableDeclarator", "variableDeclaratorId",
        "variableInitializer", "arrayInitializer", "classOrInterfaceType",
        "typeArgument", "qualifiedNameList", "formalParameters", "receiverParameter",
        "formalParameterList", "formalParameter", "lastFormalParameter",
        "lambdaLVTIList", "lambdaLVTIParameter", "qualifiedName", "literal",
        "integerLiteral", "floatLiteral", "altAnnotationQualifiedName",
        "annotation", "elementValuePairs", "elementValuePair", "elementValue",
        "elementValueArrayInitializer", "annotationTypeDeclaration", "annotationTypeBody",
        "annotationTypeElementDeclaration", "annotationTypeElementRest",
        "annotationMethodOrConstantRest", "annotationMethodRest", "annotationConstantRest",
        "defaultValue", "moduleDeclaration", "moduleBody", "moduleDirective",
        "requiresModifier", "recordDeclaration", "recordHeader", "recordComponentList",
        "recordComponent", "recordBody", "block", "blockStatement", "localVariableDeclaration",
        "identifier", "localTypeDeclaration", "statement", "catchClause",
        "catchType", "finallyBlock", "resourceSpecification", "resources",
        "resource", "switchBlockStatementGroup", "switchLabel", "forControl",
        "forInit", "enhancedForControl", "parExpression", "expressionList",
        "methodCall", "expression", "pattern", "lambdaExpression", "lambdaParameters",
        "lambdaBody", "primary", "switchExpression", "switchLabeledRule",
        "guardedPattern", "switchRuleOutcome", "classType", "creator", "createdName",
        "innerCreator", "arrayCreatorRest", "classCreatorRest", "explicitGenericInvocation",
        "typeArgumentsOrDiamond", "nonWildcardTypeArgumentsOrDiamond", "nonWildcardTypeArguments",
        "typeList", "typeType", "primitiveType", "typeArguments", "superSuffix",
        "explicitGenericInvocationSuffix", "arguments",
    ];
    get grammarFileName() { return "JavaParser.g4"; }
    get literalNames() { return JavaParser.literalNames; }
    get symbolicNames() { return JavaParser.symbolicNames; }
    get ruleNames() { return JavaParser.ruleNames; }
    get serializedATN() { return JavaParser._serializedATN; }
    createFailedPredicateException(predicate, message) {
        return new antlr.FailedPredicateException(this, predicate, message);
    }
    constructor(input) {
        super(input);
        this.interpreter = new antlr.ParserATNSimulator(this, JavaParser._ATN, JavaParser.decisionsToDFA, new antlr.PredictionContextCache());
    }
    compilationUnit() {
        let localContext = new CompilationUnitContext(this.context, this.state);
        this.enterRule(localContext, 0, JavaParser.RULE_compilationUnit);
        let _la;
        try {
            this.state = 267;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 3, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 249;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 0, this.context)) {
                            case 1:
                                {
                                    this.state = 248;
                                    this.packageDeclaration();
                                }
                                break;
                        }
                        this.state = 254;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 25) {
                            {
                                {
                                    this.state = 251;
                                    this.importDeclaration();
                                }
                            }
                            this.state = 256;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 260;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 268763650) !== 0) || ((((_la - 33)) & ~0x1F) === 0 && ((1 << (_la - 33)) & 4294705255) !== 0) || ((((_la - 65)) & ~0x1F) === 0 && ((1 << (_la - 65)) & 524291) !== 0) || _la === 123 || _la === 128) {
                            {
                                {
                                    this.state = 257;
                                    this.typeDeclaration();
                                }
                            }
                            this.state = 262;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 263;
                        this.match(JavaParser.EOF);
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 264;
                        this.moduleDeclaration();
                        this.state = 265;
                        this.match(JavaParser.EOF);
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    packageDeclaration() {
        let localContext = new PackageDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 2, JavaParser.RULE_packageDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 272;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 123 || _la === 128) {
                    {
                        {
                            this.state = 269;
                            this.annotation();
                        }
                    }
                    this.state = 274;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 275;
                this.match(JavaParser.PACKAGE);
                this.state = 276;
                this.qualifiedName();
                this.state = 277;
                this.match(JavaParser.SEMI);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    importDeclaration() {
        let localContext = new ImportDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 4, JavaParser.RULE_importDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 279;
                this.match(JavaParser.IMPORT);
                this.state = 281;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 38) {
                    {
                        this.state = 280;
                        this.match(JavaParser.STATIC);
                    }
                }
                this.state = 283;
                this.qualifiedName();
                this.state = 286;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 86) {
                    {
                        this.state = 284;
                        this.match(JavaParser.DOT);
                        this.state = 285;
                        this.match(JavaParser.MUL);
                    }
                }
                this.state = 288;
                this.match(JavaParser.SEMI);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeDeclaration() {
        let localContext = new TypeDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 6, JavaParser.RULE_typeDeclaration);
        try {
            let alternative;
            this.state = 304;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.ABSTRACT:
                case JavaParser.CLASS:
                case JavaParser.ENUM:
                case JavaParser.FINAL:
                case JavaParser.INTERFACE:
                case JavaParser.PRIVATE:
                case JavaParser.PROTECTED:
                case JavaParser.PUBLIC:
                case JavaParser.STATIC:
                case JavaParser.STRICTFP:
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.NON_SEALED:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 293;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 7, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 290;
                                        this.classOrInterfaceModifier();
                                    }
                                }
                            }
                            this.state = 295;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 7, this.context);
                        }
                        this.state = 301;
                        this.errorHandler.sync(this);
                        switch (this.tokenStream.LA(1)) {
                            case JavaParser.CLASS:
                                {
                                    this.state = 296;
                                    this.classDeclaration();
                                }
                                break;
                            case JavaParser.ENUM:
                                {
                                    this.state = 297;
                                    this.enumDeclaration();
                                }
                                break;
                            case JavaParser.INTERFACE:
                                {
                                    this.state = 298;
                                    this.interfaceDeclaration();
                                }
                                break;
                            case JavaParser.AT:
                                {
                                    this.state = 299;
                                    this.annotationTypeDeclaration();
                                }
                                break;
                            case JavaParser.RECORD:
                                {
                                    this.state = 300;
                                    this.recordDeclaration();
                                }
                                break;
                            default:
                                throw new antlr.NoViableAltException(this);
                        }
                    }
                    break;
                case JavaParser.SEMI:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 303;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    modifier() {
        let localContext = new ModifierContext(this.context, this.state);
        this.enterRule(localContext, 8, JavaParser.RULE_modifier);
        try {
            this.state = 311;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.ABSTRACT:
                case JavaParser.FINAL:
                case JavaParser.PRIVATE:
                case JavaParser.PROTECTED:
                case JavaParser.PUBLIC:
                case JavaParser.STATIC:
                case JavaParser.STRICTFP:
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.NON_SEALED:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 306;
                        this.classOrInterfaceModifier();
                    }
                    break;
                case JavaParser.NATIVE:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 307;
                        this.match(JavaParser.NATIVE);
                    }
                    break;
                case JavaParser.SYNCHRONIZED:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 308;
                        this.match(JavaParser.SYNCHRONIZED);
                    }
                    break;
                case JavaParser.TRANSIENT:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 309;
                        this.match(JavaParser.TRANSIENT);
                    }
                    break;
                case JavaParser.VOLATILE:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 310;
                        this.match(JavaParser.VOLATILE);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    classOrInterfaceModifier() {
        let localContext = new ClassOrInterfaceModifierContext(this.context, this.state);
        this.enterRule(localContext, 10, JavaParser.RULE_classOrInterfaceModifier);
        try {
            this.state = 323;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 11, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 313;
                        this.annotation();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 314;
                        this.match(JavaParser.PUBLIC);
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 315;
                        this.match(JavaParser.PROTECTED);
                    }
                    break;
                case 4:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 316;
                        this.match(JavaParser.PRIVATE);
                    }
                    break;
                case 5:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 317;
                        this.match(JavaParser.STATIC);
                    }
                    break;
                case 6:
                    this.enterOuterAlt(localContext, 6);
                    {
                        this.state = 318;
                        this.match(JavaParser.ABSTRACT);
                    }
                    break;
                case 7:
                    this.enterOuterAlt(localContext, 7);
                    {
                        this.state = 319;
                        this.match(JavaParser.FINAL);
                    }
                    break;
                case 8:
                    this.enterOuterAlt(localContext, 8);
                    {
                        this.state = 320;
                        this.match(JavaParser.STRICTFP);
                    }
                    break;
                case 9:
                    this.enterOuterAlt(localContext, 9);
                    {
                        this.state = 321;
                        this.match(JavaParser.SEALED);
                    }
                    break;
                case 10:
                    this.enterOuterAlt(localContext, 10);
                    {
                        this.state = 322;
                        this.match(JavaParser.NON_SEALED);
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    variableModifier() {
        let localContext = new VariableModifierContext(this.context, this.state);
        this.enterRule(localContext, 12, JavaParser.RULE_variableModifier);
        try {
            this.state = 327;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.FINAL:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 325;
                        this.match(JavaParser.FINAL);
                    }
                    break;
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 326;
                        this.annotation();
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    classDeclaration() {
        let localContext = new ClassDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 14, JavaParser.RULE_classDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 329;
                this.match(JavaParser.CLASS);
                this.state = 330;
                this.identifier();
                this.state = 332;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 89) {
                    {
                        this.state = 331;
                        this.typeParameters();
                    }
                }
                this.state = 336;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 17) {
                    {
                        this.state = 334;
                        this.match(JavaParser.EXTENDS);
                        this.state = 335;
                        this.typeType();
                    }
                }
                this.state = 340;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 24) {
                    {
                        this.state = 338;
                        this.match(JavaParser.IMPLEMENTS);
                        this.state = 339;
                        this.typeList();
                    }
                }
                this.state = 344;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 65) {
                    {
                        this.state = 342;
                        this.match(JavaParser.PERMITS);
                        this.state = 343;
                        this.typeList();
                    }
                }
                this.state = 346;
                this.classBody();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeParameters() {
        let localContext = new TypeParametersContext(this.context, this.state);
        this.enterRule(localContext, 16, JavaParser.RULE_typeParameters);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 348;
                this.match(JavaParser.LT);
                this.state = 349;
                this.typeParameter();
                this.state = 354;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 350;
                            this.match(JavaParser.COMMA);
                            this.state = 351;
                            this.typeParameter();
                        }
                    }
                    this.state = 356;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 357;
                this.match(JavaParser.GT);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeParameter() {
        let localContext = new TypeParameterContext(this.context, this.state);
        this.enterRule(localContext, 18, JavaParser.RULE_typeParameter);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 362;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 18, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 359;
                                this.annotation();
                            }
                        }
                    }
                    this.state = 364;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 18, this.context);
                }
                this.state = 365;
                this.identifier();
                this.state = 374;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 17) {
                    {
                        this.state = 366;
                        this.match(JavaParser.EXTENDS);
                        this.state = 370;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 19, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 367;
                                        this.annotation();
                                    }
                                }
                            }
                            this.state = 372;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 19, this.context);
                        }
                        this.state = 373;
                        this.typeBound();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeBound() {
        let localContext = new TypeBoundContext(this.context, this.state);
        this.enterRule(localContext, 20, JavaParser.RULE_typeBound);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 376;
                this.typeType();
                this.state = 381;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 106) {
                    {
                        {
                            this.state = 377;
                            this.match(JavaParser.BITAND);
                            this.state = 378;
                            this.typeType();
                        }
                    }
                    this.state = 383;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    enumDeclaration() {
        let localContext = new EnumDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 22, JavaParser.RULE_enumDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 384;
                this.match(JavaParser.ENUM);
                this.state = 385;
                this.identifier();
                this.state = 388;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 24) {
                    {
                        this.state = 386;
                        this.match(JavaParser.IMPLEMENTS);
                        this.state = 387;
                        this.typeList();
                    }
                }
                this.state = 390;
                this.match(JavaParser.LBRACE);
                this.state = 392;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 123 || _la === 128) {
                    {
                        this.state = 391;
                        this.enumConstants();
                    }
                }
                this.state = 395;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 85) {
                    {
                        this.state = 394;
                        this.match(JavaParser.COMMA);
                    }
                }
                this.state = 398;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 84) {
                    {
                        this.state = 397;
                        this.enumBodyDeclarations();
                    }
                }
                this.state = 400;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    enumConstants() {
        let localContext = new EnumConstantsContext(this.context, this.state);
        this.enterRule(localContext, 24, JavaParser.RULE_enumConstants);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 402;
                this.enumConstant();
                this.state = 407;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 26, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 403;
                                this.match(JavaParser.COMMA);
                                this.state = 404;
                                this.enumConstant();
                            }
                        }
                    }
                    this.state = 409;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 26, this.context);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    enumConstant() {
        let localContext = new EnumConstantContext(this.context, this.state);
        this.enterRule(localContext, 26, JavaParser.RULE_enumConstant);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 413;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 27, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 410;
                                this.annotation();
                            }
                        }
                    }
                    this.state = 415;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 27, this.context);
                }
                this.state = 416;
                this.identifier();
                this.state = 418;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 78) {
                    {
                        this.state = 417;
                        this.arguments();
                    }
                }
                this.state = 421;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 80) {
                    {
                        this.state = 420;
                        this.classBody();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    enumBodyDeclarations() {
        let localContext = new EnumBodyDeclarationsContext(this.context, this.state);
        this.enterRule(localContext, 28, JavaParser.RULE_enumBodyDeclarations);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 423;
                this.match(JavaParser.SEMI);
                this.state = 427;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2014659370) !== 0) || ((((_la - 33)) & ~0x1F) === 0 && ((1 << (_la - 33)) & 4294812279) !== 0) || ((((_la - 65)) & ~0x1F) === 0 && ((1 << (_la - 65)) & 17334275) !== 0) || _la === 123 || _la === 128) {
                    {
                        {
                            this.state = 424;
                            this.classBodyDeclaration();
                        }
                    }
                    this.state = 429;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    interfaceDeclaration() {
        let localContext = new InterfaceDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 30, JavaParser.RULE_interfaceDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 430;
                this.match(JavaParser.INTERFACE);
                this.state = 431;
                this.identifier();
                this.state = 433;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 89) {
                    {
                        this.state = 432;
                        this.typeParameters();
                    }
                }
                this.state = 437;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 17) {
                    {
                        this.state = 435;
                        this.match(JavaParser.EXTENDS);
                        this.state = 436;
                        this.typeList();
                    }
                }
                this.state = 439;
                this.interfaceBody();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    classBody() {
        let localContext = new ClassBodyContext(this.context, this.state);
        this.enterRule(localContext, 32, JavaParser.RULE_classBody);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 441;
                this.match(JavaParser.LBRACE);
                this.state = 445;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2014659370) !== 0) || ((((_la - 33)) & ~0x1F) === 0 && ((1 << (_la - 33)) & 4294812279) !== 0) || ((((_la - 65)) & ~0x1F) === 0 && ((1 << (_la - 65)) & 17334275) !== 0) || _la === 123 || _la === 128) {
                    {
                        {
                            this.state = 442;
                            this.classBodyDeclaration();
                        }
                    }
                    this.state = 447;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 448;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    interfaceBody() {
        let localContext = new InterfaceBodyContext(this.context, this.state);
        this.enterRule(localContext, 34, JavaParser.RULE_interfaceBody);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 450;
                this.match(JavaParser.LBRACE);
                this.state = 454;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2014663466) !== 0) || ((((_la - 33)) & ~0x1F) === 0 && ((1 << (_la - 33)) & 4294812279) !== 0) || ((((_la - 65)) & ~0x1F) === 0 && ((1 << (_la - 65)) & 17301507) !== 0) || _la === 123 || _la === 128) {
                    {
                        {
                            this.state = 451;
                            this.interfaceBodyDeclaration();
                        }
                    }
                    this.state = 456;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 457;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    classBodyDeclaration() {
        let localContext = new ClassBodyDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 36, JavaParser.RULE_classBodyDeclaration);
        let _la;
        try {
            let alternative;
            this.state = 471;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 37, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 459;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 461;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 38) {
                            {
                                this.state = 460;
                                this.match(JavaParser.STATIC);
                            }
                        }
                        this.state = 463;
                        this.block();
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 467;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 36, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 464;
                                        this.modifier();
                                    }
                                }
                            }
                            this.state = 469;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 36, this.context);
                        }
                        this.state = 470;
                        this.memberDeclaration();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    memberDeclaration() {
        let localContext = new MemberDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 38, JavaParser.RULE_memberDeclaration);
        try {
            this.state = 483;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 38, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 473;
                        this.methodDeclaration();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 474;
                        this.genericMethodDeclaration();
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 475;
                        this.fieldDeclaration();
                    }
                    break;
                case 4:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 476;
                        this.constructorDeclaration();
                    }
                    break;
                case 5:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 477;
                        this.genericConstructorDeclaration();
                    }
                    break;
                case 6:
                    this.enterOuterAlt(localContext, 6);
                    {
                        this.state = 478;
                        this.interfaceDeclaration();
                    }
                    break;
                case 7:
                    this.enterOuterAlt(localContext, 7);
                    {
                        this.state = 479;
                        this.annotationTypeDeclaration();
                    }
                    break;
                case 8:
                    this.enterOuterAlt(localContext, 8);
                    {
                        this.state = 480;
                        this.classDeclaration();
                    }
                    break;
                case 9:
                    this.enterOuterAlt(localContext, 9);
                    {
                        this.state = 481;
                        this.enumDeclaration();
                    }
                    break;
                case 10:
                    this.enterOuterAlt(localContext, 10);
                    {
                        this.state = 482;
                        this.recordDeclaration();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    methodDeclaration() {
        let localContext = new MethodDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 40, JavaParser.RULE_methodDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 485;
                this.typeTypeOrVoid();
                this.state = 486;
                this.identifier();
                this.state = 487;
                this.formalParameters();
                this.state = 492;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 82) {
                    {
                        {
                            this.state = 488;
                            this.match(JavaParser.LBRACK);
                            this.state = 489;
                            this.match(JavaParser.RBRACK);
                        }
                    }
                    this.state = 494;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 497;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 45) {
                    {
                        this.state = 495;
                        this.match(JavaParser.THROWS);
                        this.state = 496;
                        this.qualifiedNameList();
                    }
                }
                this.state = 499;
                this.methodBody();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    methodBody() {
        let localContext = new MethodBodyContext(this.context, this.state);
        this.enterRule(localContext, 42, JavaParser.RULE_methodBody);
        try {
            this.state = 503;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.LBRACE:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 501;
                        this.block();
                    }
                    break;
                case JavaParser.SEMI:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 502;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeTypeOrVoid() {
        let localContext = new TypeTypeOrVoidContext(this.context, this.state);
        this.enterRule(localContext, 44, JavaParser.RULE_typeTypeOrVoid);
        try {
            this.state = 507;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.BOOLEAN:
                case JavaParser.BYTE:
                case JavaParser.CHAR:
                case JavaParser.DOUBLE:
                case JavaParser.FLOAT:
                case JavaParser.INT:
                case JavaParser.LONG:
                case JavaParser.SHORT:
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 505;
                        this.typeType();
                    }
                    break;
                case JavaParser.VOID:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 506;
                        this.match(JavaParser.VOID);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    genericMethodDeclaration() {
        let localContext = new GenericMethodDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 46, JavaParser.RULE_genericMethodDeclaration);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 509;
                this.typeParameters();
                this.state = 510;
                this.methodDeclaration();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    genericConstructorDeclaration() {
        let localContext = new GenericConstructorDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 48, JavaParser.RULE_genericConstructorDeclaration);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 512;
                this.typeParameters();
                this.state = 513;
                this.constructorDeclaration();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    constructorDeclaration() {
        let localContext = new ConstructorDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 50, JavaParser.RULE_constructorDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 515;
                this.identifier();
                this.state = 516;
                this.formalParameters();
                this.state = 519;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 45) {
                    {
                        this.state = 517;
                        this.match(JavaParser.THROWS);
                        this.state = 518;
                        this.qualifiedNameList();
                    }
                }
                this.state = 521;
                localContext._constructorBody = this.block();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    fieldDeclaration() {
        let localContext = new FieldDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 52, JavaParser.RULE_fieldDeclaration);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 523;
                this.typeType();
                this.state = 524;
                this.variableDeclarators();
                this.state = 525;
                this.match(JavaParser.SEMI);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    interfaceBodyDeclaration() {
        let localContext = new InterfaceBodyDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 54, JavaParser.RULE_interfaceBodyDeclaration);
        try {
            let alternative;
            this.state = 535;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.ABSTRACT:
                case JavaParser.BOOLEAN:
                case JavaParser.BYTE:
                case JavaParser.CHAR:
                case JavaParser.CLASS:
                case JavaParser.DEFAULT:
                case JavaParser.DOUBLE:
                case JavaParser.ENUM:
                case JavaParser.FINAL:
                case JavaParser.FLOAT:
                case JavaParser.INT:
                case JavaParser.INTERFACE:
                case JavaParser.LONG:
                case JavaParser.NATIVE:
                case JavaParser.PRIVATE:
                case JavaParser.PROTECTED:
                case JavaParser.PUBLIC:
                case JavaParser.SHORT:
                case JavaParser.STATIC:
                case JavaParser.STRICTFP:
                case JavaParser.SYNCHRONIZED:
                case JavaParser.TRANSIENT:
                case JavaParser.VOID:
                case JavaParser.VOLATILE:
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.NON_SEALED:
                case JavaParser.LT:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 530;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 44, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 527;
                                        this.modifier();
                                    }
                                }
                            }
                            this.state = 532;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 44, this.context);
                        }
                        this.state = 533;
                        this.interfaceMemberDeclaration();
                    }
                    break;
                case JavaParser.SEMI:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 534;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    interfaceMemberDeclaration() {
        let localContext = new InterfaceMemberDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 56, JavaParser.RULE_interfaceMemberDeclaration);
        try {
            this.state = 545;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 46, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 537;
                        this.constDeclaration();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 538;
                        this.interfaceMethodDeclaration();
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 539;
                        this.genericInterfaceMethodDeclaration();
                    }
                    break;
                case 4:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 540;
                        this.interfaceDeclaration();
                    }
                    break;
                case 5:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 541;
                        this.annotationTypeDeclaration();
                    }
                    break;
                case 6:
                    this.enterOuterAlt(localContext, 6);
                    {
                        this.state = 542;
                        this.classDeclaration();
                    }
                    break;
                case 7:
                    this.enterOuterAlt(localContext, 7);
                    {
                        this.state = 543;
                        this.enumDeclaration();
                    }
                    break;
                case 8:
                    this.enterOuterAlt(localContext, 8);
                    {
                        this.state = 544;
                        this.recordDeclaration();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    constDeclaration() {
        let localContext = new ConstDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 58, JavaParser.RULE_constDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 547;
                this.typeType();
                this.state = 548;
                this.constantDeclarator();
                this.state = 553;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 549;
                            this.match(JavaParser.COMMA);
                            this.state = 550;
                            this.constantDeclarator();
                        }
                    }
                    this.state = 555;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 556;
                this.match(JavaParser.SEMI);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    constantDeclarator() {
        let localContext = new ConstantDeclaratorContext(this.context, this.state);
        this.enterRule(localContext, 60, JavaParser.RULE_constantDeclarator);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 558;
                this.identifier();
                this.state = 563;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 82) {
                    {
                        {
                            this.state = 559;
                            this.match(JavaParser.LBRACK);
                            this.state = 560;
                            this.match(JavaParser.RBRACK);
                        }
                    }
                    this.state = 565;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 566;
                this.match(JavaParser.ASSIGN);
                this.state = 567;
                this.variableInitializer();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    interfaceMethodDeclaration() {
        let localContext = new InterfaceMethodDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 62, JavaParser.RULE_interfaceMethodDeclaration);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 572;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 49, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 569;
                                this.interfaceMethodModifier();
                            }
                        }
                    }
                    this.state = 574;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 49, this.context);
                }
                this.state = 575;
                this.interfaceCommonBodyDeclaration();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    interfaceMethodModifier() {
        let localContext = new InterfaceMethodModifierContext(this.context, this.state);
        this.enterRule(localContext, 64, JavaParser.RULE_interfaceMethodModifier);
        try {
            this.state = 583;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 577;
                        this.annotation();
                    }
                    break;
                case JavaParser.PUBLIC:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 578;
                        this.match(JavaParser.PUBLIC);
                    }
                    break;
                case JavaParser.ABSTRACT:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 579;
                        this.match(JavaParser.ABSTRACT);
                    }
                    break;
                case JavaParser.DEFAULT:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 580;
                        this.match(JavaParser.DEFAULT);
                    }
                    break;
                case JavaParser.STATIC:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 581;
                        this.match(JavaParser.STATIC);
                    }
                    break;
                case JavaParser.STRICTFP:
                    this.enterOuterAlt(localContext, 6);
                    {
                        this.state = 582;
                        this.match(JavaParser.STRICTFP);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    genericInterfaceMethodDeclaration() {
        let localContext = new GenericInterfaceMethodDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 66, JavaParser.RULE_genericInterfaceMethodDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 588;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 1 || _la === 12 || ((((_la - 35)) & ~0x1F) === 0 && ((1 << (_la - 35)) & 2147418137) !== 0) || _la === 123 || _la === 128) {
                    {
                        {
                            this.state = 585;
                            this.interfaceMethodModifier();
                        }
                    }
                    this.state = 590;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 591;
                this.typeParameters();
                this.state = 592;
                this.interfaceCommonBodyDeclaration();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    interfaceCommonBodyDeclaration() {
        let localContext = new InterfaceCommonBodyDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 68, JavaParser.RULE_interfaceCommonBodyDeclaration);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 597;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 52, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 594;
                                this.annotation();
                            }
                        }
                    }
                    this.state = 599;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 52, this.context);
                }
                this.state = 600;
                this.typeTypeOrVoid();
                this.state = 601;
                this.identifier();
                this.state = 602;
                this.formalParameters();
                this.state = 607;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 82) {
                    {
                        {
                            this.state = 603;
                            this.match(JavaParser.LBRACK);
                            this.state = 604;
                            this.match(JavaParser.RBRACK);
                        }
                    }
                    this.state = 609;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 612;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 45) {
                    {
                        this.state = 610;
                        this.match(JavaParser.THROWS);
                        this.state = 611;
                        this.qualifiedNameList();
                    }
                }
                this.state = 614;
                this.methodBody();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    variableDeclarators() {
        let localContext = new VariableDeclaratorsContext(this.context, this.state);
        this.enterRule(localContext, 70, JavaParser.RULE_variableDeclarators);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 616;
                this.variableDeclarator();
                this.state = 621;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 617;
                            this.match(JavaParser.COMMA);
                            this.state = 618;
                            this.variableDeclarator();
                        }
                    }
                    this.state = 623;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    variableDeclarator() {
        let localContext = new VariableDeclaratorContext(this.context, this.state);
        this.enterRule(localContext, 72, JavaParser.RULE_variableDeclarator);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 624;
                this.variableDeclaratorId();
                this.state = 627;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 87) {
                    {
                        this.state = 625;
                        this.match(JavaParser.ASSIGN);
                        this.state = 626;
                        this.variableInitializer();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    variableDeclaratorId() {
        let localContext = new VariableDeclaratorIdContext(this.context, this.state);
        this.enterRule(localContext, 74, JavaParser.RULE_variableDeclaratorId);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 629;
                this.identifier();
                this.state = 634;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 82) {
                    {
                        {
                            this.state = 630;
                            this.match(JavaParser.LBRACK);
                            this.state = 631;
                            this.match(JavaParser.RBRACK);
                        }
                    }
                    this.state = 636;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    variableInitializer() {
        let localContext = new VariableInitializerContext(this.context, this.state);
        this.enterRule(localContext, 76, JavaParser.RULE_variableInitializer);
        try {
            this.state = 639;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.LBRACE:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 637;
                        this.arrayInitializer();
                    }
                    break;
                case JavaParser.BOOLEAN:
                case JavaParser.BYTE:
                case JavaParser.CHAR:
                case JavaParser.DOUBLE:
                case JavaParser.FLOAT:
                case JavaParser.INT:
                case JavaParser.LONG:
                case JavaParser.NEW:
                case JavaParser.SHORT:
                case JavaParser.SUPER:
                case JavaParser.SWITCH:
                case JavaParser.THIS:
                case JavaParser.VOID:
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.DECIMAL_LITERAL:
                case JavaParser.HEX_LITERAL:
                case JavaParser.OCT_LITERAL:
                case JavaParser.BINARY_LITERAL:
                case JavaParser.FLOAT_LITERAL:
                case JavaParser.HEX_FLOAT_LITERAL:
                case JavaParser.BOOL_LITERAL:
                case JavaParser.CHAR_LITERAL:
                case JavaParser.STRING_LITERAL:
                case JavaParser.TEXT_BLOCK:
                case JavaParser.NULL_LITERAL:
                case JavaParser.LPAREN:
                case JavaParser.LT:
                case JavaParser.BANG:
                case JavaParser.TILDE:
                case JavaParser.INC:
                case JavaParser.DEC:
                case JavaParser.ADD:
                case JavaParser.SUB:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 638;
                        this.expression(0);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    arrayInitializer() {
        let localContext = new ArrayInitializerContext(this.context, this.state);
        this.enterRule(localContext, 78, JavaParser.RULE_arrayInitializer);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 641;
                this.match(JavaParser.LBRACE);
                this.state = 653;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819637544) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154826751) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                    {
                        this.state = 642;
                        this.variableInitializer();
                        this.state = 647;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 59, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 643;
                                        this.match(JavaParser.COMMA);
                                        this.state = 644;
                                        this.variableInitializer();
                                    }
                                }
                            }
                            this.state = 649;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 59, this.context);
                        }
                        this.state = 651;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 85) {
                            {
                                this.state = 650;
                                this.match(JavaParser.COMMA);
                            }
                        }
                    }
                }
                this.state = 655;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    classOrInterfaceType() {
        let localContext = new ClassOrInterfaceTypeContext(this.context, this.state);
        this.enterRule(localContext, 80, JavaParser.RULE_classOrInterfaceType);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 657;
                this.identifier();
                this.state = 659;
                this.errorHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this.tokenStream, 62, this.context)) {
                    case 1:
                        {
                            this.state = 658;
                            this.typeArguments();
                        }
                        break;
                }
                this.state = 668;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 64, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 661;
                                this.match(JavaParser.DOT);
                                this.state = 662;
                                this.identifier();
                                this.state = 664;
                                this.errorHandler.sync(this);
                                switch (this.interpreter.adaptivePredict(this.tokenStream, 63, this.context)) {
                                    case 1:
                                        {
                                            this.state = 663;
                                            this.typeArguments();
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    this.state = 670;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 64, this.context);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeArgument() {
        let localContext = new TypeArgumentContext(this.context, this.state);
        this.enterRule(localContext, 82, JavaParser.RULE_typeArgument);
        let _la;
        try {
            this.state = 683;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 67, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 671;
                        this.typeType();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 675;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 123 || _la === 128) {
                            {
                                {
                                    this.state = 672;
                                    this.annotation();
                                }
                            }
                            this.state = 677;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 678;
                        this.match(JavaParser.QUESTION);
                        this.state = 681;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 17 || _la === 40) {
                            {
                                this.state = 679;
                                _la = this.tokenStream.LA(1);
                                if (!(_la === 17 || _la === 40)) {
                                    this.errorHandler.recoverInline(this);
                                }
                                else {
                                    this.errorHandler.reportMatch(this);
                                    this.consume();
                                }
                                this.state = 680;
                                this.typeType();
                            }
                        }
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    qualifiedNameList() {
        let localContext = new QualifiedNameListContext(this.context, this.state);
        this.enterRule(localContext, 84, JavaParser.RULE_qualifiedNameList);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 685;
                this.qualifiedName();
                this.state = 690;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 686;
                            this.match(JavaParser.COMMA);
                            this.state = 687;
                            this.qualifiedName();
                        }
                    }
                    this.state = 692;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    formalParameters() {
        let localContext = new FormalParametersContext(this.context, this.state);
        this.enterRule(localContext, 86, JavaParser.RULE_formalParameters);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 693;
                this.match(JavaParser.LPAREN);
                this.state = 705;
                this.errorHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this.tokenStream, 72, this.context)) {
                    case 1:
                        {
                            this.state = 695;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                            if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 672153896) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 536854529) !== 0) || _la === 123 || _la === 128) {
                                {
                                    this.state = 694;
                                    this.receiverParameter();
                                }
                            }
                        }
                        break;
                    case 2:
                        {
                            this.state = 697;
                            this.receiverParameter();
                            this.state = 700;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                            if (_la === 85) {
                                {
                                    this.state = 698;
                                    this.match(JavaParser.COMMA);
                                    this.state = 699;
                                    this.formalParameterList();
                                }
                            }
                        }
                        break;
                    case 3:
                        {
                            this.state = 703;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                            if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 672416040) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 536854529) !== 0) || _la === 123 || _la === 128) {
                                {
                                    this.state = 702;
                                    this.formalParameterList();
                                }
                            }
                        }
                        break;
                }
                this.state = 707;
                this.match(JavaParser.RPAREN);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    receiverParameter() {
        let localContext = new ReceiverParameterContext(this.context, this.state);
        this.enterRule(localContext, 88, JavaParser.RULE_receiverParameter);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 709;
                this.typeType();
                this.state = 715;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 128) {
                    {
                        {
                            this.state = 710;
                            this.identifier();
                            this.state = 711;
                            this.match(JavaParser.DOT);
                        }
                    }
                    this.state = 717;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 718;
                this.match(JavaParser.THIS);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    formalParameterList() {
        let localContext = new FormalParameterListContext(this.context, this.state);
        this.enterRule(localContext, 90, JavaParser.RULE_formalParameterList);
        let _la;
        try {
            let alternative;
            this.state = 733;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 76, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 720;
                        this.formalParameter();
                        this.state = 725;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 74, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 721;
                                        this.match(JavaParser.COMMA);
                                        this.state = 722;
                                        this.formalParameter();
                                    }
                                }
                            }
                            this.state = 727;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 74, this.context);
                        }
                        this.state = 730;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 85) {
                            {
                                this.state = 728;
                                this.match(JavaParser.COMMA);
                                this.state = 729;
                                this.lastFormalParameter();
                            }
                        }
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 732;
                        this.lastFormalParameter();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    formalParameter() {
        let localContext = new FormalParameterContext(this.context, this.state);
        this.enterRule(localContext, 92, JavaParser.RULE_formalParameter);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 738;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 77, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 735;
                                this.variableModifier();
                            }
                        }
                    }
                    this.state = 740;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 77, this.context);
                }
                this.state = 741;
                this.typeType();
                this.state = 742;
                this.variableDeclaratorId();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    lastFormalParameter() {
        let localContext = new LastFormalParameterContext(this.context, this.state);
        this.enterRule(localContext, 94, JavaParser.RULE_lastFormalParameter);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 747;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 78, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 744;
                                this.variableModifier();
                            }
                        }
                    }
                    this.state = 749;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 78, this.context);
                }
                this.state = 750;
                this.typeType();
                this.state = 754;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 123 || _la === 128) {
                    {
                        {
                            this.state = 751;
                            this.annotation();
                        }
                    }
                    this.state = 756;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 757;
                this.match(JavaParser.ELLIPSIS);
                this.state = 758;
                this.variableDeclaratorId();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    lambdaLVTIList() {
        let localContext = new LambdaLVTIListContext(this.context, this.state);
        this.enterRule(localContext, 96, JavaParser.RULE_lambdaLVTIList);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 760;
                this.lambdaLVTIParameter();
                this.state = 765;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 761;
                            this.match(JavaParser.COMMA);
                            this.state = 762;
                            this.lambdaLVTIParameter();
                        }
                    }
                    this.state = 767;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    lambdaLVTIParameter() {
        let localContext = new LambdaLVTIParameterContext(this.context, this.state);
        this.enterRule(localContext, 98, JavaParser.RULE_lambdaLVTIParameter);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 771;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 81, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 768;
                                this.variableModifier();
                            }
                        }
                    }
                    this.state = 773;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 81, this.context);
                }
                this.state = 774;
                this.match(JavaParser.VAR);
                this.state = 775;
                this.identifier();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    qualifiedName() {
        let localContext = new QualifiedNameContext(this.context, this.state);
        this.enterRule(localContext, 100, JavaParser.RULE_qualifiedName);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 777;
                this.identifier();
                this.state = 782;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 82, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 778;
                                this.match(JavaParser.DOT);
                                this.state = 779;
                                this.identifier();
                            }
                        }
                    }
                    this.state = 784;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 82, this.context);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    literal() {
        let localContext = new LiteralContext(this.context, this.state);
        this.enterRule(localContext, 102, JavaParser.RULE_literal);
        try {
            this.state = 792;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.DECIMAL_LITERAL:
                case JavaParser.HEX_LITERAL:
                case JavaParser.OCT_LITERAL:
                case JavaParser.BINARY_LITERAL:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 785;
                        this.integerLiteral();
                    }
                    break;
                case JavaParser.FLOAT_LITERAL:
                case JavaParser.HEX_FLOAT_LITERAL:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 786;
                        this.floatLiteral();
                    }
                    break;
                case JavaParser.CHAR_LITERAL:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 787;
                        this.match(JavaParser.CHAR_LITERAL);
                    }
                    break;
                case JavaParser.STRING_LITERAL:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 788;
                        this.match(JavaParser.STRING_LITERAL);
                    }
                    break;
                case JavaParser.BOOL_LITERAL:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 789;
                        this.match(JavaParser.BOOL_LITERAL);
                    }
                    break;
                case JavaParser.NULL_LITERAL:
                    this.enterOuterAlt(localContext, 6);
                    {
                        this.state = 790;
                        this.match(JavaParser.NULL_LITERAL);
                    }
                    break;
                case JavaParser.TEXT_BLOCK:
                    this.enterOuterAlt(localContext, 7);
                    {
                        this.state = 791;
                        this.match(JavaParser.TEXT_BLOCK);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    integerLiteral() {
        let localContext = new IntegerLiteralContext(this.context, this.state);
        this.enterRule(localContext, 104, JavaParser.RULE_integerLiteral);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 794;
                _la = this.tokenStream.LA(1);
                if (!(((((_la - 67)) & ~0x1F) === 0 && ((1 << (_la - 67)) & 15) !== 0))) {
                    this.errorHandler.recoverInline(this);
                }
                else {
                    this.errorHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    floatLiteral() {
        let localContext = new FloatLiteralContext(this.context, this.state);
        this.enterRule(localContext, 106, JavaParser.RULE_floatLiteral);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 796;
                _la = this.tokenStream.LA(1);
                if (!(_la === 71 || _la === 72)) {
                    this.errorHandler.recoverInline(this);
                }
                else {
                    this.errorHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    altAnnotationQualifiedName() {
        let localContext = new AltAnnotationQualifiedNameContext(this.context, this.state);
        this.enterRule(localContext, 108, JavaParser.RULE_altAnnotationQualifiedName);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 803;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 128) {
                    {
                        {
                            this.state = 798;
                            this.identifier();
                            this.state = 799;
                            this.match(JavaParser.DOT);
                        }
                    }
                    this.state = 805;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 806;
                this.match(JavaParser.AT);
                this.state = 807;
                this.identifier();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    annotation() {
        let localContext = new AnnotationContext(this.context, this.state);
        this.enterRule(localContext, 110, JavaParser.RULE_annotation);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 812;
                this.errorHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this.tokenStream, 85, this.context)) {
                    case 1:
                        {
                            this.state = 809;
                            this.match(JavaParser.AT);
                            this.state = 810;
                            this.qualifiedName();
                        }
                        break;
                    case 2:
                        {
                            this.state = 811;
                            this.altAnnotationQualifiedName();
                        }
                        break;
                }
                this.state = 820;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 78) {
                    {
                        this.state = 814;
                        this.match(JavaParser.LPAREN);
                        this.state = 817;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 86, this.context)) {
                            case 1:
                                {
                                    this.state = 815;
                                    this.elementValuePairs();
                                }
                                break;
                            case 2:
                                {
                                    this.state = 816;
                                    this.elementValue();
                                }
                                break;
                        }
                        this.state = 819;
                        this.match(JavaParser.RPAREN);
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    elementValuePairs() {
        let localContext = new ElementValuePairsContext(this.context, this.state);
        this.enterRule(localContext, 112, JavaParser.RULE_elementValuePairs);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 822;
                this.elementValuePair();
                this.state = 827;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 823;
                            this.match(JavaParser.COMMA);
                            this.state = 824;
                            this.elementValuePair();
                        }
                    }
                    this.state = 829;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    elementValuePair() {
        let localContext = new ElementValuePairContext(this.context, this.state);
        this.enterRule(localContext, 114, JavaParser.RULE_elementValuePair);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 830;
                this.identifier();
                this.state = 831;
                this.match(JavaParser.ASSIGN);
                this.state = 832;
                this.elementValue();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    elementValue() {
        let localContext = new ElementValueContext(this.context, this.state);
        this.enterRule(localContext, 116, JavaParser.RULE_elementValue);
        try {
            this.state = 837;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 89, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 834;
                        this.expression(0);
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 835;
                        this.annotation();
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 836;
                        this.elementValueArrayInitializer();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    elementValueArrayInitializer() {
        let localContext = new ElementValueArrayInitializerContext(this.context, this.state);
        this.enterRule(localContext, 118, JavaParser.RULE_elementValueArrayInitializer);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 839;
                this.match(JavaParser.LBRACE);
                this.state = 848;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819637544) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154826751) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                    {
                        this.state = 840;
                        this.elementValue();
                        this.state = 845;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 90, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 841;
                                        this.match(JavaParser.COMMA);
                                        this.state = 842;
                                        this.elementValue();
                                    }
                                }
                            }
                            this.state = 847;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 90, this.context);
                        }
                    }
                }
                this.state = 851;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 85) {
                    {
                        this.state = 850;
                        this.match(JavaParser.COMMA);
                    }
                }
                this.state = 853;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    annotationTypeDeclaration() {
        let localContext = new AnnotationTypeDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 120, JavaParser.RULE_annotationTypeDeclaration);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 855;
                this.match(JavaParser.AT);
                this.state = 856;
                this.match(JavaParser.INTERFACE);
                this.state = 857;
                this.identifier();
                this.state = 858;
                this.annotationTypeBody();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    annotationTypeBody() {
        let localContext = new AnnotationTypeBodyContext(this.context, this.state);
        this.enterRule(localContext, 122, JavaParser.RULE_annotationTypeBody);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 860;
                this.match(JavaParser.LBRACE);
                this.state = 864;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2014659370) !== 0) || ((((_la - 33)) & ~0x1F) === 0 && ((1 << (_la - 33)) & 4294779511) !== 0) || ((((_la - 65)) & ~0x1F) === 0 && ((1 << (_la - 65)) & 524291) !== 0) || _la === 123 || _la === 128) {
                    {
                        {
                            this.state = 861;
                            this.annotationTypeElementDeclaration();
                        }
                    }
                    this.state = 866;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 867;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    annotationTypeElementDeclaration() {
        let localContext = new AnnotationTypeElementDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 124, JavaParser.RULE_annotationTypeElementDeclaration);
        try {
            let alternative;
            this.state = 877;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.ABSTRACT:
                case JavaParser.BOOLEAN:
                case JavaParser.BYTE:
                case JavaParser.CHAR:
                case JavaParser.CLASS:
                case JavaParser.DOUBLE:
                case JavaParser.ENUM:
                case JavaParser.FINAL:
                case JavaParser.FLOAT:
                case JavaParser.INT:
                case JavaParser.INTERFACE:
                case JavaParser.LONG:
                case JavaParser.NATIVE:
                case JavaParser.PRIVATE:
                case JavaParser.PROTECTED:
                case JavaParser.PUBLIC:
                case JavaParser.SHORT:
                case JavaParser.STATIC:
                case JavaParser.STRICTFP:
                case JavaParser.SYNCHRONIZED:
                case JavaParser.TRANSIENT:
                case JavaParser.VOLATILE:
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.NON_SEALED:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 872;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 94, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 869;
                                        this.modifier();
                                    }
                                }
                            }
                            this.state = 874;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 94, this.context);
                        }
                        this.state = 875;
                        this.annotationTypeElementRest();
                    }
                    break;
                case JavaParser.SEMI:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 876;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    annotationTypeElementRest() {
        let localContext = new AnnotationTypeElementRestContext(this.context, this.state);
        this.enterRule(localContext, 126, JavaParser.RULE_annotationTypeElementRest);
        try {
            this.state = 903;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 101, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 879;
                        this.typeType();
                        this.state = 880;
                        this.annotationMethodOrConstantRest();
                        this.state = 881;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 883;
                        this.classDeclaration();
                        this.state = 885;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 96, this.context)) {
                            case 1:
                                {
                                    this.state = 884;
                                    this.match(JavaParser.SEMI);
                                }
                                break;
                        }
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 887;
                        this.interfaceDeclaration();
                        this.state = 889;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 97, this.context)) {
                            case 1:
                                {
                                    this.state = 888;
                                    this.match(JavaParser.SEMI);
                                }
                                break;
                        }
                    }
                    break;
                case 4:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 891;
                        this.enumDeclaration();
                        this.state = 893;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 98, this.context)) {
                            case 1:
                                {
                                    this.state = 892;
                                    this.match(JavaParser.SEMI);
                                }
                                break;
                        }
                    }
                    break;
                case 5:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 895;
                        this.annotationTypeDeclaration();
                        this.state = 897;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 99, this.context)) {
                            case 1:
                                {
                                    this.state = 896;
                                    this.match(JavaParser.SEMI);
                                }
                                break;
                        }
                    }
                    break;
                case 6:
                    this.enterOuterAlt(localContext, 6);
                    {
                        this.state = 899;
                        this.recordDeclaration();
                        this.state = 901;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 100, this.context)) {
                            case 1:
                                {
                                    this.state = 900;
                                    this.match(JavaParser.SEMI);
                                }
                                break;
                        }
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    annotationMethodOrConstantRest() {
        let localContext = new AnnotationMethodOrConstantRestContext(this.context, this.state);
        this.enterRule(localContext, 128, JavaParser.RULE_annotationMethodOrConstantRest);
        try {
            this.state = 907;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 102, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 905;
                        this.annotationMethodRest();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 906;
                        this.annotationConstantRest();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    annotationMethodRest() {
        let localContext = new AnnotationMethodRestContext(this.context, this.state);
        this.enterRule(localContext, 130, JavaParser.RULE_annotationMethodRest);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 909;
                this.identifier();
                this.state = 910;
                this.match(JavaParser.LPAREN);
                this.state = 911;
                this.match(JavaParser.RPAREN);
                this.state = 913;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 12) {
                    {
                        this.state = 912;
                        this.defaultValue();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    annotationConstantRest() {
        let localContext = new AnnotationConstantRestContext(this.context, this.state);
        this.enterRule(localContext, 132, JavaParser.RULE_annotationConstantRest);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 915;
                this.variableDeclarators();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    defaultValue() {
        let localContext = new DefaultValueContext(this.context, this.state);
        this.enterRule(localContext, 134, JavaParser.RULE_defaultValue);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 917;
                this.match(JavaParser.DEFAULT);
                this.state = 918;
                this.elementValue();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    moduleDeclaration() {
        let localContext = new ModuleDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 136, JavaParser.RULE_moduleDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 921;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 52) {
                    {
                        this.state = 920;
                        this.match(JavaParser.OPEN);
                    }
                }
                this.state = 923;
                this.match(JavaParser.MODULE);
                this.state = 924;
                this.qualifiedName();
                this.state = 925;
                this.moduleBody();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    moduleBody() {
        let localContext = new ModuleBodyContext(this.context, this.state);
        this.enterRule(localContext, 138, JavaParser.RULE_moduleBody);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 927;
                this.match(JavaParser.LBRACE);
                this.state = 931;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (((((_la - 53)) & ~0x1F) === 0 && ((1 << (_la - 53)) & 55) !== 0)) {
                    {
                        {
                            this.state = 928;
                            this.moduleDirective();
                        }
                    }
                    this.state = 933;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 934;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    moduleDirective() {
        let localContext = new ModuleDirectiveContext(this.context, this.state);
        this.enterRule(localContext, 140, JavaParser.RULE_moduleDirective);
        let _la;
        try {
            let alternative;
            this.state = 972;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.REQUIRES:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 936;
                        this.match(JavaParser.REQUIRES);
                        this.state = 940;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 106, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 937;
                                        this.requiresModifier();
                                    }
                                }
                            }
                            this.state = 942;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 106, this.context);
                        }
                        this.state = 943;
                        this.qualifiedName();
                        this.state = 944;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case JavaParser.EXPORTS:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 946;
                        this.match(JavaParser.EXPORTS);
                        this.state = 947;
                        this.qualifiedName();
                        this.state = 950;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 56) {
                            {
                                this.state = 948;
                                this.match(JavaParser.TO);
                                this.state = 949;
                                this.qualifiedName();
                            }
                        }
                        this.state = 952;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case JavaParser.OPENS:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 954;
                        this.match(JavaParser.OPENS);
                        this.state = 955;
                        this.qualifiedName();
                        this.state = 958;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 56) {
                            {
                                this.state = 956;
                                this.match(JavaParser.TO);
                                this.state = 957;
                                this.qualifiedName();
                            }
                        }
                        this.state = 960;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case JavaParser.USES:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 962;
                        this.match(JavaParser.USES);
                        this.state = 963;
                        this.qualifiedName();
                        this.state = 964;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case JavaParser.PROVIDES:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 966;
                        this.match(JavaParser.PROVIDES);
                        this.state = 967;
                        this.qualifiedName();
                        this.state = 968;
                        this.match(JavaParser.WITH);
                        this.state = 969;
                        this.qualifiedName();
                        this.state = 970;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    requiresModifier() {
        let localContext = new RequiresModifierContext(this.context, this.state);
        this.enterRule(localContext, 142, JavaParser.RULE_requiresModifier);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 974;
                _la = this.tokenStream.LA(1);
                if (!(_la === 38 || _la === 60)) {
                    this.errorHandler.recoverInline(this);
                }
                else {
                    this.errorHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    recordDeclaration() {
        let localContext = new RecordDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 144, JavaParser.RULE_recordDeclaration);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 976;
                this.match(JavaParser.RECORD);
                this.state = 977;
                this.identifier();
                this.state = 979;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 89) {
                    {
                        this.state = 978;
                        this.typeParameters();
                    }
                }
                this.state = 981;
                this.recordHeader();
                this.state = 984;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 24) {
                    {
                        this.state = 982;
                        this.match(JavaParser.IMPLEMENTS);
                        this.state = 983;
                        this.typeList();
                    }
                }
                this.state = 986;
                this.recordBody();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    recordHeader() {
        let localContext = new RecordHeaderContext(this.context, this.state);
        this.enterRule(localContext, 146, JavaParser.RULE_recordHeader);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 988;
                this.match(JavaParser.LPAREN);
                this.state = 990;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 672153896) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 536854529) !== 0) || _la === 123 || _la === 128) {
                    {
                        this.state = 989;
                        this.recordComponentList();
                    }
                }
                this.state = 992;
                this.match(JavaParser.RPAREN);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    recordComponentList() {
        let localContext = new RecordComponentListContext(this.context, this.state);
        this.enterRule(localContext, 148, JavaParser.RULE_recordComponentList);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 994;
                this.recordComponent();
                this.state = 999;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 995;
                            this.match(JavaParser.COMMA);
                            this.state = 996;
                            this.recordComponent();
                        }
                    }
                    this.state = 1001;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    recordComponent() {
        let localContext = new RecordComponentContext(this.context, this.state);
        this.enterRule(localContext, 150, JavaParser.RULE_recordComponent);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1002;
                this.typeType();
                this.state = 1003;
                this.identifier();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    recordBody() {
        let localContext = new RecordBodyContext(this.context, this.state);
        this.enterRule(localContext, 152, JavaParser.RULE_recordBody);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1005;
                this.match(JavaParser.LBRACE);
                this.state = 1009;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2014659370) !== 0) || ((((_la - 33)) & ~0x1F) === 0 && ((1 << (_la - 33)) & 4294812279) !== 0) || ((((_la - 65)) & ~0x1F) === 0 && ((1 << (_la - 65)) & 17334275) !== 0) || _la === 123 || _la === 128) {
                    {
                        {
                            this.state = 1006;
                            this.classBodyDeclaration();
                        }
                    }
                    this.state = 1011;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 1012;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    block() {
        let localContext = new BlockContext(this.context, this.state);
        this.enterRule(localContext, 154, JavaParser.RULE_block);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1014;
                this.match(JavaParser.LBRACE);
                this.state = 1018;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 3094637374) !== 0) || ((((_la - 33)) & ~0x1F) === 0 && ((1 << (_la - 33)) & 4294889471) !== 0) || ((((_la - 65)) & ~0x1F) === 0 && ((1 << (_la - 65)) & 118013951) !== 0) || ((((_la - 100)) & ~0x1F) === 0 && ((1 << (_la - 100)) & 276824079) !== 0)) {
                    {
                        {
                            this.state = 1015;
                            this.blockStatement();
                        }
                    }
                    this.state = 1020;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 1021;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    blockStatement() {
        let localContext = new BlockStatementContext(this.context, this.state);
        this.enterRule(localContext, 156, JavaParser.RULE_blockStatement);
        try {
            this.state = 1028;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 116, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1023;
                        this.localVariableDeclaration();
                        this.state = 1024;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1026;
                        this.statement();
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 1027;
                        this.localTypeDeclaration();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    localVariableDeclaration() {
        let localContext = new LocalVariableDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 158, JavaParser.RULE_localVariableDeclaration);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1033;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 117, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 1030;
                                this.variableModifier();
                            }
                        }
                    }
                    this.state = 1035;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 117, this.context);
                }
                this.state = 1036;
                this.typeType();
                this.state = 1037;
                this.variableDeclarators();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    identifier() {
        let localContext = new IdentifierContext(this.context, this.state);
        this.enterRule(localContext, 160, JavaParser.RULE_identifier);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1039;
                _la = this.tokenStream.LA(1);
                if (!(((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 128)) {
                    this.errorHandler.recoverInline(this);
                }
                else {
                    this.errorHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    localTypeDeclaration() {
        let localContext = new LocalTypeDeclarationContext(this.context, this.state);
        this.enterRule(localContext, 162, JavaParser.RULE_localTypeDeclaration);
        try {
            let alternative;
            this.state = 1053;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.ABSTRACT:
                case JavaParser.CLASS:
                case JavaParser.FINAL:
                case JavaParser.INTERFACE:
                case JavaParser.PRIVATE:
                case JavaParser.PROTECTED:
                case JavaParser.PUBLIC:
                case JavaParser.STATIC:
                case JavaParser.STRICTFP:
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.NON_SEALED:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1044;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 118, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 1041;
                                        this.classOrInterfaceModifier();
                                    }
                                }
                            }
                            this.state = 1046;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 118, this.context);
                        }
                        this.state = 1050;
                        this.errorHandler.sync(this);
                        switch (this.tokenStream.LA(1)) {
                            case JavaParser.CLASS:
                                {
                                    this.state = 1047;
                                    this.classDeclaration();
                                }
                                break;
                            case JavaParser.INTERFACE:
                                {
                                    this.state = 1048;
                                    this.interfaceDeclaration();
                                }
                                break;
                            case JavaParser.RECORD:
                                {
                                    this.state = 1049;
                                    this.recordDeclaration();
                                }
                                break;
                            default:
                                throw new antlr.NoViableAltException(this);
                        }
                    }
                    break;
                case JavaParser.SEMI:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1052;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    statement() {
        let localContext = new StatementContext(this.context, this.state);
        this.enterRule(localContext, 164, JavaParser.RULE_statement);
        let _la;
        try {
            let alternative;
            this.state = 1168;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 134, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1055;
                        localContext._blockLabel = this.block();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1056;
                        this.match(JavaParser.ASSERT);
                        this.state = 1057;
                        this.expression(0);
                        this.state = 1060;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 93) {
                            {
                                this.state = 1058;
                                this.match(JavaParser.COLON);
                                this.state = 1059;
                                this.expression(0);
                            }
                        }
                        this.state = 1062;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 1064;
                        this.match(JavaParser.IF);
                        this.state = 1065;
                        this.parExpression();
                        this.state = 1066;
                        this.statement();
                        this.state = 1069;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 122, this.context)) {
                            case 1:
                                {
                                    this.state = 1067;
                                    this.match(JavaParser.ELSE);
                                    this.state = 1068;
                                    this.statement();
                                }
                                break;
                        }
                    }
                    break;
                case 4:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 1071;
                        this.match(JavaParser.FOR);
                        this.state = 1072;
                        this.match(JavaParser.LPAREN);
                        this.state = 1073;
                        this.forControl();
                        this.state = 1074;
                        this.match(JavaParser.RPAREN);
                        this.state = 1075;
                        this.statement();
                    }
                    break;
                case 5:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 1077;
                        this.match(JavaParser.WHILE);
                        this.state = 1078;
                        this.parExpression();
                        this.state = 1079;
                        this.statement();
                    }
                    break;
                case 6:
                    this.enterOuterAlt(localContext, 6);
                    {
                        this.state = 1081;
                        this.match(JavaParser.DO);
                        this.state = 1082;
                        this.statement();
                        this.state = 1083;
                        this.match(JavaParser.WHILE);
                        this.state = 1084;
                        this.parExpression();
                        this.state = 1085;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 7:
                    this.enterOuterAlt(localContext, 7);
                    {
                        this.state = 1087;
                        this.match(JavaParser.TRY);
                        this.state = 1088;
                        this.block();
                        this.state = 1098;
                        this.errorHandler.sync(this);
                        switch (this.tokenStream.LA(1)) {
                            case JavaParser.CATCH:
                                {
                                    this.state = 1090;
                                    this.errorHandler.sync(this);
                                    _la = this.tokenStream.LA(1);
                                    do {
                                        {
                                            {
                                                this.state = 1089;
                                                this.catchClause();
                                            }
                                        }
                                        this.state = 1092;
                                        this.errorHandler.sync(this);
                                        _la = this.tokenStream.LA(1);
                                    } while (_la === 7);
                                    this.state = 1095;
                                    this.errorHandler.sync(this);
                                    _la = this.tokenStream.LA(1);
                                    if (_la === 19) {
                                        {
                                            this.state = 1094;
                                            this.finallyBlock();
                                        }
                                    }
                                }
                                break;
                            case JavaParser.FINALLY:
                                {
                                    this.state = 1097;
                                    this.finallyBlock();
                                }
                                break;
                            default:
                                throw new antlr.NoViableAltException(this);
                        }
                    }
                    break;
                case 8:
                    this.enterOuterAlt(localContext, 8);
                    {
                        this.state = 1100;
                        this.match(JavaParser.TRY);
                        this.state = 1101;
                        this.resourceSpecification();
                        this.state = 1102;
                        this.block();
                        this.state = 1106;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 7) {
                            {
                                {
                                    this.state = 1103;
                                    this.catchClause();
                                }
                            }
                            this.state = 1108;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 1110;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 19) {
                            {
                                this.state = 1109;
                                this.finallyBlock();
                            }
                        }
                    }
                    break;
                case 9:
                    this.enterOuterAlt(localContext, 9);
                    {
                        this.state = 1112;
                        this.match(JavaParser.SWITCH);
                        this.state = 1113;
                        this.parExpression();
                        this.state = 1114;
                        this.match(JavaParser.LBRACE);
                        this.state = 1118;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 128, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 1115;
                                        this.switchBlockStatementGroup();
                                    }
                                }
                            }
                            this.state = 1120;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 128, this.context);
                        }
                        this.state = 1124;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 6 || _la === 12) {
                            {
                                {
                                    this.state = 1121;
                                    this.switchLabel();
                                }
                            }
                            this.state = 1126;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 1127;
                        this.match(JavaParser.RBRACE);
                    }
                    break;
                case 10:
                    this.enterOuterAlt(localContext, 10);
                    {
                        this.state = 1129;
                        this.match(JavaParser.SYNCHRONIZED);
                        this.state = 1130;
                        this.parExpression();
                        this.state = 1131;
                        this.block();
                    }
                    break;
                case 11:
                    this.enterOuterAlt(localContext, 11);
                    {
                        this.state = 1133;
                        this.match(JavaParser.RETURN);
                        this.state = 1135;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819637544) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154824703) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                            {
                                this.state = 1134;
                                this.expression(0);
                            }
                        }
                        this.state = 1137;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 12:
                    this.enterOuterAlt(localContext, 12);
                    {
                        this.state = 1138;
                        this.match(JavaParser.THROW);
                        this.state = 1139;
                        this.expression(0);
                        this.state = 1140;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 13:
                    this.enterOuterAlt(localContext, 13);
                    {
                        this.state = 1142;
                        this.match(JavaParser.BREAK);
                        this.state = 1144;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 128) {
                            {
                                this.state = 1143;
                                this.identifier();
                            }
                        }
                        this.state = 1146;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 14:
                    this.enterOuterAlt(localContext, 14);
                    {
                        this.state = 1147;
                        this.match(JavaParser.CONTINUE);
                        this.state = 1149;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 128) {
                            {
                                this.state = 1148;
                                this.identifier();
                            }
                        }
                        this.state = 1151;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 15:
                    this.enterOuterAlt(localContext, 15);
                    {
                        this.state = 1152;
                        this.match(JavaParser.YIELD);
                        this.state = 1153;
                        this.expression(0);
                        this.state = 1154;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 16:
                    this.enterOuterAlt(localContext, 16);
                    {
                        this.state = 1156;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 17:
                    this.enterOuterAlt(localContext, 17);
                    {
                        this.state = 1157;
                        localContext._statementExpression = this.expression(0);
                        this.state = 1158;
                        this.match(JavaParser.SEMI);
                    }
                    break;
                case 18:
                    this.enterOuterAlt(localContext, 18);
                    {
                        this.state = 1160;
                        this.switchExpression();
                        this.state = 1162;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 133, this.context)) {
                            case 1:
                                {
                                    this.state = 1161;
                                    this.match(JavaParser.SEMI);
                                }
                                break;
                        }
                    }
                    break;
                case 19:
                    this.enterOuterAlt(localContext, 19);
                    {
                        this.state = 1164;
                        localContext._identifierLabel = this.identifier();
                        this.state = 1165;
                        this.match(JavaParser.COLON);
                        this.state = 1166;
                        this.statement();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    catchClause() {
        let localContext = new CatchClauseContext(this.context, this.state);
        this.enterRule(localContext, 166, JavaParser.RULE_catchClause);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1170;
                this.match(JavaParser.CATCH);
                this.state = 1171;
                this.match(JavaParser.LPAREN);
                this.state = 1175;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 135, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 1172;
                                this.variableModifier();
                            }
                        }
                    }
                    this.state = 1177;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 135, this.context);
                }
                this.state = 1178;
                this.catchType();
                this.state = 1179;
                this.identifier();
                this.state = 1180;
                this.match(JavaParser.RPAREN);
                this.state = 1181;
                this.block();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    catchType() {
        let localContext = new CatchTypeContext(this.context, this.state);
        this.enterRule(localContext, 168, JavaParser.RULE_catchType);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1183;
                this.qualifiedName();
                this.state = 1188;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 107) {
                    {
                        {
                            this.state = 1184;
                            this.match(JavaParser.BITOR);
                            this.state = 1185;
                            this.qualifiedName();
                        }
                    }
                    this.state = 1190;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    finallyBlock() {
        let localContext = new FinallyBlockContext(this.context, this.state);
        this.enterRule(localContext, 170, JavaParser.RULE_finallyBlock);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1191;
                this.match(JavaParser.FINALLY);
                this.state = 1192;
                this.block();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    resourceSpecification() {
        let localContext = new ResourceSpecificationContext(this.context, this.state);
        this.enterRule(localContext, 172, JavaParser.RULE_resourceSpecification);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1194;
                this.match(JavaParser.LPAREN);
                this.state = 1195;
                this.resources();
                this.state = 1197;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 84) {
                    {
                        this.state = 1196;
                        this.match(JavaParser.SEMI);
                    }
                }
                this.state = 1199;
                this.match(JavaParser.RPAREN);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    resources() {
        let localContext = new ResourcesContext(this.context, this.state);
        this.enterRule(localContext, 174, JavaParser.RULE_resources);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1201;
                this.resource();
                this.state = 1206;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 138, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 1202;
                                this.match(JavaParser.SEMI);
                                this.state = 1203;
                                this.resource();
                            }
                        }
                    }
                    this.state = 1208;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 138, this.context);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    resource() {
        let localContext = new ResourceContext(this.context, this.state);
        this.enterRule(localContext, 176, JavaParser.RULE_resource);
        try {
            let alternative;
            this.state = 1226;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 141, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1212;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 139, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                    {
                                        this.state = 1209;
                                        this.variableModifier();
                                    }
                                }
                            }
                            this.state = 1214;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 139, this.context);
                        }
                        this.state = 1220;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 140, this.context)) {
                            case 1:
                                {
                                    this.state = 1215;
                                    this.classOrInterfaceType();
                                    this.state = 1216;
                                    this.variableDeclaratorId();
                                }
                                break;
                            case 2:
                                {
                                    this.state = 1218;
                                    this.match(JavaParser.VAR);
                                    this.state = 1219;
                                    this.identifier();
                                }
                                break;
                        }
                        this.state = 1222;
                        this.match(JavaParser.ASSIGN);
                        this.state = 1223;
                        this.expression(0);
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1225;
                        this.identifier();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    switchBlockStatementGroup() {
        let localContext = new SwitchBlockStatementGroupContext(this.context, this.state);
        this.enterRule(localContext, 178, JavaParser.RULE_switchBlockStatementGroup);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1229;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                do {
                    {
                        {
                            this.state = 1228;
                            this.switchLabel();
                        }
                    }
                    this.state = 1231;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                } while (_la === 6 || _la === 12);
                this.state = 1234;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                do {
                    {
                        {
                            this.state = 1233;
                            this.blockStatement();
                        }
                    }
                    this.state = 1236;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                } while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 3094637374) !== 0) || ((((_la - 33)) & ~0x1F) === 0 && ((1 << (_la - 33)) & 4294889471) !== 0) || ((((_la - 65)) & ~0x1F) === 0 && ((1 << (_la - 65)) & 118013951) !== 0) || ((((_la - 100)) & ~0x1F) === 0 && ((1 << (_la - 100)) & 276824079) !== 0));
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    switchLabel() {
        let localContext = new SwitchLabelContext(this.context, this.state);
        this.enterRule(localContext, 180, JavaParser.RULE_switchLabel);
        try {
            this.state = 1249;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.CASE:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1238;
                        this.match(JavaParser.CASE);
                        this.state = 1244;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 144, this.context)) {
                            case 1:
                                {
                                    this.state = 1239;
                                    localContext._constantExpression = this.expression(0);
                                }
                                break;
                            case 2:
                                {
                                    this.state = 1240;
                                    localContext._enumConstantName = this.match(JavaParser.IDENTIFIER);
                                }
                                break;
                            case 3:
                                {
                                    this.state = 1241;
                                    this.typeType();
                                    this.state = 1242;
                                    localContext._varName = this.identifier();
                                }
                                break;
                        }
                        this.state = 1246;
                        this.match(JavaParser.COLON);
                    }
                    break;
                case JavaParser.DEFAULT:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1247;
                        this.match(JavaParser.DEFAULT);
                        this.state = 1248;
                        this.match(JavaParser.COLON);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    forControl() {
        let localContext = new ForControlContext(this.context, this.state);
        this.enterRule(localContext, 182, JavaParser.RULE_forControl);
        let _la;
        try {
            this.state = 1263;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 149, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1251;
                        this.enhancedForControl();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1253;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819899688) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154824703) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                            {
                                this.state = 1252;
                                this.forInit();
                            }
                        }
                        this.state = 1255;
                        this.match(JavaParser.SEMI);
                        this.state = 1257;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819637544) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154824703) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                            {
                                this.state = 1256;
                                this.expression(0);
                            }
                        }
                        this.state = 1259;
                        this.match(JavaParser.SEMI);
                        this.state = 1261;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819637544) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154824703) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                            {
                                this.state = 1260;
                                localContext._forUpdate = this.expressionList();
                            }
                        }
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    forInit() {
        let localContext = new ForInitContext(this.context, this.state);
        this.enterRule(localContext, 184, JavaParser.RULE_forInit);
        try {
            this.state = 1267;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 150, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1265;
                        this.localVariableDeclaration();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1266;
                        this.expressionList();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    enhancedForControl() {
        let localContext = new EnhancedForControlContext(this.context, this.state);
        this.enterRule(localContext, 186, JavaParser.RULE_enhancedForControl);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1272;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 151, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 1269;
                                this.variableModifier();
                            }
                        }
                    }
                    this.state = 1274;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 151, this.context);
                }
                this.state = 1277;
                this.errorHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this.tokenStream, 152, this.context)) {
                    case 1:
                        {
                            this.state = 1275;
                            this.typeType();
                        }
                        break;
                    case 2:
                        {
                            this.state = 1276;
                            this.match(JavaParser.VAR);
                        }
                        break;
                }
                this.state = 1279;
                this.variableDeclaratorId();
                this.state = 1280;
                this.match(JavaParser.COLON);
                this.state = 1281;
                this.expression(0);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    parExpression() {
        let localContext = new ParExpressionContext(this.context, this.state);
        this.enterRule(localContext, 188, JavaParser.RULE_parExpression);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1283;
                this.match(JavaParser.LPAREN);
                this.state = 1284;
                this.expression(0);
                this.state = 1285;
                this.match(JavaParser.RPAREN);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    expressionList() {
        let localContext = new ExpressionListContext(this.context, this.state);
        this.enterRule(localContext, 190, JavaParser.RULE_expressionList);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1287;
                this.expression(0);
                this.state = 1292;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 1288;
                            this.match(JavaParser.COMMA);
                            this.state = 1289;
                            this.expression(0);
                        }
                    }
                    this.state = 1294;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    methodCall() {
        let localContext = new MethodCallContext(this.context, this.state);
        this.enterRule(localContext, 192, JavaParser.RULE_methodCall);
        let _la;
        try {
            this.state = 1314;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1295;
                        this.identifier();
                        this.state = 1296;
                        this.match(JavaParser.LPAREN);
                        this.state = 1298;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819637544) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154824703) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                            {
                                this.state = 1297;
                                this.expressionList();
                            }
                        }
                        this.state = 1300;
                        this.match(JavaParser.RPAREN);
                    }
                    break;
                case JavaParser.THIS:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1302;
                        this.match(JavaParser.THIS);
                        this.state = 1303;
                        this.match(JavaParser.LPAREN);
                        this.state = 1305;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819637544) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154824703) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                            {
                                this.state = 1304;
                                this.expressionList();
                            }
                        }
                        this.state = 1307;
                        this.match(JavaParser.RPAREN);
                    }
                    break;
                case JavaParser.SUPER:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 1308;
                        this.match(JavaParser.SUPER);
                        this.state = 1309;
                        this.match(JavaParser.LPAREN);
                        this.state = 1311;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819637544) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154824703) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                            {
                                this.state = 1310;
                                this.expressionList();
                            }
                        }
                        this.state = 1313;
                        this.match(JavaParser.RPAREN);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    expression(_p) {
        if (_p === undefined) {
            _p = 0;
        }
        let parentContext = this.context;
        let parentState = this.state;
        let localContext = new ExpressionContext(this.context, parentState);
        let previousContext = localContext;
        let _startState = 194;
        this.enterRecursionRule(localContext, 194, JavaParser.RULE_expression, _p);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1361;
                this.errorHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this.tokenStream, 163, this.context)) {
                    case 1:
                        {
                            this.state = 1317;
                            this.primary();
                        }
                        break;
                    case 2:
                        {
                            this.state = 1318;
                            this.methodCall();
                        }
                        break;
                    case 3:
                        {
                            this.state = 1319;
                            this.match(JavaParser.NEW);
                            this.state = 1320;
                            this.creator();
                        }
                        break;
                    case 4:
                        {
                            this.state = 1321;
                            this.match(JavaParser.LPAREN);
                            this.state = 1325;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 158, this.context);
                            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                                if (alternative === 1) {
                                    {
                                        {
                                            this.state = 1322;
                                            this.annotation();
                                        }
                                    }
                                }
                                this.state = 1327;
                                this.errorHandler.sync(this);
                                alternative = this.interpreter.adaptivePredict(this.tokenStream, 158, this.context);
                            }
                            this.state = 1328;
                            this.typeType();
                            this.state = 1333;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                            while (_la === 106) {
                                {
                                    {
                                        this.state = 1329;
                                        this.match(JavaParser.BITAND);
                                        this.state = 1330;
                                        this.typeType();
                                    }
                                }
                                this.state = 1335;
                                this.errorHandler.sync(this);
                                _la = this.tokenStream.LA(1);
                            }
                            this.state = 1336;
                            this.match(JavaParser.RPAREN);
                            this.state = 1337;
                            this.expression(22);
                        }
                        break;
                    case 5:
                        {
                            this.state = 1339;
                            localContext._prefix = this.tokenStream.LT(1);
                            _la = this.tokenStream.LA(1);
                            if (!(((((_la - 100)) & ~0x1F) === 0 && ((1 << (_la - 100)) & 15) !== 0))) {
                                localContext._prefix = this.errorHandler.recoverInline(this);
                            }
                            else {
                                this.errorHandler.reportMatch(this);
                                this.consume();
                            }
                            this.state = 1340;
                            this.expression(20);
                        }
                        break;
                    case 6:
                        {
                            this.state = 1341;
                            localContext._prefix = this.tokenStream.LT(1);
                            _la = this.tokenStream.LA(1);
                            if (!(_la === 90 || _la === 91)) {
                                localContext._prefix = this.errorHandler.recoverInline(this);
                            }
                            else {
                                this.errorHandler.reportMatch(this);
                                this.consume();
                            }
                            this.state = 1342;
                            this.expression(19);
                        }
                        break;
                    case 7:
                        {
                            this.state = 1343;
                            this.lambdaExpression();
                        }
                        break;
                    case 8:
                        {
                            this.state = 1344;
                            this.switchExpression();
                        }
                        break;
                    case 9:
                        {
                            this.state = 1345;
                            this.typeType();
                            this.state = 1346;
                            this.match(JavaParser.COLONCOLON);
                            this.state = 1352;
                            this.errorHandler.sync(this);
                            switch (this.tokenStream.LA(1)) {
                                case JavaParser.MODULE:
                                case JavaParser.OPEN:
                                case JavaParser.REQUIRES:
                                case JavaParser.EXPORTS:
                                case JavaParser.OPENS:
                                case JavaParser.TO:
                                case JavaParser.USES:
                                case JavaParser.PROVIDES:
                                case JavaParser.WITH:
                                case JavaParser.TRANSITIVE:
                                case JavaParser.VAR:
                                case JavaParser.YIELD:
                                case JavaParser.RECORD:
                                case JavaParser.SEALED:
                                case JavaParser.PERMITS:
                                case JavaParser.LT:
                                case JavaParser.IDENTIFIER:
                                    {
                                        this.state = 1348;
                                        this.errorHandler.sync(this);
                                        _la = this.tokenStream.LA(1);
                                        if (_la === 89) {
                                            {
                                                this.state = 1347;
                                                this.typeArguments();
                                            }
                                        }
                                        this.state = 1350;
                                        this.identifier();
                                    }
                                    break;
                                case JavaParser.NEW:
                                    {
                                        this.state = 1351;
                                        this.match(JavaParser.NEW);
                                    }
                                    break;
                                default:
                                    throw new antlr.NoViableAltException(this);
                            }
                        }
                        break;
                    case 10:
                        {
                            this.state = 1354;
                            this.classType();
                            this.state = 1355;
                            this.match(JavaParser.COLONCOLON);
                            this.state = 1357;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                            if (_la === 89) {
                                {
                                    this.state = 1356;
                                    this.typeArguments();
                                }
                            }
                            this.state = 1359;
                            this.match(JavaParser.NEW);
                        }
                        break;
                }
                this.context.stop = this.tokenStream.LT(-1);
                this.state = 1446;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 170, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        if (this._parseListeners != null) {
                            this.triggerExitRuleEvent();
                        }
                        previousContext = localContext;
                        {
                            this.state = 1444;
                            this.errorHandler.sync(this);
                            switch (this.interpreter.adaptivePredict(this.tokenStream, 169, this.context)) {
                                case 1:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1363;
                                        if (!(this.precpred(this.context, 18))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 18)");
                                        }
                                        this.state = 1364;
                                        localContext._bop = this.tokenStream.LT(1);
                                        _la = this.tokenStream.LA(1);
                                        if (!(((((_la - 104)) & ~0x1F) === 0 && ((1 << (_la - 104)) & 35) !== 0))) {
                                            localContext._bop = this.errorHandler.recoverInline(this);
                                        }
                                        else {
                                            this.errorHandler.reportMatch(this);
                                            this.consume();
                                        }
                                        this.state = 1365;
                                        this.expression(19);
                                    }
                                    break;
                                case 2:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1366;
                                        if (!(this.precpred(this.context, 17))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 17)");
                                        }
                                        this.state = 1367;
                                        localContext._bop = this.tokenStream.LT(1);
                                        _la = this.tokenStream.LA(1);
                                        if (!(_la === 102 || _la === 103)) {
                                            localContext._bop = this.errorHandler.recoverInline(this);
                                        }
                                        else {
                                            this.errorHandler.reportMatch(this);
                                            this.consume();
                                        }
                                        this.state = 1368;
                                        this.expression(18);
                                    }
                                    break;
                                case 3:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1369;
                                        if (!(this.precpred(this.context, 16))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 16)");
                                        }
                                        this.state = 1377;
                                        this.errorHandler.sync(this);
                                        switch (this.interpreter.adaptivePredict(this.tokenStream, 164, this.context)) {
                                            case 1:
                                                {
                                                    this.state = 1370;
                                                    this.match(JavaParser.LT);
                                                    this.state = 1371;
                                                    this.match(JavaParser.LT);
                                                }
                                                break;
                                            case 2:
                                                {
                                                    this.state = 1372;
                                                    this.match(JavaParser.GT);
                                                    this.state = 1373;
                                                    this.match(JavaParser.GT);
                                                    this.state = 1374;
                                                    this.match(JavaParser.GT);
                                                }
                                                break;
                                            case 3:
                                                {
                                                    this.state = 1375;
                                                    this.match(JavaParser.GT);
                                                    this.state = 1376;
                                                    this.match(JavaParser.GT);
                                                }
                                                break;
                                        }
                                        this.state = 1379;
                                        this.expression(17);
                                    }
                                    break;
                                case 4:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1380;
                                        if (!(this.precpred(this.context, 15))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 15)");
                                        }
                                        this.state = 1381;
                                        localContext._bop = this.tokenStream.LT(1);
                                        _la = this.tokenStream.LA(1);
                                        if (!(((((_la - 88)) & ~0x1F) === 0 && ((1 << (_la - 88)) & 387) !== 0))) {
                                            localContext._bop = this.errorHandler.recoverInline(this);
                                        }
                                        else {
                                            this.errorHandler.reportMatch(this);
                                            this.consume();
                                        }
                                        this.state = 1382;
                                        this.expression(16);
                                    }
                                    break;
                                case 5:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1383;
                                        if (!(this.precpred(this.context, 13))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 13)");
                                        }
                                        this.state = 1384;
                                        localContext._bop = this.tokenStream.LT(1);
                                        _la = this.tokenStream.LA(1);
                                        if (!(_la === 94 || _la === 97)) {
                                            localContext._bop = this.errorHandler.recoverInline(this);
                                        }
                                        else {
                                            this.errorHandler.reportMatch(this);
                                            this.consume();
                                        }
                                        this.state = 1385;
                                        this.expression(14);
                                    }
                                    break;
                                case 6:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1386;
                                        if (!(this.precpred(this.context, 12))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 12)");
                                        }
                                        this.state = 1387;
                                        localContext._bop = this.match(JavaParser.BITAND);
                                        this.state = 1388;
                                        this.expression(13);
                                    }
                                    break;
                                case 7:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1389;
                                        if (!(this.precpred(this.context, 11))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 11)");
                                        }
                                        this.state = 1390;
                                        localContext._bop = this.match(JavaParser.CARET);
                                        this.state = 1391;
                                        this.expression(12);
                                    }
                                    break;
                                case 8:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1392;
                                        if (!(this.precpred(this.context, 10))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 10)");
                                        }
                                        this.state = 1393;
                                        localContext._bop = this.match(JavaParser.BITOR);
                                        this.state = 1394;
                                        this.expression(11);
                                    }
                                    break;
                                case 9:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1395;
                                        if (!(this.precpred(this.context, 9))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 9)");
                                        }
                                        this.state = 1396;
                                        localContext._bop = this.match(JavaParser.AND);
                                        this.state = 1397;
                                        this.expression(10);
                                    }
                                    break;
                                case 10:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1398;
                                        if (!(this.precpred(this.context, 8))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 8)");
                                        }
                                        this.state = 1399;
                                        localContext._bop = this.match(JavaParser.OR);
                                        this.state = 1400;
                                        this.expression(9);
                                    }
                                    break;
                                case 11:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1401;
                                        if (!(this.precpred(this.context, 7))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 7)");
                                        }
                                        this.state = 1402;
                                        localContext._bop = this.match(JavaParser.QUESTION);
                                        this.state = 1403;
                                        this.expression(0);
                                        this.state = 1404;
                                        this.match(JavaParser.COLON);
                                        this.state = 1405;
                                        this.expression(7);
                                    }
                                    break;
                                case 12:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1407;
                                        if (!(this.precpred(this.context, 6))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 6)");
                                        }
                                        this.state = 1408;
                                        localContext._bop = this.tokenStream.LT(1);
                                        _la = this.tokenStream.LA(1);
                                        if (!(((((_la - 87)) & ~0x1F) === 0 && ((1 << (_la - 87)) & 4286578689) !== 0) || _la === 119 || _la === 120)) {
                                            localContext._bop = this.errorHandler.recoverInline(this);
                                        }
                                        else {
                                            this.errorHandler.reportMatch(this);
                                            this.consume();
                                        }
                                        this.state = 1409;
                                        this.expression(6);
                                    }
                                    break;
                                case 13:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1410;
                                        if (!(this.precpred(this.context, 26))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 26)");
                                        }
                                        this.state = 1411;
                                        localContext._bop = this.match(JavaParser.DOT);
                                        this.state = 1423;
                                        this.errorHandler.sync(this);
                                        switch (this.interpreter.adaptivePredict(this.tokenStream, 166, this.context)) {
                                            case 1:
                                                {
                                                    this.state = 1412;
                                                    this.identifier();
                                                }
                                                break;
                                            case 2:
                                                {
                                                    this.state = 1413;
                                                    this.methodCall();
                                                }
                                                break;
                                            case 3:
                                                {
                                                    this.state = 1414;
                                                    this.match(JavaParser.THIS);
                                                }
                                                break;
                                            case 4:
                                                {
                                                    this.state = 1415;
                                                    this.match(JavaParser.NEW);
                                                    this.state = 1417;
                                                    this.errorHandler.sync(this);
                                                    _la = this.tokenStream.LA(1);
                                                    if (_la === 89) {
                                                        {
                                                            this.state = 1416;
                                                            this.nonWildcardTypeArguments();
                                                        }
                                                    }
                                                    this.state = 1419;
                                                    this.innerCreator();
                                                }
                                                break;
                                            case 5:
                                                {
                                                    this.state = 1420;
                                                    this.match(JavaParser.SUPER);
                                                    this.state = 1421;
                                                    this.superSuffix();
                                                }
                                                break;
                                            case 6:
                                                {
                                                    this.state = 1422;
                                                    this.explicitGenericInvocation();
                                                }
                                                break;
                                        }
                                    }
                                    break;
                                case 14:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1425;
                                        if (!(this.precpred(this.context, 25))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 25)");
                                        }
                                        this.state = 1426;
                                        this.match(JavaParser.LBRACK);
                                        this.state = 1427;
                                        this.expression(0);
                                        this.state = 1428;
                                        this.match(JavaParser.RBRACK);
                                    }
                                    break;
                                case 15:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1430;
                                        if (!(this.precpred(this.context, 21))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 21)");
                                        }
                                        this.state = 1431;
                                        localContext._postfix = this.tokenStream.LT(1);
                                        _la = this.tokenStream.LA(1);
                                        if (!(_la === 100 || _la === 101)) {
                                            localContext._postfix = this.errorHandler.recoverInline(this);
                                        }
                                        else {
                                            this.errorHandler.reportMatch(this);
                                            this.consume();
                                        }
                                    }
                                    break;
                                case 16:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1432;
                                        if (!(this.precpred(this.context, 14))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 14)");
                                        }
                                        this.state = 1433;
                                        localContext._bop = this.match(JavaParser.INSTANCEOF);
                                        this.state = 1436;
                                        this.errorHandler.sync(this);
                                        switch (this.interpreter.adaptivePredict(this.tokenStream, 167, this.context)) {
                                            case 1:
                                                {
                                                    this.state = 1434;
                                                    this.typeType();
                                                }
                                                break;
                                            case 2:
                                                {
                                                    this.state = 1435;
                                                    this.pattern();
                                                }
                                                break;
                                        }
                                    }
                                    break;
                                case 17:
                                    {
                                        localContext = new ExpressionContext(parentContext, parentState);
                                        this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_expression);
                                        this.state = 1438;
                                        if (!(this.precpred(this.context, 3))) {
                                            throw this.createFailedPredicateException("this.precpred(this.context, 3)");
                                        }
                                        this.state = 1439;
                                        this.match(JavaParser.COLONCOLON);
                                        this.state = 1441;
                                        this.errorHandler.sync(this);
                                        _la = this.tokenStream.LA(1);
                                        if (_la === 89) {
                                            {
                                                this.state = 1440;
                                                this.typeArguments();
                                            }
                                        }
                                        this.state = 1443;
                                        this.identifier();
                                    }
                                    break;
                            }
                        }
                    }
                    this.state = 1448;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 170, this.context);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.unrollRecursionContexts(parentContext);
        }
        return localContext;
    }
    pattern() {
        let localContext = new PatternContext(this.context, this.state);
        this.enterRule(localContext, 196, JavaParser.RULE_pattern);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1452;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 171, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 1449;
                                this.variableModifier();
                            }
                        }
                    }
                    this.state = 1454;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 171, this.context);
                }
                this.state = 1455;
                this.typeType();
                this.state = 1459;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 172, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 1456;
                                this.annotation();
                            }
                        }
                    }
                    this.state = 1461;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 172, this.context);
                }
                this.state = 1462;
                this.identifier();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    lambdaExpression() {
        let localContext = new LambdaExpressionContext(this.context, this.state);
        this.enterRule(localContext, 198, JavaParser.RULE_lambdaExpression);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1464;
                this.lambdaParameters();
                this.state = 1465;
                this.match(JavaParser.ARROW);
                this.state = 1466;
                this.lambdaBody();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    lambdaParameters() {
        let localContext = new LambdaParametersContext(this.context, this.state);
        this.enterRule(localContext, 200, JavaParser.RULE_lambdaParameters);
        let _la;
        try {
            this.state = 1490;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 176, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1468;
                        this.identifier();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1469;
                        this.match(JavaParser.LPAREN);
                        this.state = 1471;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 672416040) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 536854529) !== 0) || _la === 123 || _la === 128) {
                            {
                                this.state = 1470;
                                this.formalParameterList();
                            }
                        }
                        this.state = 1473;
                        this.match(JavaParser.RPAREN);
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 1474;
                        this.match(JavaParser.LPAREN);
                        this.state = 1475;
                        this.identifier();
                        this.state = 1480;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 85) {
                            {
                                {
                                    this.state = 1476;
                                    this.match(JavaParser.COMMA);
                                    this.state = 1477;
                                    this.identifier();
                                }
                            }
                            this.state = 1482;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 1483;
                        this.match(JavaParser.RPAREN);
                    }
                    break;
                case 4:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 1485;
                        this.match(JavaParser.LPAREN);
                        this.state = 1487;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 18 || ((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 123 || _la === 128) {
                            {
                                this.state = 1486;
                                this.lambdaLVTIList();
                            }
                        }
                        this.state = 1489;
                        this.match(JavaParser.RPAREN);
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    lambdaBody() {
        let localContext = new LambdaBodyContext(this.context, this.state);
        this.enterRule(localContext, 202, JavaParser.RULE_lambdaBody);
        try {
            this.state = 1494;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.BOOLEAN:
                case JavaParser.BYTE:
                case JavaParser.CHAR:
                case JavaParser.DOUBLE:
                case JavaParser.FLOAT:
                case JavaParser.INT:
                case JavaParser.LONG:
                case JavaParser.NEW:
                case JavaParser.SHORT:
                case JavaParser.SUPER:
                case JavaParser.SWITCH:
                case JavaParser.THIS:
                case JavaParser.VOID:
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.DECIMAL_LITERAL:
                case JavaParser.HEX_LITERAL:
                case JavaParser.OCT_LITERAL:
                case JavaParser.BINARY_LITERAL:
                case JavaParser.FLOAT_LITERAL:
                case JavaParser.HEX_FLOAT_LITERAL:
                case JavaParser.BOOL_LITERAL:
                case JavaParser.CHAR_LITERAL:
                case JavaParser.STRING_LITERAL:
                case JavaParser.TEXT_BLOCK:
                case JavaParser.NULL_LITERAL:
                case JavaParser.LPAREN:
                case JavaParser.LT:
                case JavaParser.BANG:
                case JavaParser.TILDE:
                case JavaParser.INC:
                case JavaParser.DEC:
                case JavaParser.ADD:
                case JavaParser.SUB:
                case JavaParser.AT:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1492;
                        this.expression(0);
                    }
                    break;
                case JavaParser.LBRACE:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1493;
                        this.block();
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    primary() {
        let localContext = new PrimaryContext(this.context, this.state);
        this.enterRule(localContext, 204, JavaParser.RULE_primary);
        try {
            this.state = 1514;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 179, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1496;
                        this.match(JavaParser.LPAREN);
                        this.state = 1497;
                        this.expression(0);
                        this.state = 1498;
                        this.match(JavaParser.RPAREN);
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1500;
                        this.match(JavaParser.THIS);
                    }
                    break;
                case 3:
                    this.enterOuterAlt(localContext, 3);
                    {
                        this.state = 1501;
                        this.match(JavaParser.SUPER);
                    }
                    break;
                case 4:
                    this.enterOuterAlt(localContext, 4);
                    {
                        this.state = 1502;
                        this.literal();
                    }
                    break;
                case 5:
                    this.enterOuterAlt(localContext, 5);
                    {
                        this.state = 1503;
                        this.identifier();
                    }
                    break;
                case 6:
                    this.enterOuterAlt(localContext, 6);
                    {
                        this.state = 1504;
                        this.typeTypeOrVoid();
                        this.state = 1505;
                        this.match(JavaParser.DOT);
                        this.state = 1506;
                        this.match(JavaParser.CLASS);
                    }
                    break;
                case 7:
                    this.enterOuterAlt(localContext, 7);
                    {
                        this.state = 1508;
                        this.nonWildcardTypeArguments();
                        this.state = 1512;
                        this.errorHandler.sync(this);
                        switch (this.tokenStream.LA(1)) {
                            case JavaParser.SUPER:
                            case JavaParser.MODULE:
                            case JavaParser.OPEN:
                            case JavaParser.REQUIRES:
                            case JavaParser.EXPORTS:
                            case JavaParser.OPENS:
                            case JavaParser.TO:
                            case JavaParser.USES:
                            case JavaParser.PROVIDES:
                            case JavaParser.WITH:
                            case JavaParser.TRANSITIVE:
                            case JavaParser.VAR:
                            case JavaParser.YIELD:
                            case JavaParser.RECORD:
                            case JavaParser.SEALED:
                            case JavaParser.PERMITS:
                            case JavaParser.IDENTIFIER:
                                {
                                    this.state = 1509;
                                    this.explicitGenericInvocationSuffix();
                                }
                                break;
                            case JavaParser.THIS:
                                {
                                    this.state = 1510;
                                    this.match(JavaParser.THIS);
                                    this.state = 1511;
                                    this.arguments();
                                }
                                break;
                            default:
                                throw new antlr.NoViableAltException(this);
                        }
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    switchExpression() {
        let localContext = new SwitchExpressionContext(this.context, this.state);
        this.enterRule(localContext, 206, JavaParser.RULE_switchExpression);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1516;
                this.match(JavaParser.SWITCH);
                this.state = 1517;
                this.parExpression();
                this.state = 1518;
                this.match(JavaParser.LBRACE);
                this.state = 1522;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 6 || _la === 12) {
                    {
                        {
                            this.state = 1519;
                            this.switchLabeledRule();
                        }
                    }
                    this.state = 1524;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 1525;
                this.match(JavaParser.RBRACE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    switchLabeledRule() {
        let localContext = new SwitchLabeledRuleContext(this.context, this.state);
        this.enterRule(localContext, 208, JavaParser.RULE_switchLabeledRule);
        let _la;
        try {
            this.state = 1538;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.CASE:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1527;
                        this.match(JavaParser.CASE);
                        this.state = 1531;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 181, this.context)) {
                            case 1:
                                {
                                    this.state = 1528;
                                    this.expressionList();
                                }
                                break;
                            case 2:
                                {
                                    this.state = 1529;
                                    this.match(JavaParser.NULL_LITERAL);
                                }
                                break;
                            case 3:
                                {
                                    this.state = 1530;
                                    this.guardedPattern(0);
                                }
                                break;
                        }
                        this.state = 1533;
                        _la = this.tokenStream.LA(1);
                        if (!(_la === 93 || _la === 121)) {
                            this.errorHandler.recoverInline(this);
                        }
                        else {
                            this.errorHandler.reportMatch(this);
                            this.consume();
                        }
                        this.state = 1534;
                        this.switchRuleOutcome();
                    }
                    break;
                case JavaParser.DEFAULT:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1535;
                        this.match(JavaParser.DEFAULT);
                        this.state = 1536;
                        _la = this.tokenStream.LA(1);
                        if (!(_la === 93 || _la === 121)) {
                            this.errorHandler.recoverInline(this);
                        }
                        else {
                            this.errorHandler.reportMatch(this);
                            this.consume();
                        }
                        this.state = 1537;
                        this.switchRuleOutcome();
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    guardedPattern(_p) {
        if (_p === undefined) {
            _p = 0;
        }
        let parentContext = this.context;
        let parentState = this.state;
        let localContext = new GuardedPatternContext(this.context, parentState);
        let previousContext = localContext;
        let _startState = 210;
        this.enterRecursionRule(localContext, 210, JavaParser.RULE_guardedPattern, _p);
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1566;
                this.errorHandler.sync(this);
                switch (this.tokenStream.LA(1)) {
                    case JavaParser.LPAREN:
                        {
                            this.state = 1541;
                            this.match(JavaParser.LPAREN);
                            this.state = 1542;
                            this.guardedPattern(0);
                            this.state = 1543;
                            this.match(JavaParser.RPAREN);
                        }
                        break;
                    case JavaParser.BOOLEAN:
                    case JavaParser.BYTE:
                    case JavaParser.CHAR:
                    case JavaParser.DOUBLE:
                    case JavaParser.FINAL:
                    case JavaParser.FLOAT:
                    case JavaParser.INT:
                    case JavaParser.LONG:
                    case JavaParser.SHORT:
                    case JavaParser.MODULE:
                    case JavaParser.OPEN:
                    case JavaParser.REQUIRES:
                    case JavaParser.EXPORTS:
                    case JavaParser.OPENS:
                    case JavaParser.TO:
                    case JavaParser.USES:
                    case JavaParser.PROVIDES:
                    case JavaParser.WITH:
                    case JavaParser.TRANSITIVE:
                    case JavaParser.VAR:
                    case JavaParser.YIELD:
                    case JavaParser.RECORD:
                    case JavaParser.SEALED:
                    case JavaParser.PERMITS:
                    case JavaParser.AT:
                    case JavaParser.IDENTIFIER:
                        {
                            this.state = 1548;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 183, this.context);
                            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                                if (alternative === 1) {
                                    {
                                        {
                                            this.state = 1545;
                                            this.variableModifier();
                                        }
                                    }
                                }
                                this.state = 1550;
                                this.errorHandler.sync(this);
                                alternative = this.interpreter.adaptivePredict(this.tokenStream, 183, this.context);
                            }
                            this.state = 1551;
                            this.typeType();
                            this.state = 1555;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 184, this.context);
                            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                                if (alternative === 1) {
                                    {
                                        {
                                            this.state = 1552;
                                            this.annotation();
                                        }
                                    }
                                }
                                this.state = 1557;
                                this.errorHandler.sync(this);
                                alternative = this.interpreter.adaptivePredict(this.tokenStream, 184, this.context);
                            }
                            this.state = 1558;
                            this.identifier();
                            this.state = 1563;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 185, this.context);
                            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                                if (alternative === 1) {
                                    {
                                        {
                                            this.state = 1559;
                                            this.match(JavaParser.AND);
                                            this.state = 1560;
                                            this.expression(0);
                                        }
                                    }
                                }
                                this.state = 1565;
                                this.errorHandler.sync(this);
                                alternative = this.interpreter.adaptivePredict(this.tokenStream, 185, this.context);
                            }
                        }
                        break;
                    default:
                        throw new antlr.NoViableAltException(this);
                }
                this.context.stop = this.tokenStream.LT(-1);
                this.state = 1573;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 187, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        if (this._parseListeners != null) {
                            this.triggerExitRuleEvent();
                        }
                        previousContext = localContext;
                        {
                            {
                                localContext = new GuardedPatternContext(parentContext, parentState);
                                this.pushNewRecursionContext(localContext, _startState, JavaParser.RULE_guardedPattern);
                                this.state = 1568;
                                if (!(this.precpred(this.context, 1))) {
                                    throw this.createFailedPredicateException("this.precpred(this.context, 1)");
                                }
                                this.state = 1569;
                                this.match(JavaParser.AND);
                                this.state = 1570;
                                this.expression(0);
                            }
                        }
                    }
                    this.state = 1575;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 187, this.context);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.unrollRecursionContexts(parentContext);
        }
        return localContext;
    }
    switchRuleOutcome() {
        let localContext = new SwitchRuleOutcomeContext(this.context, this.state);
        this.enterRule(localContext, 212, JavaParser.RULE_switchRuleOutcome);
        let _la;
        try {
            this.state = 1583;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 189, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1576;
                        this.block();
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1580;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 3094637374) !== 0) || ((((_la - 33)) & ~0x1F) === 0 && ((1 << (_la - 33)) & 4294889471) !== 0) || ((((_la - 65)) & ~0x1F) === 0 && ((1 << (_la - 65)) & 118013951) !== 0) || ((((_la - 100)) & ~0x1F) === 0 && ((1 << (_la - 100)) & 276824079) !== 0)) {
                            {
                                {
                                    this.state = 1577;
                                    this.blockStatement();
                                }
                            }
                            this.state = 1582;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    classType() {
        let localContext = new ClassTypeContext(this.context, this.state);
        this.enterRule(localContext, 214, JavaParser.RULE_classType);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1588;
                this.errorHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this.tokenStream, 190, this.context)) {
                    case 1:
                        {
                            this.state = 1585;
                            this.classOrInterfaceType();
                            this.state = 1586;
                            this.match(JavaParser.DOT);
                        }
                        break;
                }
                this.state = 1593;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 191, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 1590;
                                this.annotation();
                            }
                        }
                    }
                    this.state = 1595;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 191, this.context);
                }
                this.state = 1596;
                this.identifier();
                this.state = 1598;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 89) {
                    {
                        this.state = 1597;
                        this.typeArguments();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    creator() {
        let localContext = new CreatorContext(this.context, this.state);
        this.enterRule(localContext, 216, JavaParser.RULE_creator);
        try {
            this.state = 1609;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.LT:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1600;
                        this.nonWildcardTypeArguments();
                        this.state = 1601;
                        this.createdName();
                        this.state = 1602;
                        this.classCreatorRest();
                    }
                    break;
                case JavaParser.BOOLEAN:
                case JavaParser.BYTE:
                case JavaParser.CHAR:
                case JavaParser.DOUBLE:
                case JavaParser.FLOAT:
                case JavaParser.INT:
                case JavaParser.LONG:
                case JavaParser.SHORT:
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1604;
                        this.createdName();
                        this.state = 1607;
                        this.errorHandler.sync(this);
                        switch (this.tokenStream.LA(1)) {
                            case JavaParser.LBRACK:
                                {
                                    this.state = 1605;
                                    this.arrayCreatorRest();
                                }
                                break;
                            case JavaParser.LPAREN:
                                {
                                    this.state = 1606;
                                    this.classCreatorRest();
                                }
                                break;
                            default:
                                throw new antlr.NoViableAltException(this);
                        }
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    createdName() {
        let localContext = new CreatedNameContext(this.context, this.state);
        this.enterRule(localContext, 218, JavaParser.RULE_createdName);
        let _la;
        try {
            this.state = 1626;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1611;
                        this.identifier();
                        this.state = 1613;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 89) {
                            {
                                this.state = 1612;
                                this.typeArgumentsOrDiamond();
                            }
                        }
                        this.state = 1622;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 86) {
                            {
                                {
                                    this.state = 1615;
                                    this.match(JavaParser.DOT);
                                    this.state = 1616;
                                    this.identifier();
                                    this.state = 1618;
                                    this.errorHandler.sync(this);
                                    _la = this.tokenStream.LA(1);
                                    if (_la === 89) {
                                        {
                                            this.state = 1617;
                                            this.typeArgumentsOrDiamond();
                                        }
                                    }
                                }
                            }
                            this.state = 1624;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                    }
                    break;
                case JavaParser.BOOLEAN:
                case JavaParser.BYTE:
                case JavaParser.CHAR:
                case JavaParser.DOUBLE:
                case JavaParser.FLOAT:
                case JavaParser.INT:
                case JavaParser.LONG:
                case JavaParser.SHORT:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1625;
                        this.primitiveType();
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    innerCreator() {
        let localContext = new InnerCreatorContext(this.context, this.state);
        this.enterRule(localContext, 220, JavaParser.RULE_innerCreator);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1628;
                this.identifier();
                this.state = 1630;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 89) {
                    {
                        this.state = 1629;
                        this.nonWildcardTypeArgumentsOrDiamond();
                    }
                }
                this.state = 1632;
                this.classCreatorRest();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    arrayCreatorRest() {
        let localContext = new ArrayCreatorRestContext(this.context, this.state);
        this.enterRule(localContext, 222, JavaParser.RULE_arrayCreatorRest);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1634;
                this.match(JavaParser.LBRACK);
                this.state = 1662;
                this.errorHandler.sync(this);
                switch (this.tokenStream.LA(1)) {
                    case JavaParser.RBRACK:
                        {
                            this.state = 1635;
                            this.match(JavaParser.RBRACK);
                            this.state = 1640;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                            while (_la === 82) {
                                {
                                    {
                                        this.state = 1636;
                                        this.match(JavaParser.LBRACK);
                                        this.state = 1637;
                                        this.match(JavaParser.RBRACK);
                                    }
                                }
                                this.state = 1642;
                                this.errorHandler.sync(this);
                                _la = this.tokenStream.LA(1);
                            }
                            this.state = 1643;
                            this.arrayInitializer();
                        }
                        break;
                    case JavaParser.BOOLEAN:
                    case JavaParser.BYTE:
                    case JavaParser.CHAR:
                    case JavaParser.DOUBLE:
                    case JavaParser.FLOAT:
                    case JavaParser.INT:
                    case JavaParser.LONG:
                    case JavaParser.NEW:
                    case JavaParser.SHORT:
                    case JavaParser.SUPER:
                    case JavaParser.SWITCH:
                    case JavaParser.THIS:
                    case JavaParser.VOID:
                    case JavaParser.MODULE:
                    case JavaParser.OPEN:
                    case JavaParser.REQUIRES:
                    case JavaParser.EXPORTS:
                    case JavaParser.OPENS:
                    case JavaParser.TO:
                    case JavaParser.USES:
                    case JavaParser.PROVIDES:
                    case JavaParser.WITH:
                    case JavaParser.TRANSITIVE:
                    case JavaParser.VAR:
                    case JavaParser.YIELD:
                    case JavaParser.RECORD:
                    case JavaParser.SEALED:
                    case JavaParser.PERMITS:
                    case JavaParser.DECIMAL_LITERAL:
                    case JavaParser.HEX_LITERAL:
                    case JavaParser.OCT_LITERAL:
                    case JavaParser.BINARY_LITERAL:
                    case JavaParser.FLOAT_LITERAL:
                    case JavaParser.HEX_FLOAT_LITERAL:
                    case JavaParser.BOOL_LITERAL:
                    case JavaParser.CHAR_LITERAL:
                    case JavaParser.STRING_LITERAL:
                    case JavaParser.TEXT_BLOCK:
                    case JavaParser.NULL_LITERAL:
                    case JavaParser.LPAREN:
                    case JavaParser.LT:
                    case JavaParser.BANG:
                    case JavaParser.TILDE:
                    case JavaParser.INC:
                    case JavaParser.DEC:
                    case JavaParser.ADD:
                    case JavaParser.SUB:
                    case JavaParser.AT:
                    case JavaParser.IDENTIFIER:
                        {
                            this.state = 1644;
                            this.expression(0);
                            this.state = 1645;
                            this.match(JavaParser.RBRACK);
                            this.state = 1652;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 201, this.context);
                            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                                if (alternative === 1) {
                                    {
                                        {
                                            this.state = 1646;
                                            this.match(JavaParser.LBRACK);
                                            this.state = 1647;
                                            this.expression(0);
                                            this.state = 1648;
                                            this.match(JavaParser.RBRACK);
                                        }
                                    }
                                }
                                this.state = 1654;
                                this.errorHandler.sync(this);
                                alternative = this.interpreter.adaptivePredict(this.tokenStream, 201, this.context);
                            }
                            this.state = 1659;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 202, this.context);
                            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                                if (alternative === 1) {
                                    {
                                        {
                                            this.state = 1655;
                                            this.match(JavaParser.LBRACK);
                                            this.state = 1656;
                                            this.match(JavaParser.RBRACK);
                                        }
                                    }
                                }
                                this.state = 1661;
                                this.errorHandler.sync(this);
                                alternative = this.interpreter.adaptivePredict(this.tokenStream, 202, this.context);
                            }
                        }
                        break;
                    default:
                        throw new antlr.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    classCreatorRest() {
        let localContext = new ClassCreatorRestContext(this.context, this.state);
        this.enterRule(localContext, 224, JavaParser.RULE_classCreatorRest);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1664;
                this.arguments();
                this.state = 1666;
                this.errorHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this.tokenStream, 204, this.context)) {
                    case 1:
                        {
                            this.state = 1665;
                            this.classBody();
                        }
                        break;
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    explicitGenericInvocation() {
        let localContext = new ExplicitGenericInvocationContext(this.context, this.state);
        this.enterRule(localContext, 226, JavaParser.RULE_explicitGenericInvocation);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1668;
                this.nonWildcardTypeArguments();
                this.state = 1669;
                this.explicitGenericInvocationSuffix();
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeArgumentsOrDiamond() {
        let localContext = new TypeArgumentsOrDiamondContext(this.context, this.state);
        this.enterRule(localContext, 228, JavaParser.RULE_typeArgumentsOrDiamond);
        try {
            this.state = 1674;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 205, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1671;
                        this.match(JavaParser.LT);
                        this.state = 1672;
                        this.match(JavaParser.GT);
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1673;
                        this.typeArguments();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    nonWildcardTypeArgumentsOrDiamond() {
        let localContext = new NonWildcardTypeArgumentsOrDiamondContext(this.context, this.state);
        this.enterRule(localContext, 230, JavaParser.RULE_nonWildcardTypeArgumentsOrDiamond);
        try {
            this.state = 1679;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 206, this.context)) {
                case 1:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1676;
                        this.match(JavaParser.LT);
                        this.state = 1677;
                        this.match(JavaParser.GT);
                    }
                    break;
                case 2:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1678;
                        this.nonWildcardTypeArguments();
                    }
                    break;
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    nonWildcardTypeArguments() {
        let localContext = new NonWildcardTypeArgumentsContext(this.context, this.state);
        this.enterRule(localContext, 232, JavaParser.RULE_nonWildcardTypeArguments);
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1681;
                this.match(JavaParser.LT);
                this.state = 1682;
                this.typeList();
                this.state = 1683;
                this.match(JavaParser.GT);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeList() {
        let localContext = new TypeListContext(this.context, this.state);
        this.enterRule(localContext, 234, JavaParser.RULE_typeList);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1685;
                this.typeType();
                this.state = 1690;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 1686;
                            this.match(JavaParser.COMMA);
                            this.state = 1687;
                            this.typeType();
                        }
                    }
                    this.state = 1692;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeType() {
        let localContext = new TypeTypeContext(this.context, this.state);
        this.enterRule(localContext, 236, JavaParser.RULE_typeType);
        let _la;
        try {
            let alternative;
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1696;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 208, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 1693;
                                this.annotation();
                            }
                        }
                    }
                    this.state = 1698;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 208, this.context);
                }
                this.state = 1701;
                this.errorHandler.sync(this);
                switch (this.tokenStream.LA(1)) {
                    case JavaParser.MODULE:
                    case JavaParser.OPEN:
                    case JavaParser.REQUIRES:
                    case JavaParser.EXPORTS:
                    case JavaParser.OPENS:
                    case JavaParser.TO:
                    case JavaParser.USES:
                    case JavaParser.PROVIDES:
                    case JavaParser.WITH:
                    case JavaParser.TRANSITIVE:
                    case JavaParser.VAR:
                    case JavaParser.YIELD:
                    case JavaParser.RECORD:
                    case JavaParser.SEALED:
                    case JavaParser.PERMITS:
                    case JavaParser.IDENTIFIER:
                        {
                            this.state = 1699;
                            this.classOrInterfaceType();
                        }
                        break;
                    case JavaParser.BOOLEAN:
                    case JavaParser.BYTE:
                    case JavaParser.CHAR:
                    case JavaParser.DOUBLE:
                    case JavaParser.FLOAT:
                    case JavaParser.INT:
                    case JavaParser.LONG:
                    case JavaParser.SHORT:
                        {
                            this.state = 1700;
                            this.primitiveType();
                        }
                        break;
                    default:
                        throw new antlr.NoViableAltException(this);
                }
                this.state = 1713;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 211, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                            {
                                this.state = 1706;
                                this.errorHandler.sync(this);
                                _la = this.tokenStream.LA(1);
                                while (((((_la - 51)) & ~0x1F) === 0 && ((1 << (_la - 51)) & 32767) !== 0) || _la === 123 || _la === 128) {
                                    {
                                        {
                                            this.state = 1703;
                                            this.annotation();
                                        }
                                    }
                                    this.state = 1708;
                                    this.errorHandler.sync(this);
                                    _la = this.tokenStream.LA(1);
                                }
                                this.state = 1709;
                                this.match(JavaParser.LBRACK);
                                this.state = 1710;
                                this.match(JavaParser.RBRACK);
                            }
                        }
                    }
                    this.state = 1715;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 211, this.context);
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    primitiveType() {
        let localContext = new PrimitiveTypeContext(this.context, this.state);
        this.enterRule(localContext, 238, JavaParser.RULE_primitiveType);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1716;
                _la = this.tokenStream.LA(1);
                if (!((((_la) & ~0x1F) === 0 && ((1 << _la) & 672153896) !== 0) || _la === 37)) {
                    this.errorHandler.recoverInline(this);
                }
                else {
                    this.errorHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    typeArguments() {
        let localContext = new TypeArgumentsContext(this.context, this.state);
        this.enterRule(localContext, 240, JavaParser.RULE_typeArguments);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1718;
                this.match(JavaParser.LT);
                this.state = 1719;
                this.typeArgument();
                this.state = 1724;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 85) {
                    {
                        {
                            this.state = 1720;
                            this.match(JavaParser.COMMA);
                            this.state = 1721;
                            this.typeArgument();
                        }
                    }
                    this.state = 1726;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 1727;
                this.match(JavaParser.GT);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    superSuffix() {
        let localContext = new SuperSuffixContext(this.context, this.state);
        this.enterRule(localContext, 242, JavaParser.RULE_superSuffix);
        let _la;
        try {
            this.state = 1738;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.LPAREN:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1729;
                        this.arguments();
                    }
                    break;
                case JavaParser.DOT:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1730;
                        this.match(JavaParser.DOT);
                        this.state = 1732;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        if (_la === 89) {
                            {
                                this.state = 1731;
                                this.typeArguments();
                            }
                        }
                        this.state = 1734;
                        this.identifier();
                        this.state = 1736;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 214, this.context)) {
                            case 1:
                                {
                                    this.state = 1735;
                                    this.arguments();
                                }
                                break;
                        }
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    explicitGenericInvocationSuffix() {
        let localContext = new ExplicitGenericInvocationSuffixContext(this.context, this.state);
        this.enterRule(localContext, 244, JavaParser.RULE_explicitGenericInvocationSuffix);
        try {
            this.state = 1745;
            this.errorHandler.sync(this);
            switch (this.tokenStream.LA(1)) {
                case JavaParser.SUPER:
                    this.enterOuterAlt(localContext, 1);
                    {
                        this.state = 1740;
                        this.match(JavaParser.SUPER);
                        this.state = 1741;
                        this.superSuffix();
                    }
                    break;
                case JavaParser.MODULE:
                case JavaParser.OPEN:
                case JavaParser.REQUIRES:
                case JavaParser.EXPORTS:
                case JavaParser.OPENS:
                case JavaParser.TO:
                case JavaParser.USES:
                case JavaParser.PROVIDES:
                case JavaParser.WITH:
                case JavaParser.TRANSITIVE:
                case JavaParser.VAR:
                case JavaParser.YIELD:
                case JavaParser.RECORD:
                case JavaParser.SEALED:
                case JavaParser.PERMITS:
                case JavaParser.IDENTIFIER:
                    this.enterOuterAlt(localContext, 2);
                    {
                        this.state = 1742;
                        this.identifier();
                        this.state = 1743;
                        this.arguments();
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    arguments() {
        let localContext = new ArgumentsContext(this.context, this.state);
        this.enterRule(localContext, 246, JavaParser.RULE_arguments);
        let _la;
        try {
            this.enterOuterAlt(localContext, 1);
            {
                this.state = 1747;
                this.match(JavaParser.LPAREN);
                this.state = 1749;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if ((((_la) & ~0x1F) === 0 && ((1 << _la) & 2819637544) !== 0) || ((((_la - 37)) & ~0x1F) === 0 && ((1 << (_la - 37)) & 3758082137) !== 0) || ((((_la - 69)) & ~0x1F) === 0 && ((1 << (_la - 69)) & 2154824703) !== 0) || ((((_la - 101)) & ~0x1F) === 0 && ((1 << (_la - 101)) & 138412039) !== 0)) {
                    {
                        this.state = 1748;
                        this.expressionList();
                    }
                }
                this.state = 1751;
                this.match(JavaParser.RPAREN);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    sempred(localContext, ruleIndex, predIndex) {
        switch (ruleIndex) {
            case 97:
                return this.expression_sempred(localContext, predIndex);
            case 105:
                return this.guardedPattern_sempred(localContext, predIndex);
        }
        return true;
    }
    expression_sempred(localContext, predIndex) {
        switch (predIndex) {
            case 0:
                return this.precpred(this.context, 18);
            case 1:
                return this.precpred(this.context, 17);
            case 2:
                return this.precpred(this.context, 16);
            case 3:
                return this.precpred(this.context, 15);
            case 4:
                return this.precpred(this.context, 13);
            case 5:
                return this.precpred(this.context, 12);
            case 6:
                return this.precpred(this.context, 11);
            case 7:
                return this.precpred(this.context, 10);
            case 8:
                return this.precpred(this.context, 9);
            case 9:
                return this.precpred(this.context, 8);
            case 10:
                return this.precpred(this.context, 7);
            case 11:
                return this.precpred(this.context, 6);
            case 12:
                return this.precpred(this.context, 26);
            case 13:
                return this.precpred(this.context, 25);
            case 14:
                return this.precpred(this.context, 21);
            case 15:
                return this.precpred(this.context, 14);
            case 16:
                return this.precpred(this.context, 3);
        }
        return true;
    }
    guardedPattern_sempred(localContext, predIndex) {
        switch (predIndex) {
            case 17:
                return this.precpred(this.context, 1);
        }
        return true;
    }
    static _serializedATN = [
        4, 1, 128, 1754, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2, 4, 7, 4, 2, 5, 7, 5, 2, 6,
        7, 6, 2, 7, 7, 7, 2, 8, 7, 8, 2, 9, 7, 9, 2, 10, 7, 10, 2, 11, 7, 11, 2, 12, 7, 12, 2, 13, 7,
        13, 2, 14, 7, 14, 2, 15, 7, 15, 2, 16, 7, 16, 2, 17, 7, 17, 2, 18, 7, 18, 2, 19, 7, 19, 2,
        20, 7, 20, 2, 21, 7, 21, 2, 22, 7, 22, 2, 23, 7, 23, 2, 24, 7, 24, 2, 25, 7, 25, 2, 26, 7,
        26, 2, 27, 7, 27, 2, 28, 7, 28, 2, 29, 7, 29, 2, 30, 7, 30, 2, 31, 7, 31, 2, 32, 7, 32, 2,
        33, 7, 33, 2, 34, 7, 34, 2, 35, 7, 35, 2, 36, 7, 36, 2, 37, 7, 37, 2, 38, 7, 38, 2, 39, 7,
        39, 2, 40, 7, 40, 2, 41, 7, 41, 2, 42, 7, 42, 2, 43, 7, 43, 2, 44, 7, 44, 2, 45, 7, 45, 2,
        46, 7, 46, 2, 47, 7, 47, 2, 48, 7, 48, 2, 49, 7, 49, 2, 50, 7, 50, 2, 51, 7, 51, 2, 52, 7,
        52, 2, 53, 7, 53, 2, 54, 7, 54, 2, 55, 7, 55, 2, 56, 7, 56, 2, 57, 7, 57, 2, 58, 7, 58, 2,
        59, 7, 59, 2, 60, 7, 60, 2, 61, 7, 61, 2, 62, 7, 62, 2, 63, 7, 63, 2, 64, 7, 64, 2, 65, 7,
        65, 2, 66, 7, 66, 2, 67, 7, 67, 2, 68, 7, 68, 2, 69, 7, 69, 2, 70, 7, 70, 2, 71, 7, 71, 2,
        72, 7, 72, 2, 73, 7, 73, 2, 74, 7, 74, 2, 75, 7, 75, 2, 76, 7, 76, 2, 77, 7, 77, 2, 78, 7,
        78, 2, 79, 7, 79, 2, 80, 7, 80, 2, 81, 7, 81, 2, 82, 7, 82, 2, 83, 7, 83, 2, 84, 7, 84, 2,
        85, 7, 85, 2, 86, 7, 86, 2, 87, 7, 87, 2, 88, 7, 88, 2, 89, 7, 89, 2, 90, 7, 90, 2, 91, 7,
        91, 2, 92, 7, 92, 2, 93, 7, 93, 2, 94, 7, 94, 2, 95, 7, 95, 2, 96, 7, 96, 2, 97, 7, 97, 2,
        98, 7, 98, 2, 99, 7, 99, 2, 100, 7, 100, 2, 101, 7, 101, 2, 102, 7, 102, 2, 103, 7, 103,
        2, 104, 7, 104, 2, 105, 7, 105, 2, 106, 7, 106, 2, 107, 7, 107, 2, 108, 7, 108, 2, 109,
        7, 109, 2, 110, 7, 110, 2, 111, 7, 111, 2, 112, 7, 112, 2, 113, 7, 113, 2, 114, 7, 114,
        2, 115, 7, 115, 2, 116, 7, 116, 2, 117, 7, 117, 2, 118, 7, 118, 2, 119, 7, 119, 2, 120,
        7, 120, 2, 121, 7, 121, 2, 122, 7, 122, 2, 123, 7, 123, 1, 0, 3, 0, 250, 8, 0, 1, 0, 5,
        0, 253, 8, 0, 10, 0, 12, 0, 256, 9, 0, 1, 0, 5, 0, 259, 8, 0, 10, 0, 12, 0, 262, 9, 0, 1,
        0, 1, 0, 1, 0, 1, 0, 3, 0, 268, 8, 0, 1, 1, 5, 1, 271, 8, 1, 10, 1, 12, 1, 274, 9, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 3, 2, 282, 8, 2, 1, 2, 1, 2, 1, 2, 3, 2, 287, 8, 2, 1, 2, 1, 2,
        1, 3, 5, 3, 292, 8, 3, 10, 3, 12, 3, 295, 9, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 3, 3, 302, 8,
        3, 1, 3, 3, 3, 305, 8, 3, 1, 4, 1, 4, 1, 4, 1, 4, 1, 4, 3, 4, 312, 8, 4, 1, 5, 1, 5, 1, 5, 1,
        5, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5, 1, 5, 3, 5, 324, 8, 5, 1, 6, 1, 6, 3, 6, 328, 8, 6, 1, 7, 1,
        7, 1, 7, 3, 7, 333, 8, 7, 1, 7, 1, 7, 3, 7, 337, 8, 7, 1, 7, 1, 7, 3, 7, 341, 8, 7, 1, 7, 1,
        7, 3, 7, 345, 8, 7, 1, 7, 1, 7, 1, 8, 1, 8, 1, 8, 1, 8, 5, 8, 353, 8, 8, 10, 8, 12, 8, 356,
        9, 8, 1, 8, 1, 8, 1, 9, 5, 9, 361, 8, 9, 10, 9, 12, 9, 364, 9, 9, 1, 9, 1, 9, 1, 9, 5, 9, 369,
        8, 9, 10, 9, 12, 9, 372, 9, 9, 1, 9, 3, 9, 375, 8, 9, 1, 10, 1, 10, 1, 10, 5, 10, 380, 8,
        10, 10, 10, 12, 10, 383, 9, 10, 1, 11, 1, 11, 1, 11, 1, 11, 3, 11, 389, 8, 11, 1, 11, 1,
        11, 3, 11, 393, 8, 11, 1, 11, 3, 11, 396, 8, 11, 1, 11, 3, 11, 399, 8, 11, 1, 11, 1, 11,
        1, 12, 1, 12, 1, 12, 5, 12, 406, 8, 12, 10, 12, 12, 12, 409, 9, 12, 1, 13, 5, 13, 412,
        8, 13, 10, 13, 12, 13, 415, 9, 13, 1, 13, 1, 13, 3, 13, 419, 8, 13, 1, 13, 3, 13, 422,
        8, 13, 1, 14, 1, 14, 5, 14, 426, 8, 14, 10, 14, 12, 14, 429, 9, 14, 1, 15, 1, 15, 1, 15,
        3, 15, 434, 8, 15, 1, 15, 1, 15, 3, 15, 438, 8, 15, 1, 15, 1, 15, 1, 16, 1, 16, 5, 16, 444,
        8, 16, 10, 16, 12, 16, 447, 9, 16, 1, 16, 1, 16, 1, 17, 1, 17, 5, 17, 453, 8, 17, 10, 17,
        12, 17, 456, 9, 17, 1, 17, 1, 17, 1, 18, 1, 18, 3, 18, 462, 8, 18, 1, 18, 1, 18, 5, 18,
        466, 8, 18, 10, 18, 12, 18, 469, 9, 18, 1, 18, 3, 18, 472, 8, 18, 1, 19, 1, 19, 1, 19,
        1, 19, 1, 19, 1, 19, 1, 19, 1, 19, 1, 19, 1, 19, 3, 19, 484, 8, 19, 1, 20, 1, 20, 1, 20,
        1, 20, 1, 20, 5, 20, 491, 8, 20, 10, 20, 12, 20, 494, 9, 20, 1, 20, 1, 20, 3, 20, 498,
        8, 20, 1, 20, 1, 20, 1, 21, 1, 21, 3, 21, 504, 8, 21, 1, 22, 1, 22, 3, 22, 508, 8, 22, 1,
        23, 1, 23, 1, 23, 1, 24, 1, 24, 1, 24, 1, 25, 1, 25, 1, 25, 1, 25, 3, 25, 520, 8, 25, 1,
        25, 1, 25, 1, 26, 1, 26, 1, 26, 1, 26, 1, 27, 5, 27, 529, 8, 27, 10, 27, 12, 27, 532, 9,
        27, 1, 27, 1, 27, 3, 27, 536, 8, 27, 1, 28, 1, 28, 1, 28, 1, 28, 1, 28, 1, 28, 1, 28, 1,
        28, 3, 28, 546, 8, 28, 1, 29, 1, 29, 1, 29, 1, 29, 5, 29, 552, 8, 29, 10, 29, 12, 29, 555,
        9, 29, 1, 29, 1, 29, 1, 30, 1, 30, 1, 30, 5, 30, 562, 8, 30, 10, 30, 12, 30, 565, 9, 30,
        1, 30, 1, 30, 1, 30, 1, 31, 5, 31, 571, 8, 31, 10, 31, 12, 31, 574, 9, 31, 1, 31, 1, 31,
        1, 32, 1, 32, 1, 32, 1, 32, 1, 32, 1, 32, 3, 32, 584, 8, 32, 1, 33, 5, 33, 587, 8, 33, 10,
        33, 12, 33, 590, 9, 33, 1, 33, 1, 33, 1, 33, 1, 34, 5, 34, 596, 8, 34, 10, 34, 12, 34,
        599, 9, 34, 1, 34, 1, 34, 1, 34, 1, 34, 1, 34, 5, 34, 606, 8, 34, 10, 34, 12, 34, 609,
        9, 34, 1, 34, 1, 34, 3, 34, 613, 8, 34, 1, 34, 1, 34, 1, 35, 1, 35, 1, 35, 5, 35, 620, 8,
        35, 10, 35, 12, 35, 623, 9, 35, 1, 36, 1, 36, 1, 36, 3, 36, 628, 8, 36, 1, 37, 1, 37, 1,
        37, 5, 37, 633, 8, 37, 10, 37, 12, 37, 636, 9, 37, 1, 38, 1, 38, 3, 38, 640, 8, 38, 1,
        39, 1, 39, 1, 39, 1, 39, 5, 39, 646, 8, 39, 10, 39, 12, 39, 649, 9, 39, 1, 39, 3, 39, 652,
        8, 39, 3, 39, 654, 8, 39, 1, 39, 1, 39, 1, 40, 1, 40, 3, 40, 660, 8, 40, 1, 40, 1, 40, 1,
        40, 3, 40, 665, 8, 40, 5, 40, 667, 8, 40, 10, 40, 12, 40, 670, 9, 40, 1, 41, 1, 41, 5,
        41, 674, 8, 41, 10, 41, 12, 41, 677, 9, 41, 1, 41, 1, 41, 1, 41, 3, 41, 682, 8, 41, 3,
        41, 684, 8, 41, 1, 42, 1, 42, 1, 42, 5, 42, 689, 8, 42, 10, 42, 12, 42, 692, 9, 42, 1,
        43, 1, 43, 3, 43, 696, 8, 43, 1, 43, 1, 43, 1, 43, 3, 43, 701, 8, 43, 1, 43, 3, 43, 704,
        8, 43, 3, 43, 706, 8, 43, 1, 43, 1, 43, 1, 44, 1, 44, 1, 44, 1, 44, 5, 44, 714, 8, 44, 10,
        44, 12, 44, 717, 9, 44, 1, 44, 1, 44, 1, 45, 1, 45, 1, 45, 5, 45, 724, 8, 45, 10, 45, 12,
        45, 727, 9, 45, 1, 45, 1, 45, 3, 45, 731, 8, 45, 1, 45, 3, 45, 734, 8, 45, 1, 46, 5, 46,
        737, 8, 46, 10, 46, 12, 46, 740, 9, 46, 1, 46, 1, 46, 1, 46, 1, 47, 5, 47, 746, 8, 47,
        10, 47, 12, 47, 749, 9, 47, 1, 47, 1, 47, 5, 47, 753, 8, 47, 10, 47, 12, 47, 756, 9, 47,
        1, 47, 1, 47, 1, 47, 1, 48, 1, 48, 1, 48, 5, 48, 764, 8, 48, 10, 48, 12, 48, 767, 9, 48,
        1, 49, 5, 49, 770, 8, 49, 10, 49, 12, 49, 773, 9, 49, 1, 49, 1, 49, 1, 49, 1, 50, 1, 50,
        1, 50, 5, 50, 781, 8, 50, 10, 50, 12, 50, 784, 9, 50, 1, 51, 1, 51, 1, 51, 1, 51, 1, 51,
        1, 51, 1, 51, 3, 51, 793, 8, 51, 1, 52, 1, 52, 1, 53, 1, 53, 1, 54, 1, 54, 1, 54, 5, 54,
        802, 8, 54, 10, 54, 12, 54, 805, 9, 54, 1, 54, 1, 54, 1, 54, 1, 55, 1, 55, 1, 55, 3, 55,
        813, 8, 55, 1, 55, 1, 55, 1, 55, 3, 55, 818, 8, 55, 1, 55, 3, 55, 821, 8, 55, 1, 56, 1,
        56, 1, 56, 5, 56, 826, 8, 56, 10, 56, 12, 56, 829, 9, 56, 1, 57, 1, 57, 1, 57, 1, 57, 1,
        58, 1, 58, 1, 58, 3, 58, 838, 8, 58, 1, 59, 1, 59, 1, 59, 1, 59, 5, 59, 844, 8, 59, 10,
        59, 12, 59, 847, 9, 59, 3, 59, 849, 8, 59, 1, 59, 3, 59, 852, 8, 59, 1, 59, 1, 59, 1, 60,
        1, 60, 1, 60, 1, 60, 1, 60, 1, 61, 1, 61, 5, 61, 863, 8, 61, 10, 61, 12, 61, 866, 9, 61,
        1, 61, 1, 61, 1, 62, 5, 62, 871, 8, 62, 10, 62, 12, 62, 874, 9, 62, 1, 62, 1, 62, 3, 62,
        878, 8, 62, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 1, 63, 3, 63, 886, 8, 63, 1, 63, 1, 63, 3,
        63, 890, 8, 63, 1, 63, 1, 63, 3, 63, 894, 8, 63, 1, 63, 1, 63, 3, 63, 898, 8, 63, 1, 63,
        1, 63, 3, 63, 902, 8, 63, 3, 63, 904, 8, 63, 1, 64, 1, 64, 3, 64, 908, 8, 64, 1, 65, 1,
        65, 1, 65, 1, 65, 3, 65, 914, 8, 65, 1, 66, 1, 66, 1, 67, 1, 67, 1, 67, 1, 68, 3, 68, 922,
        8, 68, 1, 68, 1, 68, 1, 68, 1, 68, 1, 69, 1, 69, 5, 69, 930, 8, 69, 10, 69, 12, 69, 933,
        9, 69, 1, 69, 1, 69, 1, 70, 1, 70, 5, 70, 939, 8, 70, 10, 70, 12, 70, 942, 9, 70, 1, 70,
        1, 70, 1, 70, 1, 70, 1, 70, 1, 70, 1, 70, 3, 70, 951, 8, 70, 1, 70, 1, 70, 1, 70, 1, 70,
        1, 70, 1, 70, 3, 70, 959, 8, 70, 1, 70, 1, 70, 1, 70, 1, 70, 1, 70, 1, 70, 1, 70, 1, 70,
        1, 70, 1, 70, 1, 70, 1, 70, 3, 70, 973, 8, 70, 1, 71, 1, 71, 1, 72, 1, 72, 1, 72, 3, 72,
        980, 8, 72, 1, 72, 1, 72, 1, 72, 3, 72, 985, 8, 72, 1, 72, 1, 72, 1, 73, 1, 73, 3, 73, 991,
        8, 73, 1, 73, 1, 73, 1, 74, 1, 74, 1, 74, 5, 74, 998, 8, 74, 10, 74, 12, 74, 1001, 9, 74,
        1, 75, 1, 75, 1, 75, 1, 76, 1, 76, 5, 76, 1008, 8, 76, 10, 76, 12, 76, 1011, 9, 76, 1,
        76, 1, 76, 1, 77, 1, 77, 5, 77, 1017, 8, 77, 10, 77, 12, 77, 1020, 9, 77, 1, 77, 1, 77,
        1, 78, 1, 78, 1, 78, 1, 78, 1, 78, 3, 78, 1029, 8, 78, 1, 79, 5, 79, 1032, 8, 79, 10, 79,
        12, 79, 1035, 9, 79, 1, 79, 1, 79, 1, 79, 1, 80, 1, 80, 1, 81, 5, 81, 1043, 8, 81, 10,
        81, 12, 81, 1046, 9, 81, 1, 81, 1, 81, 1, 81, 3, 81, 1051, 8, 81, 1, 81, 3, 81, 1054,
        8, 81, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 3, 82, 1061, 8, 82, 1, 82, 1, 82, 1, 82, 1, 82,
        1, 82, 1, 82, 1, 82, 3, 82, 1070, 8, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82,
        1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 4, 82,
        1091, 8, 82, 11, 82, 12, 82, 1092, 1, 82, 3, 82, 1096, 8, 82, 1, 82, 3, 82, 1099, 8,
        82, 1, 82, 1, 82, 1, 82, 1, 82, 5, 82, 1105, 8, 82, 10, 82, 12, 82, 1108, 9, 82, 1, 82,
        3, 82, 1111, 8, 82, 1, 82, 1, 82, 1, 82, 1, 82, 5, 82, 1117, 8, 82, 10, 82, 12, 82, 1120,
        9, 82, 1, 82, 5, 82, 1123, 8, 82, 10, 82, 12, 82, 1126, 9, 82, 1, 82, 1, 82, 1, 82, 1,
        82, 1, 82, 1, 82, 1, 82, 1, 82, 3, 82, 1136, 8, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1,
        82, 1, 82, 3, 82, 1145, 8, 82, 1, 82, 1, 82, 1, 82, 3, 82, 1150, 8, 82, 1, 82, 1, 82, 1,
        82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 1, 82, 3, 82, 1163, 8, 82, 1, 82, 1,
        82, 1, 82, 1, 82, 3, 82, 1169, 8, 82, 1, 83, 1, 83, 1, 83, 5, 83, 1174, 8, 83, 10, 83,
        12, 83, 1177, 9, 83, 1, 83, 1, 83, 1, 83, 1, 83, 1, 83, 1, 84, 1, 84, 1, 84, 5, 84, 1187,
        8, 84, 10, 84, 12, 84, 1190, 9, 84, 1, 85, 1, 85, 1, 85, 1, 86, 1, 86, 1, 86, 3, 86, 1198,
        8, 86, 1, 86, 1, 86, 1, 87, 1, 87, 1, 87, 5, 87, 1205, 8, 87, 10, 87, 12, 87, 1208, 9,
        87, 1, 88, 5, 88, 1211, 8, 88, 10, 88, 12, 88, 1214, 9, 88, 1, 88, 1, 88, 1, 88, 1, 88,
        1, 88, 3, 88, 1221, 8, 88, 1, 88, 1, 88, 1, 88, 1, 88, 3, 88, 1227, 8, 88, 1, 89, 4, 89,
        1230, 8, 89, 11, 89, 12, 89, 1231, 1, 89, 4, 89, 1235, 8, 89, 11, 89, 12, 89, 1236,
        1, 90, 1, 90, 1, 90, 1, 90, 1, 90, 1, 90, 3, 90, 1245, 8, 90, 1, 90, 1, 90, 1, 90, 3, 90,
        1250, 8, 90, 1, 91, 1, 91, 3, 91, 1254, 8, 91, 1, 91, 1, 91, 3, 91, 1258, 8, 91, 1, 91,
        1, 91, 3, 91, 1262, 8, 91, 3, 91, 1264, 8, 91, 1, 92, 1, 92, 3, 92, 1268, 8, 92, 1, 93,
        5, 93, 1271, 8, 93, 10, 93, 12, 93, 1274, 9, 93, 1, 93, 1, 93, 3, 93, 1278, 8, 93, 1,
        93, 1, 93, 1, 93, 1, 93, 1, 94, 1, 94, 1, 94, 1, 94, 1, 95, 1, 95, 1, 95, 5, 95, 1291, 8,
        95, 10, 95, 12, 95, 1294, 9, 95, 1, 96, 1, 96, 1, 96, 3, 96, 1299, 8, 96, 1, 96, 1, 96,
        1, 96, 1, 96, 1, 96, 3, 96, 1306, 8, 96, 1, 96, 1, 96, 1, 96, 1, 96, 3, 96, 1312, 8, 96,
        1, 96, 3, 96, 1315, 8, 96, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 5, 97, 1324,
        8, 97, 10, 97, 12, 97, 1327, 9, 97, 1, 97, 1, 97, 1, 97, 5, 97, 1332, 8, 97, 10, 97, 12,
        97, 1335, 9, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1,
        97, 1, 97, 3, 97, 1349, 8, 97, 1, 97, 1, 97, 3, 97, 1353, 8, 97, 1, 97, 1, 97, 1, 97, 3,
        97, 1358, 8, 97, 1, 97, 1, 97, 3, 97, 1362, 8, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1,
        97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 3, 97, 1378, 8, 97, 1, 97, 1,
        97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1,
        97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1,
        97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 3, 97, 1418, 8,
        97, 1, 97, 1, 97, 1, 97, 1, 97, 3, 97, 1424, 8, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 1,
        97, 1, 97, 1, 97, 1, 97, 1, 97, 1, 97, 3, 97, 1437, 8, 97, 1, 97, 1, 97, 1, 97, 3, 97, 1442,
        8, 97, 1, 97, 5, 97, 1445, 8, 97, 10, 97, 12, 97, 1448, 9, 97, 1, 98, 5, 98, 1451, 8,
        98, 10, 98, 12, 98, 1454, 9, 98, 1, 98, 1, 98, 5, 98, 1458, 8, 98, 10, 98, 12, 98, 1461,
        9, 98, 1, 98, 1, 98, 1, 99, 1, 99, 1, 99, 1, 99, 1, 100, 1, 100, 1, 100, 3, 100, 1472,
        8, 100, 1, 100, 1, 100, 1, 100, 1, 100, 1, 100, 5, 100, 1479, 8, 100, 10, 100, 12, 100,
        1482, 9, 100, 1, 100, 1, 100, 1, 100, 1, 100, 3, 100, 1488, 8, 100, 1, 100, 3, 100,
        1491, 8, 100, 1, 101, 1, 101, 3, 101, 1495, 8, 101, 1, 102, 1, 102, 1, 102, 1, 102,
        1, 102, 1, 102, 1, 102, 1, 102, 1, 102, 1, 102, 1, 102, 1, 102, 1, 102, 1, 102, 1, 102,
        1, 102, 3, 102, 1513, 8, 102, 3, 102, 1515, 8, 102, 1, 103, 1, 103, 1, 103, 1, 103,
        5, 103, 1521, 8, 103, 10, 103, 12, 103, 1524, 9, 103, 1, 103, 1, 103, 1, 104, 1, 104,
        1, 104, 1, 104, 3, 104, 1532, 8, 104, 1, 104, 1, 104, 1, 104, 1, 104, 1, 104, 3, 104,
        1539, 8, 104, 1, 105, 1, 105, 1, 105, 1, 105, 1, 105, 1, 105, 5, 105, 1547, 8, 105,
        10, 105, 12, 105, 1550, 9, 105, 1, 105, 1, 105, 5, 105, 1554, 8, 105, 10, 105, 12,
        105, 1557, 9, 105, 1, 105, 1, 105, 1, 105, 5, 105, 1562, 8, 105, 10, 105, 12, 105,
        1565, 9, 105, 3, 105, 1567, 8, 105, 1, 105, 1, 105, 1, 105, 5, 105, 1572, 8, 105, 10,
        105, 12, 105, 1575, 9, 105, 1, 106, 1, 106, 5, 106, 1579, 8, 106, 10, 106, 12, 106,
        1582, 9, 106, 3, 106, 1584, 8, 106, 1, 107, 1, 107, 1, 107, 3, 107, 1589, 8, 107, 1,
        107, 5, 107, 1592, 8, 107, 10, 107, 12, 107, 1595, 9, 107, 1, 107, 1, 107, 3, 107,
        1599, 8, 107, 1, 108, 1, 108, 1, 108, 1, 108, 1, 108, 1, 108, 1, 108, 3, 108, 1608,
        8, 108, 3, 108, 1610, 8, 108, 1, 109, 1, 109, 3, 109, 1614, 8, 109, 1, 109, 1, 109,
        1, 109, 3, 109, 1619, 8, 109, 5, 109, 1621, 8, 109, 10, 109, 12, 109, 1624, 9, 109,
        1, 109, 3, 109, 1627, 8, 109, 1, 110, 1, 110, 3, 110, 1631, 8, 110, 1, 110, 1, 110,
        1, 111, 1, 111, 1, 111, 1, 111, 5, 111, 1639, 8, 111, 10, 111, 12, 111, 1642, 9, 111,
        1, 111, 1, 111, 1, 111, 1, 111, 1, 111, 1, 111, 1, 111, 5, 111, 1651, 8, 111, 10, 111,
        12, 111, 1654, 9, 111, 1, 111, 1, 111, 5, 111, 1658, 8, 111, 10, 111, 12, 111, 1661,
        9, 111, 3, 111, 1663, 8, 111, 1, 112, 1, 112, 3, 112, 1667, 8, 112, 1, 113, 1, 113,
        1, 113, 1, 114, 1, 114, 1, 114, 3, 114, 1675, 8, 114, 1, 115, 1, 115, 1, 115, 3, 115,
        1680, 8, 115, 1, 116, 1, 116, 1, 116, 1, 116, 1, 117, 1, 117, 1, 117, 5, 117, 1689,
        8, 117, 10, 117, 12, 117, 1692, 9, 117, 1, 118, 5, 118, 1695, 8, 118, 10, 118, 12,
        118, 1698, 9, 118, 1, 118, 1, 118, 3, 118, 1702, 8, 118, 1, 118, 5, 118, 1705, 8, 118,
        10, 118, 12, 118, 1708, 9, 118, 1, 118, 1, 118, 5, 118, 1712, 8, 118, 10, 118, 12,
        118, 1715, 9, 118, 1, 119, 1, 119, 1, 120, 1, 120, 1, 120, 1, 120, 5, 120, 1723, 8,
        120, 10, 120, 12, 120, 1726, 9, 120, 1, 120, 1, 120, 1, 121, 1, 121, 1, 121, 3, 121,
        1733, 8, 121, 1, 121, 1, 121, 3, 121, 1737, 8, 121, 3, 121, 1739, 8, 121, 1, 122, 1,
        122, 1, 122, 1, 122, 1, 122, 3, 122, 1746, 8, 122, 1, 123, 1, 123, 3, 123, 1750, 8,
        123, 1, 123, 1, 123, 1, 123, 0, 2, 194, 210, 124, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20,
        22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64,
        66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98, 100, 102, 104, 106,
        108, 110, 112, 114, 116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136, 138,
        140, 142, 144, 146, 148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170,
        172, 174, 176, 178, 180, 182, 184, 186, 188, 190, 192, 194, 196, 198, 200, 202,
        204, 206, 208, 210, 212, 214, 216, 218, 220, 222, 224, 226, 228, 230, 232, 234,
        236, 238, 240, 242, 244, 246, 0, 15, 2, 0, 17, 17, 40, 40, 1, 0, 67, 70, 1, 0, 71, 72,
        2, 0, 38, 38, 60, 60, 2, 0, 51, 65, 128, 128, 1, 0, 100, 103, 1, 0, 90, 91, 2, 0, 104,
        105, 109, 109, 1, 0, 102, 103, 2, 0, 88, 89, 95, 96, 2, 0, 94, 94, 97, 97, 2, 0, 87, 87,
        110, 120, 1, 0, 100, 101, 2, 0, 93, 93, 121, 121, 8, 0, 3, 3, 5, 5, 8, 8, 14, 14, 20, 20,
        27, 27, 29, 29, 37, 37, 1952, 0, 267, 1, 0, 0, 0, 2, 272, 1, 0, 0, 0, 4, 279, 1, 0, 0, 0,
        6, 304, 1, 0, 0, 0, 8, 311, 1, 0, 0, 0, 10, 323, 1, 0, 0, 0, 12, 327, 1, 0, 0, 0, 14, 329,
        1, 0, 0, 0, 16, 348, 1, 0, 0, 0, 18, 362, 1, 0, 0, 0, 20, 376, 1, 0, 0, 0, 22, 384, 1, 0,
        0, 0, 24, 402, 1, 0, 0, 0, 26, 413, 1, 0, 0, 0, 28, 423, 1, 0, 0, 0, 30, 430, 1, 0, 0, 0,
        32, 441, 1, 0, 0, 0, 34, 450, 1, 0, 0, 0, 36, 471, 1, 0, 0, 0, 38, 483, 1, 0, 0, 0, 40, 485,
        1, 0, 0, 0, 42, 503, 1, 0, 0, 0, 44, 507, 1, 0, 0, 0, 46, 509, 1, 0, 0, 0, 48, 512, 1, 0,
        0, 0, 50, 515, 1, 0, 0, 0, 52, 523, 1, 0, 0, 0, 54, 535, 1, 0, 0, 0, 56, 545, 1, 0, 0, 0,
        58, 547, 1, 0, 0, 0, 60, 558, 1, 0, 0, 0, 62, 572, 1, 0, 0, 0, 64, 583, 1, 0, 0, 0, 66, 588,
        1, 0, 0, 0, 68, 597, 1, 0, 0, 0, 70, 616, 1, 0, 0, 0, 72, 624, 1, 0, 0, 0, 74, 629, 1, 0,
        0, 0, 76, 639, 1, 0, 0, 0, 78, 641, 1, 0, 0, 0, 80, 657, 1, 0, 0, 0, 82, 683, 1, 0, 0, 0,
        84, 685, 1, 0, 0, 0, 86, 693, 1, 0, 0, 0, 88, 709, 1, 0, 0, 0, 90, 733, 1, 0, 0, 0, 92, 738,
        1, 0, 0, 0, 94, 747, 1, 0, 0, 0, 96, 760, 1, 0, 0, 0, 98, 771, 1, 0, 0, 0, 100, 777, 1, 0,
        0, 0, 102, 792, 1, 0, 0, 0, 104, 794, 1, 0, 0, 0, 106, 796, 1, 0, 0, 0, 108, 803, 1, 0,
        0, 0, 110, 812, 1, 0, 0, 0, 112, 822, 1, 0, 0, 0, 114, 830, 1, 0, 0, 0, 116, 837, 1, 0,
        0, 0, 118, 839, 1, 0, 0, 0, 120, 855, 1, 0, 0, 0, 122, 860, 1, 0, 0, 0, 124, 877, 1, 0,
        0, 0, 126, 903, 1, 0, 0, 0, 128, 907, 1, 0, 0, 0, 130, 909, 1, 0, 0, 0, 132, 915, 1, 0,
        0, 0, 134, 917, 1, 0, 0, 0, 136, 921, 1, 0, 0, 0, 138, 927, 1, 0, 0, 0, 140, 972, 1, 0,
        0, 0, 142, 974, 1, 0, 0, 0, 144, 976, 1, 0, 0, 0, 146, 988, 1, 0, 0, 0, 148, 994, 1, 0,
        0, 0, 150, 1002, 1, 0, 0, 0, 152, 1005, 1, 0, 0, 0, 154, 1014, 1, 0, 0, 0, 156, 1028,
        1, 0, 0, 0, 158, 1033, 1, 0, 0, 0, 160, 1039, 1, 0, 0, 0, 162, 1053, 1, 0, 0, 0, 164, 1168,
        1, 0, 0, 0, 166, 1170, 1, 0, 0, 0, 168, 1183, 1, 0, 0, 0, 170, 1191, 1, 0, 0, 0, 172, 1194,
        1, 0, 0, 0, 174, 1201, 1, 0, 0, 0, 176, 1226, 1, 0, 0, 0, 178, 1229, 1, 0, 0, 0, 180, 1249,
        1, 0, 0, 0, 182, 1263, 1, 0, 0, 0, 184, 1267, 1, 0, 0, 0, 186, 1272, 1, 0, 0, 0, 188, 1283,
        1, 0, 0, 0, 190, 1287, 1, 0, 0, 0, 192, 1314, 1, 0, 0, 0, 194, 1361, 1, 0, 0, 0, 196, 1452,
        1, 0, 0, 0, 198, 1464, 1, 0, 0, 0, 200, 1490, 1, 0, 0, 0, 202, 1494, 1, 0, 0, 0, 204, 1514,
        1, 0, 0, 0, 206, 1516, 1, 0, 0, 0, 208, 1538, 1, 0, 0, 0, 210, 1566, 1, 0, 0, 0, 212, 1583,
        1, 0, 0, 0, 214, 1588, 1, 0, 0, 0, 216, 1609, 1, 0, 0, 0, 218, 1626, 1, 0, 0, 0, 220, 1628,
        1, 0, 0, 0, 222, 1634, 1, 0, 0, 0, 224, 1664, 1, 0, 0, 0, 226, 1668, 1, 0, 0, 0, 228, 1674,
        1, 0, 0, 0, 230, 1679, 1, 0, 0, 0, 232, 1681, 1, 0, 0, 0, 234, 1685, 1, 0, 0, 0, 236, 1696,
        1, 0, 0, 0, 238, 1716, 1, 0, 0, 0, 240, 1718, 1, 0, 0, 0, 242, 1738, 1, 0, 0, 0, 244, 1745,
        1, 0, 0, 0, 246, 1747, 1, 0, 0, 0, 248, 250, 3, 2, 1, 0, 249, 248, 1, 0, 0, 0, 249, 250,
        1, 0, 0, 0, 250, 254, 1, 0, 0, 0, 251, 253, 3, 4, 2, 0, 252, 251, 1, 0, 0, 0, 253, 256,
        1, 0, 0, 0, 254, 252, 1, 0, 0, 0, 254, 255, 1, 0, 0, 0, 255, 260, 1, 0, 0, 0, 256, 254,
        1, 0, 0, 0, 257, 259, 3, 6, 3, 0, 258, 257, 1, 0, 0, 0, 259, 262, 1, 0, 0, 0, 260, 258,
        1, 0, 0, 0, 260, 261, 1, 0, 0, 0, 261, 263, 1, 0, 0, 0, 262, 260, 1, 0, 0, 0, 263, 268,
        5, 0, 0, 1, 264, 265, 3, 136, 68, 0, 265, 266, 5, 0, 0, 1, 266, 268, 1, 0, 0, 0, 267, 249,
        1, 0, 0, 0, 267, 264, 1, 0, 0, 0, 268, 1, 1, 0, 0, 0, 269, 271, 3, 110, 55, 0, 270, 269,
        1, 0, 0, 0, 271, 274, 1, 0, 0, 0, 272, 270, 1, 0, 0, 0, 272, 273, 1, 0, 0, 0, 273, 275,
        1, 0, 0, 0, 274, 272, 1, 0, 0, 0, 275, 276, 5, 32, 0, 0, 276, 277, 3, 100, 50, 0, 277,
        278, 5, 84, 0, 0, 278, 3, 1, 0, 0, 0, 279, 281, 5, 25, 0, 0, 280, 282, 5, 38, 0, 0, 281,
        280, 1, 0, 0, 0, 281, 282, 1, 0, 0, 0, 282, 283, 1, 0, 0, 0, 283, 286, 3, 100, 50, 0, 284,
        285, 5, 86, 0, 0, 285, 287, 5, 104, 0, 0, 286, 284, 1, 0, 0, 0, 286, 287, 1, 0, 0, 0, 287,
        288, 1, 0, 0, 0, 288, 289, 5, 84, 0, 0, 289, 5, 1, 0, 0, 0, 290, 292, 3, 10, 5, 0, 291,
        290, 1, 0, 0, 0, 292, 295, 1, 0, 0, 0, 293, 291, 1, 0, 0, 0, 293, 294, 1, 0, 0, 0, 294,
        301, 1, 0, 0, 0, 295, 293, 1, 0, 0, 0, 296, 302, 3, 14, 7, 0, 297, 302, 3, 22, 11, 0, 298,
        302, 3, 30, 15, 0, 299, 302, 3, 120, 60, 0, 300, 302, 3, 144, 72, 0, 301, 296, 1, 0,
        0, 0, 301, 297, 1, 0, 0, 0, 301, 298, 1, 0, 0, 0, 301, 299, 1, 0, 0, 0, 301, 300, 1, 0,
        0, 0, 302, 305, 1, 0, 0, 0, 303, 305, 5, 84, 0, 0, 304, 293, 1, 0, 0, 0, 304, 303, 1, 0,
        0, 0, 305, 7, 1, 0, 0, 0, 306, 312, 3, 10, 5, 0, 307, 312, 5, 30, 0, 0, 308, 312, 5, 42,
        0, 0, 309, 312, 5, 46, 0, 0, 310, 312, 5, 49, 0, 0, 311, 306, 1, 0, 0, 0, 311, 307, 1,
        0, 0, 0, 311, 308, 1, 0, 0, 0, 311, 309, 1, 0, 0, 0, 311, 310, 1, 0, 0, 0, 312, 9, 1, 0,
        0, 0, 313, 324, 3, 110, 55, 0, 314, 324, 5, 35, 0, 0, 315, 324, 5, 34, 0, 0, 316, 324,
        5, 33, 0, 0, 317, 324, 5, 38, 0, 0, 318, 324, 5, 1, 0, 0, 319, 324, 5, 18, 0, 0, 320, 324,
        5, 39, 0, 0, 321, 324, 5, 64, 0, 0, 322, 324, 5, 66, 0, 0, 323, 313, 1, 0, 0, 0, 323, 314,
        1, 0, 0, 0, 323, 315, 1, 0, 0, 0, 323, 316, 1, 0, 0, 0, 323, 317, 1, 0, 0, 0, 323, 318,
        1, 0, 0, 0, 323, 319, 1, 0, 0, 0, 323, 320, 1, 0, 0, 0, 323, 321, 1, 0, 0, 0, 323, 322,
        1, 0, 0, 0, 324, 11, 1, 0, 0, 0, 325, 328, 5, 18, 0, 0, 326, 328, 3, 110, 55, 0, 327, 325,
        1, 0, 0, 0, 327, 326, 1, 0, 0, 0, 328, 13, 1, 0, 0, 0, 329, 330, 5, 9, 0, 0, 330, 332, 3,
        160, 80, 0, 331, 333, 3, 16, 8, 0, 332, 331, 1, 0, 0, 0, 332, 333, 1, 0, 0, 0, 333, 336,
        1, 0, 0, 0, 334, 335, 5, 17, 0, 0, 335, 337, 3, 236, 118, 0, 336, 334, 1, 0, 0, 0, 336,
        337, 1, 0, 0, 0, 337, 340, 1, 0, 0, 0, 338, 339, 5, 24, 0, 0, 339, 341, 3, 234, 117, 0,
        340, 338, 1, 0, 0, 0, 340, 341, 1, 0, 0, 0, 341, 344, 1, 0, 0, 0, 342, 343, 5, 65, 0, 0,
        343, 345, 3, 234, 117, 0, 344, 342, 1, 0, 0, 0, 344, 345, 1, 0, 0, 0, 345, 346, 1, 0,
        0, 0, 346, 347, 3, 32, 16, 0, 347, 15, 1, 0, 0, 0, 348, 349, 5, 89, 0, 0, 349, 354, 3,
        18, 9, 0, 350, 351, 5, 85, 0, 0, 351, 353, 3, 18, 9, 0, 352, 350, 1, 0, 0, 0, 353, 356,
        1, 0, 0, 0, 354, 352, 1, 0, 0, 0, 354, 355, 1, 0, 0, 0, 355, 357, 1, 0, 0, 0, 356, 354,
        1, 0, 0, 0, 357, 358, 5, 88, 0, 0, 358, 17, 1, 0, 0, 0, 359, 361, 3, 110, 55, 0, 360, 359,
        1, 0, 0, 0, 361, 364, 1, 0, 0, 0, 362, 360, 1, 0, 0, 0, 362, 363, 1, 0, 0, 0, 363, 365,
        1, 0, 0, 0, 364, 362, 1, 0, 0, 0, 365, 374, 3, 160, 80, 0, 366, 370, 5, 17, 0, 0, 367,
        369, 3, 110, 55, 0, 368, 367, 1, 0, 0, 0, 369, 372, 1, 0, 0, 0, 370, 368, 1, 0, 0, 0, 370,
        371, 1, 0, 0, 0, 371, 373, 1, 0, 0, 0, 372, 370, 1, 0, 0, 0, 373, 375, 3, 20, 10, 0, 374,
        366, 1, 0, 0, 0, 374, 375, 1, 0, 0, 0, 375, 19, 1, 0, 0, 0, 376, 381, 3, 236, 118, 0, 377,
        378, 5, 106, 0, 0, 378, 380, 3, 236, 118, 0, 379, 377, 1, 0, 0, 0, 380, 383, 1, 0, 0,
        0, 381, 379, 1, 0, 0, 0, 381, 382, 1, 0, 0, 0, 382, 21, 1, 0, 0, 0, 383, 381, 1, 0, 0, 0,
        384, 385, 5, 16, 0, 0, 385, 388, 3, 160, 80, 0, 386, 387, 5, 24, 0, 0, 387, 389, 3, 234,
        117, 0, 388, 386, 1, 0, 0, 0, 388, 389, 1, 0, 0, 0, 389, 390, 1, 0, 0, 0, 390, 392, 5,
        80, 0, 0, 391, 393, 3, 24, 12, 0, 392, 391, 1, 0, 0, 0, 392, 393, 1, 0, 0, 0, 393, 395,
        1, 0, 0, 0, 394, 396, 5, 85, 0, 0, 395, 394, 1, 0, 0, 0, 395, 396, 1, 0, 0, 0, 396, 398,
        1, 0, 0, 0, 397, 399, 3, 28, 14, 0, 398, 397, 1, 0, 0, 0, 398, 399, 1, 0, 0, 0, 399, 400,
        1, 0, 0, 0, 400, 401, 5, 81, 0, 0, 401, 23, 1, 0, 0, 0, 402, 407, 3, 26, 13, 0, 403, 404,
        5, 85, 0, 0, 404, 406, 3, 26, 13, 0, 405, 403, 1, 0, 0, 0, 406, 409, 1, 0, 0, 0, 407, 405,
        1, 0, 0, 0, 407, 408, 1, 0, 0, 0, 408, 25, 1, 0, 0, 0, 409, 407, 1, 0, 0, 0, 410, 412, 3,
        110, 55, 0, 411, 410, 1, 0, 0, 0, 412, 415, 1, 0, 0, 0, 413, 411, 1, 0, 0, 0, 413, 414,
        1, 0, 0, 0, 414, 416, 1, 0, 0, 0, 415, 413, 1, 0, 0, 0, 416, 418, 3, 160, 80, 0, 417, 419,
        3, 246, 123, 0, 418, 417, 1, 0, 0, 0, 418, 419, 1, 0, 0, 0, 419, 421, 1, 0, 0, 0, 420,
        422, 3, 32, 16, 0, 421, 420, 1, 0, 0, 0, 421, 422, 1, 0, 0, 0, 422, 27, 1, 0, 0, 0, 423,
        427, 5, 84, 0, 0, 424, 426, 3, 36, 18, 0, 425, 424, 1, 0, 0, 0, 426, 429, 1, 0, 0, 0, 427,
        425, 1, 0, 0, 0, 427, 428, 1, 0, 0, 0, 428, 29, 1, 0, 0, 0, 429, 427, 1, 0, 0, 0, 430, 431,
        5, 28, 0, 0, 431, 433, 3, 160, 80, 0, 432, 434, 3, 16, 8, 0, 433, 432, 1, 0, 0, 0, 433,
        434, 1, 0, 0, 0, 434, 437, 1, 0, 0, 0, 435, 436, 5, 17, 0, 0, 436, 438, 3, 234, 117, 0,
        437, 435, 1, 0, 0, 0, 437, 438, 1, 0, 0, 0, 438, 439, 1, 0, 0, 0, 439, 440, 3, 34, 17,
        0, 440, 31, 1, 0, 0, 0, 441, 445, 5, 80, 0, 0, 442, 444, 3, 36, 18, 0, 443, 442, 1, 0,
        0, 0, 444, 447, 1, 0, 0, 0, 445, 443, 1, 0, 0, 0, 445, 446, 1, 0, 0, 0, 446, 448, 1, 0,
        0, 0, 447, 445, 1, 0, 0, 0, 448, 449, 5, 81, 0, 0, 449, 33, 1, 0, 0, 0, 450, 454, 5, 80,
        0, 0, 451, 453, 3, 54, 27, 0, 452, 451, 1, 0, 0, 0, 453, 456, 1, 0, 0, 0, 454, 452, 1,
        0, 0, 0, 454, 455, 1, 0, 0, 0, 455, 457, 1, 0, 0, 0, 456, 454, 1, 0, 0, 0, 457, 458, 5,
        81, 0, 0, 458, 35, 1, 0, 0, 0, 459, 472, 5, 84, 0, 0, 460, 462, 5, 38, 0, 0, 461, 460,
        1, 0, 0, 0, 461, 462, 1, 0, 0, 0, 462, 463, 1, 0, 0, 0, 463, 472, 3, 154, 77, 0, 464, 466,
        3, 8, 4, 0, 465, 464, 1, 0, 0, 0, 466, 469, 1, 0, 0, 0, 467, 465, 1, 0, 0, 0, 467, 468,
        1, 0, 0, 0, 468, 470, 1, 0, 0, 0, 469, 467, 1, 0, 0, 0, 470, 472, 3, 38, 19, 0, 471, 459,
        1, 0, 0, 0, 471, 461, 1, 0, 0, 0, 471, 467, 1, 0, 0, 0, 472, 37, 1, 0, 0, 0, 473, 484, 3,
        40, 20, 0, 474, 484, 3, 46, 23, 0, 475, 484, 3, 52, 26, 0, 476, 484, 3, 50, 25, 0, 477,
        484, 3, 48, 24, 0, 478, 484, 3, 30, 15, 0, 479, 484, 3, 120, 60, 0, 480, 484, 3, 14,
        7, 0, 481, 484, 3, 22, 11, 0, 482, 484, 3, 144, 72, 0, 483, 473, 1, 0, 0, 0, 483, 474,
        1, 0, 0, 0, 483, 475, 1, 0, 0, 0, 483, 476, 1, 0, 0, 0, 483, 477, 1, 0, 0, 0, 483, 478,
        1, 0, 0, 0, 483, 479, 1, 0, 0, 0, 483, 480, 1, 0, 0, 0, 483, 481, 1, 0, 0, 0, 483, 482,
        1, 0, 0, 0, 484, 39, 1, 0, 0, 0, 485, 486, 3, 44, 22, 0, 486, 487, 3, 160, 80, 0, 487,
        492, 3, 86, 43, 0, 488, 489, 5, 82, 0, 0, 489, 491, 5, 83, 0, 0, 490, 488, 1, 0, 0, 0,
        491, 494, 1, 0, 0, 0, 492, 490, 1, 0, 0, 0, 492, 493, 1, 0, 0, 0, 493, 497, 1, 0, 0, 0,
        494, 492, 1, 0, 0, 0, 495, 496, 5, 45, 0, 0, 496, 498, 3, 84, 42, 0, 497, 495, 1, 0, 0,
        0, 497, 498, 1, 0, 0, 0, 498, 499, 1, 0, 0, 0, 499, 500, 3, 42, 21, 0, 500, 41, 1, 0, 0,
        0, 501, 504, 3, 154, 77, 0, 502, 504, 5, 84, 0, 0, 503, 501, 1, 0, 0, 0, 503, 502, 1,
        0, 0, 0, 504, 43, 1, 0, 0, 0, 505, 508, 3, 236, 118, 0, 506, 508, 5, 48, 0, 0, 507, 505,
        1, 0, 0, 0, 507, 506, 1, 0, 0, 0, 508, 45, 1, 0, 0, 0, 509, 510, 3, 16, 8, 0, 510, 511,
        3, 40, 20, 0, 511, 47, 1, 0, 0, 0, 512, 513, 3, 16, 8, 0, 513, 514, 3, 50, 25, 0, 514,
        49, 1, 0, 0, 0, 515, 516, 3, 160, 80, 0, 516, 519, 3, 86, 43, 0, 517, 518, 5, 45, 0, 0,
        518, 520, 3, 84, 42, 0, 519, 517, 1, 0, 0, 0, 519, 520, 1, 0, 0, 0, 520, 521, 1, 0, 0,
        0, 521, 522, 3, 154, 77, 0, 522, 51, 1, 0, 0, 0, 523, 524, 3, 236, 118, 0, 524, 525,
        3, 70, 35, 0, 525, 526, 5, 84, 0, 0, 526, 53, 1, 0, 0, 0, 527, 529, 3, 8, 4, 0, 528, 527,
        1, 0, 0, 0, 529, 532, 1, 0, 0, 0, 530, 528, 1, 0, 0, 0, 530, 531, 1, 0, 0, 0, 531, 533,
        1, 0, 0, 0, 532, 530, 1, 0, 0, 0, 533, 536, 3, 56, 28, 0, 534, 536, 5, 84, 0, 0, 535, 530,
        1, 0, 0, 0, 535, 534, 1, 0, 0, 0, 536, 55, 1, 0, 0, 0, 537, 546, 3, 58, 29, 0, 538, 546,
        3, 62, 31, 0, 539, 546, 3, 66, 33, 0, 540, 546, 3, 30, 15, 0, 541, 546, 3, 120, 60, 0,
        542, 546, 3, 14, 7, 0, 543, 546, 3, 22, 11, 0, 544, 546, 3, 144, 72, 0, 545, 537, 1,
        0, 0, 0, 545, 538, 1, 0, 0, 0, 545, 539, 1, 0, 0, 0, 545, 540, 1, 0, 0, 0, 545, 541, 1,
        0, 0, 0, 545, 542, 1, 0, 0, 0, 545, 543, 1, 0, 0, 0, 545, 544, 1, 0, 0, 0, 546, 57, 1, 0,
        0, 0, 547, 548, 3, 236, 118, 0, 548, 553, 3, 60, 30, 0, 549, 550, 5, 85, 0, 0, 550, 552,
        3, 60, 30, 0, 551, 549, 1, 0, 0, 0, 552, 555, 1, 0, 0, 0, 553, 551, 1, 0, 0, 0, 553, 554,
        1, 0, 0, 0, 554, 556, 1, 0, 0, 0, 555, 553, 1, 0, 0, 0, 556, 557, 5, 84, 0, 0, 557, 59,
        1, 0, 0, 0, 558, 563, 3, 160, 80, 0, 559, 560, 5, 82, 0, 0, 560, 562, 5, 83, 0, 0, 561,
        559, 1, 0, 0, 0, 562, 565, 1, 0, 0, 0, 563, 561, 1, 0, 0, 0, 563, 564, 1, 0, 0, 0, 564,
        566, 1, 0, 0, 0, 565, 563, 1, 0, 0, 0, 566, 567, 5, 87, 0, 0, 567, 568, 3, 76, 38, 0, 568,
        61, 1, 0, 0, 0, 569, 571, 3, 64, 32, 0, 570, 569, 1, 0, 0, 0, 571, 574, 1, 0, 0, 0, 572,
        570, 1, 0, 0, 0, 572, 573, 1, 0, 0, 0, 573, 575, 1, 0, 0, 0, 574, 572, 1, 0, 0, 0, 575,
        576, 3, 68, 34, 0, 576, 63, 1, 0, 0, 0, 577, 584, 3, 110, 55, 0, 578, 584, 5, 35, 0, 0,
        579, 584, 5, 1, 0, 0, 580, 584, 5, 12, 0, 0, 581, 584, 5, 38, 0, 0, 582, 584, 5, 39, 0,
        0, 583, 577, 1, 0, 0, 0, 583, 578, 1, 0, 0, 0, 583, 579, 1, 0, 0, 0, 583, 580, 1, 0, 0,
        0, 583, 581, 1, 0, 0, 0, 583, 582, 1, 0, 0, 0, 584, 65, 1, 0, 0, 0, 585, 587, 3, 64, 32,
        0, 586, 585, 1, 0, 0, 0, 587, 590, 1, 0, 0, 0, 588, 586, 1, 0, 0, 0, 588, 589, 1, 0, 0,
        0, 589, 591, 1, 0, 0, 0, 590, 588, 1, 0, 0, 0, 591, 592, 3, 16, 8, 0, 592, 593, 3, 68,
        34, 0, 593, 67, 1, 0, 0, 0, 594, 596, 3, 110, 55, 0, 595, 594, 1, 0, 0, 0, 596, 599, 1,
        0, 0, 0, 597, 595, 1, 0, 0, 0, 597, 598, 1, 0, 0, 0, 598, 600, 1, 0, 0, 0, 599, 597, 1,
        0, 0, 0, 600, 601, 3, 44, 22, 0, 601, 602, 3, 160, 80, 0, 602, 607, 3, 86, 43, 0, 603,
        604, 5, 82, 0, 0, 604, 606, 5, 83, 0, 0, 605, 603, 1, 0, 0, 0, 606, 609, 1, 0, 0, 0, 607,
        605, 1, 0, 0, 0, 607, 608, 1, 0, 0, 0, 608, 612, 1, 0, 0, 0, 609, 607, 1, 0, 0, 0, 610,
        611, 5, 45, 0, 0, 611, 613, 3, 84, 42, 0, 612, 610, 1, 0, 0, 0, 612, 613, 1, 0, 0, 0, 613,
        614, 1, 0, 0, 0, 614, 615, 3, 42, 21, 0, 615, 69, 1, 0, 0, 0, 616, 621, 3, 72, 36, 0, 617,
        618, 5, 85, 0, 0, 618, 620, 3, 72, 36, 0, 619, 617, 1, 0, 0, 0, 620, 623, 1, 0, 0, 0, 621,
        619, 1, 0, 0, 0, 621, 622, 1, 0, 0, 0, 622, 71, 1, 0, 0, 0, 623, 621, 1, 0, 0, 0, 624, 627,
        3, 74, 37, 0, 625, 626, 5, 87, 0, 0, 626, 628, 3, 76, 38, 0, 627, 625, 1, 0, 0, 0, 627,
        628, 1, 0, 0, 0, 628, 73, 1, 0, 0, 0, 629, 634, 3, 160, 80, 0, 630, 631, 5, 82, 0, 0, 631,
        633, 5, 83, 0, 0, 632, 630, 1, 0, 0, 0, 633, 636, 1, 0, 0, 0, 634, 632, 1, 0, 0, 0, 634,
        635, 1, 0, 0, 0, 635, 75, 1, 0, 0, 0, 636, 634, 1, 0, 0, 0, 637, 640, 3, 78, 39, 0, 638,
        640, 3, 194, 97, 0, 639, 637, 1, 0, 0, 0, 639, 638, 1, 0, 0, 0, 640, 77, 1, 0, 0, 0, 641,
        653, 5, 80, 0, 0, 642, 647, 3, 76, 38, 0, 643, 644, 5, 85, 0, 0, 644, 646, 3, 76, 38,
        0, 645, 643, 1, 0, 0, 0, 646, 649, 1, 0, 0, 0, 647, 645, 1, 0, 0, 0, 647, 648, 1, 0, 0,
        0, 648, 651, 1, 0, 0, 0, 649, 647, 1, 0, 0, 0, 650, 652, 5, 85, 0, 0, 651, 650, 1, 0, 0,
        0, 651, 652, 1, 0, 0, 0, 652, 654, 1, 0, 0, 0, 653, 642, 1, 0, 0, 0, 653, 654, 1, 0, 0,
        0, 654, 655, 1, 0, 0, 0, 655, 656, 5, 81, 0, 0, 656, 79, 1, 0, 0, 0, 657, 659, 3, 160,
        80, 0, 658, 660, 3, 240, 120, 0, 659, 658, 1, 0, 0, 0, 659, 660, 1, 0, 0, 0, 660, 668,
        1, 0, 0, 0, 661, 662, 5, 86, 0, 0, 662, 664, 3, 160, 80, 0, 663, 665, 3, 240, 120, 0,
        664, 663, 1, 0, 0, 0, 664, 665, 1, 0, 0, 0, 665, 667, 1, 0, 0, 0, 666, 661, 1, 0, 0, 0,
        667, 670, 1, 0, 0, 0, 668, 666, 1, 0, 0, 0, 668, 669, 1, 0, 0, 0, 669, 81, 1, 0, 0, 0, 670,
        668, 1, 0, 0, 0, 671, 684, 3, 236, 118, 0, 672, 674, 3, 110, 55, 0, 673, 672, 1, 0, 0,
        0, 674, 677, 1, 0, 0, 0, 675, 673, 1, 0, 0, 0, 675, 676, 1, 0, 0, 0, 676, 678, 1, 0, 0,
        0, 677, 675, 1, 0, 0, 0, 678, 681, 5, 92, 0, 0, 679, 680, 7, 0, 0, 0, 680, 682, 3, 236,
        118, 0, 681, 679, 1, 0, 0, 0, 681, 682, 1, 0, 0, 0, 682, 684, 1, 0, 0, 0, 683, 671, 1,
        0, 0, 0, 683, 675, 1, 0, 0, 0, 684, 83, 1, 0, 0, 0, 685, 690, 3, 100, 50, 0, 686, 687,
        5, 85, 0, 0, 687, 689, 3, 100, 50, 0, 688, 686, 1, 0, 0, 0, 689, 692, 1, 0, 0, 0, 690,
        688, 1, 0, 0, 0, 690, 691, 1, 0, 0, 0, 691, 85, 1, 0, 0, 0, 692, 690, 1, 0, 0, 0, 693, 705,
        5, 78, 0, 0, 694, 696, 3, 88, 44, 0, 695, 694, 1, 0, 0, 0, 695, 696, 1, 0, 0, 0, 696, 706,
        1, 0, 0, 0, 697, 700, 3, 88, 44, 0, 698, 699, 5, 85, 0, 0, 699, 701, 3, 90, 45, 0, 700,
        698, 1, 0, 0, 0, 700, 701, 1, 0, 0, 0, 701, 706, 1, 0, 0, 0, 702, 704, 3, 90, 45, 0, 703,
        702, 1, 0, 0, 0, 703, 704, 1, 0, 0, 0, 704, 706, 1, 0, 0, 0, 705, 695, 1, 0, 0, 0, 705,
        697, 1, 0, 0, 0, 705, 703, 1, 0, 0, 0, 706, 707, 1, 0, 0, 0, 707, 708, 5, 79, 0, 0, 708,
        87, 1, 0, 0, 0, 709, 715, 3, 236, 118, 0, 710, 711, 3, 160, 80, 0, 711, 712, 5, 86, 0,
        0, 712, 714, 1, 0, 0, 0, 713, 710, 1, 0, 0, 0, 714, 717, 1, 0, 0, 0, 715, 713, 1, 0, 0,
        0, 715, 716, 1, 0, 0, 0, 716, 718, 1, 0, 0, 0, 717, 715, 1, 0, 0, 0, 718, 719, 5, 43, 0,
        0, 719, 89, 1, 0, 0, 0, 720, 725, 3, 92, 46, 0, 721, 722, 5, 85, 0, 0, 722, 724, 3, 92,
        46, 0, 723, 721, 1, 0, 0, 0, 724, 727, 1, 0, 0, 0, 725, 723, 1, 0, 0, 0, 725, 726, 1, 0,
        0, 0, 726, 730, 1, 0, 0, 0, 727, 725, 1, 0, 0, 0, 728, 729, 5, 85, 0, 0, 729, 731, 3, 94,
        47, 0, 730, 728, 1, 0, 0, 0, 730, 731, 1, 0, 0, 0, 731, 734, 1, 0, 0, 0, 732, 734, 3, 94,
        47, 0, 733, 720, 1, 0, 0, 0, 733, 732, 1, 0, 0, 0, 734, 91, 1, 0, 0, 0, 735, 737, 3, 12,
        6, 0, 736, 735, 1, 0, 0, 0, 737, 740, 1, 0, 0, 0, 738, 736, 1, 0, 0, 0, 738, 739, 1, 0,
        0, 0, 739, 741, 1, 0, 0, 0, 740, 738, 1, 0, 0, 0, 741, 742, 3, 236, 118, 0, 742, 743,
        3, 74, 37, 0, 743, 93, 1, 0, 0, 0, 744, 746, 3, 12, 6, 0, 745, 744, 1, 0, 0, 0, 746, 749,
        1, 0, 0, 0, 747, 745, 1, 0, 0, 0, 747, 748, 1, 0, 0, 0, 748, 750, 1, 0, 0, 0, 749, 747,
        1, 0, 0, 0, 750, 754, 3, 236, 118, 0, 751, 753, 3, 110, 55, 0, 752, 751, 1, 0, 0, 0, 753,
        756, 1, 0, 0, 0, 754, 752, 1, 0, 0, 0, 754, 755, 1, 0, 0, 0, 755, 757, 1, 0, 0, 0, 756,
        754, 1, 0, 0, 0, 757, 758, 5, 124, 0, 0, 758, 759, 3, 74, 37, 0, 759, 95, 1, 0, 0, 0, 760,
        765, 3, 98, 49, 0, 761, 762, 5, 85, 0, 0, 762, 764, 3, 98, 49, 0, 763, 761, 1, 0, 0, 0,
        764, 767, 1, 0, 0, 0, 765, 763, 1, 0, 0, 0, 765, 766, 1, 0, 0, 0, 766, 97, 1, 0, 0, 0, 767,
        765, 1, 0, 0, 0, 768, 770, 3, 12, 6, 0, 769, 768, 1, 0, 0, 0, 770, 773, 1, 0, 0, 0, 771,
        769, 1, 0, 0, 0, 771, 772, 1, 0, 0, 0, 772, 774, 1, 0, 0, 0, 773, 771, 1, 0, 0, 0, 774,
        775, 5, 61, 0, 0, 775, 776, 3, 160, 80, 0, 776, 99, 1, 0, 0, 0, 777, 782, 3, 160, 80,
        0, 778, 779, 5, 86, 0, 0, 779, 781, 3, 160, 80, 0, 780, 778, 1, 0, 0, 0, 781, 784, 1,
        0, 0, 0, 782, 780, 1, 0, 0, 0, 782, 783, 1, 0, 0, 0, 783, 101, 1, 0, 0, 0, 784, 782, 1,
        0, 0, 0, 785, 793, 3, 104, 52, 0, 786, 793, 3, 106, 53, 0, 787, 793, 5, 74, 0, 0, 788,
        793, 5, 75, 0, 0, 789, 793, 5, 73, 0, 0, 790, 793, 5, 77, 0, 0, 791, 793, 5, 76, 0, 0,
        792, 785, 1, 0, 0, 0, 792, 786, 1, 0, 0, 0, 792, 787, 1, 0, 0, 0, 792, 788, 1, 0, 0, 0,
        792, 789, 1, 0, 0, 0, 792, 790, 1, 0, 0, 0, 792, 791, 1, 0, 0, 0, 793, 103, 1, 0, 0, 0,
        794, 795, 7, 1, 0, 0, 795, 105, 1, 0, 0, 0, 796, 797, 7, 2, 0, 0, 797, 107, 1, 0, 0, 0,
        798, 799, 3, 160, 80, 0, 799, 800, 5, 86, 0, 0, 800, 802, 1, 0, 0, 0, 801, 798, 1, 0,
        0, 0, 802, 805, 1, 0, 0, 0, 803, 801, 1, 0, 0, 0, 803, 804, 1, 0, 0, 0, 804, 806, 1, 0,
        0, 0, 805, 803, 1, 0, 0, 0, 806, 807, 5, 123, 0, 0, 807, 808, 3, 160, 80, 0, 808, 109,
        1, 0, 0, 0, 809, 810, 5, 123, 0, 0, 810, 813, 3, 100, 50, 0, 811, 813, 3, 108, 54, 0,
        812, 809, 1, 0, 0, 0, 812, 811, 1, 0, 0, 0, 813, 820, 1, 0, 0, 0, 814, 817, 5, 78, 0, 0,
        815, 818, 3, 112, 56, 0, 816, 818, 3, 116, 58, 0, 817, 815, 1, 0, 0, 0, 817, 816, 1,
        0, 0, 0, 817, 818, 1, 0, 0, 0, 818, 819, 1, 0, 0, 0, 819, 821, 5, 79, 0, 0, 820, 814, 1,
        0, 0, 0, 820, 821, 1, 0, 0, 0, 821, 111, 1, 0, 0, 0, 822, 827, 3, 114, 57, 0, 823, 824,
        5, 85, 0, 0, 824, 826, 3, 114, 57, 0, 825, 823, 1, 0, 0, 0, 826, 829, 1, 0, 0, 0, 827,
        825, 1, 0, 0, 0, 827, 828, 1, 0, 0, 0, 828, 113, 1, 0, 0, 0, 829, 827, 1, 0, 0, 0, 830,
        831, 3, 160, 80, 0, 831, 832, 5, 87, 0, 0, 832, 833, 3, 116, 58, 0, 833, 115, 1, 0, 0,
        0, 834, 838, 3, 194, 97, 0, 835, 838, 3, 110, 55, 0, 836, 838, 3, 118, 59, 0, 837, 834,
        1, 0, 0, 0, 837, 835, 1, 0, 0, 0, 837, 836, 1, 0, 0, 0, 838, 117, 1, 0, 0, 0, 839, 848,
        5, 80, 0, 0, 840, 845, 3, 116, 58, 0, 841, 842, 5, 85, 0, 0, 842, 844, 3, 116, 58, 0,
        843, 841, 1, 0, 0, 0, 844, 847, 1, 0, 0, 0, 845, 843, 1, 0, 0, 0, 845, 846, 1, 0, 0, 0,
        846, 849, 1, 0, 0, 0, 847, 845, 1, 0, 0, 0, 848, 840, 1, 0, 0, 0, 848, 849, 1, 0, 0, 0,
        849, 851, 1, 0, 0, 0, 850, 852, 5, 85, 0, 0, 851, 850, 1, 0, 0, 0, 851, 852, 1, 0, 0, 0,
        852, 853, 1, 0, 0, 0, 853, 854, 5, 81, 0, 0, 854, 119, 1, 0, 0, 0, 855, 856, 5, 123, 0,
        0, 856, 857, 5, 28, 0, 0, 857, 858, 3, 160, 80, 0, 858, 859, 3, 122, 61, 0, 859, 121,
        1, 0, 0, 0, 860, 864, 5, 80, 0, 0, 861, 863, 3, 124, 62, 0, 862, 861, 1, 0, 0, 0, 863,
        866, 1, 0, 0, 0, 864, 862, 1, 0, 0, 0, 864, 865, 1, 0, 0, 0, 865, 867, 1, 0, 0, 0, 866,
        864, 1, 0, 0, 0, 867, 868, 5, 81, 0, 0, 868, 123, 1, 0, 0, 0, 869, 871, 3, 8, 4, 0, 870,
        869, 1, 0, 0, 0, 871, 874, 1, 0, 0, 0, 872, 870, 1, 0, 0, 0, 872, 873, 1, 0, 0, 0, 873,
        875, 1, 0, 0, 0, 874, 872, 1, 0, 0, 0, 875, 878, 3, 126, 63, 0, 876, 878, 5, 84, 0, 0,
        877, 872, 1, 0, 0, 0, 877, 876, 1, 0, 0, 0, 878, 125, 1, 0, 0, 0, 879, 880, 3, 236, 118,
        0, 880, 881, 3, 128, 64, 0, 881, 882, 5, 84, 0, 0, 882, 904, 1, 0, 0, 0, 883, 885, 3,
        14, 7, 0, 884, 886, 5, 84, 0, 0, 885, 884, 1, 0, 0, 0, 885, 886, 1, 0, 0, 0, 886, 904,
        1, 0, 0, 0, 887, 889, 3, 30, 15, 0, 888, 890, 5, 84, 0, 0, 889, 888, 1, 0, 0, 0, 889, 890,
        1, 0, 0, 0, 890, 904, 1, 0, 0, 0, 891, 893, 3, 22, 11, 0, 892, 894, 5, 84, 0, 0, 893, 892,
        1, 0, 0, 0, 893, 894, 1, 0, 0, 0, 894, 904, 1, 0, 0, 0, 895, 897, 3, 120, 60, 0, 896, 898,
        5, 84, 0, 0, 897, 896, 1, 0, 0, 0, 897, 898, 1, 0, 0, 0, 898, 904, 1, 0, 0, 0, 899, 901,
        3, 144, 72, 0, 900, 902, 5, 84, 0, 0, 901, 900, 1, 0, 0, 0, 901, 902, 1, 0, 0, 0, 902,
        904, 1, 0, 0, 0, 903, 879, 1, 0, 0, 0, 903, 883, 1, 0, 0, 0, 903, 887, 1, 0, 0, 0, 903,
        891, 1, 0, 0, 0, 903, 895, 1, 0, 0, 0, 903, 899, 1, 0, 0, 0, 904, 127, 1, 0, 0, 0, 905,
        908, 3, 130, 65, 0, 906, 908, 3, 132, 66, 0, 907, 905, 1, 0, 0, 0, 907, 906, 1, 0, 0,
        0, 908, 129, 1, 0, 0, 0, 909, 910, 3, 160, 80, 0, 910, 911, 5, 78, 0, 0, 911, 913, 5,
        79, 0, 0, 912, 914, 3, 134, 67, 0, 913, 912, 1, 0, 0, 0, 913, 914, 1, 0, 0, 0, 914, 131,
        1, 0, 0, 0, 915, 916, 3, 70, 35, 0, 916, 133, 1, 0, 0, 0, 917, 918, 5, 12, 0, 0, 918, 919,
        3, 116, 58, 0, 919, 135, 1, 0, 0, 0, 920, 922, 5, 52, 0, 0, 921, 920, 1, 0, 0, 0, 921,
        922, 1, 0, 0, 0, 922, 923, 1, 0, 0, 0, 923, 924, 5, 51, 0, 0, 924, 925, 3, 100, 50, 0,
        925, 926, 3, 138, 69, 0, 926, 137, 1, 0, 0, 0, 927, 931, 5, 80, 0, 0, 928, 930, 3, 140,
        70, 0, 929, 928, 1, 0, 0, 0, 930, 933, 1, 0, 0, 0, 931, 929, 1, 0, 0, 0, 931, 932, 1, 0,
        0, 0, 932, 934, 1, 0, 0, 0, 933, 931, 1, 0, 0, 0, 934, 935, 5, 81, 0, 0, 935, 139, 1, 0,
        0, 0, 936, 940, 5, 53, 0, 0, 937, 939, 3, 142, 71, 0, 938, 937, 1, 0, 0, 0, 939, 942,
        1, 0, 0, 0, 940, 938, 1, 0, 0, 0, 940, 941, 1, 0, 0, 0, 941, 943, 1, 0, 0, 0, 942, 940,
        1, 0, 0, 0, 943, 944, 3, 100, 50, 0, 944, 945, 5, 84, 0, 0, 945, 973, 1, 0, 0, 0, 946,
        947, 5, 54, 0, 0, 947, 950, 3, 100, 50, 0, 948, 949, 5, 56, 0, 0, 949, 951, 3, 100, 50,
        0, 950, 948, 1, 0, 0, 0, 950, 951, 1, 0, 0, 0, 951, 952, 1, 0, 0, 0, 952, 953, 5, 84, 0,
        0, 953, 973, 1, 0, 0, 0, 954, 955, 5, 55, 0, 0, 955, 958, 3, 100, 50, 0, 956, 957, 5,
        56, 0, 0, 957, 959, 3, 100, 50, 0, 958, 956, 1, 0, 0, 0, 958, 959, 1, 0, 0, 0, 959, 960,
        1, 0, 0, 0, 960, 961, 5, 84, 0, 0, 961, 973, 1, 0, 0, 0, 962, 963, 5, 57, 0, 0, 963, 964,
        3, 100, 50, 0, 964, 965, 5, 84, 0, 0, 965, 973, 1, 0, 0, 0, 966, 967, 5, 58, 0, 0, 967,
        968, 3, 100, 50, 0, 968, 969, 5, 59, 0, 0, 969, 970, 3, 100, 50, 0, 970, 971, 5, 84,
        0, 0, 971, 973, 1, 0, 0, 0, 972, 936, 1, 0, 0, 0, 972, 946, 1, 0, 0, 0, 972, 954, 1, 0,
        0, 0, 972, 962, 1, 0, 0, 0, 972, 966, 1, 0, 0, 0, 973, 141, 1, 0, 0, 0, 974, 975, 7, 3,
        0, 0, 975, 143, 1, 0, 0, 0, 976, 977, 5, 63, 0, 0, 977, 979, 3, 160, 80, 0, 978, 980,
        3, 16, 8, 0, 979, 978, 1, 0, 0, 0, 979, 980, 1, 0, 0, 0, 980, 981, 1, 0, 0, 0, 981, 984,
        3, 146, 73, 0, 982, 983, 5, 24, 0, 0, 983, 985, 3, 234, 117, 0, 984, 982, 1, 0, 0, 0,
        984, 985, 1, 0, 0, 0, 985, 986, 1, 0, 0, 0, 986, 987, 3, 152, 76, 0, 987, 145, 1, 0, 0,
        0, 988, 990, 5, 78, 0, 0, 989, 991, 3, 148, 74, 0, 990, 989, 1, 0, 0, 0, 990, 991, 1,
        0, 0, 0, 991, 992, 1, 0, 0, 0, 992, 993, 5, 79, 0, 0, 993, 147, 1, 0, 0, 0, 994, 999, 3,
        150, 75, 0, 995, 996, 5, 85, 0, 0, 996, 998, 3, 150, 75, 0, 997, 995, 1, 0, 0, 0, 998,
        1001, 1, 0, 0, 0, 999, 997, 1, 0, 0, 0, 999, 1000, 1, 0, 0, 0, 1000, 149, 1, 0, 0, 0, 1001,
        999, 1, 0, 0, 0, 1002, 1003, 3, 236, 118, 0, 1003, 1004, 3, 160, 80, 0, 1004, 151,
        1, 0, 0, 0, 1005, 1009, 5, 80, 0, 0, 1006, 1008, 3, 36, 18, 0, 1007, 1006, 1, 0, 0, 0,
        1008, 1011, 1, 0, 0, 0, 1009, 1007, 1, 0, 0, 0, 1009, 1010, 1, 0, 0, 0, 1010, 1012,
        1, 0, 0, 0, 1011, 1009, 1, 0, 0, 0, 1012, 1013, 5, 81, 0, 0, 1013, 153, 1, 0, 0, 0, 1014,
        1018, 5, 80, 0, 0, 1015, 1017, 3, 156, 78, 0, 1016, 1015, 1, 0, 0, 0, 1017, 1020, 1,
        0, 0, 0, 1018, 1016, 1, 0, 0, 0, 1018, 1019, 1, 0, 0, 0, 1019, 1021, 1, 0, 0, 0, 1020,
        1018, 1, 0, 0, 0, 1021, 1022, 5, 81, 0, 0, 1022, 155, 1, 0, 0, 0, 1023, 1024, 3, 158,
        79, 0, 1024, 1025, 5, 84, 0, 0, 1025, 1029, 1, 0, 0, 0, 1026, 1029, 3, 164, 82, 0, 1027,
        1029, 3, 162, 81, 0, 1028, 1023, 1, 0, 0, 0, 1028, 1026, 1, 0, 0, 0, 1028, 1027, 1,
        0, 0, 0, 1029, 157, 1, 0, 0, 0, 1030, 1032, 3, 12, 6, 0, 1031, 1030, 1, 0, 0, 0, 1032,
        1035, 1, 0, 0, 0, 1033, 1031, 1, 0, 0, 0, 1033, 1034, 1, 0, 0, 0, 1034, 1036, 1, 0, 0,
        0, 1035, 1033, 1, 0, 0, 0, 1036, 1037, 3, 236, 118, 0, 1037, 1038, 3, 70, 35, 0, 1038,
        159, 1, 0, 0, 0, 1039, 1040, 7, 4, 0, 0, 1040, 161, 1, 0, 0, 0, 1041, 1043, 3, 10, 5,
        0, 1042, 1041, 1, 0, 0, 0, 1043, 1046, 1, 0, 0, 0, 1044, 1042, 1, 0, 0, 0, 1044, 1045,
        1, 0, 0, 0, 1045, 1050, 1, 0, 0, 0, 1046, 1044, 1, 0, 0, 0, 1047, 1051, 3, 14, 7, 0, 1048,
        1051, 3, 30, 15, 0, 1049, 1051, 3, 144, 72, 0, 1050, 1047, 1, 0, 0, 0, 1050, 1048,
        1, 0, 0, 0, 1050, 1049, 1, 0, 0, 0, 1051, 1054, 1, 0, 0, 0, 1052, 1054, 5, 84, 0, 0, 1053,
        1044, 1, 0, 0, 0, 1053, 1052, 1, 0, 0, 0, 1054, 163, 1, 0, 0, 0, 1055, 1169, 3, 154,
        77, 0, 1056, 1057, 5, 2, 0, 0, 1057, 1060, 3, 194, 97, 0, 1058, 1059, 5, 93, 0, 0, 1059,
        1061, 3, 194, 97, 0, 1060, 1058, 1, 0, 0, 0, 1060, 1061, 1, 0, 0, 0, 1061, 1062, 1,
        0, 0, 0, 1062, 1063, 5, 84, 0, 0, 1063, 1169, 1, 0, 0, 0, 1064, 1065, 5, 22, 0, 0, 1065,
        1066, 3, 188, 94, 0, 1066, 1069, 3, 164, 82, 0, 1067, 1068, 5, 15, 0, 0, 1068, 1070,
        3, 164, 82, 0, 1069, 1067, 1, 0, 0, 0, 1069, 1070, 1, 0, 0, 0, 1070, 1169, 1, 0, 0, 0,
        1071, 1072, 5, 21, 0, 0, 1072, 1073, 5, 78, 0, 0, 1073, 1074, 3, 182, 91, 0, 1074,
        1075, 5, 79, 0, 0, 1075, 1076, 3, 164, 82, 0, 1076, 1169, 1, 0, 0, 0, 1077, 1078, 5,
        50, 0, 0, 1078, 1079, 3, 188, 94, 0, 1079, 1080, 3, 164, 82, 0, 1080, 1169, 1, 0, 0,
        0, 1081, 1082, 5, 13, 0, 0, 1082, 1083, 3, 164, 82, 0, 1083, 1084, 5, 50, 0, 0, 1084,
        1085, 3, 188, 94, 0, 1085, 1086, 5, 84, 0, 0, 1086, 1169, 1, 0, 0, 0, 1087, 1088, 5,
        47, 0, 0, 1088, 1098, 3, 154, 77, 0, 1089, 1091, 3, 166, 83, 0, 1090, 1089, 1, 0, 0,
        0, 1091, 1092, 1, 0, 0, 0, 1092, 1090, 1, 0, 0, 0, 1092, 1093, 1, 0, 0, 0, 1093, 1095,
        1, 0, 0, 0, 1094, 1096, 3, 170, 85, 0, 1095, 1094, 1, 0, 0, 0, 1095, 1096, 1, 0, 0, 0,
        1096, 1099, 1, 0, 0, 0, 1097, 1099, 3, 170, 85, 0, 1098, 1090, 1, 0, 0, 0, 1098, 1097,
        1, 0, 0, 0, 1099, 1169, 1, 0, 0, 0, 1100, 1101, 5, 47, 0, 0, 1101, 1102, 3, 172, 86,
        0, 1102, 1106, 3, 154, 77, 0, 1103, 1105, 3, 166, 83, 0, 1104, 1103, 1, 0, 0, 0, 1105,
        1108, 1, 0, 0, 0, 1106, 1104, 1, 0, 0, 0, 1106, 1107, 1, 0, 0, 0, 1107, 1110, 1, 0, 0,
        0, 1108, 1106, 1, 0, 0, 0, 1109, 1111, 3, 170, 85, 0, 1110, 1109, 1, 0, 0, 0, 1110,
        1111, 1, 0, 0, 0, 1111, 1169, 1, 0, 0, 0, 1112, 1113, 5, 41, 0, 0, 1113, 1114, 3, 188,
        94, 0, 1114, 1118, 5, 80, 0, 0, 1115, 1117, 3, 178, 89, 0, 1116, 1115, 1, 0, 0, 0, 1117,
        1120, 1, 0, 0, 0, 1118, 1116, 1, 0, 0, 0, 1118, 1119, 1, 0, 0, 0, 1119, 1124, 1, 0, 0,
        0, 1120, 1118, 1, 0, 0, 0, 1121, 1123, 3, 180, 90, 0, 1122, 1121, 1, 0, 0, 0, 1123,
        1126, 1, 0, 0, 0, 1124, 1122, 1, 0, 0, 0, 1124, 1125, 1, 0, 0, 0, 1125, 1127, 1, 0, 0,
        0, 1126, 1124, 1, 0, 0, 0, 1127, 1128, 5, 81, 0, 0, 1128, 1169, 1, 0, 0, 0, 1129, 1130,
        5, 42, 0, 0, 1130, 1131, 3, 188, 94, 0, 1131, 1132, 3, 154, 77, 0, 1132, 1169, 1, 0,
        0, 0, 1133, 1135, 5, 36, 0, 0, 1134, 1136, 3, 194, 97, 0, 1135, 1134, 1, 0, 0, 0, 1135,
        1136, 1, 0, 0, 0, 1136, 1137, 1, 0, 0, 0, 1137, 1169, 5, 84, 0, 0, 1138, 1139, 5, 44,
        0, 0, 1139, 1140, 3, 194, 97, 0, 1140, 1141, 5, 84, 0, 0, 1141, 1169, 1, 0, 0, 0, 1142,
        1144, 5, 4, 0, 0, 1143, 1145, 3, 160, 80, 0, 1144, 1143, 1, 0, 0, 0, 1144, 1145, 1,
        0, 0, 0, 1145, 1146, 1, 0, 0, 0, 1146, 1169, 5, 84, 0, 0, 1147, 1149, 5, 11, 0, 0, 1148,
        1150, 3, 160, 80, 0, 1149, 1148, 1, 0, 0, 0, 1149, 1150, 1, 0, 0, 0, 1150, 1151, 1,
        0, 0, 0, 1151, 1169, 5, 84, 0, 0, 1152, 1153, 5, 62, 0, 0, 1153, 1154, 3, 194, 97, 0,
        1154, 1155, 5, 84, 0, 0, 1155, 1169, 1, 0, 0, 0, 1156, 1169, 5, 84, 0, 0, 1157, 1158,
        3, 194, 97, 0, 1158, 1159, 5, 84, 0, 0, 1159, 1169, 1, 0, 0, 0, 1160, 1162, 3, 206,
        103, 0, 1161, 1163, 5, 84, 0, 0, 1162, 1161, 1, 0, 0, 0, 1162, 1163, 1, 0, 0, 0, 1163,
        1169, 1, 0, 0, 0, 1164, 1165, 3, 160, 80, 0, 1165, 1166, 5, 93, 0, 0, 1166, 1167, 3,
        164, 82, 0, 1167, 1169, 1, 0, 0, 0, 1168, 1055, 1, 0, 0, 0, 1168, 1056, 1, 0, 0, 0, 1168,
        1064, 1, 0, 0, 0, 1168, 1071, 1, 0, 0, 0, 1168, 1077, 1, 0, 0, 0, 1168, 1081, 1, 0, 0,
        0, 1168, 1087, 1, 0, 0, 0, 1168, 1100, 1, 0, 0, 0, 1168, 1112, 1, 0, 0, 0, 1168, 1129,
        1, 0, 0, 0, 1168, 1133, 1, 0, 0, 0, 1168, 1138, 1, 0, 0, 0, 1168, 1142, 1, 0, 0, 0, 1168,
        1147, 1, 0, 0, 0, 1168, 1152, 1, 0, 0, 0, 1168, 1156, 1, 0, 0, 0, 1168, 1157, 1, 0, 0,
        0, 1168, 1160, 1, 0, 0, 0, 1168, 1164, 1, 0, 0, 0, 1169, 165, 1, 0, 0, 0, 1170, 1171,
        5, 7, 0, 0, 1171, 1175, 5, 78, 0, 0, 1172, 1174, 3, 12, 6, 0, 1173, 1172, 1, 0, 0, 0,
        1174, 1177, 1, 0, 0, 0, 1175, 1173, 1, 0, 0, 0, 1175, 1176, 1, 0, 0, 0, 1176, 1178,
        1, 0, 0, 0, 1177, 1175, 1, 0, 0, 0, 1178, 1179, 3, 168, 84, 0, 1179, 1180, 3, 160, 80,
        0, 1180, 1181, 5, 79, 0, 0, 1181, 1182, 3, 154, 77, 0, 1182, 167, 1, 0, 0, 0, 1183,
        1188, 3, 100, 50, 0, 1184, 1185, 5, 107, 0, 0, 1185, 1187, 3, 100, 50, 0, 1186, 1184,
        1, 0, 0, 0, 1187, 1190, 1, 0, 0, 0, 1188, 1186, 1, 0, 0, 0, 1188, 1189, 1, 0, 0, 0, 1189,
        169, 1, 0, 0, 0, 1190, 1188, 1, 0, 0, 0, 1191, 1192, 5, 19, 0, 0, 1192, 1193, 3, 154,
        77, 0, 1193, 171, 1, 0, 0, 0, 1194, 1195, 5, 78, 0, 0, 1195, 1197, 3, 174, 87, 0, 1196,
        1198, 5, 84, 0, 0, 1197, 1196, 1, 0, 0, 0, 1197, 1198, 1, 0, 0, 0, 1198, 1199, 1, 0,
        0, 0, 1199, 1200, 5, 79, 0, 0, 1200, 173, 1, 0, 0, 0, 1201, 1206, 3, 176, 88, 0, 1202,
        1203, 5, 84, 0, 0, 1203, 1205, 3, 176, 88, 0, 1204, 1202, 1, 0, 0, 0, 1205, 1208, 1,
        0, 0, 0, 1206, 1204, 1, 0, 0, 0, 1206, 1207, 1, 0, 0, 0, 1207, 175, 1, 0, 0, 0, 1208,
        1206, 1, 0, 0, 0, 1209, 1211, 3, 12, 6, 0, 1210, 1209, 1, 0, 0, 0, 1211, 1214, 1, 0,
        0, 0, 1212, 1210, 1, 0, 0, 0, 1212, 1213, 1, 0, 0, 0, 1213, 1220, 1, 0, 0, 0, 1214, 1212,
        1, 0, 0, 0, 1215, 1216, 3, 80, 40, 0, 1216, 1217, 3, 74, 37, 0, 1217, 1221, 1, 0, 0,
        0, 1218, 1219, 5, 61, 0, 0, 1219, 1221, 3, 160, 80, 0, 1220, 1215, 1, 0, 0, 0, 1220,
        1218, 1, 0, 0, 0, 1221, 1222, 1, 0, 0, 0, 1222, 1223, 5, 87, 0, 0, 1223, 1224, 3, 194,
        97, 0, 1224, 1227, 1, 0, 0, 0, 1225, 1227, 3, 160, 80, 0, 1226, 1212, 1, 0, 0, 0, 1226,
        1225, 1, 0, 0, 0, 1227, 177, 1, 0, 0, 0, 1228, 1230, 3, 180, 90, 0, 1229, 1228, 1, 0,
        0, 0, 1230, 1231, 1, 0, 0, 0, 1231, 1229, 1, 0, 0, 0, 1231, 1232, 1, 0, 0, 0, 1232, 1234,
        1, 0, 0, 0, 1233, 1235, 3, 156, 78, 0, 1234, 1233, 1, 0, 0, 0, 1235, 1236, 1, 0, 0, 0,
        1236, 1234, 1, 0, 0, 0, 1236, 1237, 1, 0, 0, 0, 1237, 179, 1, 0, 0, 0, 1238, 1244, 5,
        6, 0, 0, 1239, 1245, 3, 194, 97, 0, 1240, 1245, 5, 128, 0, 0, 1241, 1242, 3, 236, 118,
        0, 1242, 1243, 3, 160, 80, 0, 1243, 1245, 1, 0, 0, 0, 1244, 1239, 1, 0, 0, 0, 1244,
        1240, 1, 0, 0, 0, 1244, 1241, 1, 0, 0, 0, 1245, 1246, 1, 0, 0, 0, 1246, 1250, 5, 93,
        0, 0, 1247, 1248, 5, 12, 0, 0, 1248, 1250, 5, 93, 0, 0, 1249, 1238, 1, 0, 0, 0, 1249,
        1247, 1, 0, 0, 0, 1250, 181, 1, 0, 0, 0, 1251, 1264, 3, 186, 93, 0, 1252, 1254, 3, 184,
        92, 0, 1253, 1252, 1, 0, 0, 0, 1253, 1254, 1, 0, 0, 0, 1254, 1255, 1, 0, 0, 0, 1255,
        1257, 5, 84, 0, 0, 1256, 1258, 3, 194, 97, 0, 1257, 1256, 1, 0, 0, 0, 1257, 1258, 1,
        0, 0, 0, 1258, 1259, 1, 0, 0, 0, 1259, 1261, 5, 84, 0, 0, 1260, 1262, 3, 190, 95, 0,
        1261, 1260, 1, 0, 0, 0, 1261, 1262, 1, 0, 0, 0, 1262, 1264, 1, 0, 0, 0, 1263, 1251,
        1, 0, 0, 0, 1263, 1253, 1, 0, 0, 0, 1264, 183, 1, 0, 0, 0, 1265, 1268, 3, 158, 79, 0,
        1266, 1268, 3, 190, 95, 0, 1267, 1265, 1, 0, 0, 0, 1267, 1266, 1, 0, 0, 0, 1268, 185,
        1, 0, 0, 0, 1269, 1271, 3, 12, 6, 0, 1270, 1269, 1, 0, 0, 0, 1271, 1274, 1, 0, 0, 0, 1272,
        1270, 1, 0, 0, 0, 1272, 1273, 1, 0, 0, 0, 1273, 1277, 1, 0, 0, 0, 1274, 1272, 1, 0, 0,
        0, 1275, 1278, 3, 236, 118, 0, 1276, 1278, 5, 61, 0, 0, 1277, 1275, 1, 0, 0, 0, 1277,
        1276, 1, 0, 0, 0, 1278, 1279, 1, 0, 0, 0, 1279, 1280, 3, 74, 37, 0, 1280, 1281, 5, 93,
        0, 0, 1281, 1282, 3, 194, 97, 0, 1282, 187, 1, 0, 0, 0, 1283, 1284, 5, 78, 0, 0, 1284,
        1285, 3, 194, 97, 0, 1285, 1286, 5, 79, 0, 0, 1286, 189, 1, 0, 0, 0, 1287, 1292, 3,
        194, 97, 0, 1288, 1289, 5, 85, 0, 0, 1289, 1291, 3, 194, 97, 0, 1290, 1288, 1, 0, 0,
        0, 1291, 1294, 1, 0, 0, 0, 1292, 1290, 1, 0, 0, 0, 1292, 1293, 1, 0, 0, 0, 1293, 191,
        1, 0, 0, 0, 1294, 1292, 1, 0, 0, 0, 1295, 1296, 3, 160, 80, 0, 1296, 1298, 5, 78, 0,
        0, 1297, 1299, 3, 190, 95, 0, 1298, 1297, 1, 0, 0, 0, 1298, 1299, 1, 0, 0, 0, 1299,
        1300, 1, 0, 0, 0, 1300, 1301, 5, 79, 0, 0, 1301, 1315, 1, 0, 0, 0, 1302, 1303, 5, 43,
        0, 0, 1303, 1305, 5, 78, 0, 0, 1304, 1306, 3, 190, 95, 0, 1305, 1304, 1, 0, 0, 0, 1305,
        1306, 1, 0, 0, 0, 1306, 1307, 1, 0, 0, 0, 1307, 1315, 5, 79, 0, 0, 1308, 1309, 5, 40,
        0, 0, 1309, 1311, 5, 78, 0, 0, 1310, 1312, 3, 190, 95, 0, 1311, 1310, 1, 0, 0, 0, 1311,
        1312, 1, 0, 0, 0, 1312, 1313, 1, 0, 0, 0, 1313, 1315, 5, 79, 0, 0, 1314, 1295, 1, 0,
        0, 0, 1314, 1302, 1, 0, 0, 0, 1314, 1308, 1, 0, 0, 0, 1315, 193, 1, 0, 0, 0, 1316, 1317,
        6, 97, -1, 0, 1317, 1362, 3, 204, 102, 0, 1318, 1362, 3, 192, 96, 0, 1319, 1320, 5,
        31, 0, 0, 1320, 1362, 3, 216, 108, 0, 1321, 1325, 5, 78, 0, 0, 1322, 1324, 3, 110,
        55, 0, 1323, 1322, 1, 0, 0, 0, 1324, 1327, 1, 0, 0, 0, 1325, 1323, 1, 0, 0, 0, 1325,
        1326, 1, 0, 0, 0, 1326, 1328, 1, 0, 0, 0, 1327, 1325, 1, 0, 0, 0, 1328, 1333, 3, 236,
        118, 0, 1329, 1330, 5, 106, 0, 0, 1330, 1332, 3, 236, 118, 0, 1331, 1329, 1, 0, 0,
        0, 1332, 1335, 1, 0, 0, 0, 1333, 1331, 1, 0, 0, 0, 1333, 1334, 1, 0, 0, 0, 1334, 1336,
        1, 0, 0, 0, 1335, 1333, 1, 0, 0, 0, 1336, 1337, 5, 79, 0, 0, 1337, 1338, 3, 194, 97,
        22, 1338, 1362, 1, 0, 0, 0, 1339, 1340, 7, 5, 0, 0, 1340, 1362, 3, 194, 97, 20, 1341,
        1342, 7, 6, 0, 0, 1342, 1362, 3, 194, 97, 19, 1343, 1362, 3, 198, 99, 0, 1344, 1362,
        3, 206, 103, 0, 1345, 1346, 3, 236, 118, 0, 1346, 1352, 5, 122, 0, 0, 1347, 1349,
        3, 240, 120, 0, 1348, 1347, 1, 0, 0, 0, 1348, 1349, 1, 0, 0, 0, 1349, 1350, 1, 0, 0,
        0, 1350, 1353, 3, 160, 80, 0, 1351, 1353, 5, 31, 0, 0, 1352, 1348, 1, 0, 0, 0, 1352,
        1351, 1, 0, 0, 0, 1353, 1362, 1, 0, 0, 0, 1354, 1355, 3, 214, 107, 0, 1355, 1357, 5,
        122, 0, 0, 1356, 1358, 3, 240, 120, 0, 1357, 1356, 1, 0, 0, 0, 1357, 1358, 1, 0, 0,
        0, 1358, 1359, 1, 0, 0, 0, 1359, 1360, 5, 31, 0, 0, 1360, 1362, 1, 0, 0, 0, 1361, 1316,
        1, 0, 0, 0, 1361, 1318, 1, 0, 0, 0, 1361, 1319, 1, 0, 0, 0, 1361, 1321, 1, 0, 0, 0, 1361,
        1339, 1, 0, 0, 0, 1361, 1341, 1, 0, 0, 0, 1361, 1343, 1, 0, 0, 0, 1361, 1344, 1, 0, 0,
        0, 1361, 1345, 1, 0, 0, 0, 1361, 1354, 1, 0, 0, 0, 1362, 1446, 1, 0, 0, 0, 1363, 1364,
        10, 18, 0, 0, 1364, 1365, 7, 7, 0, 0, 1365, 1445, 3, 194, 97, 19, 1366, 1367, 10, 17,
        0, 0, 1367, 1368, 7, 8, 0, 0, 1368, 1445, 3, 194, 97, 18, 1369, 1377, 10, 16, 0, 0,
        1370, 1371, 5, 89, 0, 0, 1371, 1378, 5, 89, 0, 0, 1372, 1373, 5, 88, 0, 0, 1373, 1374,
        5, 88, 0, 0, 1374, 1378, 5, 88, 0, 0, 1375, 1376, 5, 88, 0, 0, 1376, 1378, 5, 88, 0,
        0, 1377, 1370, 1, 0, 0, 0, 1377, 1372, 1, 0, 0, 0, 1377, 1375, 1, 0, 0, 0, 1378, 1379,
        1, 0, 0, 0, 1379, 1445, 3, 194, 97, 17, 1380, 1381, 10, 15, 0, 0, 1381, 1382, 7, 9,
        0, 0, 1382, 1445, 3, 194, 97, 16, 1383, 1384, 10, 13, 0, 0, 1384, 1385, 7, 10, 0, 0,
        1385, 1445, 3, 194, 97, 14, 1386, 1387, 10, 12, 0, 0, 1387, 1388, 5, 106, 0, 0, 1388,
        1445, 3, 194, 97, 13, 1389, 1390, 10, 11, 0, 0, 1390, 1391, 5, 108, 0, 0, 1391, 1445,
        3, 194, 97, 12, 1392, 1393, 10, 10, 0, 0, 1393, 1394, 5, 107, 0, 0, 1394, 1445, 3,
        194, 97, 11, 1395, 1396, 10, 9, 0, 0, 1396, 1397, 5, 98, 0, 0, 1397, 1445, 3, 194,
        97, 10, 1398, 1399, 10, 8, 0, 0, 1399, 1400, 5, 99, 0, 0, 1400, 1445, 3, 194, 97, 9,
        1401, 1402, 10, 7, 0, 0, 1402, 1403, 5, 92, 0, 0, 1403, 1404, 3, 194, 97, 0, 1404,
        1405, 5, 93, 0, 0, 1405, 1406, 3, 194, 97, 7, 1406, 1445, 1, 0, 0, 0, 1407, 1408, 10,
        6, 0, 0, 1408, 1409, 7, 11, 0, 0, 1409, 1445, 3, 194, 97, 6, 1410, 1411, 10, 26, 0,
        0, 1411, 1423, 5, 86, 0, 0, 1412, 1424, 3, 160, 80, 0, 1413, 1424, 3, 192, 96, 0, 1414,
        1424, 5, 43, 0, 0, 1415, 1417, 5, 31, 0, 0, 1416, 1418, 3, 232, 116, 0, 1417, 1416,
        1, 0, 0, 0, 1417, 1418, 1, 0, 0, 0, 1418, 1419, 1, 0, 0, 0, 1419, 1424, 3, 220, 110,
        0, 1420, 1421, 5, 40, 0, 0, 1421, 1424, 3, 242, 121, 0, 1422, 1424, 3, 226, 113, 0,
        1423, 1412, 1, 0, 0, 0, 1423, 1413, 1, 0, 0, 0, 1423, 1414, 1, 0, 0, 0, 1423, 1415,
        1, 0, 0, 0, 1423, 1420, 1, 0, 0, 0, 1423, 1422, 1, 0, 0, 0, 1424, 1445, 1, 0, 0, 0, 1425,
        1426, 10, 25, 0, 0, 1426, 1427, 5, 82, 0, 0, 1427, 1428, 3, 194, 97, 0, 1428, 1429,
        5, 83, 0, 0, 1429, 1445, 1, 0, 0, 0, 1430, 1431, 10, 21, 0, 0, 1431, 1445, 7, 12, 0,
        0, 1432, 1433, 10, 14, 0, 0, 1433, 1436, 5, 26, 0, 0, 1434, 1437, 3, 236, 118, 0, 1435,
        1437, 3, 196, 98, 0, 1436, 1434, 1, 0, 0, 0, 1436, 1435, 1, 0, 0, 0, 1437, 1445, 1,
        0, 0, 0, 1438, 1439, 10, 3, 0, 0, 1439, 1441, 5, 122, 0, 0, 1440, 1442, 3, 240, 120,
        0, 1441, 1440, 1, 0, 0, 0, 1441, 1442, 1, 0, 0, 0, 1442, 1443, 1, 0, 0, 0, 1443, 1445,
        3, 160, 80, 0, 1444, 1363, 1, 0, 0, 0, 1444, 1366, 1, 0, 0, 0, 1444, 1369, 1, 0, 0, 0,
        1444, 1380, 1, 0, 0, 0, 1444, 1383, 1, 0, 0, 0, 1444, 1386, 1, 0, 0, 0, 1444, 1389,
        1, 0, 0, 0, 1444, 1392, 1, 0, 0, 0, 1444, 1395, 1, 0, 0, 0, 1444, 1398, 1, 0, 0, 0, 1444,
        1401, 1, 0, 0, 0, 1444, 1407, 1, 0, 0, 0, 1444, 1410, 1, 0, 0, 0, 1444, 1425, 1, 0, 0,
        0, 1444, 1430, 1, 0, 0, 0, 1444, 1432, 1, 0, 0, 0, 1444, 1438, 1, 0, 0, 0, 1445, 1448,
        1, 0, 0, 0, 1446, 1444, 1, 0, 0, 0, 1446, 1447, 1, 0, 0, 0, 1447, 195, 1, 0, 0, 0, 1448,
        1446, 1, 0, 0, 0, 1449, 1451, 3, 12, 6, 0, 1450, 1449, 1, 0, 0, 0, 1451, 1454, 1, 0,
        0, 0, 1452, 1450, 1, 0, 0, 0, 1452, 1453, 1, 0, 0, 0, 1453, 1455, 1, 0, 0, 0, 1454, 1452,
        1, 0, 0, 0, 1455, 1459, 3, 236, 118, 0, 1456, 1458, 3, 110, 55, 0, 1457, 1456, 1, 0,
        0, 0, 1458, 1461, 1, 0, 0, 0, 1459, 1457, 1, 0, 0, 0, 1459, 1460, 1, 0, 0, 0, 1460, 1462,
        1, 0, 0, 0, 1461, 1459, 1, 0, 0, 0, 1462, 1463, 3, 160, 80, 0, 1463, 197, 1, 0, 0, 0,
        1464, 1465, 3, 200, 100, 0, 1465, 1466, 5, 121, 0, 0, 1466, 1467, 3, 202, 101, 0,
        1467, 199, 1, 0, 0, 0, 1468, 1491, 3, 160, 80, 0, 1469, 1471, 5, 78, 0, 0, 1470, 1472,
        3, 90, 45, 0, 1471, 1470, 1, 0, 0, 0, 1471, 1472, 1, 0, 0, 0, 1472, 1473, 1, 0, 0, 0,
        1473, 1491, 5, 79, 0, 0, 1474, 1475, 5, 78, 0, 0, 1475, 1480, 3, 160, 80, 0, 1476,
        1477, 5, 85, 0, 0, 1477, 1479, 3, 160, 80, 0, 1478, 1476, 1, 0, 0, 0, 1479, 1482, 1,
        0, 0, 0, 1480, 1478, 1, 0, 0, 0, 1480, 1481, 1, 0, 0, 0, 1481, 1483, 1, 0, 0, 0, 1482,
        1480, 1, 0, 0, 0, 1483, 1484, 5, 79, 0, 0, 1484, 1491, 1, 0, 0, 0, 1485, 1487, 5, 78,
        0, 0, 1486, 1488, 3, 96, 48, 0, 1487, 1486, 1, 0, 0, 0, 1487, 1488, 1, 0, 0, 0, 1488,
        1489, 1, 0, 0, 0, 1489, 1491, 5, 79, 0, 0, 1490, 1468, 1, 0, 0, 0, 1490, 1469, 1, 0,
        0, 0, 1490, 1474, 1, 0, 0, 0, 1490, 1485, 1, 0, 0, 0, 1491, 201, 1, 0, 0, 0, 1492, 1495,
        3, 194, 97, 0, 1493, 1495, 3, 154, 77, 0, 1494, 1492, 1, 0, 0, 0, 1494, 1493, 1, 0,
        0, 0, 1495, 203, 1, 0, 0, 0, 1496, 1497, 5, 78, 0, 0, 1497, 1498, 3, 194, 97, 0, 1498,
        1499, 5, 79, 0, 0, 1499, 1515, 1, 0, 0, 0, 1500, 1515, 5, 43, 0, 0, 1501, 1515, 5, 40,
        0, 0, 1502, 1515, 3, 102, 51, 0, 1503, 1515, 3, 160, 80, 0, 1504, 1505, 3, 44, 22,
        0, 1505, 1506, 5, 86, 0, 0, 1506, 1507, 5, 9, 0, 0, 1507, 1515, 1, 0, 0, 0, 1508, 1512,
        3, 232, 116, 0, 1509, 1513, 3, 244, 122, 0, 1510, 1511, 5, 43, 0, 0, 1511, 1513, 3,
        246, 123, 0, 1512, 1509, 1, 0, 0, 0, 1512, 1510, 1, 0, 0, 0, 1513, 1515, 1, 0, 0, 0,
        1514, 1496, 1, 0, 0, 0, 1514, 1500, 1, 0, 0, 0, 1514, 1501, 1, 0, 0, 0, 1514, 1502,
        1, 0, 0, 0, 1514, 1503, 1, 0, 0, 0, 1514, 1504, 1, 0, 0, 0, 1514, 1508, 1, 0, 0, 0, 1515,
        205, 1, 0, 0, 0, 1516, 1517, 5, 41, 0, 0, 1517, 1518, 3, 188, 94, 0, 1518, 1522, 5,
        80, 0, 0, 1519, 1521, 3, 208, 104, 0, 1520, 1519, 1, 0, 0, 0, 1521, 1524, 1, 0, 0, 0,
        1522, 1520, 1, 0, 0, 0, 1522, 1523, 1, 0, 0, 0, 1523, 1525, 1, 0, 0, 0, 1524, 1522,
        1, 0, 0, 0, 1525, 1526, 5, 81, 0, 0, 1526, 207, 1, 0, 0, 0, 1527, 1531, 5, 6, 0, 0, 1528,
        1532, 3, 190, 95, 0, 1529, 1532, 5, 77, 0, 0, 1530, 1532, 3, 210, 105, 0, 1531, 1528,
        1, 0, 0, 0, 1531, 1529, 1, 0, 0, 0, 1531, 1530, 1, 0, 0, 0, 1532, 1533, 1, 0, 0, 0, 1533,
        1534, 7, 13, 0, 0, 1534, 1539, 3, 212, 106, 0, 1535, 1536, 5, 12, 0, 0, 1536, 1537,
        7, 13, 0, 0, 1537, 1539, 3, 212, 106, 0, 1538, 1527, 1, 0, 0, 0, 1538, 1535, 1, 0, 0,
        0, 1539, 209, 1, 0, 0, 0, 1540, 1541, 6, 105, -1, 0, 1541, 1542, 5, 78, 0, 0, 1542,
        1543, 3, 210, 105, 0, 1543, 1544, 5, 79, 0, 0, 1544, 1567, 1, 0, 0, 0, 1545, 1547,
        3, 12, 6, 0, 1546, 1545, 1, 0, 0, 0, 1547, 1550, 1, 0, 0, 0, 1548, 1546, 1, 0, 0, 0, 1548,
        1549, 1, 0, 0, 0, 1549, 1551, 1, 0, 0, 0, 1550, 1548, 1, 0, 0, 0, 1551, 1555, 3, 236,
        118, 0, 1552, 1554, 3, 110, 55, 0, 1553, 1552, 1, 0, 0, 0, 1554, 1557, 1, 0, 0, 0, 1555,
        1553, 1, 0, 0, 0, 1555, 1556, 1, 0, 0, 0, 1556, 1558, 1, 0, 0, 0, 1557, 1555, 1, 0, 0,
        0, 1558, 1563, 3, 160, 80, 0, 1559, 1560, 5, 98, 0, 0, 1560, 1562, 3, 194, 97, 0, 1561,
        1559, 1, 0, 0, 0, 1562, 1565, 1, 0, 0, 0, 1563, 1561, 1, 0, 0, 0, 1563, 1564, 1, 0, 0,
        0, 1564, 1567, 1, 0, 0, 0, 1565, 1563, 1, 0, 0, 0, 1566, 1540, 1, 0, 0, 0, 1566, 1548,
        1, 0, 0, 0, 1567, 1573, 1, 0, 0, 0, 1568, 1569, 10, 1, 0, 0, 1569, 1570, 5, 98, 0, 0,
        1570, 1572, 3, 194, 97, 0, 1571, 1568, 1, 0, 0, 0, 1572, 1575, 1, 0, 0, 0, 1573, 1571,
        1, 0, 0, 0, 1573, 1574, 1, 0, 0, 0, 1574, 211, 1, 0, 0, 0, 1575, 1573, 1, 0, 0, 0, 1576,
        1584, 3, 154, 77, 0, 1577, 1579, 3, 156, 78, 0, 1578, 1577, 1, 0, 0, 0, 1579, 1582,
        1, 0, 0, 0, 1580, 1578, 1, 0, 0, 0, 1580, 1581, 1, 0, 0, 0, 1581, 1584, 1, 0, 0, 0, 1582,
        1580, 1, 0, 0, 0, 1583, 1576, 1, 0, 0, 0, 1583, 1580, 1, 0, 0, 0, 1584, 213, 1, 0, 0,
        0, 1585, 1586, 3, 80, 40, 0, 1586, 1587, 5, 86, 0, 0, 1587, 1589, 1, 0, 0, 0, 1588,
        1585, 1, 0, 0, 0, 1588, 1589, 1, 0, 0, 0, 1589, 1593, 1, 0, 0, 0, 1590, 1592, 3, 110,
        55, 0, 1591, 1590, 1, 0, 0, 0, 1592, 1595, 1, 0, 0, 0, 1593, 1591, 1, 0, 0, 0, 1593,
        1594, 1, 0, 0, 0, 1594, 1596, 1, 0, 0, 0, 1595, 1593, 1, 0, 0, 0, 1596, 1598, 3, 160,
        80, 0, 1597, 1599, 3, 240, 120, 0, 1598, 1597, 1, 0, 0, 0, 1598, 1599, 1, 0, 0, 0, 1599,
        215, 1, 0, 0, 0, 1600, 1601, 3, 232, 116, 0, 1601, 1602, 3, 218, 109, 0, 1602, 1603,
        3, 224, 112, 0, 1603, 1610, 1, 0, 0, 0, 1604, 1607, 3, 218, 109, 0, 1605, 1608, 3,
        222, 111, 0, 1606, 1608, 3, 224, 112, 0, 1607, 1605, 1, 0, 0, 0, 1607, 1606, 1, 0,
        0, 0, 1608, 1610, 1, 0, 0, 0, 1609, 1600, 1, 0, 0, 0, 1609, 1604, 1, 0, 0, 0, 1610, 217,
        1, 0, 0, 0, 1611, 1613, 3, 160, 80, 0, 1612, 1614, 3, 228, 114, 0, 1613, 1612, 1, 0,
        0, 0, 1613, 1614, 1, 0, 0, 0, 1614, 1622, 1, 0, 0, 0, 1615, 1616, 5, 86, 0, 0, 1616,
        1618, 3, 160, 80, 0, 1617, 1619, 3, 228, 114, 0, 1618, 1617, 1, 0, 0, 0, 1618, 1619,
        1, 0, 0, 0, 1619, 1621, 1, 0, 0, 0, 1620, 1615, 1, 0, 0, 0, 1621, 1624, 1, 0, 0, 0, 1622,
        1620, 1, 0, 0, 0, 1622, 1623, 1, 0, 0, 0, 1623, 1627, 1, 0, 0, 0, 1624, 1622, 1, 0, 0,
        0, 1625, 1627, 3, 238, 119, 0, 1626, 1611, 1, 0, 0, 0, 1626, 1625, 1, 0, 0, 0, 1627,
        219, 1, 0, 0, 0, 1628, 1630, 3, 160, 80, 0, 1629, 1631, 3, 230, 115, 0, 1630, 1629,
        1, 0, 0, 0, 1630, 1631, 1, 0, 0, 0, 1631, 1632, 1, 0, 0, 0, 1632, 1633, 3, 224, 112,
        0, 1633, 221, 1, 0, 0, 0, 1634, 1662, 5, 82, 0, 0, 1635, 1640, 5, 83, 0, 0, 1636, 1637,
        5, 82, 0, 0, 1637, 1639, 5, 83, 0, 0, 1638, 1636, 1, 0, 0, 0, 1639, 1642, 1, 0, 0, 0,
        1640, 1638, 1, 0, 0, 0, 1640, 1641, 1, 0, 0, 0, 1641, 1643, 1, 0, 0, 0, 1642, 1640,
        1, 0, 0, 0, 1643, 1663, 3, 78, 39, 0, 1644, 1645, 3, 194, 97, 0, 1645, 1652, 5, 83,
        0, 0, 1646, 1647, 5, 82, 0, 0, 1647, 1648, 3, 194, 97, 0, 1648, 1649, 5, 83, 0, 0, 1649,
        1651, 1, 0, 0, 0, 1650, 1646, 1, 0, 0, 0, 1651, 1654, 1, 0, 0, 0, 1652, 1650, 1, 0, 0,
        0, 1652, 1653, 1, 0, 0, 0, 1653, 1659, 1, 0, 0, 0, 1654, 1652, 1, 0, 0, 0, 1655, 1656,
        5, 82, 0, 0, 1656, 1658, 5, 83, 0, 0, 1657, 1655, 1, 0, 0, 0, 1658, 1661, 1, 0, 0, 0,
        1659, 1657, 1, 0, 0, 0, 1659, 1660, 1, 0, 0, 0, 1660, 1663, 1, 0, 0, 0, 1661, 1659,
        1, 0, 0, 0, 1662, 1635, 1, 0, 0, 0, 1662, 1644, 1, 0, 0, 0, 1663, 223, 1, 0, 0, 0, 1664,
        1666, 3, 246, 123, 0, 1665, 1667, 3, 32, 16, 0, 1666, 1665, 1, 0, 0, 0, 1666, 1667,
        1, 0, 0, 0, 1667, 225, 1, 0, 0, 0, 1668, 1669, 3, 232, 116, 0, 1669, 1670, 3, 244, 122,
        0, 1670, 227, 1, 0, 0, 0, 1671, 1672, 5, 89, 0, 0, 1672, 1675, 5, 88, 0, 0, 1673, 1675,
        3, 240, 120, 0, 1674, 1671, 1, 0, 0, 0, 1674, 1673, 1, 0, 0, 0, 1675, 229, 1, 0, 0, 0,
        1676, 1677, 5, 89, 0, 0, 1677, 1680, 5, 88, 0, 0, 1678, 1680, 3, 232, 116, 0, 1679,
        1676, 1, 0, 0, 0, 1679, 1678, 1, 0, 0, 0, 1680, 231, 1, 0, 0, 0, 1681, 1682, 5, 89, 0,
        0, 1682, 1683, 3, 234, 117, 0, 1683, 1684, 5, 88, 0, 0, 1684, 233, 1, 0, 0, 0, 1685,
        1690, 3, 236, 118, 0, 1686, 1687, 5, 85, 0, 0, 1687, 1689, 3, 236, 118, 0, 1688, 1686,
        1, 0, 0, 0, 1689, 1692, 1, 0, 0, 0, 1690, 1688, 1, 0, 0, 0, 1690, 1691, 1, 0, 0, 0, 1691,
        235, 1, 0, 0, 0, 1692, 1690, 1, 0, 0, 0, 1693, 1695, 3, 110, 55, 0, 1694, 1693, 1, 0,
        0, 0, 1695, 1698, 1, 0, 0, 0, 1696, 1694, 1, 0, 0, 0, 1696, 1697, 1, 0, 0, 0, 1697, 1701,
        1, 0, 0, 0, 1698, 1696, 1, 0, 0, 0, 1699, 1702, 3, 80, 40, 0, 1700, 1702, 3, 238, 119,
        0, 1701, 1699, 1, 0, 0, 0, 1701, 1700, 1, 0, 0, 0, 1702, 1713, 1, 0, 0, 0, 1703, 1705,
        3, 110, 55, 0, 1704, 1703, 1, 0, 0, 0, 1705, 1708, 1, 0, 0, 0, 1706, 1704, 1, 0, 0, 0,
        1706, 1707, 1, 0, 0, 0, 1707, 1709, 1, 0, 0, 0, 1708, 1706, 1, 0, 0, 0, 1709, 1710,
        5, 82, 0, 0, 1710, 1712, 5, 83, 0, 0, 1711, 1706, 1, 0, 0, 0, 1712, 1715, 1, 0, 0, 0,
        1713, 1711, 1, 0, 0, 0, 1713, 1714, 1, 0, 0, 0, 1714, 237, 1, 0, 0, 0, 1715, 1713, 1,
        0, 0, 0, 1716, 1717, 7, 14, 0, 0, 1717, 239, 1, 0, 0, 0, 1718, 1719, 5, 89, 0, 0, 1719,
        1724, 3, 82, 41, 0, 1720, 1721, 5, 85, 0, 0, 1721, 1723, 3, 82, 41, 0, 1722, 1720,
        1, 0, 0, 0, 1723, 1726, 1, 0, 0, 0, 1724, 1722, 1, 0, 0, 0, 1724, 1725, 1, 0, 0, 0, 1725,
        1727, 1, 0, 0, 0, 1726, 1724, 1, 0, 0, 0, 1727, 1728, 5, 88, 0, 0, 1728, 241, 1, 0, 0,
        0, 1729, 1739, 3, 246, 123, 0, 1730, 1732, 5, 86, 0, 0, 1731, 1733, 3, 240, 120, 0,
        1732, 1731, 1, 0, 0, 0, 1732, 1733, 1, 0, 0, 0, 1733, 1734, 1, 0, 0, 0, 1734, 1736,
        3, 160, 80, 0, 1735, 1737, 3, 246, 123, 0, 1736, 1735, 1, 0, 0, 0, 1736, 1737, 1, 0,
        0, 0, 1737, 1739, 1, 0, 0, 0, 1738, 1729, 1, 0, 0, 0, 1738, 1730, 1, 0, 0, 0, 1739, 243,
        1, 0, 0, 0, 1740, 1741, 5, 40, 0, 0, 1741, 1746, 3, 242, 121, 0, 1742, 1743, 3, 160,
        80, 0, 1743, 1744, 3, 246, 123, 0, 1744, 1746, 1, 0, 0, 0, 1745, 1740, 1, 0, 0, 0, 1745,
        1742, 1, 0, 0, 0, 1746, 245, 1, 0, 0, 0, 1747, 1749, 5, 78, 0, 0, 1748, 1750, 3, 190,
        95, 0, 1749, 1748, 1, 0, 0, 0, 1749, 1750, 1, 0, 0, 0, 1750, 1751, 1, 0, 0, 0, 1751,
        1752, 5, 79, 0, 0, 1752, 247, 1, 0, 0, 0, 218, 249, 254, 260, 267, 272, 281, 286, 293,
        301, 304, 311, 323, 327, 332, 336, 340, 344, 354, 362, 370, 374, 381, 388, 392,
        395, 398, 407, 413, 418, 421, 427, 433, 437, 445, 454, 461, 467, 471, 483, 492,
        497, 503, 507, 519, 530, 535, 545, 553, 563, 572, 583, 588, 597, 607, 612, 621,
        627, 634, 639, 647, 651, 653, 659, 664, 668, 675, 681, 683, 690, 695, 700, 703,
        705, 715, 725, 730, 733, 738, 747, 754, 765, 771, 782, 792, 803, 812, 817, 820,
        827, 837, 845, 848, 851, 864, 872, 877, 885, 889, 893, 897, 901, 903, 907, 913,
        921, 931, 940, 950, 958, 972, 979, 984, 990, 999, 1009, 1018, 1028, 1033, 1044,
        1050, 1053, 1060, 1069, 1092, 1095, 1098, 1106, 1110, 1118, 1124, 1135, 1144,
        1149, 1162, 1168, 1175, 1188, 1197, 1206, 1212, 1220, 1226, 1231, 1236, 1244,
        1249, 1253, 1257, 1261, 1263, 1267, 1272, 1277, 1292, 1298, 1305, 1311, 1314,
        1325, 1333, 1348, 1352, 1357, 1361, 1377, 1417, 1423, 1436, 1441, 1444, 1446,
        1452, 1459, 1471, 1480, 1487, 1490, 1494, 1512, 1514, 1522, 1531, 1538, 1548,
        1555, 1563, 1566, 1573, 1580, 1583, 1588, 1593, 1598, 1607, 1609, 1613, 1618,
        1622, 1626, 1630, 1640, 1652, 1659, 1662, 1666, 1674, 1679, 1690, 1696, 1701,
        1706, 1713, 1724, 1732, 1736, 1738, 1745, 1749
    ];
    static __ATN;
    static get _ATN() {
        if (!JavaParser.__ATN) {
            JavaParser.__ATN = new antlr.ATNDeserializer().deserialize(JavaParser._serializedATN);
        }
        return JavaParser.__ATN;
    }
    static vocabulary = new antlr.Vocabulary(JavaParser.literalNames, JavaParser.symbolicNames, []);
    get vocabulary() {
        return JavaParser.vocabulary;
    }
    static decisionsToDFA = JavaParser._ATN.decisionToState.map((ds, index) => new antlr.DFA(ds, index));
}
export class CompilationUnitContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    EOF() {
        return this.getToken(JavaParser.EOF, 0);
    }
    packageDeclaration() {
        return this.getRuleContext(0, PackageDeclarationContext);
    }
    importDeclaration(i) {
        if (i === undefined) {
            return this.getRuleContexts(ImportDeclarationContext);
        }
        return this.getRuleContext(i, ImportDeclarationContext);
    }
    typeDeclaration(i) {
        if (i === undefined) {
            return this.getRuleContexts(TypeDeclarationContext);
        }
        return this.getRuleContext(i, TypeDeclarationContext);
    }
    moduleDeclaration() {
        return this.getRuleContext(0, ModuleDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_compilationUnit;
    }
    enterRule(listener) {
        if (listener.enterCompilationUnit) {
            listener.enterCompilationUnit(this);
        }
    }
    exitRule(listener) {
        if (listener.exitCompilationUnit) {
            listener.exitCompilationUnit(this);
        }
    }
    accept(visitor) {
        if (visitor.visitCompilationUnit) {
            return visitor.visitCompilationUnit(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class PackageDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    PACKAGE() {
        return this.getToken(JavaParser.PACKAGE, 0);
    }
    qualifiedName() {
        return this.getRuleContext(0, QualifiedNameContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_packageDeclaration;
    }
    enterRule(listener) {
        if (listener.enterPackageDeclaration) {
            listener.enterPackageDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitPackageDeclaration) {
            listener.exitPackageDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitPackageDeclaration) {
            return visitor.visitPackageDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ImportDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    IMPORT() {
        return this.getToken(JavaParser.IMPORT, 0);
    }
    qualifiedName() {
        return this.getRuleContext(0, QualifiedNameContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    STATIC() {
        return this.getToken(JavaParser.STATIC, 0);
    }
    DOT() {
        return this.getToken(JavaParser.DOT, 0);
    }
    MUL() {
        return this.getToken(JavaParser.MUL, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_importDeclaration;
    }
    enterRule(listener) {
        if (listener.enterImportDeclaration) {
            listener.enterImportDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitImportDeclaration) {
            listener.exitImportDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitImportDeclaration) {
            return visitor.visitImportDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    classDeclaration() {
        return this.getRuleContext(0, ClassDeclarationContext);
    }
    enumDeclaration() {
        return this.getRuleContext(0, EnumDeclarationContext);
    }
    interfaceDeclaration() {
        return this.getRuleContext(0, InterfaceDeclarationContext);
    }
    annotationTypeDeclaration() {
        return this.getRuleContext(0, AnnotationTypeDeclarationContext);
    }
    recordDeclaration() {
        return this.getRuleContext(0, RecordDeclarationContext);
    }
    classOrInterfaceModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(ClassOrInterfaceModifierContext);
        }
        return this.getRuleContext(i, ClassOrInterfaceModifierContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_typeDeclaration;
    }
    enterRule(listener) {
        if (listener.enterTypeDeclaration) {
            listener.enterTypeDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeDeclaration) {
            listener.exitTypeDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeDeclaration) {
            return visitor.visitTypeDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ModifierContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    classOrInterfaceModifier() {
        return this.getRuleContext(0, ClassOrInterfaceModifierContext);
    }
    NATIVE() {
        return this.getToken(JavaParser.NATIVE, 0);
    }
    SYNCHRONIZED() {
        return this.getToken(JavaParser.SYNCHRONIZED, 0);
    }
    TRANSIENT() {
        return this.getToken(JavaParser.TRANSIENT, 0);
    }
    VOLATILE() {
        return this.getToken(JavaParser.VOLATILE, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_modifier;
    }
    enterRule(listener) {
        if (listener.enterModifier) {
            listener.enterModifier(this);
        }
    }
    exitRule(listener) {
        if (listener.exitModifier) {
            listener.exitModifier(this);
        }
    }
    accept(visitor) {
        if (visitor.visitModifier) {
            return visitor.visitModifier(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ClassOrInterfaceModifierContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    annotation() {
        return this.getRuleContext(0, AnnotationContext);
    }
    PUBLIC() {
        return this.getToken(JavaParser.PUBLIC, 0);
    }
    PROTECTED() {
        return this.getToken(JavaParser.PROTECTED, 0);
    }
    PRIVATE() {
        return this.getToken(JavaParser.PRIVATE, 0);
    }
    STATIC() {
        return this.getToken(JavaParser.STATIC, 0);
    }
    ABSTRACT() {
        return this.getToken(JavaParser.ABSTRACT, 0);
    }
    FINAL() {
        return this.getToken(JavaParser.FINAL, 0);
    }
    STRICTFP() {
        return this.getToken(JavaParser.STRICTFP, 0);
    }
    SEALED() {
        return this.getToken(JavaParser.SEALED, 0);
    }
    NON_SEALED() {
        return this.getToken(JavaParser.NON_SEALED, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_classOrInterfaceModifier;
    }
    enterRule(listener) {
        if (listener.enterClassOrInterfaceModifier) {
            listener.enterClassOrInterfaceModifier(this);
        }
    }
    exitRule(listener) {
        if (listener.exitClassOrInterfaceModifier) {
            listener.exitClassOrInterfaceModifier(this);
        }
    }
    accept(visitor) {
        if (visitor.visitClassOrInterfaceModifier) {
            return visitor.visitClassOrInterfaceModifier(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class VariableModifierContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    FINAL() {
        return this.getToken(JavaParser.FINAL, 0);
    }
    annotation() {
        return this.getRuleContext(0, AnnotationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_variableModifier;
    }
    enterRule(listener) {
        if (listener.enterVariableModifier) {
            listener.enterVariableModifier(this);
        }
    }
    exitRule(listener) {
        if (listener.exitVariableModifier) {
            listener.exitVariableModifier(this);
        }
    }
    accept(visitor) {
        if (visitor.visitVariableModifier) {
            return visitor.visitVariableModifier(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ClassDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    CLASS() {
        return this.getToken(JavaParser.CLASS, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    classBody() {
        return this.getRuleContext(0, ClassBodyContext);
    }
    typeParameters() {
        return this.getRuleContext(0, TypeParametersContext);
    }
    EXTENDS() {
        return this.getToken(JavaParser.EXTENDS, 0);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    IMPLEMENTS() {
        return this.getToken(JavaParser.IMPLEMENTS, 0);
    }
    typeList(i) {
        if (i === undefined) {
            return this.getRuleContexts(TypeListContext);
        }
        return this.getRuleContext(i, TypeListContext);
    }
    PERMITS() {
        return this.getToken(JavaParser.PERMITS, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_classDeclaration;
    }
    enterRule(listener) {
        if (listener.enterClassDeclaration) {
            listener.enterClassDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitClassDeclaration) {
            listener.exitClassDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitClassDeclaration) {
            return visitor.visitClassDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeParametersContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LT() {
        return this.getToken(JavaParser.LT, 0);
    }
    typeParameter(i) {
        if (i === undefined) {
            return this.getRuleContexts(TypeParameterContext);
        }
        return this.getRuleContext(i, TypeParameterContext);
    }
    GT() {
        return this.getToken(JavaParser.GT, 0);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_typeParameters;
    }
    enterRule(listener) {
        if (listener.enterTypeParameters) {
            listener.enterTypeParameters(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeParameters) {
            listener.exitTypeParameters(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeParameters) {
            return visitor.visitTypeParameters(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeParameterContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    EXTENDS() {
        return this.getToken(JavaParser.EXTENDS, 0);
    }
    typeBound() {
        return this.getRuleContext(0, TypeBoundContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_typeParameter;
    }
    enterRule(listener) {
        if (listener.enterTypeParameter) {
            listener.enterTypeParameter(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeParameter) {
            listener.exitTypeParameter(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeParameter) {
            return visitor.visitTypeParameter(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeBoundContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType(i) {
        if (i === undefined) {
            return this.getRuleContexts(TypeTypeContext);
        }
        return this.getRuleContext(i, TypeTypeContext);
    }
    BITAND(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.BITAND);
        }
        else {
            return this.getToken(JavaParser.BITAND, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_typeBound;
    }
    enterRule(listener) {
        if (listener.enterTypeBound) {
            listener.enterTypeBound(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeBound) {
            listener.exitTypeBound(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeBound) {
            return visitor.visitTypeBound(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class EnumDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    ENUM() {
        return this.getToken(JavaParser.ENUM, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    IMPLEMENTS() {
        return this.getToken(JavaParser.IMPLEMENTS, 0);
    }
    typeList() {
        return this.getRuleContext(0, TypeListContext);
    }
    enumConstants() {
        return this.getRuleContext(0, EnumConstantsContext);
    }
    COMMA() {
        return this.getToken(JavaParser.COMMA, 0);
    }
    enumBodyDeclarations() {
        return this.getRuleContext(0, EnumBodyDeclarationsContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_enumDeclaration;
    }
    enterRule(listener) {
        if (listener.enterEnumDeclaration) {
            listener.enterEnumDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitEnumDeclaration) {
            listener.exitEnumDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitEnumDeclaration) {
            return visitor.visitEnumDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class EnumConstantsContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    enumConstant(i) {
        if (i === undefined) {
            return this.getRuleContexts(EnumConstantContext);
        }
        return this.getRuleContext(i, EnumConstantContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_enumConstants;
    }
    enterRule(listener) {
        if (listener.enterEnumConstants) {
            listener.enterEnumConstants(this);
        }
    }
    exitRule(listener) {
        if (listener.exitEnumConstants) {
            listener.exitEnumConstants(this);
        }
    }
    accept(visitor) {
        if (visitor.visitEnumConstants) {
            return visitor.visitEnumConstants(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class EnumConstantContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    arguments() {
        return this.getRuleContext(0, ArgumentsContext);
    }
    classBody() {
        return this.getRuleContext(0, ClassBodyContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_enumConstant;
    }
    enterRule(listener) {
        if (listener.enterEnumConstant) {
            listener.enterEnumConstant(this);
        }
    }
    exitRule(listener) {
        if (listener.exitEnumConstant) {
            listener.exitEnumConstant(this);
        }
    }
    accept(visitor) {
        if (visitor.visitEnumConstant) {
            return visitor.visitEnumConstant(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class EnumBodyDeclarationsContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    classBodyDeclaration(i) {
        if (i === undefined) {
            return this.getRuleContexts(ClassBodyDeclarationContext);
        }
        return this.getRuleContext(i, ClassBodyDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_enumBodyDeclarations;
    }
    enterRule(listener) {
        if (listener.enterEnumBodyDeclarations) {
            listener.enterEnumBodyDeclarations(this);
        }
    }
    exitRule(listener) {
        if (listener.exitEnumBodyDeclarations) {
            listener.exitEnumBodyDeclarations(this);
        }
    }
    accept(visitor) {
        if (visitor.visitEnumBodyDeclarations) {
            return visitor.visitEnumBodyDeclarations(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class InterfaceDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    INTERFACE() {
        return this.getToken(JavaParser.INTERFACE, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    interfaceBody() {
        return this.getRuleContext(0, InterfaceBodyContext);
    }
    typeParameters() {
        return this.getRuleContext(0, TypeParametersContext);
    }
    EXTENDS() {
        return this.getToken(JavaParser.EXTENDS, 0);
    }
    typeList() {
        return this.getRuleContext(0, TypeListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_interfaceDeclaration;
    }
    enterRule(listener) {
        if (listener.enterInterfaceDeclaration) {
            listener.enterInterfaceDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitInterfaceDeclaration) {
            listener.exitInterfaceDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitInterfaceDeclaration) {
            return visitor.visitInterfaceDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ClassBodyContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    classBodyDeclaration(i) {
        if (i === undefined) {
            return this.getRuleContexts(ClassBodyDeclarationContext);
        }
        return this.getRuleContext(i, ClassBodyDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_classBody;
    }
    enterRule(listener) {
        if (listener.enterClassBody) {
            listener.enterClassBody(this);
        }
    }
    exitRule(listener) {
        if (listener.exitClassBody) {
            listener.exitClassBody(this);
        }
    }
    accept(visitor) {
        if (visitor.visitClassBody) {
            return visitor.visitClassBody(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class InterfaceBodyContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    interfaceBodyDeclaration(i) {
        if (i === undefined) {
            return this.getRuleContexts(InterfaceBodyDeclarationContext);
        }
        return this.getRuleContext(i, InterfaceBodyDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_interfaceBody;
    }
    enterRule(listener) {
        if (listener.enterInterfaceBody) {
            listener.enterInterfaceBody(this);
        }
    }
    exitRule(listener) {
        if (listener.exitInterfaceBody) {
            listener.exitInterfaceBody(this);
        }
    }
    accept(visitor) {
        if (visitor.visitInterfaceBody) {
            return visitor.visitInterfaceBody(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ClassBodyDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    block() {
        return this.getRuleContext(0, BlockContext);
    }
    STATIC() {
        return this.getToken(JavaParser.STATIC, 0);
    }
    memberDeclaration() {
        return this.getRuleContext(0, MemberDeclarationContext);
    }
    modifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(ModifierContext);
        }
        return this.getRuleContext(i, ModifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_classBodyDeclaration;
    }
    enterRule(listener) {
        if (listener.enterClassBodyDeclaration) {
            listener.enterClassBodyDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitClassBodyDeclaration) {
            listener.exitClassBodyDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitClassBodyDeclaration) {
            return visitor.visitClassBodyDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class MemberDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    methodDeclaration() {
        return this.getRuleContext(0, MethodDeclarationContext);
    }
    genericMethodDeclaration() {
        return this.getRuleContext(0, GenericMethodDeclarationContext);
    }
    fieldDeclaration() {
        return this.getRuleContext(0, FieldDeclarationContext);
    }
    constructorDeclaration() {
        return this.getRuleContext(0, ConstructorDeclarationContext);
    }
    genericConstructorDeclaration() {
        return this.getRuleContext(0, GenericConstructorDeclarationContext);
    }
    interfaceDeclaration() {
        return this.getRuleContext(0, InterfaceDeclarationContext);
    }
    annotationTypeDeclaration() {
        return this.getRuleContext(0, AnnotationTypeDeclarationContext);
    }
    classDeclaration() {
        return this.getRuleContext(0, ClassDeclarationContext);
    }
    enumDeclaration() {
        return this.getRuleContext(0, EnumDeclarationContext);
    }
    recordDeclaration() {
        return this.getRuleContext(0, RecordDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_memberDeclaration;
    }
    enterRule(listener) {
        if (listener.enterMemberDeclaration) {
            listener.enterMemberDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitMemberDeclaration) {
            listener.exitMemberDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitMemberDeclaration) {
            return visitor.visitMemberDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class MethodDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeTypeOrVoid() {
        return this.getRuleContext(0, TypeTypeOrVoidContext);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    formalParameters() {
        return this.getRuleContext(0, FormalParametersContext);
    }
    methodBody() {
        return this.getRuleContext(0, MethodBodyContext);
    }
    LBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.LBRACK);
        }
        else {
            return this.getToken(JavaParser.LBRACK, i);
        }
    }
    RBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.RBRACK);
        }
        else {
            return this.getToken(JavaParser.RBRACK, i);
        }
    }
    THROWS() {
        return this.getToken(JavaParser.THROWS, 0);
    }
    qualifiedNameList() {
        return this.getRuleContext(0, QualifiedNameListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_methodDeclaration;
    }
    enterRule(listener) {
        if (listener.enterMethodDeclaration) {
            listener.enterMethodDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitMethodDeclaration) {
            listener.exitMethodDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitMethodDeclaration) {
            return visitor.visitMethodDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class MethodBodyContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    block() {
        return this.getRuleContext(0, BlockContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_methodBody;
    }
    enterRule(listener) {
        if (listener.enterMethodBody) {
            listener.enterMethodBody(this);
        }
    }
    exitRule(listener) {
        if (listener.exitMethodBody) {
            listener.exitMethodBody(this);
        }
    }
    accept(visitor) {
        if (visitor.visitMethodBody) {
            return visitor.visitMethodBody(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeTypeOrVoidContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    VOID() {
        return this.getToken(JavaParser.VOID, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_typeTypeOrVoid;
    }
    enterRule(listener) {
        if (listener.enterTypeTypeOrVoid) {
            listener.enterTypeTypeOrVoid(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeTypeOrVoid) {
            listener.exitTypeTypeOrVoid(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeTypeOrVoid) {
            return visitor.visitTypeTypeOrVoid(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class GenericMethodDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeParameters() {
        return this.getRuleContext(0, TypeParametersContext);
    }
    methodDeclaration() {
        return this.getRuleContext(0, MethodDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_genericMethodDeclaration;
    }
    enterRule(listener) {
        if (listener.enterGenericMethodDeclaration) {
            listener.enterGenericMethodDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitGenericMethodDeclaration) {
            listener.exitGenericMethodDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitGenericMethodDeclaration) {
            return visitor.visitGenericMethodDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class GenericConstructorDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeParameters() {
        return this.getRuleContext(0, TypeParametersContext);
    }
    constructorDeclaration() {
        return this.getRuleContext(0, ConstructorDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_genericConstructorDeclaration;
    }
    enterRule(listener) {
        if (listener.enterGenericConstructorDeclaration) {
            listener.enterGenericConstructorDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitGenericConstructorDeclaration) {
            listener.exitGenericConstructorDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitGenericConstructorDeclaration) {
            return visitor.visitGenericConstructorDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ConstructorDeclarationContext extends antlr.ParserRuleContext {
    _constructorBody;
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    formalParameters() {
        return this.getRuleContext(0, FormalParametersContext);
    }
    block() {
        return this.getRuleContext(0, BlockContext);
    }
    THROWS() {
        return this.getToken(JavaParser.THROWS, 0);
    }
    qualifiedNameList() {
        return this.getRuleContext(0, QualifiedNameListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_constructorDeclaration;
    }
    enterRule(listener) {
        if (listener.enterConstructorDeclaration) {
            listener.enterConstructorDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitConstructorDeclaration) {
            listener.exitConstructorDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitConstructorDeclaration) {
            return visitor.visitConstructorDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class FieldDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    variableDeclarators() {
        return this.getRuleContext(0, VariableDeclaratorsContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_fieldDeclaration;
    }
    enterRule(listener) {
        if (listener.enterFieldDeclaration) {
            listener.enterFieldDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitFieldDeclaration) {
            listener.exitFieldDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitFieldDeclaration) {
            return visitor.visitFieldDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class InterfaceBodyDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    interfaceMemberDeclaration() {
        return this.getRuleContext(0, InterfaceMemberDeclarationContext);
    }
    modifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(ModifierContext);
        }
        return this.getRuleContext(i, ModifierContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_interfaceBodyDeclaration;
    }
    enterRule(listener) {
        if (listener.enterInterfaceBodyDeclaration) {
            listener.enterInterfaceBodyDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitInterfaceBodyDeclaration) {
            listener.exitInterfaceBodyDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitInterfaceBodyDeclaration) {
            return visitor.visitInterfaceBodyDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class InterfaceMemberDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    constDeclaration() {
        return this.getRuleContext(0, ConstDeclarationContext);
    }
    interfaceMethodDeclaration() {
        return this.getRuleContext(0, InterfaceMethodDeclarationContext);
    }
    genericInterfaceMethodDeclaration() {
        return this.getRuleContext(0, GenericInterfaceMethodDeclarationContext);
    }
    interfaceDeclaration() {
        return this.getRuleContext(0, InterfaceDeclarationContext);
    }
    annotationTypeDeclaration() {
        return this.getRuleContext(0, AnnotationTypeDeclarationContext);
    }
    classDeclaration() {
        return this.getRuleContext(0, ClassDeclarationContext);
    }
    enumDeclaration() {
        return this.getRuleContext(0, EnumDeclarationContext);
    }
    recordDeclaration() {
        return this.getRuleContext(0, RecordDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_interfaceMemberDeclaration;
    }
    enterRule(listener) {
        if (listener.enterInterfaceMemberDeclaration) {
            listener.enterInterfaceMemberDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitInterfaceMemberDeclaration) {
            listener.exitInterfaceMemberDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitInterfaceMemberDeclaration) {
            return visitor.visitInterfaceMemberDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ConstDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    constantDeclarator(i) {
        if (i === undefined) {
            return this.getRuleContexts(ConstantDeclaratorContext);
        }
        return this.getRuleContext(i, ConstantDeclaratorContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_constDeclaration;
    }
    enterRule(listener) {
        if (listener.enterConstDeclaration) {
            listener.enterConstDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitConstDeclaration) {
            listener.exitConstDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitConstDeclaration) {
            return visitor.visitConstDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ConstantDeclaratorContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    ASSIGN() {
        return this.getToken(JavaParser.ASSIGN, 0);
    }
    variableInitializer() {
        return this.getRuleContext(0, VariableInitializerContext);
    }
    LBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.LBRACK);
        }
        else {
            return this.getToken(JavaParser.LBRACK, i);
        }
    }
    RBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.RBRACK);
        }
        else {
            return this.getToken(JavaParser.RBRACK, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_constantDeclarator;
    }
    enterRule(listener) {
        if (listener.enterConstantDeclarator) {
            listener.enterConstantDeclarator(this);
        }
    }
    exitRule(listener) {
        if (listener.exitConstantDeclarator) {
            listener.exitConstantDeclarator(this);
        }
    }
    accept(visitor) {
        if (visitor.visitConstantDeclarator) {
            return visitor.visitConstantDeclarator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class InterfaceMethodDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    interfaceCommonBodyDeclaration() {
        return this.getRuleContext(0, InterfaceCommonBodyDeclarationContext);
    }
    interfaceMethodModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(InterfaceMethodModifierContext);
        }
        return this.getRuleContext(i, InterfaceMethodModifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_interfaceMethodDeclaration;
    }
    enterRule(listener) {
        if (listener.enterInterfaceMethodDeclaration) {
            listener.enterInterfaceMethodDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitInterfaceMethodDeclaration) {
            listener.exitInterfaceMethodDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitInterfaceMethodDeclaration) {
            return visitor.visitInterfaceMethodDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class InterfaceMethodModifierContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    annotation() {
        return this.getRuleContext(0, AnnotationContext);
    }
    PUBLIC() {
        return this.getToken(JavaParser.PUBLIC, 0);
    }
    ABSTRACT() {
        return this.getToken(JavaParser.ABSTRACT, 0);
    }
    DEFAULT() {
        return this.getToken(JavaParser.DEFAULT, 0);
    }
    STATIC() {
        return this.getToken(JavaParser.STATIC, 0);
    }
    STRICTFP() {
        return this.getToken(JavaParser.STRICTFP, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_interfaceMethodModifier;
    }
    enterRule(listener) {
        if (listener.enterInterfaceMethodModifier) {
            listener.enterInterfaceMethodModifier(this);
        }
    }
    exitRule(listener) {
        if (listener.exitInterfaceMethodModifier) {
            listener.exitInterfaceMethodModifier(this);
        }
    }
    accept(visitor) {
        if (visitor.visitInterfaceMethodModifier) {
            return visitor.visitInterfaceMethodModifier(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class GenericInterfaceMethodDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeParameters() {
        return this.getRuleContext(0, TypeParametersContext);
    }
    interfaceCommonBodyDeclaration() {
        return this.getRuleContext(0, InterfaceCommonBodyDeclarationContext);
    }
    interfaceMethodModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(InterfaceMethodModifierContext);
        }
        return this.getRuleContext(i, InterfaceMethodModifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_genericInterfaceMethodDeclaration;
    }
    enterRule(listener) {
        if (listener.enterGenericInterfaceMethodDeclaration) {
            listener.enterGenericInterfaceMethodDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitGenericInterfaceMethodDeclaration) {
            listener.exitGenericInterfaceMethodDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitGenericInterfaceMethodDeclaration) {
            return visitor.visitGenericInterfaceMethodDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class InterfaceCommonBodyDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeTypeOrVoid() {
        return this.getRuleContext(0, TypeTypeOrVoidContext);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    formalParameters() {
        return this.getRuleContext(0, FormalParametersContext);
    }
    methodBody() {
        return this.getRuleContext(0, MethodBodyContext);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    LBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.LBRACK);
        }
        else {
            return this.getToken(JavaParser.LBRACK, i);
        }
    }
    RBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.RBRACK);
        }
        else {
            return this.getToken(JavaParser.RBRACK, i);
        }
    }
    THROWS() {
        return this.getToken(JavaParser.THROWS, 0);
    }
    qualifiedNameList() {
        return this.getRuleContext(0, QualifiedNameListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_interfaceCommonBodyDeclaration;
    }
    enterRule(listener) {
        if (listener.enterInterfaceCommonBodyDeclaration) {
            listener.enterInterfaceCommonBodyDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitInterfaceCommonBodyDeclaration) {
            listener.exitInterfaceCommonBodyDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitInterfaceCommonBodyDeclaration) {
            return visitor.visitInterfaceCommonBodyDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class VariableDeclaratorsContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    variableDeclarator(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableDeclaratorContext);
        }
        return this.getRuleContext(i, VariableDeclaratorContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_variableDeclarators;
    }
    enterRule(listener) {
        if (listener.enterVariableDeclarators) {
            listener.enterVariableDeclarators(this);
        }
    }
    exitRule(listener) {
        if (listener.exitVariableDeclarators) {
            listener.exitVariableDeclarators(this);
        }
    }
    accept(visitor) {
        if (visitor.visitVariableDeclarators) {
            return visitor.visitVariableDeclarators(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class VariableDeclaratorContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    variableDeclaratorId() {
        return this.getRuleContext(0, VariableDeclaratorIdContext);
    }
    ASSIGN() {
        return this.getToken(JavaParser.ASSIGN, 0);
    }
    variableInitializer() {
        return this.getRuleContext(0, VariableInitializerContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_variableDeclarator;
    }
    enterRule(listener) {
        if (listener.enterVariableDeclarator) {
            listener.enterVariableDeclarator(this);
        }
    }
    exitRule(listener) {
        if (listener.exitVariableDeclarator) {
            listener.exitVariableDeclarator(this);
        }
    }
    accept(visitor) {
        if (visitor.visitVariableDeclarator) {
            return visitor.visitVariableDeclarator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class VariableDeclaratorIdContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    LBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.LBRACK);
        }
        else {
            return this.getToken(JavaParser.LBRACK, i);
        }
    }
    RBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.RBRACK);
        }
        else {
            return this.getToken(JavaParser.RBRACK, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_variableDeclaratorId;
    }
    enterRule(listener) {
        if (listener.enterVariableDeclaratorId) {
            listener.enterVariableDeclaratorId(this);
        }
    }
    exitRule(listener) {
        if (listener.exitVariableDeclaratorId) {
            listener.exitVariableDeclaratorId(this);
        }
    }
    accept(visitor) {
        if (visitor.visitVariableDeclaratorId) {
            return visitor.visitVariableDeclaratorId(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class VariableInitializerContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    arrayInitializer() {
        return this.getRuleContext(0, ArrayInitializerContext);
    }
    expression() {
        return this.getRuleContext(0, ExpressionContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_variableInitializer;
    }
    enterRule(listener) {
        if (listener.enterVariableInitializer) {
            listener.enterVariableInitializer(this);
        }
    }
    exitRule(listener) {
        if (listener.exitVariableInitializer) {
            listener.exitVariableInitializer(this);
        }
    }
    accept(visitor) {
        if (visitor.visitVariableInitializer) {
            return visitor.visitVariableInitializer(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ArrayInitializerContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    variableInitializer(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableInitializerContext);
        }
        return this.getRuleContext(i, VariableInitializerContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_arrayInitializer;
    }
    enterRule(listener) {
        if (listener.enterArrayInitializer) {
            listener.enterArrayInitializer(this);
        }
    }
    exitRule(listener) {
        if (listener.exitArrayInitializer) {
            listener.exitArrayInitializer(this);
        }
    }
    accept(visitor) {
        if (visitor.visitArrayInitializer) {
            return visitor.visitArrayInitializer(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ClassOrInterfaceTypeContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(IdentifierContext);
        }
        return this.getRuleContext(i, IdentifierContext);
    }
    typeArguments(i) {
        if (i === undefined) {
            return this.getRuleContexts(TypeArgumentsContext);
        }
        return this.getRuleContext(i, TypeArgumentsContext);
    }
    DOT(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.DOT);
        }
        else {
            return this.getToken(JavaParser.DOT, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_classOrInterfaceType;
    }
    enterRule(listener) {
        if (listener.enterClassOrInterfaceType) {
            listener.enterClassOrInterfaceType(this);
        }
    }
    exitRule(listener) {
        if (listener.exitClassOrInterfaceType) {
            listener.exitClassOrInterfaceType(this);
        }
    }
    accept(visitor) {
        if (visitor.visitClassOrInterfaceType) {
            return visitor.visitClassOrInterfaceType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeArgumentContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    QUESTION() {
        return this.getToken(JavaParser.QUESTION, 0);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    EXTENDS() {
        return this.getToken(JavaParser.EXTENDS, 0);
    }
    SUPER() {
        return this.getToken(JavaParser.SUPER, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_typeArgument;
    }
    enterRule(listener) {
        if (listener.enterTypeArgument) {
            listener.enterTypeArgument(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeArgument) {
            listener.exitTypeArgument(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeArgument) {
            return visitor.visitTypeArgument(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class QualifiedNameListContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    qualifiedName(i) {
        if (i === undefined) {
            return this.getRuleContexts(QualifiedNameContext);
        }
        return this.getRuleContext(i, QualifiedNameContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_qualifiedNameList;
    }
    enterRule(listener) {
        if (listener.enterQualifiedNameList) {
            listener.enterQualifiedNameList(this);
        }
    }
    exitRule(listener) {
        if (listener.exitQualifiedNameList) {
            listener.exitQualifiedNameList(this);
        }
    }
    accept(visitor) {
        if (visitor.visitQualifiedNameList) {
            return visitor.visitQualifiedNameList(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class FormalParametersContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    receiverParameter() {
        return this.getRuleContext(0, ReceiverParameterContext);
    }
    COMMA() {
        return this.getToken(JavaParser.COMMA, 0);
    }
    formalParameterList() {
        return this.getRuleContext(0, FormalParameterListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_formalParameters;
    }
    enterRule(listener) {
        if (listener.enterFormalParameters) {
            listener.enterFormalParameters(this);
        }
    }
    exitRule(listener) {
        if (listener.exitFormalParameters) {
            listener.exitFormalParameters(this);
        }
    }
    accept(visitor) {
        if (visitor.visitFormalParameters) {
            return visitor.visitFormalParameters(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ReceiverParameterContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    THIS() {
        return this.getToken(JavaParser.THIS, 0);
    }
    identifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(IdentifierContext);
        }
        return this.getRuleContext(i, IdentifierContext);
    }
    DOT(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.DOT);
        }
        else {
            return this.getToken(JavaParser.DOT, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_receiverParameter;
    }
    enterRule(listener) {
        if (listener.enterReceiverParameter) {
            listener.enterReceiverParameter(this);
        }
    }
    exitRule(listener) {
        if (listener.exitReceiverParameter) {
            listener.exitReceiverParameter(this);
        }
    }
    accept(visitor) {
        if (visitor.visitReceiverParameter) {
            return visitor.visitReceiverParameter(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class FormalParameterListContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    formalParameter(i) {
        if (i === undefined) {
            return this.getRuleContexts(FormalParameterContext);
        }
        return this.getRuleContext(i, FormalParameterContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    lastFormalParameter() {
        return this.getRuleContext(0, LastFormalParameterContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_formalParameterList;
    }
    enterRule(listener) {
        if (listener.enterFormalParameterList) {
            listener.enterFormalParameterList(this);
        }
    }
    exitRule(listener) {
        if (listener.exitFormalParameterList) {
            listener.exitFormalParameterList(this);
        }
    }
    accept(visitor) {
        if (visitor.visitFormalParameterList) {
            return visitor.visitFormalParameterList(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class FormalParameterContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    variableDeclaratorId() {
        return this.getRuleContext(0, VariableDeclaratorIdContext);
    }
    variableModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableModifierContext);
        }
        return this.getRuleContext(i, VariableModifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_formalParameter;
    }
    enterRule(listener) {
        if (listener.enterFormalParameter) {
            listener.enterFormalParameter(this);
        }
    }
    exitRule(listener) {
        if (listener.exitFormalParameter) {
            listener.exitFormalParameter(this);
        }
    }
    accept(visitor) {
        if (visitor.visitFormalParameter) {
            return visitor.visitFormalParameter(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class LastFormalParameterContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    ELLIPSIS() {
        return this.getToken(JavaParser.ELLIPSIS, 0);
    }
    variableDeclaratorId() {
        return this.getRuleContext(0, VariableDeclaratorIdContext);
    }
    variableModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableModifierContext);
        }
        return this.getRuleContext(i, VariableModifierContext);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_lastFormalParameter;
    }
    enterRule(listener) {
        if (listener.enterLastFormalParameter) {
            listener.enterLastFormalParameter(this);
        }
    }
    exitRule(listener) {
        if (listener.exitLastFormalParameter) {
            listener.exitLastFormalParameter(this);
        }
    }
    accept(visitor) {
        if (visitor.visitLastFormalParameter) {
            return visitor.visitLastFormalParameter(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class LambdaLVTIListContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    lambdaLVTIParameter(i) {
        if (i === undefined) {
            return this.getRuleContexts(LambdaLVTIParameterContext);
        }
        return this.getRuleContext(i, LambdaLVTIParameterContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_lambdaLVTIList;
    }
    enterRule(listener) {
        if (listener.enterLambdaLVTIList) {
            listener.enterLambdaLVTIList(this);
        }
    }
    exitRule(listener) {
        if (listener.exitLambdaLVTIList) {
            listener.exitLambdaLVTIList(this);
        }
    }
    accept(visitor) {
        if (visitor.visitLambdaLVTIList) {
            return visitor.visitLambdaLVTIList(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class LambdaLVTIParameterContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    VAR() {
        return this.getToken(JavaParser.VAR, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    variableModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableModifierContext);
        }
        return this.getRuleContext(i, VariableModifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_lambdaLVTIParameter;
    }
    enterRule(listener) {
        if (listener.enterLambdaLVTIParameter) {
            listener.enterLambdaLVTIParameter(this);
        }
    }
    exitRule(listener) {
        if (listener.exitLambdaLVTIParameter) {
            listener.exitLambdaLVTIParameter(this);
        }
    }
    accept(visitor) {
        if (visitor.visitLambdaLVTIParameter) {
            return visitor.visitLambdaLVTIParameter(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class QualifiedNameContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(IdentifierContext);
        }
        return this.getRuleContext(i, IdentifierContext);
    }
    DOT(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.DOT);
        }
        else {
            return this.getToken(JavaParser.DOT, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_qualifiedName;
    }
    enterRule(listener) {
        if (listener.enterQualifiedName) {
            listener.enterQualifiedName(this);
        }
    }
    exitRule(listener) {
        if (listener.exitQualifiedName) {
            listener.exitQualifiedName(this);
        }
    }
    accept(visitor) {
        if (visitor.visitQualifiedName) {
            return visitor.visitQualifiedName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class LiteralContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    integerLiteral() {
        return this.getRuleContext(0, IntegerLiteralContext);
    }
    floatLiteral() {
        return this.getRuleContext(0, FloatLiteralContext);
    }
    CHAR_LITERAL() {
        return this.getToken(JavaParser.CHAR_LITERAL, 0);
    }
    STRING_LITERAL() {
        return this.getToken(JavaParser.STRING_LITERAL, 0);
    }
    BOOL_LITERAL() {
        return this.getToken(JavaParser.BOOL_LITERAL, 0);
    }
    NULL_LITERAL() {
        return this.getToken(JavaParser.NULL_LITERAL, 0);
    }
    TEXT_BLOCK() {
        return this.getToken(JavaParser.TEXT_BLOCK, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_literal;
    }
    enterRule(listener) {
        if (listener.enterLiteral) {
            listener.enterLiteral(this);
        }
    }
    exitRule(listener) {
        if (listener.exitLiteral) {
            listener.exitLiteral(this);
        }
    }
    accept(visitor) {
        if (visitor.visitLiteral) {
            return visitor.visitLiteral(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class IntegerLiteralContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    DECIMAL_LITERAL() {
        return this.getToken(JavaParser.DECIMAL_LITERAL, 0);
    }
    HEX_LITERAL() {
        return this.getToken(JavaParser.HEX_LITERAL, 0);
    }
    OCT_LITERAL() {
        return this.getToken(JavaParser.OCT_LITERAL, 0);
    }
    BINARY_LITERAL() {
        return this.getToken(JavaParser.BINARY_LITERAL, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_integerLiteral;
    }
    enterRule(listener) {
        if (listener.enterIntegerLiteral) {
            listener.enterIntegerLiteral(this);
        }
    }
    exitRule(listener) {
        if (listener.exitIntegerLiteral) {
            listener.exitIntegerLiteral(this);
        }
    }
    accept(visitor) {
        if (visitor.visitIntegerLiteral) {
            return visitor.visitIntegerLiteral(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class FloatLiteralContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    FLOAT_LITERAL() {
        return this.getToken(JavaParser.FLOAT_LITERAL, 0);
    }
    HEX_FLOAT_LITERAL() {
        return this.getToken(JavaParser.HEX_FLOAT_LITERAL, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_floatLiteral;
    }
    enterRule(listener) {
        if (listener.enterFloatLiteral) {
            listener.enterFloatLiteral(this);
        }
    }
    exitRule(listener) {
        if (listener.exitFloatLiteral) {
            listener.exitFloatLiteral(this);
        }
    }
    accept(visitor) {
        if (visitor.visitFloatLiteral) {
            return visitor.visitFloatLiteral(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class AltAnnotationQualifiedNameContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    AT() {
        return this.getToken(JavaParser.AT, 0);
    }
    identifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(IdentifierContext);
        }
        return this.getRuleContext(i, IdentifierContext);
    }
    DOT(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.DOT);
        }
        else {
            return this.getToken(JavaParser.DOT, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_altAnnotationQualifiedName;
    }
    enterRule(listener) {
        if (listener.enterAltAnnotationQualifiedName) {
            listener.enterAltAnnotationQualifiedName(this);
        }
    }
    exitRule(listener) {
        if (listener.exitAltAnnotationQualifiedName) {
            listener.exitAltAnnotationQualifiedName(this);
        }
    }
    accept(visitor) {
        if (visitor.visitAltAnnotationQualifiedName) {
            return visitor.visitAltAnnotationQualifiedName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class AnnotationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    AT() {
        return this.getToken(JavaParser.AT, 0);
    }
    qualifiedName() {
        return this.getRuleContext(0, QualifiedNameContext);
    }
    altAnnotationQualifiedName() {
        return this.getRuleContext(0, AltAnnotationQualifiedNameContext);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    elementValuePairs() {
        return this.getRuleContext(0, ElementValuePairsContext);
    }
    elementValue() {
        return this.getRuleContext(0, ElementValueContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_annotation;
    }
    enterRule(listener) {
        if (listener.enterAnnotation) {
            listener.enterAnnotation(this);
        }
    }
    exitRule(listener) {
        if (listener.exitAnnotation) {
            listener.exitAnnotation(this);
        }
    }
    accept(visitor) {
        if (visitor.visitAnnotation) {
            return visitor.visitAnnotation(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ElementValuePairsContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    elementValuePair(i) {
        if (i === undefined) {
            return this.getRuleContexts(ElementValuePairContext);
        }
        return this.getRuleContext(i, ElementValuePairContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_elementValuePairs;
    }
    enterRule(listener) {
        if (listener.enterElementValuePairs) {
            listener.enterElementValuePairs(this);
        }
    }
    exitRule(listener) {
        if (listener.exitElementValuePairs) {
            listener.exitElementValuePairs(this);
        }
    }
    accept(visitor) {
        if (visitor.visitElementValuePairs) {
            return visitor.visitElementValuePairs(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ElementValuePairContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    ASSIGN() {
        return this.getToken(JavaParser.ASSIGN, 0);
    }
    elementValue() {
        return this.getRuleContext(0, ElementValueContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_elementValuePair;
    }
    enterRule(listener) {
        if (listener.enterElementValuePair) {
            listener.enterElementValuePair(this);
        }
    }
    exitRule(listener) {
        if (listener.exitElementValuePair) {
            listener.exitElementValuePair(this);
        }
    }
    accept(visitor) {
        if (visitor.visitElementValuePair) {
            return visitor.visitElementValuePair(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ElementValueContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    expression() {
        return this.getRuleContext(0, ExpressionContext);
    }
    annotation() {
        return this.getRuleContext(0, AnnotationContext);
    }
    elementValueArrayInitializer() {
        return this.getRuleContext(0, ElementValueArrayInitializerContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_elementValue;
    }
    enterRule(listener) {
        if (listener.enterElementValue) {
            listener.enterElementValue(this);
        }
    }
    exitRule(listener) {
        if (listener.exitElementValue) {
            listener.exitElementValue(this);
        }
    }
    accept(visitor) {
        if (visitor.visitElementValue) {
            return visitor.visitElementValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ElementValueArrayInitializerContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    elementValue(i) {
        if (i === undefined) {
            return this.getRuleContexts(ElementValueContext);
        }
        return this.getRuleContext(i, ElementValueContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_elementValueArrayInitializer;
    }
    enterRule(listener) {
        if (listener.enterElementValueArrayInitializer) {
            listener.enterElementValueArrayInitializer(this);
        }
    }
    exitRule(listener) {
        if (listener.exitElementValueArrayInitializer) {
            listener.exitElementValueArrayInitializer(this);
        }
    }
    accept(visitor) {
        if (visitor.visitElementValueArrayInitializer) {
            return visitor.visitElementValueArrayInitializer(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class AnnotationTypeDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    AT() {
        return this.getToken(JavaParser.AT, 0);
    }
    INTERFACE() {
        return this.getToken(JavaParser.INTERFACE, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    annotationTypeBody() {
        return this.getRuleContext(0, AnnotationTypeBodyContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_annotationTypeDeclaration;
    }
    enterRule(listener) {
        if (listener.enterAnnotationTypeDeclaration) {
            listener.enterAnnotationTypeDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitAnnotationTypeDeclaration) {
            listener.exitAnnotationTypeDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitAnnotationTypeDeclaration) {
            return visitor.visitAnnotationTypeDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class AnnotationTypeBodyContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    annotationTypeElementDeclaration(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationTypeElementDeclarationContext);
        }
        return this.getRuleContext(i, AnnotationTypeElementDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_annotationTypeBody;
    }
    enterRule(listener) {
        if (listener.enterAnnotationTypeBody) {
            listener.enterAnnotationTypeBody(this);
        }
    }
    exitRule(listener) {
        if (listener.exitAnnotationTypeBody) {
            listener.exitAnnotationTypeBody(this);
        }
    }
    accept(visitor) {
        if (visitor.visitAnnotationTypeBody) {
            return visitor.visitAnnotationTypeBody(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class AnnotationTypeElementDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    annotationTypeElementRest() {
        return this.getRuleContext(0, AnnotationTypeElementRestContext);
    }
    modifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(ModifierContext);
        }
        return this.getRuleContext(i, ModifierContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_annotationTypeElementDeclaration;
    }
    enterRule(listener) {
        if (listener.enterAnnotationTypeElementDeclaration) {
            listener.enterAnnotationTypeElementDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitAnnotationTypeElementDeclaration) {
            listener.exitAnnotationTypeElementDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitAnnotationTypeElementDeclaration) {
            return visitor.visitAnnotationTypeElementDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class AnnotationTypeElementRestContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    annotationMethodOrConstantRest() {
        return this.getRuleContext(0, AnnotationMethodOrConstantRestContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    classDeclaration() {
        return this.getRuleContext(0, ClassDeclarationContext);
    }
    interfaceDeclaration() {
        return this.getRuleContext(0, InterfaceDeclarationContext);
    }
    enumDeclaration() {
        return this.getRuleContext(0, EnumDeclarationContext);
    }
    annotationTypeDeclaration() {
        return this.getRuleContext(0, AnnotationTypeDeclarationContext);
    }
    recordDeclaration() {
        return this.getRuleContext(0, RecordDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_annotationTypeElementRest;
    }
    enterRule(listener) {
        if (listener.enterAnnotationTypeElementRest) {
            listener.enterAnnotationTypeElementRest(this);
        }
    }
    exitRule(listener) {
        if (listener.exitAnnotationTypeElementRest) {
            listener.exitAnnotationTypeElementRest(this);
        }
    }
    accept(visitor) {
        if (visitor.visitAnnotationTypeElementRest) {
            return visitor.visitAnnotationTypeElementRest(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class AnnotationMethodOrConstantRestContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    annotationMethodRest() {
        return this.getRuleContext(0, AnnotationMethodRestContext);
    }
    annotationConstantRest() {
        return this.getRuleContext(0, AnnotationConstantRestContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_annotationMethodOrConstantRest;
    }
    enterRule(listener) {
        if (listener.enterAnnotationMethodOrConstantRest) {
            listener.enterAnnotationMethodOrConstantRest(this);
        }
    }
    exitRule(listener) {
        if (listener.exitAnnotationMethodOrConstantRest) {
            listener.exitAnnotationMethodOrConstantRest(this);
        }
    }
    accept(visitor) {
        if (visitor.visitAnnotationMethodOrConstantRest) {
            return visitor.visitAnnotationMethodOrConstantRest(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class AnnotationMethodRestContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    defaultValue() {
        return this.getRuleContext(0, DefaultValueContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_annotationMethodRest;
    }
    enterRule(listener) {
        if (listener.enterAnnotationMethodRest) {
            listener.enterAnnotationMethodRest(this);
        }
    }
    exitRule(listener) {
        if (listener.exitAnnotationMethodRest) {
            listener.exitAnnotationMethodRest(this);
        }
    }
    accept(visitor) {
        if (visitor.visitAnnotationMethodRest) {
            return visitor.visitAnnotationMethodRest(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class AnnotationConstantRestContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    variableDeclarators() {
        return this.getRuleContext(0, VariableDeclaratorsContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_annotationConstantRest;
    }
    enterRule(listener) {
        if (listener.enterAnnotationConstantRest) {
            listener.enterAnnotationConstantRest(this);
        }
    }
    exitRule(listener) {
        if (listener.exitAnnotationConstantRest) {
            listener.exitAnnotationConstantRest(this);
        }
    }
    accept(visitor) {
        if (visitor.visitAnnotationConstantRest) {
            return visitor.visitAnnotationConstantRest(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class DefaultValueContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    DEFAULT() {
        return this.getToken(JavaParser.DEFAULT, 0);
    }
    elementValue() {
        return this.getRuleContext(0, ElementValueContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_defaultValue;
    }
    enterRule(listener) {
        if (listener.enterDefaultValue) {
            listener.enterDefaultValue(this);
        }
    }
    exitRule(listener) {
        if (listener.exitDefaultValue) {
            listener.exitDefaultValue(this);
        }
    }
    accept(visitor) {
        if (visitor.visitDefaultValue) {
            return visitor.visitDefaultValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ModuleDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    MODULE() {
        return this.getToken(JavaParser.MODULE, 0);
    }
    qualifiedName() {
        return this.getRuleContext(0, QualifiedNameContext);
    }
    moduleBody() {
        return this.getRuleContext(0, ModuleBodyContext);
    }
    OPEN() {
        return this.getToken(JavaParser.OPEN, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_moduleDeclaration;
    }
    enterRule(listener) {
        if (listener.enterModuleDeclaration) {
            listener.enterModuleDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitModuleDeclaration) {
            listener.exitModuleDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitModuleDeclaration) {
            return visitor.visitModuleDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ModuleBodyContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    moduleDirective(i) {
        if (i === undefined) {
            return this.getRuleContexts(ModuleDirectiveContext);
        }
        return this.getRuleContext(i, ModuleDirectiveContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_moduleBody;
    }
    enterRule(listener) {
        if (listener.enterModuleBody) {
            listener.enterModuleBody(this);
        }
    }
    exitRule(listener) {
        if (listener.exitModuleBody) {
            listener.exitModuleBody(this);
        }
    }
    accept(visitor) {
        if (visitor.visitModuleBody) {
            return visitor.visitModuleBody(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ModuleDirectiveContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    REQUIRES() {
        return this.getToken(JavaParser.REQUIRES, 0);
    }
    qualifiedName(i) {
        if (i === undefined) {
            return this.getRuleContexts(QualifiedNameContext);
        }
        return this.getRuleContext(i, QualifiedNameContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    requiresModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(RequiresModifierContext);
        }
        return this.getRuleContext(i, RequiresModifierContext);
    }
    EXPORTS() {
        return this.getToken(JavaParser.EXPORTS, 0);
    }
    TO() {
        return this.getToken(JavaParser.TO, 0);
    }
    OPENS() {
        return this.getToken(JavaParser.OPENS, 0);
    }
    USES() {
        return this.getToken(JavaParser.USES, 0);
    }
    PROVIDES() {
        return this.getToken(JavaParser.PROVIDES, 0);
    }
    WITH() {
        return this.getToken(JavaParser.WITH, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_moduleDirective;
    }
    enterRule(listener) {
        if (listener.enterModuleDirective) {
            listener.enterModuleDirective(this);
        }
    }
    exitRule(listener) {
        if (listener.exitModuleDirective) {
            listener.exitModuleDirective(this);
        }
    }
    accept(visitor) {
        if (visitor.visitModuleDirective) {
            return visitor.visitModuleDirective(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class RequiresModifierContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    TRANSITIVE() {
        return this.getToken(JavaParser.TRANSITIVE, 0);
    }
    STATIC() {
        return this.getToken(JavaParser.STATIC, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_requiresModifier;
    }
    enterRule(listener) {
        if (listener.enterRequiresModifier) {
            listener.enterRequiresModifier(this);
        }
    }
    exitRule(listener) {
        if (listener.exitRequiresModifier) {
            listener.exitRequiresModifier(this);
        }
    }
    accept(visitor) {
        if (visitor.visitRequiresModifier) {
            return visitor.visitRequiresModifier(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class RecordDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    RECORD() {
        return this.getToken(JavaParser.RECORD, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    recordHeader() {
        return this.getRuleContext(0, RecordHeaderContext);
    }
    recordBody() {
        return this.getRuleContext(0, RecordBodyContext);
    }
    typeParameters() {
        return this.getRuleContext(0, TypeParametersContext);
    }
    IMPLEMENTS() {
        return this.getToken(JavaParser.IMPLEMENTS, 0);
    }
    typeList() {
        return this.getRuleContext(0, TypeListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_recordDeclaration;
    }
    enterRule(listener) {
        if (listener.enterRecordDeclaration) {
            listener.enterRecordDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitRecordDeclaration) {
            listener.exitRecordDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitRecordDeclaration) {
            return visitor.visitRecordDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class RecordHeaderContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    recordComponentList() {
        return this.getRuleContext(0, RecordComponentListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_recordHeader;
    }
    enterRule(listener) {
        if (listener.enterRecordHeader) {
            listener.enterRecordHeader(this);
        }
    }
    exitRule(listener) {
        if (listener.exitRecordHeader) {
            listener.exitRecordHeader(this);
        }
    }
    accept(visitor) {
        if (visitor.visitRecordHeader) {
            return visitor.visitRecordHeader(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class RecordComponentListContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    recordComponent(i) {
        if (i === undefined) {
            return this.getRuleContexts(RecordComponentContext);
        }
        return this.getRuleContext(i, RecordComponentContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_recordComponentList;
    }
    enterRule(listener) {
        if (listener.enterRecordComponentList) {
            listener.enterRecordComponentList(this);
        }
    }
    exitRule(listener) {
        if (listener.exitRecordComponentList) {
            listener.exitRecordComponentList(this);
        }
    }
    accept(visitor) {
        if (visitor.visitRecordComponentList) {
            return visitor.visitRecordComponentList(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class RecordComponentContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_recordComponent;
    }
    enterRule(listener) {
        if (listener.enterRecordComponent) {
            listener.enterRecordComponent(this);
        }
    }
    exitRule(listener) {
        if (listener.exitRecordComponent) {
            listener.exitRecordComponent(this);
        }
    }
    accept(visitor) {
        if (visitor.visitRecordComponent) {
            return visitor.visitRecordComponent(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class RecordBodyContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    classBodyDeclaration(i) {
        if (i === undefined) {
            return this.getRuleContexts(ClassBodyDeclarationContext);
        }
        return this.getRuleContext(i, ClassBodyDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_recordBody;
    }
    enterRule(listener) {
        if (listener.enterRecordBody) {
            listener.enterRecordBody(this);
        }
    }
    exitRule(listener) {
        if (listener.exitRecordBody) {
            listener.exitRecordBody(this);
        }
    }
    accept(visitor) {
        if (visitor.visitRecordBody) {
            return visitor.visitRecordBody(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class BlockContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    blockStatement(i) {
        if (i === undefined) {
            return this.getRuleContexts(BlockStatementContext);
        }
        return this.getRuleContext(i, BlockStatementContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_block;
    }
    enterRule(listener) {
        if (listener.enterBlock) {
            listener.enterBlock(this);
        }
    }
    exitRule(listener) {
        if (listener.exitBlock) {
            listener.exitBlock(this);
        }
    }
    accept(visitor) {
        if (visitor.visitBlock) {
            return visitor.visitBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class BlockStatementContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    localVariableDeclaration() {
        return this.getRuleContext(0, LocalVariableDeclarationContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    statement() {
        return this.getRuleContext(0, StatementContext);
    }
    localTypeDeclaration() {
        return this.getRuleContext(0, LocalTypeDeclarationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_blockStatement;
    }
    enterRule(listener) {
        if (listener.enterBlockStatement) {
            listener.enterBlockStatement(this);
        }
    }
    exitRule(listener) {
        if (listener.exitBlockStatement) {
            listener.exitBlockStatement(this);
        }
    }
    accept(visitor) {
        if (visitor.visitBlockStatement) {
            return visitor.visitBlockStatement(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class LocalVariableDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    variableDeclarators() {
        return this.getRuleContext(0, VariableDeclaratorsContext);
    }
    variableModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableModifierContext);
        }
        return this.getRuleContext(i, VariableModifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_localVariableDeclaration;
    }
    enterRule(listener) {
        if (listener.enterLocalVariableDeclaration) {
            listener.enterLocalVariableDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitLocalVariableDeclaration) {
            listener.exitLocalVariableDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitLocalVariableDeclaration) {
            return visitor.visitLocalVariableDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class IdentifierContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    IDENTIFIER() {
        return this.getToken(JavaParser.IDENTIFIER, 0);
    }
    MODULE() {
        return this.getToken(JavaParser.MODULE, 0);
    }
    OPEN() {
        return this.getToken(JavaParser.OPEN, 0);
    }
    REQUIRES() {
        return this.getToken(JavaParser.REQUIRES, 0);
    }
    EXPORTS() {
        return this.getToken(JavaParser.EXPORTS, 0);
    }
    OPENS() {
        return this.getToken(JavaParser.OPENS, 0);
    }
    TO() {
        return this.getToken(JavaParser.TO, 0);
    }
    USES() {
        return this.getToken(JavaParser.USES, 0);
    }
    PROVIDES() {
        return this.getToken(JavaParser.PROVIDES, 0);
    }
    WITH() {
        return this.getToken(JavaParser.WITH, 0);
    }
    TRANSITIVE() {
        return this.getToken(JavaParser.TRANSITIVE, 0);
    }
    YIELD() {
        return this.getToken(JavaParser.YIELD, 0);
    }
    SEALED() {
        return this.getToken(JavaParser.SEALED, 0);
    }
    PERMITS() {
        return this.getToken(JavaParser.PERMITS, 0);
    }
    RECORD() {
        return this.getToken(JavaParser.RECORD, 0);
    }
    VAR() {
        return this.getToken(JavaParser.VAR, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_identifier;
    }
    enterRule(listener) {
        if (listener.enterIdentifier) {
            listener.enterIdentifier(this);
        }
    }
    exitRule(listener) {
        if (listener.exitIdentifier) {
            listener.exitIdentifier(this);
        }
    }
    accept(visitor) {
        if (visitor.visitIdentifier) {
            return visitor.visitIdentifier(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class LocalTypeDeclarationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    classDeclaration() {
        return this.getRuleContext(0, ClassDeclarationContext);
    }
    interfaceDeclaration() {
        return this.getRuleContext(0, InterfaceDeclarationContext);
    }
    recordDeclaration() {
        return this.getRuleContext(0, RecordDeclarationContext);
    }
    classOrInterfaceModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(ClassOrInterfaceModifierContext);
        }
        return this.getRuleContext(i, ClassOrInterfaceModifierContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_localTypeDeclaration;
    }
    enterRule(listener) {
        if (listener.enterLocalTypeDeclaration) {
            listener.enterLocalTypeDeclaration(this);
        }
    }
    exitRule(listener) {
        if (listener.exitLocalTypeDeclaration) {
            listener.exitLocalTypeDeclaration(this);
        }
    }
    accept(visitor) {
        if (visitor.visitLocalTypeDeclaration) {
            return visitor.visitLocalTypeDeclaration(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class StatementContext extends antlr.ParserRuleContext {
    _blockLabel;
    _statementExpression;
    _identifierLabel;
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    block() {
        return this.getRuleContext(0, BlockContext);
    }
    ASSERT() {
        return this.getToken(JavaParser.ASSERT, 0);
    }
    expression(i) {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }
        return this.getRuleContext(i, ExpressionContext);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    COLON() {
        return this.getToken(JavaParser.COLON, 0);
    }
    IF() {
        return this.getToken(JavaParser.IF, 0);
    }
    parExpression() {
        return this.getRuleContext(0, ParExpressionContext);
    }
    statement(i) {
        if (i === undefined) {
            return this.getRuleContexts(StatementContext);
        }
        return this.getRuleContext(i, StatementContext);
    }
    ELSE() {
        return this.getToken(JavaParser.ELSE, 0);
    }
    FOR() {
        return this.getToken(JavaParser.FOR, 0);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    forControl() {
        return this.getRuleContext(0, ForControlContext);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    WHILE() {
        return this.getToken(JavaParser.WHILE, 0);
    }
    DO() {
        return this.getToken(JavaParser.DO, 0);
    }
    TRY() {
        return this.getToken(JavaParser.TRY, 0);
    }
    finallyBlock() {
        return this.getRuleContext(0, FinallyBlockContext);
    }
    catchClause(i) {
        if (i === undefined) {
            return this.getRuleContexts(CatchClauseContext);
        }
        return this.getRuleContext(i, CatchClauseContext);
    }
    resourceSpecification() {
        return this.getRuleContext(0, ResourceSpecificationContext);
    }
    SWITCH() {
        return this.getToken(JavaParser.SWITCH, 0);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    switchBlockStatementGroup(i) {
        if (i === undefined) {
            return this.getRuleContexts(SwitchBlockStatementGroupContext);
        }
        return this.getRuleContext(i, SwitchBlockStatementGroupContext);
    }
    switchLabel(i) {
        if (i === undefined) {
            return this.getRuleContexts(SwitchLabelContext);
        }
        return this.getRuleContext(i, SwitchLabelContext);
    }
    SYNCHRONIZED() {
        return this.getToken(JavaParser.SYNCHRONIZED, 0);
    }
    RETURN() {
        return this.getToken(JavaParser.RETURN, 0);
    }
    THROW() {
        return this.getToken(JavaParser.THROW, 0);
    }
    BREAK() {
        return this.getToken(JavaParser.BREAK, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    CONTINUE() {
        return this.getToken(JavaParser.CONTINUE, 0);
    }
    YIELD() {
        return this.getToken(JavaParser.YIELD, 0);
    }
    switchExpression() {
        return this.getRuleContext(0, SwitchExpressionContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_statement;
    }
    enterRule(listener) {
        if (listener.enterStatement) {
            listener.enterStatement(this);
        }
    }
    exitRule(listener) {
        if (listener.exitStatement) {
            listener.exitStatement(this);
        }
    }
    accept(visitor) {
        if (visitor.visitStatement) {
            return visitor.visitStatement(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class CatchClauseContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    CATCH() {
        return this.getToken(JavaParser.CATCH, 0);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    catchType() {
        return this.getRuleContext(0, CatchTypeContext);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    block() {
        return this.getRuleContext(0, BlockContext);
    }
    variableModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableModifierContext);
        }
        return this.getRuleContext(i, VariableModifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_catchClause;
    }
    enterRule(listener) {
        if (listener.enterCatchClause) {
            listener.enterCatchClause(this);
        }
    }
    exitRule(listener) {
        if (listener.exitCatchClause) {
            listener.exitCatchClause(this);
        }
    }
    accept(visitor) {
        if (visitor.visitCatchClause) {
            return visitor.visitCatchClause(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class CatchTypeContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    qualifiedName(i) {
        if (i === undefined) {
            return this.getRuleContexts(QualifiedNameContext);
        }
        return this.getRuleContext(i, QualifiedNameContext);
    }
    BITOR(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.BITOR);
        }
        else {
            return this.getToken(JavaParser.BITOR, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_catchType;
    }
    enterRule(listener) {
        if (listener.enterCatchType) {
            listener.enterCatchType(this);
        }
    }
    exitRule(listener) {
        if (listener.exitCatchType) {
            listener.exitCatchType(this);
        }
    }
    accept(visitor) {
        if (visitor.visitCatchType) {
            return visitor.visitCatchType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class FinallyBlockContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    FINALLY() {
        return this.getToken(JavaParser.FINALLY, 0);
    }
    block() {
        return this.getRuleContext(0, BlockContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_finallyBlock;
    }
    enterRule(listener) {
        if (listener.enterFinallyBlock) {
            listener.enterFinallyBlock(this);
        }
    }
    exitRule(listener) {
        if (listener.exitFinallyBlock) {
            listener.exitFinallyBlock(this);
        }
    }
    accept(visitor) {
        if (visitor.visitFinallyBlock) {
            return visitor.visitFinallyBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ResourceSpecificationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    resources() {
        return this.getRuleContext(0, ResourcesContext);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    SEMI() {
        return this.getToken(JavaParser.SEMI, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_resourceSpecification;
    }
    enterRule(listener) {
        if (listener.enterResourceSpecification) {
            listener.enterResourceSpecification(this);
        }
    }
    exitRule(listener) {
        if (listener.exitResourceSpecification) {
            listener.exitResourceSpecification(this);
        }
    }
    accept(visitor) {
        if (visitor.visitResourceSpecification) {
            return visitor.visitResourceSpecification(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ResourcesContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    resource(i) {
        if (i === undefined) {
            return this.getRuleContexts(ResourceContext);
        }
        return this.getRuleContext(i, ResourceContext);
    }
    SEMI(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.SEMI);
        }
        else {
            return this.getToken(JavaParser.SEMI, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_resources;
    }
    enterRule(listener) {
        if (listener.enterResources) {
            listener.enterResources(this);
        }
    }
    exitRule(listener) {
        if (listener.exitResources) {
            listener.exitResources(this);
        }
    }
    accept(visitor) {
        if (visitor.visitResources) {
            return visitor.visitResources(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ResourceContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    ASSIGN() {
        return this.getToken(JavaParser.ASSIGN, 0);
    }
    expression() {
        return this.getRuleContext(0, ExpressionContext);
    }
    classOrInterfaceType() {
        return this.getRuleContext(0, ClassOrInterfaceTypeContext);
    }
    variableDeclaratorId() {
        return this.getRuleContext(0, VariableDeclaratorIdContext);
    }
    VAR() {
        return this.getToken(JavaParser.VAR, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    variableModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableModifierContext);
        }
        return this.getRuleContext(i, VariableModifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_resource;
    }
    enterRule(listener) {
        if (listener.enterResource) {
            listener.enterResource(this);
        }
    }
    exitRule(listener) {
        if (listener.exitResource) {
            listener.exitResource(this);
        }
    }
    accept(visitor) {
        if (visitor.visitResource) {
            return visitor.visitResource(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class SwitchBlockStatementGroupContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    switchLabel(i) {
        if (i === undefined) {
            return this.getRuleContexts(SwitchLabelContext);
        }
        return this.getRuleContext(i, SwitchLabelContext);
    }
    blockStatement(i) {
        if (i === undefined) {
            return this.getRuleContexts(BlockStatementContext);
        }
        return this.getRuleContext(i, BlockStatementContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_switchBlockStatementGroup;
    }
    enterRule(listener) {
        if (listener.enterSwitchBlockStatementGroup) {
            listener.enterSwitchBlockStatementGroup(this);
        }
    }
    exitRule(listener) {
        if (listener.exitSwitchBlockStatementGroup) {
            listener.exitSwitchBlockStatementGroup(this);
        }
    }
    accept(visitor) {
        if (visitor.visitSwitchBlockStatementGroup) {
            return visitor.visitSwitchBlockStatementGroup(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class SwitchLabelContext extends antlr.ParserRuleContext {
    _constantExpression;
    _enumConstantName;
    _varName;
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    CASE() {
        return this.getToken(JavaParser.CASE, 0);
    }
    COLON() {
        return this.getToken(JavaParser.COLON, 0);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    expression() {
        return this.getRuleContext(0, ExpressionContext);
    }
    IDENTIFIER() {
        return this.getToken(JavaParser.IDENTIFIER, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    DEFAULT() {
        return this.getToken(JavaParser.DEFAULT, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_switchLabel;
    }
    enterRule(listener) {
        if (listener.enterSwitchLabel) {
            listener.enterSwitchLabel(this);
        }
    }
    exitRule(listener) {
        if (listener.exitSwitchLabel) {
            listener.exitSwitchLabel(this);
        }
    }
    accept(visitor) {
        if (visitor.visitSwitchLabel) {
            return visitor.visitSwitchLabel(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ForControlContext extends antlr.ParserRuleContext {
    _forUpdate;
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    enhancedForControl() {
        return this.getRuleContext(0, EnhancedForControlContext);
    }
    SEMI(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.SEMI);
        }
        else {
            return this.getToken(JavaParser.SEMI, i);
        }
    }
    forInit() {
        return this.getRuleContext(0, ForInitContext);
    }
    expression() {
        return this.getRuleContext(0, ExpressionContext);
    }
    expressionList() {
        return this.getRuleContext(0, ExpressionListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_forControl;
    }
    enterRule(listener) {
        if (listener.enterForControl) {
            listener.enterForControl(this);
        }
    }
    exitRule(listener) {
        if (listener.exitForControl) {
            listener.exitForControl(this);
        }
    }
    accept(visitor) {
        if (visitor.visitForControl) {
            return visitor.visitForControl(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ForInitContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    localVariableDeclaration() {
        return this.getRuleContext(0, LocalVariableDeclarationContext);
    }
    expressionList() {
        return this.getRuleContext(0, ExpressionListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_forInit;
    }
    enterRule(listener) {
        if (listener.enterForInit) {
            listener.enterForInit(this);
        }
    }
    exitRule(listener) {
        if (listener.exitForInit) {
            listener.exitForInit(this);
        }
    }
    accept(visitor) {
        if (visitor.visitForInit) {
            return visitor.visitForInit(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class EnhancedForControlContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    variableDeclaratorId() {
        return this.getRuleContext(0, VariableDeclaratorIdContext);
    }
    COLON() {
        return this.getToken(JavaParser.COLON, 0);
    }
    expression() {
        return this.getRuleContext(0, ExpressionContext);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    VAR() {
        return this.getToken(JavaParser.VAR, 0);
    }
    variableModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableModifierContext);
        }
        return this.getRuleContext(i, VariableModifierContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_enhancedForControl;
    }
    enterRule(listener) {
        if (listener.enterEnhancedForControl) {
            listener.enterEnhancedForControl(this);
        }
    }
    exitRule(listener) {
        if (listener.exitEnhancedForControl) {
            listener.exitEnhancedForControl(this);
        }
    }
    accept(visitor) {
        if (visitor.visitEnhancedForControl) {
            return visitor.visitEnhancedForControl(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ParExpressionContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    expression() {
        return this.getRuleContext(0, ExpressionContext);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_parExpression;
    }
    enterRule(listener) {
        if (listener.enterParExpression) {
            listener.enterParExpression(this);
        }
    }
    exitRule(listener) {
        if (listener.exitParExpression) {
            listener.exitParExpression(this);
        }
    }
    accept(visitor) {
        if (visitor.visitParExpression) {
            return visitor.visitParExpression(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ExpressionListContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    expression(i) {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }
        return this.getRuleContext(i, ExpressionContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_expressionList;
    }
    enterRule(listener) {
        if (listener.enterExpressionList) {
            listener.enterExpressionList(this);
        }
    }
    exitRule(listener) {
        if (listener.exitExpressionList) {
            listener.exitExpressionList(this);
        }
    }
    accept(visitor) {
        if (visitor.visitExpressionList) {
            return visitor.visitExpressionList(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class MethodCallContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    expressionList() {
        return this.getRuleContext(0, ExpressionListContext);
    }
    THIS() {
        return this.getToken(JavaParser.THIS, 0);
    }
    SUPER() {
        return this.getToken(JavaParser.SUPER, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_methodCall;
    }
    enterRule(listener) {
        if (listener.enterMethodCall) {
            listener.enterMethodCall(this);
        }
    }
    exitRule(listener) {
        if (listener.exitMethodCall) {
            listener.exitMethodCall(this);
        }
    }
    accept(visitor) {
        if (visitor.visitMethodCall) {
            return visitor.visitMethodCall(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ExpressionContext extends antlr.ParserRuleContext {
    _prefix = null;
    _bop = null;
    _postfix = null;
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    primary() {
        return this.getRuleContext(0, PrimaryContext);
    }
    methodCall() {
        return this.getRuleContext(0, MethodCallContext);
    }
    NEW() {
        return this.getToken(JavaParser.NEW, 0);
    }
    creator() {
        return this.getRuleContext(0, CreatorContext);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    typeType(i) {
        if (i === undefined) {
            return this.getRuleContexts(TypeTypeContext);
        }
        return this.getRuleContext(i, TypeTypeContext);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    expression(i) {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }
        return this.getRuleContext(i, ExpressionContext);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    BITAND(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.BITAND);
        }
        else {
            return this.getToken(JavaParser.BITAND, i);
        }
    }
    ADD() {
        return this.getToken(JavaParser.ADD, 0);
    }
    SUB() {
        return this.getToken(JavaParser.SUB, 0);
    }
    INC() {
        return this.getToken(JavaParser.INC, 0);
    }
    DEC() {
        return this.getToken(JavaParser.DEC, 0);
    }
    TILDE() {
        return this.getToken(JavaParser.TILDE, 0);
    }
    BANG() {
        return this.getToken(JavaParser.BANG, 0);
    }
    lambdaExpression() {
        return this.getRuleContext(0, LambdaExpressionContext);
    }
    switchExpression() {
        return this.getRuleContext(0, SwitchExpressionContext);
    }
    COLONCOLON() {
        return this.getToken(JavaParser.COLONCOLON, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    typeArguments() {
        return this.getRuleContext(0, TypeArgumentsContext);
    }
    classType() {
        return this.getRuleContext(0, ClassTypeContext);
    }
    MUL() {
        return this.getToken(JavaParser.MUL, 0);
    }
    DIV() {
        return this.getToken(JavaParser.DIV, 0);
    }
    MOD() {
        return this.getToken(JavaParser.MOD, 0);
    }
    LT(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.LT);
        }
        else {
            return this.getToken(JavaParser.LT, i);
        }
    }
    GT(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.GT);
        }
        else {
            return this.getToken(JavaParser.GT, i);
        }
    }
    LE() {
        return this.getToken(JavaParser.LE, 0);
    }
    GE() {
        return this.getToken(JavaParser.GE, 0);
    }
    EQUAL() {
        return this.getToken(JavaParser.EQUAL, 0);
    }
    NOTEQUAL() {
        return this.getToken(JavaParser.NOTEQUAL, 0);
    }
    CARET() {
        return this.getToken(JavaParser.CARET, 0);
    }
    BITOR() {
        return this.getToken(JavaParser.BITOR, 0);
    }
    AND() {
        return this.getToken(JavaParser.AND, 0);
    }
    OR() {
        return this.getToken(JavaParser.OR, 0);
    }
    COLON() {
        return this.getToken(JavaParser.COLON, 0);
    }
    QUESTION() {
        return this.getToken(JavaParser.QUESTION, 0);
    }
    ASSIGN() {
        return this.getToken(JavaParser.ASSIGN, 0);
    }
    ADD_ASSIGN() {
        return this.getToken(JavaParser.ADD_ASSIGN, 0);
    }
    SUB_ASSIGN() {
        return this.getToken(JavaParser.SUB_ASSIGN, 0);
    }
    MUL_ASSIGN() {
        return this.getToken(JavaParser.MUL_ASSIGN, 0);
    }
    DIV_ASSIGN() {
        return this.getToken(JavaParser.DIV_ASSIGN, 0);
    }
    AND_ASSIGN() {
        return this.getToken(JavaParser.AND_ASSIGN, 0);
    }
    OR_ASSIGN() {
        return this.getToken(JavaParser.OR_ASSIGN, 0);
    }
    XOR_ASSIGN() {
        return this.getToken(JavaParser.XOR_ASSIGN, 0);
    }
    RSHIFT_ASSIGN() {
        return this.getToken(JavaParser.RSHIFT_ASSIGN, 0);
    }
    URSHIFT_ASSIGN() {
        return this.getToken(JavaParser.URSHIFT_ASSIGN, 0);
    }
    LSHIFT_ASSIGN() {
        return this.getToken(JavaParser.LSHIFT_ASSIGN, 0);
    }
    MOD_ASSIGN() {
        return this.getToken(JavaParser.MOD_ASSIGN, 0);
    }
    DOT() {
        return this.getToken(JavaParser.DOT, 0);
    }
    THIS() {
        return this.getToken(JavaParser.THIS, 0);
    }
    innerCreator() {
        return this.getRuleContext(0, InnerCreatorContext);
    }
    SUPER() {
        return this.getToken(JavaParser.SUPER, 0);
    }
    superSuffix() {
        return this.getRuleContext(0, SuperSuffixContext);
    }
    explicitGenericInvocation() {
        return this.getRuleContext(0, ExplicitGenericInvocationContext);
    }
    nonWildcardTypeArguments() {
        return this.getRuleContext(0, NonWildcardTypeArgumentsContext);
    }
    LBRACK() {
        return this.getToken(JavaParser.LBRACK, 0);
    }
    RBRACK() {
        return this.getToken(JavaParser.RBRACK, 0);
    }
    INSTANCEOF() {
        return this.getToken(JavaParser.INSTANCEOF, 0);
    }
    pattern() {
        return this.getRuleContext(0, PatternContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_expression;
    }
    enterRule(listener) {
        if (listener.enterExpression) {
            listener.enterExpression(this);
        }
    }
    exitRule(listener) {
        if (listener.exitExpression) {
            listener.exitExpression(this);
        }
    }
    accept(visitor) {
        if (visitor.visitExpression) {
            return visitor.visitExpression(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class PatternContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    variableModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableModifierContext);
        }
        return this.getRuleContext(i, VariableModifierContext);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_pattern;
    }
    enterRule(listener) {
        if (listener.enterPattern) {
            listener.enterPattern(this);
        }
    }
    exitRule(listener) {
        if (listener.exitPattern) {
            listener.exitPattern(this);
        }
    }
    accept(visitor) {
        if (visitor.visitPattern) {
            return visitor.visitPattern(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class LambdaExpressionContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    lambdaParameters() {
        return this.getRuleContext(0, LambdaParametersContext);
    }
    ARROW() {
        return this.getToken(JavaParser.ARROW, 0);
    }
    lambdaBody() {
        return this.getRuleContext(0, LambdaBodyContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_lambdaExpression;
    }
    enterRule(listener) {
        if (listener.enterLambdaExpression) {
            listener.enterLambdaExpression(this);
        }
    }
    exitRule(listener) {
        if (listener.exitLambdaExpression) {
            listener.exitLambdaExpression(this);
        }
    }
    accept(visitor) {
        if (visitor.visitLambdaExpression) {
            return visitor.visitLambdaExpression(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class LambdaParametersContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(IdentifierContext);
        }
        return this.getRuleContext(i, IdentifierContext);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    formalParameterList() {
        return this.getRuleContext(0, FormalParameterListContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    lambdaLVTIList() {
        return this.getRuleContext(0, LambdaLVTIListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_lambdaParameters;
    }
    enterRule(listener) {
        if (listener.enterLambdaParameters) {
            listener.enterLambdaParameters(this);
        }
    }
    exitRule(listener) {
        if (listener.exitLambdaParameters) {
            listener.exitLambdaParameters(this);
        }
    }
    accept(visitor) {
        if (visitor.visitLambdaParameters) {
            return visitor.visitLambdaParameters(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class LambdaBodyContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    expression() {
        return this.getRuleContext(0, ExpressionContext);
    }
    block() {
        return this.getRuleContext(0, BlockContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_lambdaBody;
    }
    enterRule(listener) {
        if (listener.enterLambdaBody) {
            listener.enterLambdaBody(this);
        }
    }
    exitRule(listener) {
        if (listener.exitLambdaBody) {
            listener.exitLambdaBody(this);
        }
    }
    accept(visitor) {
        if (visitor.visitLambdaBody) {
            return visitor.visitLambdaBody(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class PrimaryContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    expression() {
        return this.getRuleContext(0, ExpressionContext);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    THIS() {
        return this.getToken(JavaParser.THIS, 0);
    }
    SUPER() {
        return this.getToken(JavaParser.SUPER, 0);
    }
    literal() {
        return this.getRuleContext(0, LiteralContext);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    typeTypeOrVoid() {
        return this.getRuleContext(0, TypeTypeOrVoidContext);
    }
    DOT() {
        return this.getToken(JavaParser.DOT, 0);
    }
    CLASS() {
        return this.getToken(JavaParser.CLASS, 0);
    }
    nonWildcardTypeArguments() {
        return this.getRuleContext(0, NonWildcardTypeArgumentsContext);
    }
    explicitGenericInvocationSuffix() {
        return this.getRuleContext(0, ExplicitGenericInvocationSuffixContext);
    }
    arguments() {
        return this.getRuleContext(0, ArgumentsContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_primary;
    }
    enterRule(listener) {
        if (listener.enterPrimary) {
            listener.enterPrimary(this);
        }
    }
    exitRule(listener) {
        if (listener.exitPrimary) {
            listener.exitPrimary(this);
        }
    }
    accept(visitor) {
        if (visitor.visitPrimary) {
            return visitor.visitPrimary(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class SwitchExpressionContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    SWITCH() {
        return this.getToken(JavaParser.SWITCH, 0);
    }
    parExpression() {
        return this.getRuleContext(0, ParExpressionContext);
    }
    LBRACE() {
        return this.getToken(JavaParser.LBRACE, 0);
    }
    RBRACE() {
        return this.getToken(JavaParser.RBRACE, 0);
    }
    switchLabeledRule(i) {
        if (i === undefined) {
            return this.getRuleContexts(SwitchLabeledRuleContext);
        }
        return this.getRuleContext(i, SwitchLabeledRuleContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_switchExpression;
    }
    enterRule(listener) {
        if (listener.enterSwitchExpression) {
            listener.enterSwitchExpression(this);
        }
    }
    exitRule(listener) {
        if (listener.exitSwitchExpression) {
            listener.exitSwitchExpression(this);
        }
    }
    accept(visitor) {
        if (visitor.visitSwitchExpression) {
            return visitor.visitSwitchExpression(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class SwitchLabeledRuleContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    CASE() {
        return this.getToken(JavaParser.CASE, 0);
    }
    switchRuleOutcome() {
        return this.getRuleContext(0, SwitchRuleOutcomeContext);
    }
    ARROW() {
        return this.getToken(JavaParser.ARROW, 0);
    }
    COLON() {
        return this.getToken(JavaParser.COLON, 0);
    }
    expressionList() {
        return this.getRuleContext(0, ExpressionListContext);
    }
    NULL_LITERAL() {
        return this.getToken(JavaParser.NULL_LITERAL, 0);
    }
    guardedPattern() {
        return this.getRuleContext(0, GuardedPatternContext);
    }
    DEFAULT() {
        return this.getToken(JavaParser.DEFAULT, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_switchLabeledRule;
    }
    enterRule(listener) {
        if (listener.enterSwitchLabeledRule) {
            listener.enterSwitchLabeledRule(this);
        }
    }
    exitRule(listener) {
        if (listener.exitSwitchLabeledRule) {
            listener.exitSwitchLabeledRule(this);
        }
    }
    accept(visitor) {
        if (visitor.visitSwitchLabeledRule) {
            return visitor.visitSwitchLabeledRule(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class GuardedPatternContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    guardedPattern() {
        return this.getRuleContext(0, GuardedPatternContext);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    typeType() {
        return this.getRuleContext(0, TypeTypeContext);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    variableModifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(VariableModifierContext);
        }
        return this.getRuleContext(i, VariableModifierContext);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    AND(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.AND);
        }
        else {
            return this.getToken(JavaParser.AND, i);
        }
    }
    expression(i) {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }
        return this.getRuleContext(i, ExpressionContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_guardedPattern;
    }
    enterRule(listener) {
        if (listener.enterGuardedPattern) {
            listener.enterGuardedPattern(this);
        }
    }
    exitRule(listener) {
        if (listener.exitGuardedPattern) {
            listener.exitGuardedPattern(this);
        }
    }
    accept(visitor) {
        if (visitor.visitGuardedPattern) {
            return visitor.visitGuardedPattern(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class SwitchRuleOutcomeContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    block() {
        return this.getRuleContext(0, BlockContext);
    }
    blockStatement(i) {
        if (i === undefined) {
            return this.getRuleContexts(BlockStatementContext);
        }
        return this.getRuleContext(i, BlockStatementContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_switchRuleOutcome;
    }
    enterRule(listener) {
        if (listener.enterSwitchRuleOutcome) {
            listener.enterSwitchRuleOutcome(this);
        }
    }
    exitRule(listener) {
        if (listener.exitSwitchRuleOutcome) {
            listener.exitSwitchRuleOutcome(this);
        }
    }
    accept(visitor) {
        if (visitor.visitSwitchRuleOutcome) {
            return visitor.visitSwitchRuleOutcome(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ClassTypeContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    classOrInterfaceType() {
        return this.getRuleContext(0, ClassOrInterfaceTypeContext);
    }
    DOT() {
        return this.getToken(JavaParser.DOT, 0);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    typeArguments() {
        return this.getRuleContext(0, TypeArgumentsContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_classType;
    }
    enterRule(listener) {
        if (listener.enterClassType) {
            listener.enterClassType(this);
        }
    }
    exitRule(listener) {
        if (listener.exitClassType) {
            listener.exitClassType(this);
        }
    }
    accept(visitor) {
        if (visitor.visitClassType) {
            return visitor.visitClassType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class CreatorContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    nonWildcardTypeArguments() {
        return this.getRuleContext(0, NonWildcardTypeArgumentsContext);
    }
    createdName() {
        return this.getRuleContext(0, CreatedNameContext);
    }
    classCreatorRest() {
        return this.getRuleContext(0, ClassCreatorRestContext);
    }
    arrayCreatorRest() {
        return this.getRuleContext(0, ArrayCreatorRestContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_creator;
    }
    enterRule(listener) {
        if (listener.enterCreator) {
            listener.enterCreator(this);
        }
    }
    exitRule(listener) {
        if (listener.exitCreator) {
            listener.exitCreator(this);
        }
    }
    accept(visitor) {
        if (visitor.visitCreator) {
            return visitor.visitCreator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class CreatedNameContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier(i) {
        if (i === undefined) {
            return this.getRuleContexts(IdentifierContext);
        }
        return this.getRuleContext(i, IdentifierContext);
    }
    typeArgumentsOrDiamond(i) {
        if (i === undefined) {
            return this.getRuleContexts(TypeArgumentsOrDiamondContext);
        }
        return this.getRuleContext(i, TypeArgumentsOrDiamondContext);
    }
    DOT(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.DOT);
        }
        else {
            return this.getToken(JavaParser.DOT, i);
        }
    }
    primitiveType() {
        return this.getRuleContext(0, PrimitiveTypeContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_createdName;
    }
    enterRule(listener) {
        if (listener.enterCreatedName) {
            listener.enterCreatedName(this);
        }
    }
    exitRule(listener) {
        if (listener.exitCreatedName) {
            listener.exitCreatedName(this);
        }
    }
    accept(visitor) {
        if (visitor.visitCreatedName) {
            return visitor.visitCreatedName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class InnerCreatorContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    classCreatorRest() {
        return this.getRuleContext(0, ClassCreatorRestContext);
    }
    nonWildcardTypeArgumentsOrDiamond() {
        return this.getRuleContext(0, NonWildcardTypeArgumentsOrDiamondContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_innerCreator;
    }
    enterRule(listener) {
        if (listener.enterInnerCreator) {
            listener.enterInnerCreator(this);
        }
    }
    exitRule(listener) {
        if (listener.exitInnerCreator) {
            listener.exitInnerCreator(this);
        }
    }
    accept(visitor) {
        if (visitor.visitInnerCreator) {
            return visitor.visitInnerCreator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ArrayCreatorRestContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.LBRACK);
        }
        else {
            return this.getToken(JavaParser.LBRACK, i);
        }
    }
    RBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.RBRACK);
        }
        else {
            return this.getToken(JavaParser.RBRACK, i);
        }
    }
    arrayInitializer() {
        return this.getRuleContext(0, ArrayInitializerContext);
    }
    expression(i) {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }
        return this.getRuleContext(i, ExpressionContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_arrayCreatorRest;
    }
    enterRule(listener) {
        if (listener.enterArrayCreatorRest) {
            listener.enterArrayCreatorRest(this);
        }
    }
    exitRule(listener) {
        if (listener.exitArrayCreatorRest) {
            listener.exitArrayCreatorRest(this);
        }
    }
    accept(visitor) {
        if (visitor.visitArrayCreatorRest) {
            return visitor.visitArrayCreatorRest(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ClassCreatorRestContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    arguments() {
        return this.getRuleContext(0, ArgumentsContext);
    }
    classBody() {
        return this.getRuleContext(0, ClassBodyContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_classCreatorRest;
    }
    enterRule(listener) {
        if (listener.enterClassCreatorRest) {
            listener.enterClassCreatorRest(this);
        }
    }
    exitRule(listener) {
        if (listener.exitClassCreatorRest) {
            listener.exitClassCreatorRest(this);
        }
    }
    accept(visitor) {
        if (visitor.visitClassCreatorRest) {
            return visitor.visitClassCreatorRest(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ExplicitGenericInvocationContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    nonWildcardTypeArguments() {
        return this.getRuleContext(0, NonWildcardTypeArgumentsContext);
    }
    explicitGenericInvocationSuffix() {
        return this.getRuleContext(0, ExplicitGenericInvocationSuffixContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_explicitGenericInvocation;
    }
    enterRule(listener) {
        if (listener.enterExplicitGenericInvocation) {
            listener.enterExplicitGenericInvocation(this);
        }
    }
    exitRule(listener) {
        if (listener.exitExplicitGenericInvocation) {
            listener.exitExplicitGenericInvocation(this);
        }
    }
    accept(visitor) {
        if (visitor.visitExplicitGenericInvocation) {
            return visitor.visitExplicitGenericInvocation(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeArgumentsOrDiamondContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LT() {
        return this.getToken(JavaParser.LT, 0);
    }
    GT() {
        return this.getToken(JavaParser.GT, 0);
    }
    typeArguments() {
        return this.getRuleContext(0, TypeArgumentsContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_typeArgumentsOrDiamond;
    }
    enterRule(listener) {
        if (listener.enterTypeArgumentsOrDiamond) {
            listener.enterTypeArgumentsOrDiamond(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeArgumentsOrDiamond) {
            listener.exitTypeArgumentsOrDiamond(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeArgumentsOrDiamond) {
            return visitor.visitTypeArgumentsOrDiamond(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class NonWildcardTypeArgumentsOrDiamondContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LT() {
        return this.getToken(JavaParser.LT, 0);
    }
    GT() {
        return this.getToken(JavaParser.GT, 0);
    }
    nonWildcardTypeArguments() {
        return this.getRuleContext(0, NonWildcardTypeArgumentsContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_nonWildcardTypeArgumentsOrDiamond;
    }
    enterRule(listener) {
        if (listener.enterNonWildcardTypeArgumentsOrDiamond) {
            listener.enterNonWildcardTypeArgumentsOrDiamond(this);
        }
    }
    exitRule(listener) {
        if (listener.exitNonWildcardTypeArgumentsOrDiamond) {
            listener.exitNonWildcardTypeArgumentsOrDiamond(this);
        }
    }
    accept(visitor) {
        if (visitor.visitNonWildcardTypeArgumentsOrDiamond) {
            return visitor.visitNonWildcardTypeArgumentsOrDiamond(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class NonWildcardTypeArgumentsContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LT() {
        return this.getToken(JavaParser.LT, 0);
    }
    typeList() {
        return this.getRuleContext(0, TypeListContext);
    }
    GT() {
        return this.getToken(JavaParser.GT, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_nonWildcardTypeArguments;
    }
    enterRule(listener) {
        if (listener.enterNonWildcardTypeArguments) {
            listener.enterNonWildcardTypeArguments(this);
        }
    }
    exitRule(listener) {
        if (listener.exitNonWildcardTypeArguments) {
            listener.exitNonWildcardTypeArguments(this);
        }
    }
    accept(visitor) {
        if (visitor.visitNonWildcardTypeArguments) {
            return visitor.visitNonWildcardTypeArguments(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeListContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    typeType(i) {
        if (i === undefined) {
            return this.getRuleContexts(TypeTypeContext);
        }
        return this.getRuleContext(i, TypeTypeContext);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_typeList;
    }
    enterRule(listener) {
        if (listener.enterTypeList) {
            listener.enterTypeList(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeList) {
            listener.exitTypeList(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeList) {
            return visitor.visitTypeList(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeTypeContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    classOrInterfaceType() {
        return this.getRuleContext(0, ClassOrInterfaceTypeContext);
    }
    primitiveType() {
        return this.getRuleContext(0, PrimitiveTypeContext);
    }
    annotation(i) {
        if (i === undefined) {
            return this.getRuleContexts(AnnotationContext);
        }
        return this.getRuleContext(i, AnnotationContext);
    }
    LBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.LBRACK);
        }
        else {
            return this.getToken(JavaParser.LBRACK, i);
        }
    }
    RBRACK(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.RBRACK);
        }
        else {
            return this.getToken(JavaParser.RBRACK, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_typeType;
    }
    enterRule(listener) {
        if (listener.enterTypeType) {
            listener.enterTypeType(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeType) {
            listener.exitTypeType(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeType) {
            return visitor.visitTypeType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class PrimitiveTypeContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    BOOLEAN() {
        return this.getToken(JavaParser.BOOLEAN, 0);
    }
    CHAR() {
        return this.getToken(JavaParser.CHAR, 0);
    }
    BYTE() {
        return this.getToken(JavaParser.BYTE, 0);
    }
    SHORT() {
        return this.getToken(JavaParser.SHORT, 0);
    }
    INT() {
        return this.getToken(JavaParser.INT, 0);
    }
    LONG() {
        return this.getToken(JavaParser.LONG, 0);
    }
    FLOAT() {
        return this.getToken(JavaParser.FLOAT, 0);
    }
    DOUBLE() {
        return this.getToken(JavaParser.DOUBLE, 0);
    }
    get ruleIndex() {
        return JavaParser.RULE_primitiveType;
    }
    enterRule(listener) {
        if (listener.enterPrimitiveType) {
            listener.enterPrimitiveType(this);
        }
    }
    exitRule(listener) {
        if (listener.exitPrimitiveType) {
            listener.exitPrimitiveType(this);
        }
    }
    accept(visitor) {
        if (visitor.visitPrimitiveType) {
            return visitor.visitPrimitiveType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class TypeArgumentsContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LT() {
        return this.getToken(JavaParser.LT, 0);
    }
    typeArgument(i) {
        if (i === undefined) {
            return this.getRuleContexts(TypeArgumentContext);
        }
        return this.getRuleContext(i, TypeArgumentContext);
    }
    GT() {
        return this.getToken(JavaParser.GT, 0);
    }
    COMMA(i) {
        if (i === undefined) {
            return this.getTokens(JavaParser.COMMA);
        }
        else {
            return this.getToken(JavaParser.COMMA, i);
        }
    }
    get ruleIndex() {
        return JavaParser.RULE_typeArguments;
    }
    enterRule(listener) {
        if (listener.enterTypeArguments) {
            listener.enterTypeArguments(this);
        }
    }
    exitRule(listener) {
        if (listener.exitTypeArguments) {
            listener.exitTypeArguments(this);
        }
    }
    accept(visitor) {
        if (visitor.visitTypeArguments) {
            return visitor.visitTypeArguments(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class SuperSuffixContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    arguments() {
        return this.getRuleContext(0, ArgumentsContext);
    }
    DOT() {
        return this.getToken(JavaParser.DOT, 0);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    typeArguments() {
        return this.getRuleContext(0, TypeArgumentsContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_superSuffix;
    }
    enterRule(listener) {
        if (listener.enterSuperSuffix) {
            listener.enterSuperSuffix(this);
        }
    }
    exitRule(listener) {
        if (listener.exitSuperSuffix) {
            listener.exitSuperSuffix(this);
        }
    }
    accept(visitor) {
        if (visitor.visitSuperSuffix) {
            return visitor.visitSuperSuffix(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ExplicitGenericInvocationSuffixContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    SUPER() {
        return this.getToken(JavaParser.SUPER, 0);
    }
    superSuffix() {
        return this.getRuleContext(0, SuperSuffixContext);
    }
    identifier() {
        return this.getRuleContext(0, IdentifierContext);
    }
    arguments() {
        return this.getRuleContext(0, ArgumentsContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_explicitGenericInvocationSuffix;
    }
    enterRule(listener) {
        if (listener.enterExplicitGenericInvocationSuffix) {
            listener.enterExplicitGenericInvocationSuffix(this);
        }
    }
    exitRule(listener) {
        if (listener.exitExplicitGenericInvocationSuffix) {
            listener.exitExplicitGenericInvocationSuffix(this);
        }
    }
    accept(visitor) {
        if (visitor.visitExplicitGenericInvocationSuffix) {
            return visitor.visitExplicitGenericInvocationSuffix(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
export class ArgumentsContext extends antlr.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    LPAREN() {
        return this.getToken(JavaParser.LPAREN, 0);
    }
    RPAREN() {
        return this.getToken(JavaParser.RPAREN, 0);
    }
    expressionList() {
        return this.getRuleContext(0, ExpressionListContext);
    }
    get ruleIndex() {
        return JavaParser.RULE_arguments;
    }
    enterRule(listener) {
        if (listener.enterArguments) {
            listener.enterArguments(this);
        }
    }
    exitRule(listener) {
        if (listener.exitArguments) {
            listener.exitArguments(this);
        }
    }
    accept(visitor) {
        if (visitor.visitArguments) {
            return visitor.visitArguments(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
//# sourceMappingURL=JavaParser.js.map