// Generated from ./parser/JavaParser.g4 by ANTLR 4.13.1
/**
 * This interface defines a complete listener for a parse tree produced by
 * `JavaParser`.
 */
export class JavaParserListener {
    /**
     * Enter a parse tree produced by `JavaParser.compilationUnit`.
     * @param ctx the parse tree
     */
    enterCompilationUnit;
    /**
     * Exit a parse tree produced by `JavaParser.compilationUnit`.
     * @param ctx the parse tree
     */
    exitCompilationUnit;
    /**
     * Enter a parse tree produced by `JavaParser.packageDeclaration`.
     * @param ctx the parse tree
     */
    enterPackageDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.packageDeclaration`.
     * @param ctx the parse tree
     */
    exitPackageDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.importDeclaration`.
     * @param ctx the parse tree
     */
    enterImportDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.importDeclaration`.
     * @param ctx the parse tree
     */
    exitImportDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.typeDeclaration`.
     * @param ctx the parse tree
     */
    enterTypeDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.typeDeclaration`.
     * @param ctx the parse tree
     */
    exitTypeDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.modifier`.
     * @param ctx the parse tree
     */
    enterModifier;
    /**
     * Exit a parse tree produced by `JavaParser.modifier`.
     * @param ctx the parse tree
     */
    exitModifier;
    /**
     * Enter a parse tree produced by `JavaParser.classOrInterfaceModifier`.
     * @param ctx the parse tree
     */
    enterClassOrInterfaceModifier;
    /**
     * Exit a parse tree produced by `JavaParser.classOrInterfaceModifier`.
     * @param ctx the parse tree
     */
    exitClassOrInterfaceModifier;
    /**
     * Enter a parse tree produced by `JavaParser.variableModifier`.
     * @param ctx the parse tree
     */
    enterVariableModifier;
    /**
     * Exit a parse tree produced by `JavaParser.variableModifier`.
     * @param ctx the parse tree
     */
    exitVariableModifier;
    /**
     * Enter a parse tree produced by `JavaParser.classDeclaration`.
     * @param ctx the parse tree
     */
    enterClassDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.classDeclaration`.
     * @param ctx the parse tree
     */
    exitClassDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.typeParameters`.
     * @param ctx the parse tree
     */
    enterTypeParameters;
    /**
     * Exit a parse tree produced by `JavaParser.typeParameters`.
     * @param ctx the parse tree
     */
    exitTypeParameters;
    /**
     * Enter a parse tree produced by `JavaParser.typeParameter`.
     * @param ctx the parse tree
     */
    enterTypeParameter;
    /**
     * Exit a parse tree produced by `JavaParser.typeParameter`.
     * @param ctx the parse tree
     */
    exitTypeParameter;
    /**
     * Enter a parse tree produced by `JavaParser.typeBound`.
     * @param ctx the parse tree
     */
    enterTypeBound;
    /**
     * Exit a parse tree produced by `JavaParser.typeBound`.
     * @param ctx the parse tree
     */
    exitTypeBound;
    /**
     * Enter a parse tree produced by `JavaParser.enumDeclaration`.
     * @param ctx the parse tree
     */
    enterEnumDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.enumDeclaration`.
     * @param ctx the parse tree
     */
    exitEnumDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.enumConstants`.
     * @param ctx the parse tree
     */
    enterEnumConstants;
    /**
     * Exit a parse tree produced by `JavaParser.enumConstants`.
     * @param ctx the parse tree
     */
    exitEnumConstants;
    /**
     * Enter a parse tree produced by `JavaParser.enumConstant`.
     * @param ctx the parse tree
     */
    enterEnumConstant;
    /**
     * Exit a parse tree produced by `JavaParser.enumConstant`.
     * @param ctx the parse tree
     */
    exitEnumConstant;
    /**
     * Enter a parse tree produced by `JavaParser.enumBodyDeclarations`.
     * @param ctx the parse tree
     */
    enterEnumBodyDeclarations;
    /**
     * Exit a parse tree produced by `JavaParser.enumBodyDeclarations`.
     * @param ctx the parse tree
     */
    exitEnumBodyDeclarations;
    /**
     * Enter a parse tree produced by `JavaParser.interfaceDeclaration`.
     * @param ctx the parse tree
     */
    enterInterfaceDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.interfaceDeclaration`.
     * @param ctx the parse tree
     */
    exitInterfaceDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.classBody`.
     * @param ctx the parse tree
     */
    enterClassBody;
    /**
     * Exit a parse tree produced by `JavaParser.classBody`.
     * @param ctx the parse tree
     */
    exitClassBody;
    /**
     * Enter a parse tree produced by `JavaParser.interfaceBody`.
     * @param ctx the parse tree
     */
    enterInterfaceBody;
    /**
     * Exit a parse tree produced by `JavaParser.interfaceBody`.
     * @param ctx the parse tree
     */
    exitInterfaceBody;
    /**
     * Enter a parse tree produced by `JavaParser.classBodyDeclaration`.
     * @param ctx the parse tree
     */
    enterClassBodyDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.classBodyDeclaration`.
     * @param ctx the parse tree
     */
    exitClassBodyDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.memberDeclaration`.
     * @param ctx the parse tree
     */
    enterMemberDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.memberDeclaration`.
     * @param ctx the parse tree
     */
    exitMemberDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.methodDeclaration`.
     * @param ctx the parse tree
     */
    enterMethodDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.methodDeclaration`.
     * @param ctx the parse tree
     */
    exitMethodDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.methodBody`.
     * @param ctx the parse tree
     */
    enterMethodBody;
    /**
     * Exit a parse tree produced by `JavaParser.methodBody`.
     * @param ctx the parse tree
     */
    exitMethodBody;
    /**
     * Enter a parse tree produced by `JavaParser.typeTypeOrVoid`.
     * @param ctx the parse tree
     */
    enterTypeTypeOrVoid;
    /**
     * Exit a parse tree produced by `JavaParser.typeTypeOrVoid`.
     * @param ctx the parse tree
     */
    exitTypeTypeOrVoid;
    /**
     * Enter a parse tree produced by `JavaParser.genericMethodDeclaration`.
     * @param ctx the parse tree
     */
    enterGenericMethodDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.genericMethodDeclaration`.
     * @param ctx the parse tree
     */
    exitGenericMethodDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.genericConstructorDeclaration`.
     * @param ctx the parse tree
     */
    enterGenericConstructorDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.genericConstructorDeclaration`.
     * @param ctx the parse tree
     */
    exitGenericConstructorDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.constructorDeclaration`.
     * @param ctx the parse tree
     */
    enterConstructorDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.constructorDeclaration`.
     * @param ctx the parse tree
     */
    exitConstructorDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.fieldDeclaration`.
     * @param ctx the parse tree
     */
    enterFieldDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.fieldDeclaration`.
     * @param ctx the parse tree
     */
    exitFieldDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.interfaceBodyDeclaration`.
     * @param ctx the parse tree
     */
    enterInterfaceBodyDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.interfaceBodyDeclaration`.
     * @param ctx the parse tree
     */
    exitInterfaceBodyDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.interfaceMemberDeclaration`.
     * @param ctx the parse tree
     */
    enterInterfaceMemberDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.interfaceMemberDeclaration`.
     * @param ctx the parse tree
     */
    exitInterfaceMemberDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.constDeclaration`.
     * @param ctx the parse tree
     */
    enterConstDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.constDeclaration`.
     * @param ctx the parse tree
     */
    exitConstDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.constantDeclarator`.
     * @param ctx the parse tree
     */
    enterConstantDeclarator;
    /**
     * Exit a parse tree produced by `JavaParser.constantDeclarator`.
     * @param ctx the parse tree
     */
    exitConstantDeclarator;
    /**
     * Enter a parse tree produced by `JavaParser.interfaceMethodDeclaration`.
     * @param ctx the parse tree
     */
    enterInterfaceMethodDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.interfaceMethodDeclaration`.
     * @param ctx the parse tree
     */
    exitInterfaceMethodDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.interfaceMethodModifier`.
     * @param ctx the parse tree
     */
    enterInterfaceMethodModifier;
    /**
     * Exit a parse tree produced by `JavaParser.interfaceMethodModifier`.
     * @param ctx the parse tree
     */
    exitInterfaceMethodModifier;
    /**
     * Enter a parse tree produced by `JavaParser.genericInterfaceMethodDeclaration`.
     * @param ctx the parse tree
     */
    enterGenericInterfaceMethodDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.genericInterfaceMethodDeclaration`.
     * @param ctx the parse tree
     */
    exitGenericInterfaceMethodDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.interfaceCommonBodyDeclaration`.
     * @param ctx the parse tree
     */
    enterInterfaceCommonBodyDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.interfaceCommonBodyDeclaration`.
     * @param ctx the parse tree
     */
    exitInterfaceCommonBodyDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.variableDeclarators`.
     * @param ctx the parse tree
     */
    enterVariableDeclarators;
    /**
     * Exit a parse tree produced by `JavaParser.variableDeclarators`.
     * @param ctx the parse tree
     */
    exitVariableDeclarators;
    /**
     * Enter a parse tree produced by `JavaParser.variableDeclarator`.
     * @param ctx the parse tree
     */
    enterVariableDeclarator;
    /**
     * Exit a parse tree produced by `JavaParser.variableDeclarator`.
     * @param ctx the parse tree
     */
    exitVariableDeclarator;
    /**
     * Enter a parse tree produced by `JavaParser.variableDeclaratorId`.
     * @param ctx the parse tree
     */
    enterVariableDeclaratorId;
    /**
     * Exit a parse tree produced by `JavaParser.variableDeclaratorId`.
     * @param ctx the parse tree
     */
    exitVariableDeclaratorId;
    /**
     * Enter a parse tree produced by `JavaParser.variableInitializer`.
     * @param ctx the parse tree
     */
    enterVariableInitializer;
    /**
     * Exit a parse tree produced by `JavaParser.variableInitializer`.
     * @param ctx the parse tree
     */
    exitVariableInitializer;
    /**
     * Enter a parse tree produced by `JavaParser.arrayInitializer`.
     * @param ctx the parse tree
     */
    enterArrayInitializer;
    /**
     * Exit a parse tree produced by `JavaParser.arrayInitializer`.
     * @param ctx the parse tree
     */
    exitArrayInitializer;
    /**
     * Enter a parse tree produced by `JavaParser.classOrInterfaceType`.
     * @param ctx the parse tree
     */
    enterClassOrInterfaceType;
    /**
     * Exit a parse tree produced by `JavaParser.classOrInterfaceType`.
     * @param ctx the parse tree
     */
    exitClassOrInterfaceType;
    /**
     * Enter a parse tree produced by `JavaParser.typeArgument`.
     * @param ctx the parse tree
     */
    enterTypeArgument;
    /**
     * Exit a parse tree produced by `JavaParser.typeArgument`.
     * @param ctx the parse tree
     */
    exitTypeArgument;
    /**
     * Enter a parse tree produced by `JavaParser.qualifiedNameList`.
     * @param ctx the parse tree
     */
    enterQualifiedNameList;
    /**
     * Exit a parse tree produced by `JavaParser.qualifiedNameList`.
     * @param ctx the parse tree
     */
    exitQualifiedNameList;
    /**
     * Enter a parse tree produced by `JavaParser.formalParameters`.
     * @param ctx the parse tree
     */
    enterFormalParameters;
    /**
     * Exit a parse tree produced by `JavaParser.formalParameters`.
     * @param ctx the parse tree
     */
    exitFormalParameters;
    /**
     * Enter a parse tree produced by `JavaParser.receiverParameter`.
     * @param ctx the parse tree
     */
    enterReceiverParameter;
    /**
     * Exit a parse tree produced by `JavaParser.receiverParameter`.
     * @param ctx the parse tree
     */
    exitReceiverParameter;
    /**
     * Enter a parse tree produced by `JavaParser.formalParameterList`.
     * @param ctx the parse tree
     */
    enterFormalParameterList;
    /**
     * Exit a parse tree produced by `JavaParser.formalParameterList`.
     * @param ctx the parse tree
     */
    exitFormalParameterList;
    /**
     * Enter a parse tree produced by `JavaParser.formalParameter`.
     * @param ctx the parse tree
     */
    enterFormalParameter;
    /**
     * Exit a parse tree produced by `JavaParser.formalParameter`.
     * @param ctx the parse tree
     */
    exitFormalParameter;
    /**
     * Enter a parse tree produced by `JavaParser.lastFormalParameter`.
     * @param ctx the parse tree
     */
    enterLastFormalParameter;
    /**
     * Exit a parse tree produced by `JavaParser.lastFormalParameter`.
     * @param ctx the parse tree
     */
    exitLastFormalParameter;
    /**
     * Enter a parse tree produced by `JavaParser.lambdaLVTIList`.
     * @param ctx the parse tree
     */
    enterLambdaLVTIList;
    /**
     * Exit a parse tree produced by `JavaParser.lambdaLVTIList`.
     * @param ctx the parse tree
     */
    exitLambdaLVTIList;
    /**
     * Enter a parse tree produced by `JavaParser.lambdaLVTIParameter`.
     * @param ctx the parse tree
     */
    enterLambdaLVTIParameter;
    /**
     * Exit a parse tree produced by `JavaParser.lambdaLVTIParameter`.
     * @param ctx the parse tree
     */
    exitLambdaLVTIParameter;
    /**
     * Enter a parse tree produced by `JavaParser.qualifiedName`.
     * @param ctx the parse tree
     */
    enterQualifiedName;
    /**
     * Exit a parse tree produced by `JavaParser.qualifiedName`.
     * @param ctx the parse tree
     */
    exitQualifiedName;
    /**
     * Enter a parse tree produced by `JavaParser.literal`.
     * @param ctx the parse tree
     */
    enterLiteral;
    /**
     * Exit a parse tree produced by `JavaParser.literal`.
     * @param ctx the parse tree
     */
    exitLiteral;
    /**
     * Enter a parse tree produced by `JavaParser.integerLiteral`.
     * @param ctx the parse tree
     */
    enterIntegerLiteral;
    /**
     * Exit a parse tree produced by `JavaParser.integerLiteral`.
     * @param ctx the parse tree
     */
    exitIntegerLiteral;
    /**
     * Enter a parse tree produced by `JavaParser.floatLiteral`.
     * @param ctx the parse tree
     */
    enterFloatLiteral;
    /**
     * Exit a parse tree produced by `JavaParser.floatLiteral`.
     * @param ctx the parse tree
     */
    exitFloatLiteral;
    /**
     * Enter a parse tree produced by `JavaParser.altAnnotationQualifiedName`.
     * @param ctx the parse tree
     */
    enterAltAnnotationQualifiedName;
    /**
     * Exit a parse tree produced by `JavaParser.altAnnotationQualifiedName`.
     * @param ctx the parse tree
     */
    exitAltAnnotationQualifiedName;
    /**
     * Enter a parse tree produced by `JavaParser.annotation`.
     * @param ctx the parse tree
     */
    enterAnnotation;
    /**
     * Exit a parse tree produced by `JavaParser.annotation`.
     * @param ctx the parse tree
     */
    exitAnnotation;
    /**
     * Enter a parse tree produced by `JavaParser.elementValuePairs`.
     * @param ctx the parse tree
     */
    enterElementValuePairs;
    /**
     * Exit a parse tree produced by `JavaParser.elementValuePairs`.
     * @param ctx the parse tree
     */
    exitElementValuePairs;
    /**
     * Enter a parse tree produced by `JavaParser.elementValuePair`.
     * @param ctx the parse tree
     */
    enterElementValuePair;
    /**
     * Exit a parse tree produced by `JavaParser.elementValuePair`.
     * @param ctx the parse tree
     */
    exitElementValuePair;
    /**
     * Enter a parse tree produced by `JavaParser.elementValue`.
     * @param ctx the parse tree
     */
    enterElementValue;
    /**
     * Exit a parse tree produced by `JavaParser.elementValue`.
     * @param ctx the parse tree
     */
    exitElementValue;
    /**
     * Enter a parse tree produced by `JavaParser.elementValueArrayInitializer`.
     * @param ctx the parse tree
     */
    enterElementValueArrayInitializer;
    /**
     * Exit a parse tree produced by `JavaParser.elementValueArrayInitializer`.
     * @param ctx the parse tree
     */
    exitElementValueArrayInitializer;
    /**
     * Enter a parse tree produced by `JavaParser.annotationTypeDeclaration`.
     * @param ctx the parse tree
     */
    enterAnnotationTypeDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.annotationTypeDeclaration`.
     * @param ctx the parse tree
     */
    exitAnnotationTypeDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.annotationTypeBody`.
     * @param ctx the parse tree
     */
    enterAnnotationTypeBody;
    /**
     * Exit a parse tree produced by `JavaParser.annotationTypeBody`.
     * @param ctx the parse tree
     */
    exitAnnotationTypeBody;
    /**
     * Enter a parse tree produced by `JavaParser.annotationTypeElementDeclaration`.
     * @param ctx the parse tree
     */
    enterAnnotationTypeElementDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.annotationTypeElementDeclaration`.
     * @param ctx the parse tree
     */
    exitAnnotationTypeElementDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.annotationTypeElementRest`.
     * @param ctx the parse tree
     */
    enterAnnotationTypeElementRest;
    /**
     * Exit a parse tree produced by `JavaParser.annotationTypeElementRest`.
     * @param ctx the parse tree
     */
    exitAnnotationTypeElementRest;
    /**
     * Enter a parse tree produced by `JavaParser.annotationMethodOrConstantRest`.
     * @param ctx the parse tree
     */
    enterAnnotationMethodOrConstantRest;
    /**
     * Exit a parse tree produced by `JavaParser.annotationMethodOrConstantRest`.
     * @param ctx the parse tree
     */
    exitAnnotationMethodOrConstantRest;
    /**
     * Enter a parse tree produced by `JavaParser.annotationMethodRest`.
     * @param ctx the parse tree
     */
    enterAnnotationMethodRest;
    /**
     * Exit a parse tree produced by `JavaParser.annotationMethodRest`.
     * @param ctx the parse tree
     */
    exitAnnotationMethodRest;
    /**
     * Enter a parse tree produced by `JavaParser.annotationConstantRest`.
     * @param ctx the parse tree
     */
    enterAnnotationConstantRest;
    /**
     * Exit a parse tree produced by `JavaParser.annotationConstantRest`.
     * @param ctx the parse tree
     */
    exitAnnotationConstantRest;
    /**
     * Enter a parse tree produced by `JavaParser.defaultValue`.
     * @param ctx the parse tree
     */
    enterDefaultValue;
    /**
     * Exit a parse tree produced by `JavaParser.defaultValue`.
     * @param ctx the parse tree
     */
    exitDefaultValue;
    /**
     * Enter a parse tree produced by `JavaParser.moduleDeclaration`.
     * @param ctx the parse tree
     */
    enterModuleDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.moduleDeclaration`.
     * @param ctx the parse tree
     */
    exitModuleDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.moduleBody`.
     * @param ctx the parse tree
     */
    enterModuleBody;
    /**
     * Exit a parse tree produced by `JavaParser.moduleBody`.
     * @param ctx the parse tree
     */
    exitModuleBody;
    /**
     * Enter a parse tree produced by `JavaParser.moduleDirective`.
     * @param ctx the parse tree
     */
    enterModuleDirective;
    /**
     * Exit a parse tree produced by `JavaParser.moduleDirective`.
     * @param ctx the parse tree
     */
    exitModuleDirective;
    /**
     * Enter a parse tree produced by `JavaParser.requiresModifier`.
     * @param ctx the parse tree
     */
    enterRequiresModifier;
    /**
     * Exit a parse tree produced by `JavaParser.requiresModifier`.
     * @param ctx the parse tree
     */
    exitRequiresModifier;
    /**
     * Enter a parse tree produced by `JavaParser.recordDeclaration`.
     * @param ctx the parse tree
     */
    enterRecordDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.recordDeclaration`.
     * @param ctx the parse tree
     */
    exitRecordDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.recordHeader`.
     * @param ctx the parse tree
     */
    enterRecordHeader;
    /**
     * Exit a parse tree produced by `JavaParser.recordHeader`.
     * @param ctx the parse tree
     */
    exitRecordHeader;
    /**
     * Enter a parse tree produced by `JavaParser.recordComponentList`.
     * @param ctx the parse tree
     */
    enterRecordComponentList;
    /**
     * Exit a parse tree produced by `JavaParser.recordComponentList`.
     * @param ctx the parse tree
     */
    exitRecordComponentList;
    /**
     * Enter a parse tree produced by `JavaParser.recordComponent`.
     * @param ctx the parse tree
     */
    enterRecordComponent;
    /**
     * Exit a parse tree produced by `JavaParser.recordComponent`.
     * @param ctx the parse tree
     */
    exitRecordComponent;
    /**
     * Enter a parse tree produced by `JavaParser.recordBody`.
     * @param ctx the parse tree
     */
    enterRecordBody;
    /**
     * Exit a parse tree produced by `JavaParser.recordBody`.
     * @param ctx the parse tree
     */
    exitRecordBody;
    /**
     * Enter a parse tree produced by `JavaParser.block`.
     * @param ctx the parse tree
     */
    enterBlock;
    /**
     * Exit a parse tree produced by `JavaParser.block`.
     * @param ctx the parse tree
     */
    exitBlock;
    /**
     * Enter a parse tree produced by `JavaParser.blockStatement`.
     * @param ctx the parse tree
     */
    enterBlockStatement;
    /**
     * Exit a parse tree produced by `JavaParser.blockStatement`.
     * @param ctx the parse tree
     */
    exitBlockStatement;
    /**
     * Enter a parse tree produced by `JavaParser.localVariableDeclaration`.
     * @param ctx the parse tree
     */
    enterLocalVariableDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.localVariableDeclaration`.
     * @param ctx the parse tree
     */
    exitLocalVariableDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.identifier`.
     * @param ctx the parse tree
     */
    enterIdentifier;
    /**
     * Exit a parse tree produced by `JavaParser.identifier`.
     * @param ctx the parse tree
     */
    exitIdentifier;
    /**
     * Enter a parse tree produced by `JavaParser.localTypeDeclaration`.
     * @param ctx the parse tree
     */
    enterLocalTypeDeclaration;
    /**
     * Exit a parse tree produced by `JavaParser.localTypeDeclaration`.
     * @param ctx the parse tree
     */
    exitLocalTypeDeclaration;
    /**
     * Enter a parse tree produced by `JavaParser.statement`.
     * @param ctx the parse tree
     */
    enterStatement;
    /**
     * Exit a parse tree produced by `JavaParser.statement`.
     * @param ctx the parse tree
     */
    exitStatement;
    /**
     * Enter a parse tree produced by `JavaParser.catchClause`.
     * @param ctx the parse tree
     */
    enterCatchClause;
    /**
     * Exit a parse tree produced by `JavaParser.catchClause`.
     * @param ctx the parse tree
     */
    exitCatchClause;
    /**
     * Enter a parse tree produced by `JavaParser.catchType`.
     * @param ctx the parse tree
     */
    enterCatchType;
    /**
     * Exit a parse tree produced by `JavaParser.catchType`.
     * @param ctx the parse tree
     */
    exitCatchType;
    /**
     * Enter a parse tree produced by `JavaParser.finallyBlock`.
     * @param ctx the parse tree
     */
    enterFinallyBlock;
    /**
     * Exit a parse tree produced by `JavaParser.finallyBlock`.
     * @param ctx the parse tree
     */
    exitFinallyBlock;
    /**
     * Enter a parse tree produced by `JavaParser.resourceSpecification`.
     * @param ctx the parse tree
     */
    enterResourceSpecification;
    /**
     * Exit a parse tree produced by `JavaParser.resourceSpecification`.
     * @param ctx the parse tree
     */
    exitResourceSpecification;
    /**
     * Enter a parse tree produced by `JavaParser.resources`.
     * @param ctx the parse tree
     */
    enterResources;
    /**
     * Exit a parse tree produced by `JavaParser.resources`.
     * @param ctx the parse tree
     */
    exitResources;
    /**
     * Enter a parse tree produced by `JavaParser.resource`.
     * @param ctx the parse tree
     */
    enterResource;
    /**
     * Exit a parse tree produced by `JavaParser.resource`.
     * @param ctx the parse tree
     */
    exitResource;
    /**
     * Enter a parse tree produced by `JavaParser.switchBlockStatementGroup`.
     * @param ctx the parse tree
     */
    enterSwitchBlockStatementGroup;
    /**
     * Exit a parse tree produced by `JavaParser.switchBlockStatementGroup`.
     * @param ctx the parse tree
     */
    exitSwitchBlockStatementGroup;
    /**
     * Enter a parse tree produced by `JavaParser.switchLabel`.
     * @param ctx the parse tree
     */
    enterSwitchLabel;
    /**
     * Exit a parse tree produced by `JavaParser.switchLabel`.
     * @param ctx the parse tree
     */
    exitSwitchLabel;
    /**
     * Enter a parse tree produced by `JavaParser.forControl`.
     * @param ctx the parse tree
     */
    enterForControl;
    /**
     * Exit a parse tree produced by `JavaParser.forControl`.
     * @param ctx the parse tree
     */
    exitForControl;
    /**
     * Enter a parse tree produced by `JavaParser.forInit`.
     * @param ctx the parse tree
     */
    enterForInit;
    /**
     * Exit a parse tree produced by `JavaParser.forInit`.
     * @param ctx the parse tree
     */
    exitForInit;
    /**
     * Enter a parse tree produced by `JavaParser.enhancedForControl`.
     * @param ctx the parse tree
     */
    enterEnhancedForControl;
    /**
     * Exit a parse tree produced by `JavaParser.enhancedForControl`.
     * @param ctx the parse tree
     */
    exitEnhancedForControl;
    /**
     * Enter a parse tree produced by `JavaParser.parExpression`.
     * @param ctx the parse tree
     */
    enterParExpression;
    /**
     * Exit a parse tree produced by `JavaParser.parExpression`.
     * @param ctx the parse tree
     */
    exitParExpression;
    /**
     * Enter a parse tree produced by `JavaParser.expressionList`.
     * @param ctx the parse tree
     */
    enterExpressionList;
    /**
     * Exit a parse tree produced by `JavaParser.expressionList`.
     * @param ctx the parse tree
     */
    exitExpressionList;
    /**
     * Enter a parse tree produced by `JavaParser.methodCall`.
     * @param ctx the parse tree
     */
    enterMethodCall;
    /**
     * Exit a parse tree produced by `JavaParser.methodCall`.
     * @param ctx the parse tree
     */
    exitMethodCall;
    /**
     * Enter a parse tree produced by `JavaParser.expression`.
     * @param ctx the parse tree
     */
    enterExpression;
    /**
     * Exit a parse tree produced by `JavaParser.expression`.
     * @param ctx the parse tree
     */
    exitExpression;
    /**
     * Enter a parse tree produced by `JavaParser.pattern`.
     * @param ctx the parse tree
     */
    enterPattern;
    /**
     * Exit a parse tree produced by `JavaParser.pattern`.
     * @param ctx the parse tree
     */
    exitPattern;
    /**
     * Enter a parse tree produced by `JavaParser.lambdaExpression`.
     * @param ctx the parse tree
     */
    enterLambdaExpression;
    /**
     * Exit a parse tree produced by `JavaParser.lambdaExpression`.
     * @param ctx the parse tree
     */
    exitLambdaExpression;
    /**
     * Enter a parse tree produced by `JavaParser.lambdaParameters`.
     * @param ctx the parse tree
     */
    enterLambdaParameters;
    /**
     * Exit a parse tree produced by `JavaParser.lambdaParameters`.
     * @param ctx the parse tree
     */
    exitLambdaParameters;
    /**
     * Enter a parse tree produced by `JavaParser.lambdaBody`.
     * @param ctx the parse tree
     */
    enterLambdaBody;
    /**
     * Exit a parse tree produced by `JavaParser.lambdaBody`.
     * @param ctx the parse tree
     */
    exitLambdaBody;
    /**
     * Enter a parse tree produced by `JavaParser.primary`.
     * @param ctx the parse tree
     */
    enterPrimary;
    /**
     * Exit a parse tree produced by `JavaParser.primary`.
     * @param ctx the parse tree
     */
    exitPrimary;
    /**
     * Enter a parse tree produced by `JavaParser.switchExpression`.
     * @param ctx the parse tree
     */
    enterSwitchExpression;
    /**
     * Exit a parse tree produced by `JavaParser.switchExpression`.
     * @param ctx the parse tree
     */
    exitSwitchExpression;
    /**
     * Enter a parse tree produced by `JavaParser.switchLabeledRule`.
     * @param ctx the parse tree
     */
    enterSwitchLabeledRule;
    /**
     * Exit a parse tree produced by `JavaParser.switchLabeledRule`.
     * @param ctx the parse tree
     */
    exitSwitchLabeledRule;
    /**
     * Enter a parse tree produced by `JavaParser.guardedPattern`.
     * @param ctx the parse tree
     */
    enterGuardedPattern;
    /**
     * Exit a parse tree produced by `JavaParser.guardedPattern`.
     * @param ctx the parse tree
     */
    exitGuardedPattern;
    /**
     * Enter a parse tree produced by `JavaParser.switchRuleOutcome`.
     * @param ctx the parse tree
     */
    enterSwitchRuleOutcome;
    /**
     * Exit a parse tree produced by `JavaParser.switchRuleOutcome`.
     * @param ctx the parse tree
     */
    exitSwitchRuleOutcome;
    /**
     * Enter a parse tree produced by `JavaParser.classType`.
     * @param ctx the parse tree
     */
    enterClassType;
    /**
     * Exit a parse tree produced by `JavaParser.classType`.
     * @param ctx the parse tree
     */
    exitClassType;
    /**
     * Enter a parse tree produced by `JavaParser.creator`.
     * @param ctx the parse tree
     */
    enterCreator;
    /**
     * Exit a parse tree produced by `JavaParser.creator`.
     * @param ctx the parse tree
     */
    exitCreator;
    /**
     * Enter a parse tree produced by `JavaParser.createdName`.
     * @param ctx the parse tree
     */
    enterCreatedName;
    /**
     * Exit a parse tree produced by `JavaParser.createdName`.
     * @param ctx the parse tree
     */
    exitCreatedName;
    /**
     * Enter a parse tree produced by `JavaParser.innerCreator`.
     * @param ctx the parse tree
     */
    enterInnerCreator;
    /**
     * Exit a parse tree produced by `JavaParser.innerCreator`.
     * @param ctx the parse tree
     */
    exitInnerCreator;
    /**
     * Enter a parse tree produced by `JavaParser.arrayCreatorRest`.
     * @param ctx the parse tree
     */
    enterArrayCreatorRest;
    /**
     * Exit a parse tree produced by `JavaParser.arrayCreatorRest`.
     * @param ctx the parse tree
     */
    exitArrayCreatorRest;
    /**
     * Enter a parse tree produced by `JavaParser.classCreatorRest`.
     * @param ctx the parse tree
     */
    enterClassCreatorRest;
    /**
     * Exit a parse tree produced by `JavaParser.classCreatorRest`.
     * @param ctx the parse tree
     */
    exitClassCreatorRest;
    /**
     * Enter a parse tree produced by `JavaParser.explicitGenericInvocation`.
     * @param ctx the parse tree
     */
    enterExplicitGenericInvocation;
    /**
     * Exit a parse tree produced by `JavaParser.explicitGenericInvocation`.
     * @param ctx the parse tree
     */
    exitExplicitGenericInvocation;
    /**
     * Enter a parse tree produced by `JavaParser.typeArgumentsOrDiamond`.
     * @param ctx the parse tree
     */
    enterTypeArgumentsOrDiamond;
    /**
     * Exit a parse tree produced by `JavaParser.typeArgumentsOrDiamond`.
     * @param ctx the parse tree
     */
    exitTypeArgumentsOrDiamond;
    /**
     * Enter a parse tree produced by `JavaParser.nonWildcardTypeArgumentsOrDiamond`.
     * @param ctx the parse tree
     */
    enterNonWildcardTypeArgumentsOrDiamond;
    /**
     * Exit a parse tree produced by `JavaParser.nonWildcardTypeArgumentsOrDiamond`.
     * @param ctx the parse tree
     */
    exitNonWildcardTypeArgumentsOrDiamond;
    /**
     * Enter a parse tree produced by `JavaParser.nonWildcardTypeArguments`.
     * @param ctx the parse tree
     */
    enterNonWildcardTypeArguments;
    /**
     * Exit a parse tree produced by `JavaParser.nonWildcardTypeArguments`.
     * @param ctx the parse tree
     */
    exitNonWildcardTypeArguments;
    /**
     * Enter a parse tree produced by `JavaParser.typeList`.
     * @param ctx the parse tree
     */
    enterTypeList;
    /**
     * Exit a parse tree produced by `JavaParser.typeList`.
     * @param ctx the parse tree
     */
    exitTypeList;
    /**
     * Enter a parse tree produced by `JavaParser.typeType`.
     * @param ctx the parse tree
     */
    enterTypeType;
    /**
     * Exit a parse tree produced by `JavaParser.typeType`.
     * @param ctx the parse tree
     */
    exitTypeType;
    /**
     * Enter a parse tree produced by `JavaParser.primitiveType`.
     * @param ctx the parse tree
     */
    enterPrimitiveType;
    /**
     * Exit a parse tree produced by `JavaParser.primitiveType`.
     * @param ctx the parse tree
     */
    exitPrimitiveType;
    /**
     * Enter a parse tree produced by `JavaParser.typeArguments`.
     * @param ctx the parse tree
     */
    enterTypeArguments;
    /**
     * Exit a parse tree produced by `JavaParser.typeArguments`.
     * @param ctx the parse tree
     */
    exitTypeArguments;
    /**
     * Enter a parse tree produced by `JavaParser.superSuffix`.
     * @param ctx the parse tree
     */
    enterSuperSuffix;
    /**
     * Exit a parse tree produced by `JavaParser.superSuffix`.
     * @param ctx the parse tree
     */
    exitSuperSuffix;
    /**
     * Enter a parse tree produced by `JavaParser.explicitGenericInvocationSuffix`.
     * @param ctx the parse tree
     */
    enterExplicitGenericInvocationSuffix;
    /**
     * Exit a parse tree produced by `JavaParser.explicitGenericInvocationSuffix`.
     * @param ctx the parse tree
     */
    exitExplicitGenericInvocationSuffix;
    /**
     * Enter a parse tree produced by `JavaParser.arguments`.
     * @param ctx the parse tree
     */
    enterArguments;
    /**
     * Exit a parse tree produced by `JavaParser.arguments`.
     * @param ctx the parse tree
     */
    exitArguments;
    visitTerminal(node) { }
    visitErrorNode(node) { }
    enterEveryRule(node) { }
    exitEveryRule(node) { }
}
//# sourceMappingURL=JavaParserListener.js.map