/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export async function up(knex) {
    return await Promise.all([
        knex.raw(
            `DROP TABLE IF EXISTS activities, todos, javatoarkts`,
        ),
        knex.raw(
            `CREATE TABLE activities (
            id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            email VARCHAR(250) NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
            title VARCHAR(100) NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
            created_at TIMESTAMP NULL DEFAULT current_timestamp(),
            updated_at TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
            PRIMARY KEY (id) USING BTREE,
            INDEX email (email) USING HASH
        )
        COLLATE='utf8mb4_general_ci'
        ENGINE=MYISAM`,
        ),
        knex.raw(
            `CREATE TABLE todos (
      id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
      title VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
      activity_group_id INT(10) UNSIGNED NULL DEFAULT '0',
      is_active TINYINT(1) UNSIGNED NULL DEFAULT '1',
      priority ENUM('very-high','high','normal','low','very-low') NULL DEFAULT 'very-high' COLLATE 'utf8mb4_general_ci',
      PRIMARY KEY (id) USING BTREE,
      INDEX activity_group_id (activity_group_id) USING BTREE
    )
    COLLATE='utf8mb4_general_ci'
    ENGINE=MyISAM`,
        ),
        knex.raw(
            `CREATE TABLE javatoarkts (
      id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
      timestamp VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
      java_code text COMMENT 'java_code',
      ast text COMMENT 'ast',
      arkts_code text COMMENT 'arkts_code',
      status INT(10) UNSIGNED NULL DEFAULT '0',
      options text COMMENT 'options',
      PRIMARY KEY (id) USING BTREE
    )
    COLLATE='utf8mb4_general_ci'
    ENGINE=MyISAM`,
        ),
    ]);
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export function down(knex) {
    return knex.schema.dropTable('activities').dropTable('todos').dropTable('javatoarkts');
}
