// Update with your config settings.
import { db ,MYSQL_ENV } from './mysql.config.js';
/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
export default {

      development: {
        client: 'mysql2',
        connection: {
          host: process.env.MYSQL_HOST,
          port: 3306,
          user: process.env.MYSQL_USER,
          password: process.env.MYSQL_PASSWORD,
          database: process.env.MYSQL_DBNAME
        },
        migrations: {
          directory: 'migrations'
        }
      },

      staging: {
        client: 'mysql2',
        connection: {
          host: process.env.MYSQL_HOST,
          port: 3306,
          user: process.env.MYSQL_USER,
          password: process.env.MYSQL_PASSWORD,
          database: process.env.MYSQL_DBNAME
        },
        migrations: {
          directory: 'migrations'
        }
      },

      production: {
        client: 'mysql2',
        connection: {
          host: process.env.MYSQL_HOST,
          port: 3306,
          user: process.env.MYSQL_USER,
          password: process.env.MYSQL_PASSWORD,
          database: process.env.MYSQL_DBNAME
        },
        migrations: {
          directory: 'migrations'
        }
      }

  /*
        development: {
          client: 'mysql2',
          connection: {
            host: MYSQL_ENV.MYSQL_HOST,
            port: 3306,
            user: MYSQL_ENV.MYSQL_USER,
            password: MYSQL_ENV.MYSQL_PASSWORD,
            database: MYSQL_ENV.MYSQL_DBNAME
          },
          migrations: {
            directory: 'migrations'
          }
        },

        staging: {
          client: 'mysql2',
          connection: {
            host: MYSQL_ENV.MYSQL_HOST,
            port: 3306,
            user: MYSQL_ENV.MYSQL_USER,
            password: MYSQL_ENV.MYSQL_PASSWORD,
            database: MYSQL_ENV.MYSQL_DBNAME
          },
          migrations: {
            directory: 'migrations'
          }
        },

        production: {
          client: 'mysql2',
          connection: {
            host: MYSQL_ENV.MYSQL_HOST,
            port: 3306,
            user: MYSQL_ENV.MYSQL_USER,
            password: MYSQL_ENV.MYSQL_PASSWORD,
            database: MYSQL_ENV.MYSQL_DBNAME
          },
          migrations: {
            directory: 'migrations'
          }
        }

   */
};
