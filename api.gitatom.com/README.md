# gitatom backend  
api.gitatom.com  是采用[UCToo](https://gitee.com/uctoo/uctoo) V3 headless CMS服务端技术选型集成java2typescript第三方开源项目，实现的一个将java代码转换为ArkTS代码的服务器端应用程序。  

### 版本 v0.0.1 Beta  

## 概述
服务端技术选型 Hyper-express + Mysql。Hyper-express是nodejs生态中运行效率最高的一款开发框架。Mysql用于保存代码转换请求和结果。通过集成[java2typescript](https://github.com/mike-lischke/java2typescript) 开源项目实现将Java代码转换为TypeScript代码。
java2typescript在features文档中说明了不支持Threading特性。而OpenHarmony代码转换器并发特性转换赛题对转换Java代码要求较窄，仅需考虑Thread类、Runnable接口、synchronized关键字，因此本项目v0.0.1仅对synchronized关键字转换为ArkTS的taskpool等价语义进行了特定处理。   

## 主要功能和流程  
  
1.  在routes.js配置了以 http://domain:3030/java2ts 地址提供API服务。   
2.  在handlerJava2ts.js集成了java2typescript第三方开源项目进行代码转换。原java2typescript支持转换整个目录中的所有Java代码，本项目仅将接口接收的java_code参数保存为以时间戳命名的单文件，调用java2typescript第三方开源项目进行了单文件源码转换。   
3.  作为第三方开发者，仅能从现有OpenHarmony公开资料和ArkTS示例程序中推断出部分ArkTS语法，因此本项目目前并未完整示例一个[Antlr](https://github.com/antlr)新增自定义语法的流程，仅对synchronized关键字进行了特殊处理。  
4.  在java2typescript目录复制了一份第三方开源项目源码进行定制开发，未使用node_modules中安装的原版第三方开源项目。在 java2typescript\output\src\conversion\FileProcessor.js 文件中对synchronized关键字转为ArkTS对应等价语义进行了特殊处理。  
5.  在sourcecode目录以 \时间戳\时间戳.java文件保存了请求转换的java代码，在\时间戳\output\时间戳.ts 目录保存了转换后的ArkTS代码，同时也在mysql数据库中保存了一条对应记录。   

## 安装
nodejs版本建议18.x，其他版本未作兼容性测试。  
### linux  
需先安装docker和docker-compose。以下命令可参考：  
```shell
$ docker-compose build                      # 构建或者重新构建服务
$ docker-compose up                         # 创建并且启动所有容器
$ docker-compose up -d                      # 创建并且后台运行方式启动所有容器
$ docker-compose down                       # 停止并删除容器，网络，图像和挂载卷
$ docker-compose build --force-rm --no-cache   #构建服务删除缓存
```  

正常启动服务后，将运行两个Container服务，一个是mysqlcontainer，在3306端口提供数据库服务，一个是后端app，在3030端口提供API服务，例如，http://domain:3030/java2ts ，如部署在公有云环境，需注意配置公有云安全规则开放3030端口访问限制。

### windows  
需先安装WSL、docker desktop。安装、运行命令与linux环境相同。

### 本地开发测试环境  
以非docker方式部署本地开发测试环境，可参考此步骤：  
1. 启动mysql服务，运行 migrations 目录下脚本，初始化数据库。  可通过npm run migrate:make 命令初始化数据库。  
2. 编辑 mysql.config.js 和 knexfile.js 文件中注释掉的配置覆盖未注释的部分。  
3. npm install 命令安装依赖。如遇到依赖安装超时，可通过配置国内镜像加速安装。如遇到Hyper-express安装不成功，可通过 npm i hyper-express 命令单独安装。  
4. npm run start 启动服务。  
5. handlerJava2ts.js 文件对应实现 http://domain:3030/java2ts 接口的业务逻辑。java2typescript 目录是本地化定制开发的一个版本。 java2typescript\output\src\conversion\FileProcessor.js 文件中对synchronized关键字转为ArkTS对应等价语义进行了特殊处理。  

## 条件限制    
  由于采用 java2typescript 生成的代码仅能运行在 [antlr4ng](https://github.com/mike-lischke/antlr4ng) 运行时，因此本项目生成的代码目前无法作为完备的ArkTS代码在DevEco Studio编译运行，目前仅用于技术可行性验证。    
  
## 版本演进    
1.  [Antlr](https://github.com/antlr)有完备的开源代码和文档资料，以及众多语言的[grammars-v4](https://github.com/antlr/grammars-v4) 语法库，可考虑将ArkTS的语法规范加入Antlr以及在ArkCompiler和antlr4ng之间开发一套完备的语法转换规则，理论上可以给各种语言转换为ArkTS提供一条技术路线。    
2.  本项目已较好的支持了容器化，后续通过集成公有云的云原生技术体系，可实现开发者一键开通和部署一个公有云的云开发和测试环境。  

## 技术交流  
  开发者交流QQ群984748053
  更多信息请关注gitatom官网  https://www.gitatom.com   (开发中)  

## 参考资料  
  UCToo  https://gitee.com/uctoo/uctoo  
  Hyper-express  https://github.com/kartikk221/hyper-express  
  java2typescript  https://github.com/mike-lischke/java2typescript  
  Antlr  https://github.com/antlr  
  ArkTS  https://docs.openharmony.cn/pages/v4.1/zh-cn/application-dev/arkts-utils/arkts-commonlibrary-overview.md  
            
## 版权信息  
本项目采用MIT开源协议发布。
本项目包含的第三方源码和二进制文件之版权信息另行标注。
版权所有Copyright © 2014-2024 UCToo (https://www.uctoo.com)
All rights reserved