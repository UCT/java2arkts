import { Server } from 'hyper-express';
import { migratedb } from './migrate.js';
import  cors  from  'cors';
import { api_router } from './routes.js';
const webserver = new Server();

//  跨域处理
webserver.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials,request-from,Access-Control-Allow-Methods,Access-Control-Allow-Origin,Access-Control-Allow-Headers");
    res.header("Access-Control-Allow-Credentials", "true");
    if (req.method == "OPTIONS") {
        res.send(200);
    }
    else {
        next();
    }
});
/*
webserver.options('*', cors()) // include before other routes

const corsOptions = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    "preflightContinue": false,
    "optionsSuccessStatus": 200
};


webserver.use(cors(corsOptions));
*/

webserver.use('/', api_router);

webserver.listen(3030)
    .then((socket) => console.log('Webserver started on port 3030'))
    .catch((error) => console.log('Failed to start webserver on port 3030'));